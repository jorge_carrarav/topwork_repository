﻿@ModelType TopWork.QuotationDataProvider.Quotation_AddEdit_Formatted
@Code
    ViewData("AppNode") = "Cotizaciones > Versión Anterior"
        
End Code



<style>

    #div_createdInfo {
        font-style:italic;
        color:darkblue;
        position:absolute;
        right:1%;
        top:1%;        
    }

    .quotationAddWidth {
        width:80%;
        margin:auto;
    }

    /* ------------------------------------------------------------- */

    #div_upperComment {
        text-align:center;
    }
    #div_clientInfo {
        border: 1px solid red;
        padding-top:10px;
        padding-bottom:10px;
    }
    #table_clientInfo {
         margin:auto;
    }
    .tr_clientInfo_rows {
        height:23px;
    }
    .td_clientInfo_1rst {
        width:100px;
    }
    .td_clientInfo_3rd {
        width:250px;
    }
    .td_clientInfo_halfSeparator {
        width:50px;
    }

    /* ------------------------------------------------------------- */
    
    #div_quotationDetails {
        padding:10px;
    }
    #table_quotationDetails {
        padding-top:10px;
        margin:auto;
    }
    .tr_quotationDetails_rows {
        height:30px;
    }
    .td_quotationDetails_1rst {
        vertical-align:top;
        width:170px;
    }
    .td_quotationDetails_1rst_right {
        vertical-align:top;        
        width:150px;
    }
    .td_quotationDetails_2nd {
        width:1%;
        vertical-align:top;
    }
    .td_quotationDetails_3rd {
        vertical-align:top;
        text-align:center;
        width:260px;
    }
    .td_quotationDetails_halfSeparator {
        width:50px;
    }

    
    /* ------------------------------------------------------------- */
    
    #div_itemsDetails {
        border:1px solid black;
        background-color:lightgray;
        padding:10px;
    }
    #div_item {
        border:1px solid black;
        background-color:white;
        padding:10px;
        width:70%;
        margin:auto;
    }
    #table_item {
        margin:auto;
    }
    .tr_item_rows {
        height:30px;
    }
    .td_item_1rst {
        vertical-align:top;
        width:25%;
    }
    .td_item_3rd {
        padding-left:30px;
    }
   /* -------------------------------------------------------------ItemList */
   
    #div_itemList {
        border:1px solid black;
        background-color:white;
        padding:10px;
        width:80%;
        margin:auto;
    }
    #table_itemList {
        border:1px solid gray;
        margin:auto;
        width:100%;
    }
    .tr_item_rows {
        height:25px;
    }
    .tr_itemList_title {
        background-color: #DCE6F1;
        text-align:center;
        height:25px;
    }
    .td_itemList_title_itemNum {
        width:5%;
    }
    .td_itemList_title_description {
    }
    .td_itemList_title_quantity {
        width: 10%;
    }
    .td_itemList_title_unitValue {
        width:15%;
    }
    .td_itemList_title_netValue {
        width:15%;
    }
    .td_itemList_title_controls {
        width:7%;
    }

    
    .td_itemList_itemNum {
        text-align:center;
    }
    .td_itemList_description {
        text-align:left;
        padding-left:5px;
        padding-right:5px;
    }
    .td_itemList_quantity {
        text-align:center;
    }
    .td_itemList_unitValue {
        text-align:right;
        padding-right:5px;
    }
    .td_itemList_netValue {
        text-align:right;
        padding-right:5px;
    }
    .td_itemList_controls {
        text-align:center;
        padding-left: 4px;
        padding-right: 0px;
    }

    .td_itemList_item_leftBorder {        
        border-left:solid 1px gray;
    }
    .td_itemList_item_common {
        padding-bottom:10px;
        padding-left:5px;
        padding-right:5px;
    }


    .td_itemList_totals_netRow {
        border-top:1px solid gray;
    }
    .td_itemList_totalsStr {
        padding-right:5px;
        text-align:right;
    }
    .td_itemList_totals {
        padding-right:5px;
        text-align:right;
    }


    /* ------------------------------------------------------------- */

    #lbl_item_message {
        color:red;
    }

    #div_quotationAdd_buttonsContainer {
        text-align:right;
    }
    

    /* ------------------------------------------------------------- */
    
    .ui-autocomplete {
        background-color: #FCFDD2;
        text-align:right;
    }

    div.ui-datepicker {
        font-size: 75.5%;    
    }


</style>




<script>
    var _item_jsonList = [];
    
    $(document).ready(function () {
     
        
        var itemList_str = document.getElementById("tb_itemList").value;
        if (itemList_str != "") {
            _item_jsonList = JSON.parse(itemList_str);
            insertingItems_toTable();
            insertingTotals();
        }


        bindOldVersionSelect();



        $("#div_message").dialog({
            modal: true,
            dialogClass: "noclose",
            title: "Estado de Cotización",
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
        

        $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: '<Ant',
            nextText: 'Sig>',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        
        $.datepicker.setDefaults($.datepicker.regional['es']);
        
        $("#tb_validityDate").datepicker({
            changeMonth: true,
            changeYear: true
        });

    })


    
    //----------------------------------------------------------------- Limpia la tabla e inserta todos los datos de la lista Json y los Totales
    function loadTableItems() {

        loadingItemList_ToModel();

        deletingItems_fromTable();
        insertingItems_toTable();
        insertingTotals();

    }
    //----------------------------------------------------------------- Limpia la tabla e inserta todos los datos de la lista Json y los Totales
    function loadingItemList_ToModel() {
        document.getElementById("tb_itemList").value = JSON.stringify(_item_jsonList);        
    }
    //------------------------------------------------------------------Eliminar todos los items de la tabla
    function deletingItems_fromTable() {
        $(".rowToDelete").remove();
    }
    //------------------------------------------------------------------Añadir todo el JSON con items a la tabla
    function insertingItems_toTable() {

        var i = 0;
        
        for (i; i < _item_jsonList.length; i++) {

            var itemNumber = i + 1;

            var description = _item_jsonList[i].Description;
            var quantity = _item_jsonList[i].Quantity;
            var unitValue = _item_jsonList[i].UnitValue;
            var netValue = quantity * unitValue;

            $("#table_itemList")
               .append($("" +
                   "<tr id='tr_itemList_" + i + "' class='rowToDelete'>" +
                       "<td class='td_itemList_itemNum td_itemList_item_common'> " + itemNumber + " </td>" +
                       "<td id='td_itemList_description_" + i + "' class='td_itemList_description td_itemList_item_leftBorder td_itemList_item_common'> " + description + " </td>" +
                       "<td id='td_itemList_quantity_" + i + "' class='td_itemList_quantity td_itemList_item_leftBorder td_itemList_item_common'> " + thousandSeparator(quantity) + " </td>" +
                       "<td id='td_itemList_unitValue_" + i + "' class='td_itemList_unitValue td_itemList_item_leftBorder td_itemList_item_common'> $ " + thousandSeparator(unitValue) + " </td>" +
                       "<td id='td_itemList_netValue_" + i + "' class='td_itemList_netValue td_itemList_item_leftBorder td_itemList_item_common'> <span>$ " + thousandSeparator(netValue) + "</span> </td>" +
                   "</tr>")
               );
            
        }

    }

    function insertingTotals() {
        var ivaPercentage = document.getElementById("tb_ivaPercentage").value;
                
        var finalNetValue = 0;
        var finalIVAValue = 0;
        var finalTotalValue = 0;

        var i = 0;
        for (i; i < _item_jsonList.length; i++) {
            var netValue = _item_jsonList[i].Quantity * _item_jsonList[i].UnitValue;
            finalNetValue = finalNetValue + netValue;
        }

        finalIVAValue = roundNumber(ivaPercentage * finalNetValue / 100);
        finalTotalValue = finalNetValue + finalIVAValue;

        document.getElementById("lbl_itemList_totals_finalNetValue").innerHTML = "$ " + thousandSeparator(finalNetValue);
        document.getElementById("lbl_itemList_totals_finalIVAValue").innerHTML = "$ " + thousandSeparator(finalIVAValue);
        document.getElementById("lbl_itemList_totals_finalTotalValue").innerHTML = "$ " + thousandSeparator(finalTotalValue);        
    }
    


    function bindOldVersionSelect() {

        var slc_oldVersions = document.getElementById("slc_oldVersions");
        var btn_loadOldVersion = document.getElementById("btn_loadOldVersion");

        var tb_oldVersionsInfo = document.getElementById("tb_oldVersionsInfo").value;
        var oldVersionsInfo_jsonList = JSON.parse(tb_oldVersionsInfo);
        
        
        if (oldVersionsInfo_jsonList.length == 0) {
            slc_oldVersions.disabled = true;
            btn_loadOldVersion.disabled = true;
        }
        else {
            var versionLoadedIndex = 0;

            for (var i = 0; i < oldVersionsInfo_jsonList.length; i++) {
               
                option = document.createElement("option");
                //option.text = oldVersionsInfo_jsonList[i].Quotation_Num + "." + oldVersionsInfo_jsonList[i].Version + " | " + oldVersionsInfo_jsonList[i].CreatedDate + " por " + oldVersionsInfo_jsonList[i].CreatedBy + ")"
                if (oldVersionsInfo_jsonList[i].Version == 1)
                    option.text = oldVersionsInfo_jsonList[i].Quotation_Num;
                else
                    option.text = oldVersionsInfo_jsonList[i].Quotation_Num + "." + oldVersionsInfo_jsonList[i].Version
                slc_oldVersions.add(option);

                if (option.text == "@Model.QuotationNumberVersion") {
                    versionLoadedIndex = i;
                }
            }
            slc_oldVersions.selectedIndex = versionLoadedIndex;

        }

    }

    function btn_loadOldVersion_onClick() {

        var slc_oldVersions = document.getElementById("slc_oldVersions");

        var quotNum;
        var quotVer = 1;

        if (slc_oldVersions.options[slc_oldVersions.selectedIndex].text.split(".").length == 1) {
            quotNum = slc_oldVersions.options[slc_oldVersions.selectedIndex].text.split(".")[0];
        }
        else {
            quotNum = slc_oldVersions.options[slc_oldVersions.selectedIndex].text.split(".")[0];
            quotVer = slc_oldVersions.options[slc_oldVersions.selectedIndex].text.split(".")[1];
        }

        var oldVersionUrl = '@Url.Action("QuotationOldVersion", "Quotation")';

        window.location = oldVersionUrl + "?quotNum=" + quotNum + "&version=" + quotVer;

    }


    function btn_goBack_onClick() {
        var goBackPoint = '@ViewBag.GoBackPoint';

        if (goBackPoint == "QuotationEdit")
            window.location.href = '@Url.Action("QuotationList", "Quotation", New With {.wwo = 1})';
        else if (goBackPoint == "QuotationReview")
            window.location.href = '@Url.Action("QuotationList", "Quotation", New With {.wwo = 1})';
        else
            window.location.href = '@Url.Action("QuotationList", "Quotation", New With {.wwo = 1})';
    }


    function btn_loadCurrentVersion_onClick() {

        var editVersionUrl = '@Url.Action("QuotationEdit", "Quotation")';

        window.location = editVersionUrl + "?quotId=" + "@Model.Quotation_Id" + "&result=CurrentVersion";

    }

    function lastVersion_onClick() {
        
        document.getElementById("btn_loadCurrentVersion").click();
    }

</script>

<div id="div_createdInfo">
    Última modificación: @Model.CreatedDate por @Model.CreatedBy
</div>

<div id="div_confirmDeletion" style="visibility:hidden; display:none;"  >   
    ¿Desea realmente eliminar cotización? 
</div>

<form id="form_quotationAdd" method="post" action="QuotationAdd">
    <div id="div_upperComment" class="quotationAddWidth" style="font-weight:bold;">
        Versión anterior de cotización <a style=" color:blue; font-weight:bold; cursor:pointer" onclick="lastVersion_onClick()"> @ViewBag.QuotationNumberVersion </a>
    </div>
    <div id="div_clientInfo" class="quotationAddWidth">
        <table id="table_clientInfo">
            <tr class="tr_clientInfo_rows">
                <td class="td_clientInfo_1rst">Cliente</td>
                <td style="width:1%;">:</td>
                <td class="td_clientInfo_3rd">@ViewBag.QuotationAdd_ClientName</td>
                <td class="td_clientInfo_halfSeparator"></td>
                <td class="td_clientInfo_1rst">Ciudad</td>
                <td style="width:1%;">:</td>
                <td class="td_clientInfo_3rd">@ViewBag.QuotationAdd_City</td>
            </tr>
        
            <tr class="tr_clientInfo_rows">
                <td class="td_clientInfo_1rst">Rut</td>
                <td style="width:1%;">:</td>
                <td class="td_clientInfo_3rd">@ViewBag.QuotationAdd_Rut</td>            
                <td class="td_clientInfo_halfSeparator"></td>
                <td class="td_clientInfo_1rst">Email</td>
                <td style="width:1%;">:</td>
                <td class="td_clientInfo_3rd">@ViewBag.QuotationAdd_Email</td>
            </tr>
            <tr class="tr_clientInfo_rows">
                <td class="td_clientInfo_1rst">Giro</td>
                <td style="width:1%;">:</td>
                <td class="td_clientInfo_3rd">@ViewBag.QuotationAdd_ServiceType</td>
                <td class="td_clientInfo_halfSeparator"></td>
                <td class="td_clientInfo_1rst">Fono</td>
                <td style="width:1%;">:</td>
                <td class="td_clientInfo_3rd">@ViewBag.QuotationAdd_Phone</td>
            </tr>
            <tr class="tr_clientInfo_rows">
                <td class="td_clientInfo_1rst">Dirección</td>
                <td style="width:1%;">:</td>
                <td class="td_clientInfo_3rd">@ViewBag.QuotationAdd_Adderss</td>
                <td class="td_clientInfo_halfSeparator"></td>
                <td class="td_clientInfo_1rst"></td>
                <td style="width:1%;"></td>
                <td class="td_clientInfo_3rd"></td>
            </tr>
        </table>
    </div>


    <div id="div_quotationDetails" class="quotationAddWidth">
    
        <table id="table_quotationDetails">
            <tr class="tr_quotationDetails_rows">
                <td class="td_quotationDetails_1rst">
                    N° Cotización
                </td>
                <td class="td_quotationDetails_2nd">:</td>
                <td class="td_quotationDetails_3rd" style="text-align:center;">
                    @Html.ValueFor(Function(model) model.QuotationNumberVersion)             
                </td>
                <td class="td_quotationDetails_halfSeparator"></td>
                <td class="td_quotationDetails_1rst_right">
                    Estado
                </td>
                <td class="td_quotationDetails_2nd">:</td>
                <td class="td_quotationDetails_3rd" style="text-align:center;">
                    @Html.ValueFor(Function(model) model.StatusName)        
                </td>
            </tr>
            <tr class="tr_quotationDetails_rows">
                <td class="td_quotationDetails_1rst">
                    Nombre de Contacto
                </td>
                <td class="td_quotationDetails_2nd">:</td>
                <td class="td_quotationDetails_3rd">
                    @Html.ValueFor(Function(model) model.ContactName)
                </td>
                <td class="td_quotationDetails_halfSeparator"></td>
                <td class="td_quotationDetails_1rst_right" rowspan="5">
                    Observaciones
                </td>
                <td class="td_quotationDetails_2nd" rowspan="5">:</td>
                <td class="td_quotationDetails_3rd" rowspan="5">
                    @Html.ValueFor(Function(model) model.Observations)
                </td>
            </tr>
            <tr class="tr_quotationDetails_rows">
                <td class="td_quotationDetails_1rst">
                    Destino
                </td>
                <td class="td_quotationDetails_2nd">:</td>
                <td class="td_quotationDetails_3rd">
                    @Html.ValueFor(Function(model) model.WorkDestiny)
                </td>
            </tr>
            <tr class="tr_quotationDetails_rows">
                <td class="td_quotationDetails_1rst">
                    Tipo de Trabajo
                </td>
                <td class="td_quotationDetails_2nd">:</td>
                <td class="td_quotationDetails_3rd">
                    @Html.ValueFor(Function(model) model.WorkTypeQ)
                </td>
                <td class="td_quotationDetails_halfSeparator"></td>
            </tr>
            <tr class="tr_quotationDetails_rows">
                <td class="td_quotationDetails_1rst">
                    Forma de Pago
                </td>
                <td class="td_quotationDetails_2nd">:</td>
                <td class="td_quotationDetails_3rd">
                    @Html.ValueFor(Function(model) model.MethodOfPayment)
                </td>
                <td class="td_quotationDetails_halfSeparator"></td>
            </tr>
            <tr class="tr_quotationDetails_rows">
                <td class="td_quotationDetails_1rst">
                    Fecha de Validez
                </td>
                <td class="td_quotationDetails_2nd">:</td>
                <td class="td_quotationDetails_3rd">
                    @Html.ValueFor(Function(model) model.ValidityDate)
                </td>
                <td class="td_quotationDetails_halfSeparator"></td>
            </tr>
        </table>
    </div>


    <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ITEMS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->


    <div id="div_itemsDetails" class="quotationAddWidth">

        <div id="div_itemList">
            <table id="table_itemList" cellspacing="0">
                <tr class="tr_itemList_title">
                    <td class="td_itemList_title_itemNum">
                        N°
                    </td>
                    <td class="td_itemList_title_description">
                        Descripción
                    </td>
                    <td class="td_itemList_title_quantity">
                        Cantidad
                    </td>
                    <td class="td_itemList_title_unitValue">
                        Valor Unit.
                    </td>
                    <td class="td_itemList_title_netValue">
                        Valor Neto
                    </td>
                </tr>
                <tbody>
                    <!-- ------------------------------------ ITEMS A INSERTAR ------------------------------------->
                </tbody>
                <tr>
                    <td>
                        <br /><br />
                    </td>
                    <td class="td_itemList_item_leftBorder">
                    
                    </td>
                    <td class="td_itemList_item_leftBorder">
                    
                    </td>
                    <td class="td_itemList_item_leftBorder">
                    
                    </td>
                    <td class="td_itemList_item_leftBorder">
                    </td>               
                </tr>               
            
            
                       
                <tr class="">
                    <td class="td_itemList_totals_netRow">
                    
                    </td>
                    <td class="td_itemList_totals_netRow">
                    
                    </td>
                    <td class="td_itemList_totals_netRow">
                    
                    </td>
                    <td class="td_itemList_totals_netRow td_itemList_totalsStr">
                        Neto :
                    </td>
                    <td class="td_itemList_totals_netRow td_itemList_totals">
                        <label id="lbl_itemList_totals_finalNetValue"></label>
                    </td>           
                </tr>    
                <tr>
                    <td>
                    
                    </td>
                    <td>
                    
                    </td>
                    <td>
                    
                    </td>
                    <td class="td_itemList_totalsStr">
                        IVA :
                    </td>
                    <td class="td_itemList_totals">
                        <label id="lbl_itemList_totals_finalIVAValue"></label>
                    </td>             
                </tr>
                <tr>
                    <td>
                    
                    </td>
                    <td>
                    
                    </td>
                    <td>
                    
                    </td>
                    <td class="td_itemList_totalsStr">
                        Total :
                    </td>
                    <td class="td_itemList_totals">
                        <label id="lbl_itemList_totals_finalTotalValue"></label>
                    </td>             
                </tr>
            </table>
        </div>

    </div>
    <br />
    <div style="width:49%; float:left; ">
        <a style="font-weight:bold; padding-left:15px; ">Historial</a>
        <br />
        <select id="slc_oldVersions" style="float:left; height:27px; margin-top:2px;"></select>
        <button type="button" id="btn_loadOldVersion" class="btn_common btn_common_color2 btn_common_size1" style="float:left; margin-left:10px;" onclick="btn_loadOldVersion_onClick()">Ver</button>
    </div>
    <div id="div_quotationAdd_buttonsContainer" style="width:49%; float:left; ">
        <button type="button" id="btn_goBack" onclick="btn_goBack_onClick()" class="btn_common btn_common_color2 btn_common_size1">Volver</button>
        &nbsp
        <button type="button" id="btn_loadCurrentVersion" class="btn_common btn_common_color1 btn_common_size2" onclick="btn_loadCurrentVersion_onClick()">Ver Actual</button>
    </div>
    
    
    @Html.TextBoxFor(Function(model) model.Item_list, New With {.id = "tb_itemList", .style = "display:none;"})
    @Html.TextBoxFor(Function(model) model.ClientRut, New With {.id = "tb_clientRut_model", .style = "display:none;"})
    @Html.TextBoxFor(Function(model) model.Quotation_Id, New With {.id = "tb_quotationId", .style = "display:none;"})
    @Html.TextBoxFor(Function(model) model.IVAPercentage, New With {.id = "tb_ivaPercentage", .style = "display:none;"})
    @Html.TextBoxFor(Function(model) model.Version, New With {.id = "tb_version", .style = "display:none;"})
    @Html.TextBoxFor(Function(model) model.OldVersionsInfo, New With {.id = "tb_oldVersionsInfo", .style = "display:none;"})

</form>




<div style="visibility:hidden;">
    <input id="hf_itemToEdit_rowNum" type="hidden" />
</div>
