﻿@Code
    ViewData("AppNode") = "Cotizaciones > Búsqueda Avanzada"
    
End Code

<style>
    #div_container {
    }
    #table_searchFields {
        width:400px;
    }
    .td_searchFields_3rthCol {
        text-align:center;
    }


    #div_buttonContainer {
        text-align:center;
    }
</style>


<script>

    $(document).ready(function () {

        disableAll();
        document.getElementById("tb_clientName").disabled = false;
        document.getElementById("rad_clientName").checked = true;;

    });

    $(function () {

        var rut_list_viewbag = "@ViewBag.RutList";
            var rut_list = rut_list_viewbag.split("&#39;").join("").split(',');

            $("#tb_clientRutNumber").autocomplete({
                source: rut_list
            });


            var name_list_viewbag = "@ViewBag.NameList";
        var name_list = name_list_viewbag.split("&#39;").join("").split(',');

        $("#tb_clientName").autocomplete({
            source: name_list,
        });

        });

    function btn_search_onClick() {

        $("#div_loading").dialog("open");

        var params = "";

        if (document.getElementById("rad_clientName").checked) {
            params = "clientName=" + document.getElementById("tb_clientName").value;
        }
        else if (document.getElementById("rad_clientRutNumber").checked) {
            params = "clientRut=" + document.getElementById("tb_clientRutNumber").value;
        }
        else if (document.getElementById("rad_quotationNumber").checked) {
            params = "quotNum=" + document.getElementById("tb_quotationNumber").value;
        }
        else if (document.getElementById("rad_workOrderNumber").checked) {
            params = "workOrderNum=" + document.getElementById("tb_workOrderNumber").value;
        }
        else if (document.getElementById("rad_purchaseOrderNumber").checked) {
            params = "purchaseOrderNumber=" + document.getElementById("tb_purchaseOrderNumber").value;
        }
        else if (document.getElementById("rad_wayBillNumber").checked) {
            params = "wayBillNum=" + document.getElementById("tb_wayBillNumber").value;
        }
        else if (document.getElementById("rad_invoiceNumber").checked) {
            params = "invoiceNum=" + document.getElementById("tb_invoiceNumber").value;
        }
        
        var advSearchURL = '@Url.Action("QuotationList_AdvSearch", "Quotation")';

        window.location = advSearchURL + "?" + params;
    }


    function tb_onKeyPress(e) {
        if (e.keyCode == 13) {
            var btn_search = document.getElementById("btn_search");
            btn_search.click();
        }
    }

    function tb_clientName_onClick() {
        disableAll();
        document.getElementById("tb_clientName").disabled = false;
        document.getElementById("rad_clientName").checked = true;;
    }
    function tb_clientRutNumber_onClick() {
        disableAll();
        document.getElementById("tb_clientRutNumber").disabled = false;
        document.getElementById("rad_clientRutNumber").checked = true;
    }
    function tb_quotationNumber_onClick() {
        disableAll();
        document.getElementById("tb_quotationNumber").disabled = false;
        document.getElementById("rad_quotationNumber").checked = true;;
    }
    function tb_workOrderNumber_onClick() {
        disableAll();
        document.getElementById("tb_workOrderNumber").disabled = false;
        document.getElementById("rad_workOrderNumber").checked = true;;
    }
    function tb_purchaseOrderNumber_onClick() {
        disableAll();
        document.getElementById("tb_purchaseOrderNumber").disabled = false;
        document.getElementById("rad_purchaseOrderNumber").checked = true;;
    }
    function tb_wayBillNumber_onClick() {
        disableAll();
        document.getElementById("tb_wayBillNumber").disabled = false;
        document.getElementById("rad_wayBillNumber").checked = true;;
    }
    function tb_invoiceNumber_onClick() {
        disableAll();
        document.getElementById("tb_invoiceNumber").disabled = false;
        document.getElementById("rad_invoiceNumber").checked = true;;
    }

    function disableAll() {
        document.getElementById("tb_clientName").value = "";
        document.getElementById("tb_clientRutNumber").value = "";
        document.getElementById("tb_quotationNumber").value = "";
        document.getElementById("tb_workOrderNumber").value = "";
        document.getElementById("tb_purchaseOrderNumber").value = "";
        document.getElementById("tb_wayBillNumber").value = "";
        document.getElementById("tb_invoiceNumber").value = "";

        document.getElementById("tb_clientName").disabled = "disabled";
        document.getElementById("tb_clientRutNumber").disabled = "disabled";
        document.getElementById("tb_quotationNumber").disabled = "disabled";
        document.getElementById("tb_workOrderNumber").disabled = "disabled";
        document.getElementById("tb_purchaseOrderNumber").disabled = "disabled";
        document.getElementById("tb_wayBillNumber").disabled = "disabled";
        document.getElementById("tb_invoiceNumber").disabled = "disabled";
    }




</script>


<div id="div_container">
<table id="table_searchFields" align="center">
    <tr>
        <td>
            <input id="rad_clientName" type="radio" name="advSearch" />
        </td>
        <td>
            <label onClick="tb_clientName_onClick()">
                Nombre de Client
            </label>
        </td>
        <td style="width:1px;">:</td>
        <td class="td_searchFields_3rthCol">
            <input type="text" id="tb_clientName" onClick="tb_clientName_onClick()" onkeyup="guion(event)" onkeypress="tb_onKeyPress(event)" />
        </td>
    </tr>
    <tr>
        <td>
            <input id="rad_clientRutNumber" type="radio" name="advSearch" />
        </td>
        <td>
            <label onClick="tb_clientRutNumber_onClick()">
                Rut de Client
            </label>
        </td>
        <td style="width:1px;">:</td>
        <td class="td_searchFields_3rthCol">
            <input type="text" id="tb_clientRutNumber" style="text-align:right;" maxlength="10" onClick="tb_clientRutNumber_onClick()" onkeyup="checkIfNumber_or_dash(this.id, event)" onkeypress="tb_onKeyPress(event)" />
        </td>
    </tr>
    <tr>
        <td>
            <input id="rad_quotationNumber" type="radio" name="advSearch" />
        </td>
        <td>
            <label onClick="tb_quotationNumber_onClick()">
                N° Cotización
            </label>
        </td>
        <td style="width:1px;">:</td>
        <td class="td_searchFields_3rthCol">
            <input type="text" id="tb_quotationNumber" style="text-align:center; width:140px;" onClick="tb_quotationNumber_onClick()" onkeyup="checkIfNumber_or_point(this.id, event)" onkeypress="tb_onKeyPress(event)" />
        </td>
    </tr>
    <tr>
        <td>
            <input id="rad_workOrderNumber" type="radio" name="advSearch" />
        </td>
        <td>
            <label onClick="tb_workOrderNumber_onClick()">
                N° O.T.
            </label>
        </td>
        <td style="width:1px;">:</td>
        <td class="td_searchFields_3rthCol">
            <input type="text" id="tb_workOrderNumber" style="text-align:center; width:140px;" onClick="tb_workOrderNumber_onClick()" onkeyup="checkIfNumber(this.id, event)" onkeypress="tb_onKeyPress(event)" />
        </td>
    </tr>
    <tr>
        <td>
            <input id="rad_purchaseOrderNumber" type="radio" name="advSearch" />
        </td>
        <td>
            <label onClick="tb_purchaseOrderNumber_onClick()">
                N° O.C.
            </label>
        </td>
        <td style="width:1px;">:</td>
        <td class="td_searchFields_3rthCol">
            <input type="text" id="tb_purchaseOrderNumber" style="text-align:center; width:140px;" onClick="tb_purchaseOrderNumber_onClick()" onkeyup="checkIfNumber(this.id, event)" onkeypress="tb_onKeyPress(event)" />
        </td>
    </tr>
    <tr>
        <td>
            <input id="rad_wayBillNumber" type="radio" name="advSearch" />
        </td>
        <td>
            <label onClick="tb_wayBillNumber_onClick()">
                N° Guía de Despacho
            </label>
        </td>
        <td style="width:1px;">:</td>
        <td class="td_searchFields_3rthCol">
            <input type="text" id="tb_wayBillNumber" style="text-align:center; width:140px;" onClick="tb_wayBillNumber_onClick()" onkeyup="checkIfNumber(this.id, event)" onkeypress="tb_onKeyPress(event)" />
        </td>
    </tr>
    <tr>
        <td>
            <input id="rad_invoiceNumber" type="radio" name="advSearch" />
        </td>
        <td>
            <label onClick="tb_invoiceNumber_onClick()">
                N° Factura
            </label>
        </td>
        <td style="width:1px;">:</td>
        <td class="td_searchFields_3rthCol">
            <input type="text" id="tb_invoiceNumber" style="text-align:center; width:140px;" onClick="tb_invoiceNumber_onClick()" onkeyup="checkIfNumber(this.id, event)" onkeypress="tb_onKeyPress(event)" />
        </td>
    </tr>
</table>
    <br />
    <div id="div_buttonContainer">
        <button type="button" id="btn_goBack" onclick="window.location.href='@Url.Action("QuotationList", "Quotation")'" class="btn_common btn_common_color2 btn_common_size1">Volver</button>
        &nbsp
        <button type="button" id="btn_search" value="search" class="btn_common btn_common_color1 btn_common_size2" onclick="btn_search_onClick()">Buscar</button>
    </div>
</div>