﻿@ModelType IEnumerable(of TopWork.QuotationDataProvider.Quotation_List_Formatted)
@Code
    ViewData("AppNode") = "Cotizaciones"
    
    Dim grid = New WebGrid(Model, canPage:=True, rowsPerPage:=15)
    grid.Pager(WebGridPagerModes.All)
End Code


<style>

    #table_filterContainer {
        width:100%;
    }
    #lbl_searchResult {
        color:blue;
    }

    #div_searchContainer {
        text-align:center;
        margin-top:-40px;
    }

    .td_nearToFinish_cell {
        background-color:red;
    }

    .td_creationDate {
        width:10%;
    }
    .td_clientName {
        text-align:left;
        width:18%;
    }
    .td_quotationNumberVersion {
        text-align:right;
        width:6%;
    }
    .td_workType {
        width:15%;
    }
    .td_validityDate {
        width:10%;
    }
    .td_netValue {
        text-align:right;
        width:10%;
    }
    .td_woNumber {
        text-align:right;
        width:6%;
    }
    .td_status {
        width:15%;
    }

    

    .td_statusFilter {
        width:70%
    }
    .td_searchButton {
        padding-left:10px;
        width:30px;
        text-align:center;  
    }
    .td_searchResult {
        text-align:right;
    }

    .tb_search {
        text-align:right;
        width:150px;
    }

    .webGrid_table {
        width:100%;
    }


    

</style>

<script>


    $(document).ready(function () {

        if (document.URL.split("quotNum=").length > 1) {
            tb_searchByQuotNum.value = document.URL.split("quotNum=")[1].split("&")[0]
        }

        document.getElementById("cb_ws_a").checked = true;
        if (document.URL.split("ws_a=").length > 1) {
            var isChecked = document.URL.split("ws_a=")[1].split("&")[0];
            if (isChecked == "0")
                document.getElementById("cb_ws_a").checked = false;
        }
        if (document.URL.split("ws_b=").length > 1) {
            var isChecked = document.URL.split("ws_b=")[1].split("&")[0];
            if (isChecked == "1")
                document.getElementById("cb_ws_b").checked = true;
        }
        if (document.URL.split("ws_c=").length > 1) {
            var isChecked = document.URL.split("ws_c=")[1].split("&")[0];
            if (isChecked == "1")
                document.getElementById("cb_ws_c").checked = true;
        }
        if (document.URL.split("ws_d=").length > 1) {
            var isChecked = document.URL.split("ws_d=")[1].split("&")[0];
            if (isChecked == "1")
                document.getElementById("cb_ws_d").checked = true;
        }
        if (document.URL.split("ws_e=").length > 1) {
            var isChecked = document.URL.split("ws_e=")[1].split("&")[0];
            if (isChecked == "1")
                document.getElementById("cb_ws_e").checked = true;
        }

        if (tb_searchByQuotNum.value != "") {
            document.getElementById("cb_ws_a").disabled = true;
            document.getElementById("cb_ws_b").disabled = true;
            document.getElementById("cb_ws_c").disabled = true;
            document.getElementById("cb_ws_d").disabled = true;
            document.getElementById("cb_ws_e").disabled = true;
        }



        quotationValidityDate();

    });


    function quotationValidityDate() {
        
        var validityDateCellIndex = 4;

        var table_quotationList = document.getElementById("table_quotationList");
        
        for (var i = 1; i < table_quotationList.rows.length; i++) {

            var validityDate_str = table_quotationList.rows[i].cells[validityDateCellIndex].innerHTML;
            
            var day = validityDate_str.split('-')[0];
            var month = validityDate_str.split('-')[1];
            var year = validityDate_str.split('-')[2];

            var validityDate = new Date(month + "/" + day + "/" + year);
            var currentDate = new Date();

            var diff = Math.floor((currentDate - validityDate) / (1000 * 60 * 60 * 24));
            if (diff == -1) //Diff arroja la cantidad de días que quedan.
            {
                table_quotationList.rows[i].style.backgroundColor = "#F7E79A";
            }

        }

    }

    function img_search_onClick() {
        updateParameters();
    }

    function tb_searchByQuotNum_onKeyPress(e) {

        if (e.keyCode == 13) {
            var a_img_magnifyIcon = document.getElementById("a_img_magnifyIcon");
            a_img_magnifyIcon.click();
        }
    }
    function tb_searchByQuotNum_onKeyUp(e) {

        if (tb_searchByQuotNum.value != "") {

            document.getElementById("cb_ws_a").disabled = true;
            document.getElementById("cb_ws_b").disabled = true;
            document.getElementById("cb_ws_c").disabled = true;
            document.getElementById("cb_ws_d").disabled = true;
            document.getElementById("cb_ws_e").disabled = true;
        }
        else {

            document.getElementById("cb_ws_a").disabled = false;
            document.getElementById("cb_ws_b").disabled = false;
            document.getElementById("cb_ws_c").disabled = false;
            document.getElementById("cb_ws_d").disabled = false;
            document.getElementById("cb_ws_e").disabled = false;
        }

        if (!IsNumber_or_point(e))
            tb_searchByQuotNum.value = "";
    }


    function updateParameters(e) {

        var ws_a = 0;
        var ws_b = 0;
        var ws_c = 0;
        var ws_d = 0;
        var ws_e = 0;

        if (document.getElementById("cb_ws_a").checked)
            ws_a = 1;
        if (document.getElementById("cb_ws_b").checked)
            ws_b = 1;
        if (document.getElementById("cb_ws_c").checked)
            ws_c = 1;
        if (document.getElementById("cb_ws_d").checked)
            ws_d = 1;
        if (document.getElementById("cb_ws_e").checked)
            ws_e = 1;

        var parameters;
        parameters = "quotNum=" + tb_searchByQuotNum.value;
        parameters += "&ws_a=" + ws_a;
        parameters += "&ws_b=" + ws_b;
        parameters += "&ws_c=" + ws_c;
        parameters += "&ws_d=" + ws_d;
        parameters += "&ws_e=" + ws_e;


        window.location = '@Url.Action("QuotationList", "Quotation")' + "?" +  parameters;
    }

    function lbl_ws_a_onClick(e) {
        if (tb_searchByQuotNum.value == "") {
            if (document.getElementById("cb_ws_a").checked)
                document.getElementById("cb_ws_a").checked = false;
            else
                document.getElementById("cb_ws_a").checked = true;

            updateParameters();
        }
    }
    function lbl_ws_b_onClick() {
        if (tb_searchByQuotNum.value == "") {
            if (document.getElementById("cb_ws_b").checked)
                document.getElementById("cb_ws_b").checked = false;
            else
                document.getElementById("cb_ws_b").checked = true;

            updateParameters();
        }
    }
    function lbl_ws_c_onClick() {
        if (tb_searchByQuotNum.value == "") {
            if (document.getElementById("cb_ws_c").checked)
                document.getElementById("cb_ws_c").checked = false;
            else
                document.getElementById("cb_ws_c").checked = true;

            updateParameters();
        }
    }
    function lbl_ws_d_onClick() {
        if (tb_searchByQuotNum.value == "") {
            if (document.getElementById("cb_ws_d").checked)
                document.getElementById("cb_ws_d").checked = false;
            else
                document.getElementById("cb_ws_d").checked = true;

            updateParameters();
        }
    }
    function lbl_ws_e_onClick() {
        if (tb_searchByQuotNum.value == "") {
            if (document.getElementById("cb_ws_e").checked)
                document.getElementById("cb_ws_e").checked = false;
            else
                document.getElementById("cb_ws_e").checked = true;

            updateParameters();
        }
    }

</script>


@section ButtonContainer
    
    <button type="button" class="btn_common btn_common_color2 btn_common_size3" onclick="window.location.href='@Url.Action("QuotationAdvancedSearch", "Quotation")'">Búsqueda Avda.</button>
    <button type="button" class="btn_common btn_common_color1 btn_common_size2" onclick="window.location.href='@Url.Action("QuotationAdd", "Quotation")'">Agregar</button>
    
End section



    <div>


        
        <div id="div_searchContainer">
            <label id="lbl_searchByQuotNum">N° Cotización</label>
            &nbsp
            <input id="tb_searchByQuotNum" class="tb_search" maxlength="10" onkeypress="tb_searchByQuotNum_onKeyPress(event)" onkeyup="tb_searchByQuotNum_onKeyUp(event)" type="text" />

            <a id="a_img_magnifyIcon" title="Buscar" style="cursor:pointer;" onclick="img_search_onClick()" >
                <img class="img_magnifyIcon"  />
            </a>
        </div>

        <br />
        <br />

        <table id="table_filterContainer">
            <tr>
                <td class="td_statusFilter">                    
                    <input id="cb_ws_a" type="checkbox" onchange="updateParameters()" /> <label id="lbl_ws_a" onclick="lbl_ws_a_onClick()"> @ViewBag.ws_a </label>
                    &nbsp&nbsp
                    <input id="cb_ws_b" type="checkbox"  onchange="updateParameters()" /> <label id="lbl_ws_b" onclick="lbl_ws_b_onClick()"> @ViewBag.ws_b </label>
                    &nbsp&nbsp
                    <input id="cb_ws_c" type="checkbox"  onchange="updateParameters()" /> <label id="lbl_ws_c" onclick="lbl_ws_c_onClick()"> @ViewBag.ws_c </label> 
                    &nbsp&nbsp
                    <input id="cb_ws_d" type="checkbox"  onchange="updateParameters()" /> <label id="lbl_ws_d" onclick="lbl_ws_d_onClick()"> @ViewBag.ws_d </label>
                    &nbsp&nbsp
                    <input id="cb_ws_e" type="checkbox"  onchange="updateParameters()" /> <label id="lbl_ws_e" onclick="lbl_ws_e_onClick()"> @ViewBag.ws_e </label>
                </td>
                <td class="td_searchResult">
                    <label id="lbl_searchResult">
                        @ViewBag.SearchResult
                    </label>
                </td>
            </tr>
        </table>
        

        <div id="div_content">
            @grid.GetHtml(
           htmlAttributes:=New With {.id = "table_quotationList"},
           tableStyle:="webGrid_table",
           headerStyle:="webGrid_header",
           footerStyle:="webGrid_footer",
           alternatingRowStyle:="webGrid_alternating_row",
           rowStyle:="",
           columns:=grid.Columns(
               grid.Column(
                   columnName:="CreationDate",
                   header:="Fecha Creación",
                   style:="td_creationDate"
                   ),
               grid.Column(
                   columnName:="ClientName",
                   header:="Cliente",
                   style:="td_clientName"
                   ),
               grid.Column(
                   columnName:="QuotationNumberVersion",
                   header:="N° Cot.",
                   style:="td_quotationNumberVersion"
                   ),
               grid.Column(
                   columnName:="WorkTypeQ",
                   header:="Tipo de Trabajo",
                   style:="td_workType"
                   ),
               grid.Column(
                   columnName:="ValidityDate",
                   header:="Fecha Validez",
                   style:="td_validityDate"
                   ),
               grid.Column(
                   columnName:="WorkOrderNumber",
                   header:="N° O.T.",
                   style:="td_woNumber",
                   format:=@@<a title="Documento" href="@Url.Action("WorkOrderEdit", "WorkOrder", New With {.workId = item.WorkOrder_Id, .fromNode = "QuotationList"})">@item.WorkOrderNumber</a>
                   ),
               grid.Column(
                   columnName:="NetValue",
                   header:="Monto Neto",
                   style:="td_netValue"
                   ),
               grid.Column(
                   columnName:="Status",
                   header:="Estado",
                   style:="td_status"
                   ),
               grid.Column(
                   header:="Doc.",
                   style:="webGrid_col_document",
                   format:=@@<a title="Documento" href="@Url.Action("GenerateQuotationPDF", "Quotation", New With {.quotId = item.Quotation_Id})">
                                <img class="img_downloadDocumentIcon"/>
                            </a>
                   ),
               grid.Column(
                   header:="",
                   style:="webGrid_col_controls",
                   format:=@@<a title="Editar" href="@Url.Action("QuotationEdit", "Quotation", New With {.quotId = item.Quotation_Id, .fromNode = "QuotationList"})">
                                <img class="img_editIcon" />
                            </a>
                   )
           ))


    </div>
        

</div>
