﻿@ModelType TopWork.LoginModel

@Code
    ViewData("AppNode") = "Log in"
    Layout = Nothing
End Code

<html>
    
<head>

    @Styles.Render("~/Styles/Login")
    @Scripts.Render("~/Scripts/jquery")

    <script type="text/javascript">
        $(document).ready(function () {
            $(this).volume = 0.0;
            $('#btn_login').click(function () {

                var txt_username = document.getElementById("txt_username");
                var txt_password = document.getElementById("txt_password");

                if (txt_username.value == "")
                    document.getElementById("lbl_fieldValidator_username").style.visibility = "Visible";
                else
                    document.getElementById("lbl_fieldValidator_username").style.visibility = "Hidden";
                if (txt_password.value == "")
                    document.getElementById("lbl_fieldValidator_password").style.visibility = "Visible";
                else
                    document.getElementById("lbl_fieldValidator_password").style.visibility = "Hidden";

                

                var Block = false
                if (Block) {
                    $.ajax({
                        url: "Home/Login",
                        type: "post",
                        data: {
                            "user": $('#txt_user').val(),
                            "password": $('#txt_password').val(),
                            "rememberMe": $('#cb_rememberMe').is(':checked'),
                        },
                        success: function (response, textStatus, jqXHR) {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var lbl_message = document.getElementById("lbl_message");
                            lbl_message.value = "Ha ocurrido un problema. | " + textStatus;
                        },
                        complete: function () {
                        }
                    });
                }
            });

        });

        function textBoxex_onKeyPress(e) {
            if (e.keyCode == 13) {
                document.getElementById("btn_login").click();
            }
        }


    </script>



</head>
<body>
    <div id="div_logo">
        <img id="img_logo" src="~/Images/app_logo_login.png" />
    </div>
        
    <div id="div_loginContainer">
        @Html.ValidationMessage("LogOnError", New With {.id="lbl_incorrectLogin"})
        <form id="form_login" method="post">
            <table>
                <tr>
                    <td class="td_items_textboxes">
                        Nombre de Usuario
                        <br />
                        @Html.TextBoxFor(Function(m) m.UserName, New With { .id="txt_username", .maxLength= "10", .class="TextBoxes", .onkeypress="textBoxex_onKeyPress(event)" })
                        <label id="lbl_fieldValidator_username" class="lbl_fieldValidator" style="visibility:hidden;"> * </label>
                    </td>
                </tr>
                <tr>
                    <td class="td_items_textboxes">
                        Contraseña
                        <br />
                        @Html.PasswordFor(Function(m) m.Password, New With { .id="txt_password", .class="TextBoxes", .onkeypress="textBoxex_onKeyPress(event)" })
                        <label id="lbl_fieldValidator_password" class="lbl_fieldValidator" style="visibility:hidden;"> * </label>    
                    </td>
                </tr>
                <tr>
                    <td class="td_items_checkbox">
                        <label>No cerrar sesión @Html.CheckBoxFor(Function(m) m.RememberMe)</label>                       
                    </td>
                </tr>
            </table>
            <br />
            <input id="btn_login" class="btn_common" style="width:140px; height:33px;" type="submit" value="Entrar"/>
        </form>
        


    </div>
    

</body>
</html>