﻿<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>@ViewData("AppNode") | TopWork - Maestranza Indumetal</title>
        <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <meta name="viewport" content="width=device-width" />
    </head>
    <body style="background-color:#d0d0d0">
          
            <div id="div_body" class="body-container-size">
                
                <section id="section_pages">
                    <div id="div_renderBody">
                        @RenderBody()
                    </div>
                </section>

            </div>
            <footer class="body-container-size">
                <div class="content-wrapper">
                   <p>@DateTime.Now.Year - Pockture Ltda.</p>
                </div>
            </footer>

    </body>
</html>
