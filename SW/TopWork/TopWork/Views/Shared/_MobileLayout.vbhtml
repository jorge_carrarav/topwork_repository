﻿@imports Models.DataProvider
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>@ViewData("AppNode") | TopWork - Maestranza Indumetal</title>
        <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <meta name="viewport" content="width=device-width" />


        <!-- Style -->
        @Styles.Render("~/Styles/Layout")
        @Styles.Render("~/Styles/Sites")
        @Styles.Render("~/Styles/jquery")
        @Styles.Render("~/Styles/ZMStyle")
        <!-- /Style -->
        <!-- Script -->
        @Scripts.Render("~/Scripts/jquery")
        @Scripts.Render("~/Scripts/zmjquery")
        <!-- /Script -->
        

        @If (ViewBag.UserFirstName = "") Then
            Dim userName As String = WebSecurity.CurrentUserName
            Dim userId As Integer = WebSecurity.GetUserId(userName)
            Dim context As New TopWork.TopWorkEntities
            Try
                Dim firstName As String = (From u In context.UserInfo
                                           Where u.UserId = userId
                                           Select u).FirstOrDefault().FirstName
                ViewBag.UserFirstName = firstName
            Catch ex As Exception
                ViewBag.UserFirstName = "error"
            End Try
        End If
            

    <script>
        $(function () {
            $('.zetta-menu').zettaMenu({
                showOn: "hover",
                sticky: true,
                fixed: true,
                effect: {
                    name: "fade",
                    easing: "ease",
                    speed: 80,
                    delay: 0
                }
            });
        });

                


    </script>
    </head>
    <body>
        
         
            <div id="div_menu" class="body-container-size">
                <ul id="ul_menu" class="zetta-menu">
		                <li>@Html.ActionLink("Cotizaciones", "Home", "Quotations")</li>
                        <li>@Html.ActionLink("Órdenes de Trabajo", "Home", "Quotations")</li>
		                <li class="zm-right-align">
			                <a>Registros</a>
				            <ul>
					            <li>@Html.ActionLink("Clientes", "Home", "Quotations")</li>
                                <li>@Html.ActionLink("Proveedores", "Providers", "Register")</li>
				            </ul>   			                
		                </li>
		                <li class="zm-right-align">
			                <a>TABLET</a>
				            <ul>
                                <li style="width:183px;">@Html.ActionLink("Órdenes de Trabajo", "Home", "Quotations")</li>
				            </ul>
		                </li>
		                
	                </ul>
            </div>     

            <div id="div_body" class="body-container-size">
                
                <section id="section_pages">
                    <div id="div_appNodeContainer" >
                        @ViewBag.AppNode
                    </div>
                    <div id="div_resultMessage" >
                        <label class="lbl_commonResult">
                            @ViewBag.ResultMessage
                        </label>
                    </div>
                    <div id="div_upperButtonsContainer" >
                        @RenderSection("ButtonContainer", required:=False)
                    </div>
                    <br />
                    <div id="div_renderBody">
                        @RenderBody()
                    </div>
                </section>

            </div>
         


    </body>
</html>
