﻿@imports Models.DataProvider
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>@ViewData("AppNode") | TopWork - Maestranza Indumetal </title>
        <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <meta name="viewport" content="width=device-width" />


        <!-- Style -->
        @Styles.Render("~/Styles/Layout")
        @Styles.Render("~/Styles/Sites")
        @Styles.Render("~/Styles/jquery")
        @Styles.Render("~/Styles/WebGridTables")
        @Styles.Render("~/Styles/ZMStyle")

        <!-- /Style -->
        <!-- Script -->
        @Scripts.Render("~/Scripts/jquery")
        @Scripts.Render("~/Scripts/zmjquery")
        @Scripts.Render("~/Scripts/Tools")
        @Scripts.Render("~/Scripts/JQuery_Settings")
        <!-- /Script -->
        

        @If (ViewBag.UserFirstName = "") Then
            Dim userName As String = WebSecurity.CurrentUserName
            Dim userId As Integer = WebSecurity.GetUserId(userName)
            Dim context As New TopWork.TopWorkEntities
            Try
                Dim firstName As String = (From u In context.UserInfo
                                           Where u.UserId = userId
                                           Select u).FirstOrDefault().FirstName
                ViewBag.UserFirstName = firstName
            Catch ex As Exception
                ViewBag.UserFirstName = "error"
            End Try
        End If
            

    <script>

        

        $(document).ready(function (){
            
            $("#div_loading").dialog({
                resizable: false,
                height: 170,
                width: 170,
                dialogClass: "noclose",
                autoOpen: false,
                modal: true,
                title: "Cargando ..."
            });

        });

        $(function () {
            $('.zetta-menu').zettaMenu({
                showOn: "hover",
                sticky: true,
                fixed: true,
                effect: {
                    name: "fade",
                    easing: "ease",
                    speed: 80,
                    delay: 0
                }
            });
            
            //Después de cada request, el scroll se va a cero
            $('html, body').animate({

                scrollTop: $("#header").offset().top
            }, 100);
        });

        var hideResultMessage_timeout = 5000;
                
        $(document).ready(function () {
            
            showResultMessage();

        });

        function showResultMessage() {

            var lbl_resultMessage = document.getElementById("lbl_resultMessage");

            lbl_resultMessage.style.visibility = "Visible";

            window.setTimeout(function () {
                lbl_resultMessage.style.visibility = "Hidden";
            }
            , hideResultMessage_timeout);
        }

    </script>
    </head>
    <body>

<div id="div_loading" style="display:none; text-align:center; vertical-align:middle;">
    <img style="width:80px; padding-top:10px;" src="~/Images/Sites/loading.gif"> 
</div>
        
            <header id="header" class="body-container-size">
                <div id="div_appLogo">
                    <img id="img_appLogo" src="~/Images/app_logo_banner.png" />
                </div>
                <div id="div_loginData">
                    Bienvenido @ViewBag.UserFirstName ( @Html.ActionLink("Salir", "Logout", "Account", Nothing, New With { .id="al_logout"} ) )
                </div>
                <div id="div_companyLogo">
                    <img id="img_companyLogo"  src="~/Images/company_logo.png" />
                </div>
            </header>
            <div id="div_menu" class="body-container-size">
                <ul id="ul_menu" class="zetta-menu">
		                <li>@Html.ActionLink("Cotizaciones", "QuotationList", "Quotation")</li>
                        <li>@Html.ActionLink("Trabajos", "WorkOrderList", "WorkOrder")</li>
					    <li>@Html.ActionLink("Ordenes de Compra", "PurchaseOrderList", "PurchaseOrder")</li>
		                <!--<li class="zm-right-align">
			                <a>Bodega</a>
				            <ul>
					            <li style="width:170px;">@Html.ActionLink("Stock Venta", "ClientList", "Register")</li>
                                <li style="width:170px;">@Html.ActionLink("Stock Materiales", "SupplierList", "Register")</li>
                                <li style="width:170px;">@Html.ActionLink("Stock en camino", "SupplierList", "Register")</li>
				            </ul>   			                
		                </li>-->
		                <li class="zm-right-align">
			                <a>Registros</a>
				            <ul>
					            <li style="width:170px;">@Html.ActionLink("Clientes", "ClientList", "Register")</li>
                                <li style="width:170px;">@Html.ActionLink("Proveedores", "SupplierList", "Register")</li>
				            </ul>   			                
		                </li>
		                <li class="zm-right-align">
			                <a>Informes</a>
				            <ul>
                                <li style="width:220px;">@Html.ActionLink("Informe de Cotizaciones", "QuotationsReport", "Report")</li>
                                <li style="width:220px;">@Html.ActionLink("Informe de Utilidades", "UtilitiesReport", "Report")</li>
				            </ul>
		                </li>
		                
	                </ul>
            </div>     

            <div id="div_body" class="body-container-size">
                
                <section id="section_pages">
                    <div id="div_appNodeContainer" >
                        @ViewBag.AppNode
                    </div>
                    <div id="div_resultMessage" >
                        <label id="lbl_resultMessage" class="lbl_commonResult">
                            @ViewBag.ResultMessage
                        </label>
                    </div>
                    <div id="div_upperButtonsContainer" >
                        @RenderSection("ButtonContainer", required:=False)
                    </div>
                    <br />
                    <div id="div_renderBody">
                        @RenderBody()
                    </div>
                </section>

            </div>
            <footer class="body-container-size">
                <div class="content-wrapper">
                   <p>@DateTime.Now.Year - Pockture Ltda.</p>
                </div>
            </footer>


    </body>
</html>
