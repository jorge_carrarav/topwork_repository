﻿@Code
    ViewData("AppNode") = "Detalles del Servidor"
End Code


<style>
    
    .td_1rstCol {
        width:200px;
    }
    .td_2ndCol {
        text-align:right;
    }

</style>

<table>
    <tr>
        <td class="td_1rstCol">
            ServerName
        </td>
        <td style="width:1px;">:</td>
        <td class="td_2ndCol">
            @ViewBag.ServerName
        </td>
    </tr>
    <tr>
        <td class="td_1rstCol">
            DomainName
        </td>
        <td style="width:1px;">:</td>
        <td class="td_2ndCol">
            @ViewBag.DomainName
        </td>
    </tr>
    <tr>
        <td class="td_1rstCol">
            OSVersion
        </td>
        <td style="width:1px;">:</td>
        <td class="td_2ndCol">
            @ViewBag.OSVersion
        </td>
    </tr>
    <tr>
        <td class="td_1rstCol">
            ProcessorCount
        </td>
        <td style="width:1px;">:</td>
        <td class="td_2ndCol">
            @ViewBag.ProcessorCount
        </td>
    </tr>
</table>