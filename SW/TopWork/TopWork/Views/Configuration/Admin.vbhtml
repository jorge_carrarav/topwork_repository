﻿@Code
    ViewData("AppNode") = "Administración"
End Code


<style>
    .div_buttonContainer
    {
        text-align:center;
    }
</style>

<div class="div_buttonContainer">
    <button type="button" onclick="window.location.href='@Url.Action("UserList", "Configuration")'" class="btn_common btn_common_color1 btn_common_size3"> Usuarios </button>
</div>
<br />
<div class="div_buttonContainer">
    <button type="button" onclick="window.location.href='@Url.Action("WorkTypeQList", "Configuration")'" class="btn_common btn_common_color1 btn_common_size3"> Tipos de Trabajo </button>
</div>
<br />
<div class="div_buttonContainer">
    <button type="button" onclick="window.location.href='@Url.Action("MethodOfPaymentList", "Configuration")'" class="btn_common btn_common_color1 btn_common_size3"> Formas de Pago</button>
</div>
<br />
<div class="div_buttonContainer">
    <button type="button" onclick="window.location.href='@Url.Action("AppConfiguration", "Configuration")'" class="btn_common btn_common_color1 btn_common_size3"> Config. de sistema </button>
</div>
