﻿@ModelType IEnumerable(of TopWork.UserDataProvider.UserCompleteInfoModel)
@Code
    ViewData("AppNode") = "Administración > Usuarios"
    Layout = "~/Views/Shared/_MainLayout.vbhtml"
    Dim rowIntercalation As integer = 1
End Code

<style>
    #table_users
    {
        width: 600px;
    }
</style>

@section ButtonContainer
     <button type="button" id="btn_goBack" class="btn_common btn_common_color2 btn_common_size1" onclick="window.location.href='@Url.Action("Admin","Configuration")'">Volver</button>
    &nbsp
    <button type="button" class="btn_common btn_common_color1 btn_common_size2" onclick="window.location.href='@Url.Action("UserAdd","Configuration")'">Agregar</button>
End section



    <table id="table_users" class="table_common table_common_marginTop" align="center" cellspacing="0">
        <tr>
            <td class="td_commonHeader_forCommonTable">
                Nombre de usuario
            </td>
            <td class="td_commonHeader_forCommonTable">
                Nombre
            </td>
            <td class="td_commonHeader_forCommonTable">
                Apellidos
            </td>
            <td class="td_commonHeader_forCommonTable">
                Activo
            </td>
            <td class="td_commonHeader_forCommonTable">
            
            </td>
        </tr>

    
        @For Each User As Object In Model
        
                @<tr>
                    <td class="td_row_forCommonTable td_rowColor_forCommonTable_@rowIntercalation">
                        @User.UserName()
                    </td>
                    <td class="td_row_forCommonTable td_rowColor_forCommonTable_@rowIntercalation">
                        @User.FirstName()
                    </td>
                    <td class="td_row_forCommonTable td_rowColor_forCommonTable_@rowIntercalation">
                        @User.LastName()
                    </td>
                    <td class="td_row_forCommonTable td_rowColor_forCommonTable_@rowIntercalation">
                        @User.Active()
                    </td>
                    <td   class="td_row_forCommonTable td_rowColor_forCommonTable_@rowIntercalation td_controls_forCommonTable">
                        <a title="Editar" href="@Url.Action("UserEdit", "Configuration", New With { .userId = User.UserId })">
                            <img class="img_editIcon" />
                        </a>
                    </td>
                </tr>
        
            If rowIntercalation = 1 Then
                rowIntercalation = 2
            ElseIf rowIntercalation = 2 Then
                rowIntercalation = 1
            End If
        Next
        

    </table>

