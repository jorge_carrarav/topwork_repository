﻿@ModelType IEnumerable(of TopWork.MethodOfPayment)
@Code
    ViewData("AppNode") = "Administración > Formas de Pago"
    Dim rowIntercalation As Integer = 1
End Code



<style>
    #table_workTypes
    {
        width: 500px;
    }
</style>

@section ButtonContainer
    <button type="button" id="btn_goBack" class="btn_common btn_common_color2 btn_common_size1" onclick="window.location.href='@Url.Action("Admin","Configuration")'">Volver</button>
    &nbsp
    <button type="button" class="btn_common btn_common_color1 btn_common_size2" onclick="window.location.href='@Url.Action("MethodOfPaymentAdd","Configuration")'">Agregar</button>
End section


    <table id="table_workTypes" class="table_common table_common_marginTop" align="center" cellspacing="0">
        <tr>
            <td class="td_commonHeader_forCommonTable">
                Formas de Pago
            </td>
            <td class="td_commonHeader_forCommonTable">
                Activo
            </td>
            <td class="td_commonHeader_forCommonTable">
            
            </td>
        </tr>
            
        @For Each method As TopWork.MethodOfPayment In Model
        
                @<tr>
                    <td class="td_row_forCommonTable td_rowColor_forCommonTable_@rowIntercalation">
                        @method.MethodName
                    </td>
                    <td class="td_row_forCommonTable td_rowColor_forCommonTable_@rowIntercalation">
                        @method.Active
                    </td>
                    <td class="td_row_forCommonTable td_rowColor_forCommonTable_@rowIntercalation td_controls_forCommonTable">
                        <a title="Editar" href="@Url.Action("MethodOfPaymentEdit", "Configuration", New With { .methodId = method.MethodOfPayment_Id })">
                            <img class="img_editIcon" />
                        </a>
                    </td>
                </tr>
        
            If rowIntercalation = 1 Then
                rowIntercalation = 2
            ElseIf rowIntercalation = 2 Then
                rowIntercalation = 1
            End If
        Next
        

    </table>


