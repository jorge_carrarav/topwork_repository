﻿@Code
    ViewData("AppNode") = "Administración > Config. de sistema"
End Code

<style>

    #table_appConfiguration {
        border:1px solid gray;
        margin:auto;
        padding:30px;
    }
    .td_1rstCol {
        width:150px;
        height:50px;
    }
    .td_3rdCol {
        padding-left:30px; 
        width:400px;
    }
    .ta_code {
        width:90%;
        height:150px;
    }

</style>

<script>


    function btn_enable_onClick() {
        
    }

    function btn_disable_onClick() {

    }


</script>

@section ButtonContainer
    <button type="button" id="btn_goBack" class="btn_common btn_common_color2 btn_common_size1" onclick="window.location.href='@Url.Action("Admin", "Configuration")'">Volver</button>
End section

<table id="table_appConfiguration">
    <tr>
        <td class="td_1rstCol">
            IVA %
        </td>
        <td style="width:1px;">
            :
        </td>
        <td class="td_3rdCol">
            @ViewBag.IVAPercentage
        </td>
    </tr>
    <tr>
        <td class="td_1rstCol">
            Fecha Inicio
        </td>
        <td style="width:1px;">
            :
        </td>
        <td class="td_3rdCol">
            @ViewBag.AppStartDate
        </td>
    </tr>
    <tr>
        <td class="td_1rstCol">
            Fecha Expiración
        </td>
        <td style="width:1px;">
            :
        </td>
        <td class="td_3rdCol">
            @ViewBag.AppExpirationDate
        </td>
    </tr>
    <tr>
        <td class="td_1rstCol">
            DB Code
        </td>
        <td style="width:1px;">
            :
        </td>
        <td class="td_3rdCol">
            <textarea class="ta_code">                
                @ViewBag.AppCode
            </textarea>
        </td>
    </tr>
    <tr>
        <td class="td_1rstCol">
            Real Code
        </td>
        <td style="width:1px;">
            :
        </td>
        <td class="td_3rdCol">
            <textarea class="ta_code">                
                @ViewBag.ThisServerCode
            </textarea>
        </td>
    </tr>
</table>

<div id="div_mainButtonsContainer">
    <button type="button" id="btn_disable" value="block" onclick="btn_disable_onClick()" class="btn_common btn_common_color2 btn_common_size2">Bloquear (beta)</button>
    &nbsp
    <button type="submit" id="btn_enable" value="save" onclick="btn_enable_onClick()" class="btn_common btn_common_color1 btn_common_size2">Habilitar (beta)</button>
</div>
