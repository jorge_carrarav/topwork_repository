﻿@ModelType TopWork.MethodOfPayment
@Code
    ViewData("AppNode") = "Administración > Formas de Pago > Agregar"
End Code


<style>
    #div_formContainer {
        width:20%;
    }
</style>

<form method="post" action="MethodOfPaymentAdd">

    <div id="div_formContainer">

        <div class="div_formItem"> 
            <div class="div_fieldDescription">
                Forma de Pago
                <span class="span_requiredIcon">*</span>
            </div>

            @Html.TextBoxFor(Function(model) model.MethodName, New With { .maxLength= "50"})
        </div>
        <div class="div_formItem"> 
            <div class="div_fieldDescription">
                Activo
            </div>

            @Html.CheckBoxFor(Function(model) model.Active, New With {.Checked = True})
        </div>
               
        <br />
        <br />

        <div id="div_mainButtonsContainer">
            <button type="button" id="btn_goBack" onclick="window.location.href='@Url.Action("MethodOfPaymentList", "Configuration")'" class="btn_common btn_common_color2 btn_common_size1">Volver</button>
            &nbsp
            <button type="submit" id="btn_save" class="btn_common btn_common_color1 btn_common_size2">Crear Forma</button>
        </div>

    </div>
    


</form>