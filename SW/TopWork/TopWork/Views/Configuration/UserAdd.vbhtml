﻿@ModelType TopWork.UserRegistrationModel
@Code
    ViewData("AppNode") = "Administración > Usuarios > Agregar"
End Code


<style>
    #div_formContainer {
        width:20%;
    }
</style>

<form method="post" action="UserAdd">

    <div id="div_formContainer" style="overflow:hidden;">

        <div class="div_formItem">
            <div class="div_fieldDescription">
                Nombre de Usuario
                <span class="span_requiredIcon">*</span>
            </div>

            @Html.TextBoxFor(Function(model) model.UserName, New With { .maxLength= "15"})
        </div>
        <div class="div_formItem">            
            <div class="div_fieldDescription">
                Contraseña
                <span class="span_requiredIcon">*</span>
            </div>
            @Html.TextBoxFor(Function(model) model.Password, New With {.maxLength= "12"})
        </div>
        <div class="div_formItem">
            <div class="div_fieldDescription">
                Nombre
                <span class="span_requiredIcon">*</span>
            </div>
            
            @Html.TextBoxFor(Function(model) model.FirstName, New With { .maxLength= "25" })
        </div>
        <div class="div_formItem">
            <div class="div_fieldDescription">
                Apellidos
                <span class="span_requiredIcon">*</span>
            </div>
            
            @Html.TextBoxFor(Function(model) model.LastName, New With {.maxLength= "25"})
        </div>
        <div class="div_formItem">
            <div class="div_fieldDescription">
                Activo
            </div>
            
            @Html.CheckBoxFor(Function(model) model.Active, New With {.Checked = "True"})
        </div>
        
        <br />
        <br />

        <div id="div_mainButtonsContainer">
            <button type="button" id="btn_goBack" onclick="window.location.href='@Url.Action("UserList","Configuration")'" class="btn_common btn_common_color2 btn_common_size1">Volver</button>
            &nbsp
            <button type="submit" id="btn_save" class="btn_common btn_common_color1 btn_common_size2">Crear Usuario</button>
        </div>



    </div>
    


</form>