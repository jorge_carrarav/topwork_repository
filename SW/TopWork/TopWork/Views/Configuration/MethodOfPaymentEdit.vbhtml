﻿@ModelType TopWork.MethodOfPayment
@Code
    ViewData("AppNode") = "Administración > Formas de Pago > Modificar"
End Code


<script>

    function btn_delete_onClick() {

        $("#div_confirmDeletion").dialog({
            resizable: false,
            height: 100,
            dialogClass: "noclose",
            modal: true,
            title: "¿Desea eliminar?",
            buttons: {
                "Si": function () {
                    window.location.href = '@Url.Action("MethodOfPaymentDelete", "Configuration", New With {.methodId = Model.MethodOfPayment_Id})'
                },
                "Cancelar": function () {
                    $(this).dialog("close");
                }
            }
        });
        
    }

</script>
<style>
    #div_formContainer {
        width:20%;
    }
</style>

<div id="div_confirmDeletion" style="visibility:hidden; display:none;"  >    
</div>


<form method="post" action="MethodOfPaymentEdit">

    <div id="div_formContainer">
        
        <div class="div_formItem"> 
            <div class="div_fieldDescription">
                Forma de Pago
                <span class="span_requiredIcon">*</span>
            </div>
            @Html.TextBoxFor(Function(model) model.MethodName, New With { .maxLength= "50 "})
        </div>
        <div class="div_formItem"> 
            <div class="div_fieldDescription">
                Activo
            </div>
            
            @Html.CheckBoxFor(Function(model) model.Active)
        </div>
       
        <br />
        <br />

        <div id="div_mainButtonsContainer">
            <button type="button" id="btn_goBack" onclick="window.location.href='@Url.Action("MethodOfPaymentList","Configuration")'" class="btn_common btn_common_color2 btn_common_size1">Volver</button>
            &nbsp
            <button type="button" id="btn_delete" value="delete" onclick="btn_delete_onClick()" class="btn_common btn_common_color2 btn_common_size2">Eliminar</button>
            &nbsp
            <button type="submit" id="btn_save" value="save" class="btn_common btn_common_color1 btn_common_size2">Guardar</button>
        </div>

    </div>
    

     <!-- Este textbox debe ir para que el Action reciba todos los atributos del modelo -->
    @Html.TextBoxFor(Function(model) model.MethodOfPayment_Id, New With {.style = "visibility:hidden"})

</form>