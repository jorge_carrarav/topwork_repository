﻿@ModelType TopWork.ReportDataProvider.QuotationsReportModel
@Code
    ViewData("AppNode") = "Informes > Informe de Cotizaciones"
End Code


<style>

    #table_reportFilters {
        width:700px;
        border:2px solid lightGray;
        margin:auto;        
    }
    #td_filters_left {
        vertical-align:top;
    }
    #td_filters_right {
        vertical-align:top;
    }
    #table_reportFilters_left {
        vertical-align:top;
        width:100%;
    }
    #table_reportFilters_right {
        vertical-align:top;
        width:100%;
        height:250px;
    }
    
    .td_reportFilter_left_col1 {
        vertical-align:top;
        width:150px;
    }
    .td_reportFilter_left_col2 {
        vertical-align:top;
    }

    .td_reportFilter_right_col1 {
        vertical-align:top;
        width:150px;
    }
    .td_reportFilter_right_col2 {
        vertical-align:top;
    }

        .ui-autocomplete {
        background-color: #FCFDD2;
        text-align:right;
    }

</style>

<script>

    $(document).ready(function () {

        initValues();

        $(function () {
            $("#tb_dateFrom").datepicker({
                changeMonth: true,
                changeYear: true
            });
            $("#tb_dateTo").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });

        $(function () {
            var rut_list_viewbag = "@ViewBag.RutList";
            var rut_list = rut_list_viewbag.split("&#39;").join("").split(',');

            $("#tb_clientRut").autocomplete({
                source: rut_list
            });


            var name_list_viewbag = "@ViewBag.NameList";
            var name_list = name_list_viewbag.split("&#39;").join("").split(',');

            $("#tb_clientName").autocomplete({
                source: name_list
            });
        });
        
    });

    function initValues() {

        var dateTo = new Date();

        document.getElementById("tb_dateFrom").value = getFormatDate("01-01-" + dateTo.getFullYear());
        document.getElementById("tb_dateTo").value = getFormatDate(dateTo.getDate() + "-" + (dateTo.getMonth() + 1) + "-" + dateTo.getFullYear());

        document.getElementById("tb_clientName").value = "Todos";
        document.getElementById("tb_clientRut").value = "Todos";
        
        document.getElementById("tb_valueFrom").value = "Todos";
        document.getElementById("tb_valueTo").value = "Todos";

    }

    
    function itemOnFocus(id) {
        if (document.getElementById(id).value == "Todos" || document.getElementById(id).value == "0") {
            document.getElementById(id).value = "";
        }
        document.getElementById(id).style.color = "Black";
    }
    function itemOnBlur(id) {
        if (document.getElementById(id).value == "") {
            if (id == "tb_valueFrom" || id == "tb_valueTo") {
                document.getElementById(id).value = "";
                document.getElementById(id).style.color = "Gray";
            }
            else {
                document.getElementById(id).value = "Todos";
                document.getElementById(id).style.color = "Gray";
            }
        }
    }

    function tb_clientRut_onKeyPress() {
        document.getElementById("tb_clientName").style.color = "Gray";
        document.getElementById("tb_clientName").value = "Todos";
    }
    function tb_clientName_onKeyPress() {
        document.getElementById("tb_clientRut").style.color = "Gray";
        document.getElementById("tb_clientRut").value = "Todos";
    }

    function ddl_valueType_onChange() {

        document.getElementById("tb_valueFrom").style.color = "Gray";
        document.getElementById("tb_valueTo").style.color = "Gray";
        if (document.getElementById("ddl_valueType").selectedIndex != 0) {
            document.getElementById("tb_valueFrom").value = "0";
            document.getElementById("tb_valueTo").value = "0";
        }
        else {
            document.getElementById("tb_valueFrom").value = "Todos";
            document.getElementById("tb_valueTo").value = "Todos";
        }
    }

</script>


<form method="post" action="@Url.Action("GenerateQuotationsReportPDF", "Report")" >

    <div style="margin:auto; text-align:center;">
        <label style="font-size:x-large;">Informe de Cotizaciones</label>
    </div>
    
    <br />
    <br />

    <table id="table_reportFilters">
        <tr>
            <td id="td_filters_left">

                <table id="table_reportFilters_left">
                    <tr>
                        <td class="td_reportFilter_left_col1">
                            Fecha Desde                
                        </td>
                        <td class="td_reportFilter_left_col2">
                            @Html.TextBoxFor(Function(model) model.DateFrom, New With {.id = "tb_dateFrom", .maxLength = "10", .style = "width:120px; text-align:center; ", .onchange="setDateFormat(this.id)"})
                        </td>
                    </tr>
                    <tr>
                        <td class="td_reportFilter_left_col1">
                            Fecha Hasta
                            <br />
                            <br />
                            <br />
                        </td>
                        <td class="td_reportFilter_left_col2">
                            @Html.TextBoxFor(Function(model) model.DateTo, New With {.id = "tb_dateTo", .maxLength = "10", .style = "width:120px; text-align:center; ", .onchange="setDateFormat(this.id)"})
                        </td>
                    </tr>
                    <tr>
                        <td class="td_reportFilter_left_col1">
                            Rut Cliente          
                        </td>
                        <td class="td_reportFilter_left_col2">
                            @Html.TextBoxFor(Function(model) model.ClientRut, New With {.id = "tb_clientRut", .maxLength = "10", .style = "width:120px; text-align:center; color:Gray;", .onFocus="itemOnFocus(this.id)", .onBlur="itemOnBlur(this.id)", .onKeyPress="tb_clientRut_onKeyPress()"})
                        </td>
                    </tr>
                    <tr>
                        <td class="td_reportFilter_left_col1">  
                            Nombre Cliente      
                            <br />
                            <br />     
                            <br />     
                        </td>
                        <td class="td_reportFilter_left_col2">
                            @Html.TextBoxFor(Function(model) model.ClientName, New With {.id = "tb_clientName", .maxLength = "10", .style = "width:160px; text-align:center; color:Gray;", .onFocus="itemOnFocus(this.id)", .onBlur="itemOnBlur(this.id)", .onKeyPress="tb_clientName_onKeyPress()"})
                        </td>
                    </tr>
                    <tr>
                        <td class="td_reportFilter_left_col1">
                            Tipo de Monto                            
                        </td>
                        <td class="td_reportFilter_left_col2">
                            @Html.DropDownListFor(Function(model) model.ValueType, New SelectList(ViewBag.ValueType_list), New With {.id = "ddl_valueType", .style = "width:160px; height: 24px;", .onChange="ddl_valueType_onChange()"})   
                        </td>
                    </tr>
                    <tr>
                        <td class="td_reportFilter_left_col1">
                            Monto Desde
                        </td>
                        <td class="td_reportFilter_left_col2">
                            @Html.TextBoxFor(Function(model) model.ValueFrom, New With {.id = "tb_valueFrom", .maxLength = "15", .style = "width:120px; text-align:right; color:Gray;", .onFocus="itemOnFocus(this.id)", .onKeyUp="checkIfNumberOrNegative_thousandSeparator(this.id,event)", .onBlur="itemOnBlur(this.id)"})
                        </td>
                    </tr>
                    <tr>
                        <td class="td_reportFilter_left_col1">
                            Monto Hasta
                        </td>
                        <td class="td_reportFilter_left_col2">
                            @Html.TextBoxFor(Function(model) model.ValueTo, New With {.id = "tb_valueTo", .maxLength = "15", .style = "width:120px; text-align:right; color:Gray;", .onFocus="itemOnFocus(this.id)", .onKeyUp="checkIfNumberOrNegative_thousandSeparator(this.id,event)", .onBlur="itemOnBlur(this.id)"})                
                        </td>
                    </tr>
                </table>
            </td>
            <td id="td_filters_right">
                <table id="table_reportFilters_right">
                   <tr>
                        <td class="td_reportFilter_right_col1">
                            Tipo de Trabajo
                        </td>
                        <td class="td_reportFilter_right_col2">
                            @Html.DropDownListFor(Function(model) model.WorkTypeQ, New SelectList(ViewBag.WorkTypeQ_list), New With {.id = "ddl_workType", .style = "width:160px; height: 24px;"})
                        </td>
                    </tr>
                    <tr>
                        <td class="td_reportFilter_right_col1">
                            Estado del Trabajo
                        </td>
                        <td class="td_reportFilter_right_col2">
                            @Html.DropDownListFor(Function(model) model.WorkStatus, New SelectList(ViewBag.WorkStatus_list), New With {.id = "ddl_workType", .style = "width:160px; height: 24px;"})
                        </td>
                    </tr>
                    <tr>
                        <td class="td_reportFilter_right_col1">
                            Usuario asignado
                        </td>
                        <td class="td_reportFilter_right_col2">
                            @Html.DropDownListFor(Function(model) model.CreatedBy, New SelectList(ViewBag.CreatedBy_list), New With {.id = "ddl_CreatedBy", .style = "width:160px; height: 24px;"})
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
    
    <br />
    <br />

    <div id="div_mainButtonsContainer">
        <button type="submit" id="btn_create" class="btn_common btn_common_color1 btn_common_size3">Generar Informe</button>
    </div>

</form>