﻿@ModelType IEnumerable(Of TopWork.WayBillDataProvider.WayBill_List_Formatted)
@Code
    ViewData("AppNode") = "Guías de Despacho"
    
    Dim grid = New WebGrid(Model, canPage:=True, rowsPerPage:=15)
    grid.Pager(WebGridPagerModes.All)
End Code


<style>
    #div_controlsContainer
    {
    }
    #table_searchControls {
        width:100%;
    }
    #lbl_searchResult {
        color:blue;
    }

    #div_searchContainer {
        text-align:center;
        margin-top:-40px;
    }


    .td_creationDate {
        width:15%;
    }
    .td_clientName {
        text-align:left;
        width:18%;
    }
    .td_wayBillNumber {
        text-align:right;
        width:15%;
    }
    .td_workOrderNumber {
        text-align:right;
        width:15%;
    }
    .td_status {
        width:15%;
    }
    
   
    .td_searchResult {
        text-align:right;
    }

    .tb_search {
        text-align:right;
        width:150px;
    }

    .webGrid_table {
        width:100%;
    }
    
</style>


<script>

    $(document).ready(function () {


        if (document.URL.split("wayBillNum=").length > 1) {
            tb_searchBywayBillNum.value = document.URL.split("wayBillNum=")[1].split("&")[0]
        }
        
    });
    
    function tb_searchByWayBillNum_onKeyPress(e) {

        if (e.keyCode == 13) {
            var a_img_magnifyIcon = document.getElementById("a_img_magnifyIcon");
            a_img_magnifyIcon.click();
        }

    }

    function tb_searchByWayBillNum_onKeyUp(id, e) {
        checkIfNumber(id, e);
    }


    function img_search_onClick() {

        var parameters;
        parameters = "wayBillNum=" + tb_searchByWayBillNum.value;

        window.location = '@Url.Action("WayBillList", "WayBill")' + "?" + parameters;
    }

    
      
    
</script>



@section ButtonContainer
    
    <button type="button" class="btn_common btn_common_color2 btn_common_size3" onclick="window.location.href='@Url.Action("WorkAdvancedSearch", "WorkOrder")'">Búsqueda Avda.</button>
    <button type="button" class="btn_common btn_common_color1 btn_common_size2" onclick="window.location.href='@Url.Action("WorkOrderAdd", "WorkOrder")'">Agregar</button>
    
End section



<div>

    <div id="div_searchContainer">
            
        <label id="lbl_searchBywayBillNum">N° O.T.</label>
        &nbsp
        <input id="tb_searchByWayBillNum" class="tb_search" maxlength="10" onkeypress="tb_searchByWayBillNum_onKeyPress(event)" onkeyup="tb_searchByWayBillNum_onKeyUp(this.id,event)" type="text" />
        <a id="a_img_magnifyIcon" title="Buscar" style="cursor:pointer;" onclick="img_search_onClick()" >
            <img class="img_magnifyIcon" />
        </a>
            
    </div>

    <br />
    <br />

    <table id="table_searchControls" >
        <tr>
            <td class="td_statusFilter">                    

            </td>
            <td class="td_searchResult">
                <label id="lbl_searchResult">
                    @ViewBag.SearchResult
                </label>
            </td>
        </tr>
    </table>
     

    <div id="div_content">
        @grid.GetHtml(
        htmlAttributes:=New With {.id = "table_workList"},
        tableStyle:="webGrid_table",
        headerStyle:="webGrid_header",
        footerStyle:="webGrid_footer",
        alternatingRowStyle:="webGrid_alternating_row",
        rowStyle:="",
        columns:=grid.Columns(
            grid.Column(
                columnName:="CreationDate",
                header:="Fecha Creación",
                style:="td_creationDate"
                ),
            grid.Column(
                columnName:="ClientName",
                header:="Cliente",
                style:="td_clientName"
                ),
            grid.Column(
                columnName:="WayBillNumber",
                header:="N° Guía de Despacho",
                style:="td_wayBillNumber"
                ),
            grid.Column(
                columnName:="WorkOrderNumber",
                header:="N° O.T.",
                style:="tb_workOrderNumber"
                ),
            grid.Column(
                columnName:="Status",
                header:="Estado",
                style:="td_status"
                ),
            grid.Column(
                header:="",
                style:="webGrid_col_controls",
                format:=@@<a title="Editar" href="@Url.Action("WayBillEdit", "WayBill", New With {.wayBillId = item.WayBill_Id})">
                            <img class="img_editIcon" />
                        </a>
                )
        ))


    </div>
        

</div>

