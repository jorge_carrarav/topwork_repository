﻿@ModelType IEnumerable(of TopWork.PurchaseOrderDataProvider.PurchaseOrder_List_Formatted)
@Code
    ViewData("AppNode") = "Órdenes de Compra"
    
    Dim grid = New WebGrid(Model, canPage:=True, rowsPerPage:=15)
    grid.Pager(WebGridPagerModes.All)
End Code


<style>

    #table_filterContainer {
        width:100%;
    }
    #lbl_searchResult {
        color:blue;
    }

    #div_searchContainer {
        text-align:center;
        margin-top:-40px;
    }

    .td_nearToFinish_cell {
        background-color:red;
    }


    .td_creationDate {
        width:11%;
    }
    .td_supplierName {
        text-align:left;
        width:22%;
    }
    .td_purchaseOrderNumber {
        text-align:right;
        width:13%;
    }
    .td_workType {
        width:18%;
    }
    .td_validityDate {
        width:11%;
    }
    .td_netValue {
        text-align:right;
        width:15%;
    }    



    .td_statusFilter {
        width:70%
    }
    .td_searchButton {
        padding-left:10px;
        width:30px;
        text-align:center;  
    }
    .td_searchResult {
        text-align:right;
    }

    .tb_search {
        text-align:right;
        width:150px;
    }

    .webGrid_table {
        width:90%;
        margin:auto;
    }


    

</style>

<script>


    $(document).ready(function () {

        if (document.URL.split("purchOrderNumber=").length > 1) {
            tb_searchByPurchOrderNum.value = document.URL.split("purchOrderNumber=")[1].split("&")[0]
        }

      
        if (tb_searchByPurchOrderNum.value != "") {

        }
        
        purchaseOrderValidityDate();

    });


    function purchaseOrderValidityDate() {
        
        var validityDateCellIndex = 4;

        var table_purchaseOrderList = document.getElementById("table_purchaseOrderList");
        
        for (var i = 1; i < table_purchaseOrderList.rows.length; i++) {

            var validityDate_str = table_purchaseOrderList.rows[i].cells[validityDateCellIndex].innerHTML;
            
            var day = validityDate_str.split('-')[0];
            var month = validityDate_str.split('-')[1];
            var year = validityDate_str.split('-')[2];

            var validityDate = new Date(month + "/" + day + "/" + year);
            var currentDate = new Date();

            var diff = Math.floor((currentDate - validityDate) / (1000 * 60 * 60 * 24));
            if (diff == -1) //Diff arroja la cantidad de días que quedan.
            {
                table_purchaseOrderList.rows[i].style.backgroundColor = "#F7E79A";
            }

        }

    }

    function img_search_onClick() {
        updateParameters();
    }

    function tb_searchByPurchOrderNum_onKeyPress(e) {

        if (e.keyCode == 13) {
            var a_img_magnifyIcon = document.getElementById("a_img_magnifyIcon");
            a_img_magnifyIcon.click();
        }
    }
    function tb_searchByPurchOrderNum_onKeyUp(e) {

        if (tb_searchByPurchOrderNum.value != "") {

        }
        else {

        }

        if (!IsNumber_or_point(e))
            tb_searchByPurchOrderNum.value = "";
    }


    function updateParameters(e) {

      
        var parameters;
        parameters = "purchOrderNumber=" + tb_searchByPurchOrderNum.value;

        window.location = '@Url.Action("PurchaseOrderList", "PurchaseOrder")' + "?" +  parameters;
    }

 
</script>


@section ButtonContainer
    
    <button type="button" class="btn_common btn_common_color1 btn_common_size2" onclick="window.location.href='@Url.Action("PurchaseOrderAdd", "PurchaseOrder")'">Agregar</button>
    
End section



    <div>


        
        <div id="div_searchContainer">
            <label id="lbl_searchByPurchOrderId">N° Orden de Compra</label>
            &nbsp
            <input id="tb_searchByPurchOrderNum" class="tb_search" maxlength="10" onkeypress="tb_searchByPurchOrderNum_onKeyPress(event)" onkeyup="tb_searchByPurchOrderNum_onKeyUp(event)" type="text" />

            <a id="a_img_magnifyIcon" title="Buscar" style="cursor:pointer;" onclick="img_search_onClick()" >
                <img class="img_magnifyIcon"  />
            </a>
        </div>

        <br />
        <br />

        <table id="table_filterContainer">
            <tr>
                <td class="td_statusFilter">                    
                   
                </td>
                <td class="td_searchResult">
                    <label id="lbl_searchResult">
                        @ViewBag.SearchResult
                    </label>
                </td>
            </tr>
        </table>
        

        <div id="div_content">
            @grid.GetHtml(
           htmlAttributes:=New With {.id = "table_purchaseOrderList"},
           tableStyle:="webGrid_table",
           headerStyle:="webGrid_header",
           footerStyle:="webGrid_footer",
           alternatingRowStyle:="webGrid_alternating_row",
           rowStyle:="",
           columns:=grid.Columns(
               grid.Column(
                   columnName:="CreationDate",
                   header:="Fecha Creación",
                   style:="td_creationDate"
                   ),
               grid.Column(
                   columnName:="SupplierName",
                   header:="Proveedor",
                   style:="td_supplierName"
                   ),
               grid.Column(
                   columnName:="PurchaseOrderNumber",
                   header:="N° Orden de Compra.",
                   style:="td_purchaseOrderNumber"
                   ),
               grid.Column(
                   columnName:="WorkType",
                   header:="Tipo de Trabajo",
                   style:="td_workType"
                   ),
               grid.Column(
                   columnName:="ValidityDate",
                   header:="Fecha Validez",
                   style:="td_validityDate"
                   ),
               grid.Column(
                   columnName:="NetValue",
                   header:="Monto Neto",
                   style:="td_netValue"
                   ),
               grid.Column(
                   header:="Doc.",
                   style:="webGrid_col_document",
                   format:=@@<a title="Documento" href="@Url.Action("GeneratePurchaseOrderPDF", "PurchaseOrder", New With {.purchOrderId = item.PurchaseOrder_Id})">
                                <img class="img_downloadDocumentIcon"/>
                            </a>
                   ),
               grid.Column(
                   header:="",
                   style:="webGrid_col_controls",
                   format:=@@<a title="Editar" href="@Url.Action("PurchaseOrderEdit", "PurchaseOrder", New With {.purchOrderId = item.PurchaseOrder_Id, .fromNode = "PurchaseOrderList"})">
                                <img class="img_editIcon" />
                            </a>
                   )
           ))


    </div>
        

</div>
