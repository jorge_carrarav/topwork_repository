﻿@ModelType TopWork.PurchaseOrderDataProvider.PurchaseOrder_AddEdit_Formatted
@Code
    ViewData("AppNode") = "Órdenes de Compra > Editar"
        
End Code



<style>

    #div_createdInfo {
        font-style:italic;
        color:darkblue;
        position:absolute;
        right:1%;
        top:1%;        
    }

    .purchaseOrderAddWidth {
        width:80%;
        margin:auto;
    }

    /* ------------------------------------------------------------- */

    #div_supplierInfo {
        border: 1px solid red;
        padding-top:10px;
        padding-bottom:10px;
    }
    #table_supplierInfo {
         margin:auto;
    }
    .tr_supplierInfo_rows {
        height:23px;
    }
    .td_supplierInfo_1rst {
        width:100px;
    }
    .td_supplierInfo_3rd {
        width:250px;
    }
    .td_supplierInfo_halfSeparator {
        width:50px;
    }

    /* ------------------------------------------------------------- */
    
    #div_purchaseOrderDetails {
        padding:10px;
    }
    #table_purchaseOrderDetails {
        padding-top:10px;
        margin:auto;
    }
    .tr_purchaseOrderDetails_rows {
        height:30px;
    }
    .td_purchaseOrderDetails_1rst {
        vertical-align:top;
        width:170px;
    }
    .td_purchaseOrderDetails_1rst_right {
        vertical-align:top;        
        width:150px;
    }
    .td_purchaseOrderDetails_2nd {
        width:1%;
        vertical-align:top;
    }
    .td_purchaseOrderDetails_3rd {
        vertical-align:top;
        text-align:right;
        width:260px;
    }
    .td_purchaseOrderDetails_halfSeparator {
        width:50px;
    }

    
    /* ------------------------------------------------------------- */
    
    #div_itemsDetails {
        border:1px solid black;
        background-color:lightgray;
        padding-top:10px;
        padding-bottom:10px;
    }
    #div_item {
        border:1px solid black;
        background-color:white;
        padding:10px;
        width:70%;
        margin:auto;
    }
    #table_item {
        margin:auto;
    }
    .tr_item_rows {
        height:30px;
    }
    .td_item_1rst {
        vertical-align:top;
        width:25%;
    }
    .td_item_3rd {
        padding-left:30px;
    }
   /* -------------------------------------------------------------ItemList */
   
    #div_itemList {
        border:1px solid black;
        background-color:white;
        padding:10px;
        width:95%;
        margin:auto;
    }
    #table_itemList {
        border:1px solid gray;
        margin:auto;
        width:100%;
    }
    .tr_item_rows {
        height:25px;
    }
    .tr_itemList_title {
        background-color: #DCE6F1;
        text-align:center;
        height:25px;
    }
    .td_itemList_title_itemNum {
        width:5%;
    }
    .td_itemList_title_description {
    }
    .td_itemList_title_quantity {
        width: 12%;
    }
    .td_itemList_title_unitValue {
        width:12%;
    }
    .td_itemList_title_netValue {
        width:12%;
    }
    .td_itemList_title_controls {
        width:7%;
    }

    
    .td_itemList_itemNum {
        text-align:center;
    }
    .td_itemList_description {
        text-align:left;
        padding-left:5px;
        padding-right:5px;
    }
    .td_itemList_quantity {
        text-align:center;
    }
    .td_itemList_unitValue {
        text-align:right;
        padding-right:5px;
    }
    .td_itemList_netValue {
        text-align:right;
        padding-right:5px;
    }
    .td_itemList_controls {
        text-align:center;
        padding-left: 4px;
        padding-right: 0px;
    }

    .td_itemList_item_leftBorder {        
        border-left:solid 1px gray;
    }
    .td_itemList_item_common {
        padding-bottom:10px;
        padding-left:5px;
        padding-right:5px;
    }


    .td_itemList_totals_netRow {
        border-top:1px solid gray;
    }
    .td_itemList_totalsStr {
        padding-right:5px;
        text-align:right;
    }
    .td_itemList_totals {
        padding-right:5px;
        text-align:right;
    }


    /* ------------------------------------------------------------- */

    #lbl_item_message {
        color:red;
    }

    #div_purchaseOrderAdd_buttonsContainer {
        text-align:right;
    }
    

    /* ------------------------------------------------------------- */
    
    .ui-autocomplete {
        background-color: #FCFDD2;
        text-align:right;
    }



</style>




<script>
    var _item_jsonList = [];
    
    $(document).ready(function () {
     
        
        var itemList_str = document.getElementById("tb_itemList").value;
        if (itemList_str != "") {
            _item_jsonList = JSON.parse(itemList_str);
            insertingItems_toTable();
            insertingTotals();
        }


        document.getElementById("a_addItem").style.display = "block";

        

        $("#div_message").dialog({
            modal: true,
            dialogClass: "noclose",
            title: "Estado de Orden de Compra",
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
        

        $("#tb_validityDate").datepicker({
            changeMonth: true,
            changeYear: true
        });


        function bindUnitsOfMeasureSelect() {

            var unitOfMeasure_list = "@(ViewBag.UnitsOfMeasure)".split(",");

            var ddl_unitOfMeasure = document.getElementById("ddl_unitOfMeasure");

            var option = document.createElement("option");
            option.text = "---";
            ddl_unitOfMeasure.add(option);

            for (var i = 0; i < unitOfMeasure_list.length; i++) {
                option = document.createElement("option");
                option.text = unitOfMeasure_list[i];
                ddl_unitOfMeasure.add(option);
            }

        }
        bindUnitsOfMeasureSelect();


    })



    function btn_save_onClick() {

        $("#div_loading").dialog("open");

        if (validatingFields()) {
            
            var postUrl = '@Url.Action("PurchaseOrderEdit", "PurchaseOrder")';

            var myForm = $("#form_purchaseOrderAdd");

            $.ajax({
                type: "POST",
                url: postUrl,
                data: myForm.serialize(),
                datatype: "html",
                success: function (purchaseOrderId) {

                    var purchaseOrderEditUrl = '@Url.Action("PurchaseOrderEdit", "PurchaseOrder")';

                    window.location = purchaseOrderEditUrl + "?purchOrderId=" + purchaseOrderId + "&result=SuccessEdition";

                }
            });

        }
        else {
            $("#div_loading").dialog("close");
        };
    }


    function validatingFields() {

        var tb_contactName = document.getElementById("tb_contactName");
        var ddl_workType = document.getElementById("ddl_workType");
        var ddl_methodOfPayment = document.getElementById("ddl_methodOfPayment");
        var tb_validityDate = document.getElementById("tb_validityDate");

        if (tb_contactName.value == "") {
            alert("Debe ingresar un nombre de contacto");
            return false;
        }
        else if (ddl_workType.selectedIndex == 0) {
            alert("Debe seleccionar un Tipo de Trabajo");
            return false;
        }
        else if (ddl_methodOfPayment.selectedIndex == 0) {
            alert("Debe seleccionar una Forma de Pago");
            return false;
        }
        else if (tb_validityDate.value == "") {
            alert("Debe seleccionar una Fecha de Validez");
            return false;
        }
        else if (_item_jsonList.length == 0) {
            alert("Debe ingresar al menos 1 item");
            return false;
        }

        return true
    }

    
    function tb_item_onKeyPress(e) {
        if (e.keyCode == 13) {
            
            var a_addItem = document.getElementById("a_addItem");
            var a_saveItem = document.getElementById("a_saveItem");
            
            if (a_addItem.style.display == "block") {
                a_addItem.click();
            }
            else if (a_saveItem.style.display == "block") {
                a_saveItem.click();
            }            
        }
    }
    


    //------------------------------------------------------------------Añadir item al listado de items
    function a_addItem_onClick() {

        if (checkItemInsertion()) {
            addingItem();
        }

    }

    //------------------------------------------------------------------Revisar que los campos hayan sido completados antes de ser insertados.
    function checkItemInsertion() {
        var tb_item_message = document.getElementById("tb_item_message")
        var lbl_item_message = document.getElementById("lbl_item_message");

        var tb_item_description = document.getElementById("tb_item_description");
        var tb_item_quantity = document.getElementById("tb_item_quantity");
        var ddl_item_unitOfMeasure = document.getElementById("ddl_unitOfMeasure");
        var tb_item_unitValue = document.getElementById("tb_item_unitValue");

        if (tb_item_description.value != "") {

            if (tb_item_quantity.value != "") {
                if (ddl_item_unitOfMeasure.selectedIndex > 0) {
                    if (tb_item_unitValue.value != "") {

                        tb_item_message.style.height = "0px";
                        lbl_item_message.style.visibility = "Hidden";

                        return true;
                    }
                    else {
                        tb_item_message.style.height = "20px";
                        lbl_item_message.style.visibility = "Visible";
                        lbl_item_message.innerHTML = "Debe ingresar un valor unitario"
                    }
                }
                else {
                    tb_item_message.style.height = "20px";
                    lbl_item_message.style.visibility = "Visible";
                    lbl_item_message.innerHTML = "Debe ingresar una unidad de medida"
                }
            }
            else {
                tb_item_message.style.height = "20px";
                lbl_item_message.style.visibility = "Visible";
                lbl_item_message.innerHTML = "Debe ingresar una cantidad"
            }
        }
        else {
            tb_item_message.style.height = "20px";
            lbl_item_message.style.visibility = "Visible";
            lbl_item_message.innerHTML = "Debe ingresar una descripción"
        }

        window.setTimeout(function () {
            tb_item_message.style.height = "0px";
            lbl_item_message.style.visibility = "Hidden";
        },3000);

        return false;
    }

    //------------------------------------------------------------------Añadir item al listado de items JSON
    function addingItem() {
                
        var description = document.getElementById("tb_item_description").value;
        var quantity = document.getElementById("tb_item_quantity").value.replace(".", "").replace(".", "").replace(".", "").replace(".", "");
        var unitOfMeasure = document.getElementById("ddl_unitOfMeasure").value;
        var unitValue = document.getElementById("tb_item_unitValue").value.replace(".", "").replace(".", "").replace(".", "").replace(".", "");
        var netValue = quantity * unitValue;
        

        _item_jsonList.push(
            {
                "Description": description,
                "Quantity": quantity,
                "UnitOfMeasure": unitOfMeasure,
                "UnitValue": unitValue
            }
        );

        loadTableItems();

        document.getElementById("tb_item_description").value = "";
        document.getElementById("tb_item_quantity").value = "";
        document.getElementById("ddl_unitOfMeasure").selectedIndex = 0;
        document.getElementById("tb_item_unitValue").value = "";
    }
    
    //----------------------------------------------------------------- Click en el ícono Editar de un Item
    function a_itemList_item_edit_onClick(id) {
        
        var index = id.split("#")[1];
        document.getElementById("hf_itemToEdit_rowNum").value = index;

       
        document.getElementById("tb_item_description").value = _item_jsonList[index].Description;
        document.getElementById("tb_item_quantity").value = thousandSeparator(_item_jsonList[index].Quantity);
        document.getElementById("ddl_unitOfMeasure").value = _item_jsonList[index].UnitOfMeasure;
        document.getElementById("tb_item_unitValue").value = thousandSeparator(_item_jsonList[index].UnitValue);

        var a_addItem = document.getElementById("a_addItem");
        var a_saveItem = document.getElementById("a_saveItem");
        var a_cancelItem = document.getElementById("a_cancelItem");

        a_addItem.style.display = "none";
        a_saveItem.style.display = "block";
        a_cancelItem.style.display = "block";

    }

    //----------------------------------------------------------------- Guardar item modificado
    function a_saveItem_onClick() {

        if (checkItemInsertion()) {

            var index = document.getElementById("hf_itemToEdit_rowNum").value;

            var description = document.getElementById("tb_item_description").value;
            var quantity = document.getElementById("tb_item_quantity").value.replace(".", "").replace(".", "").replace(".", "");
            var unitOfMeasure = document.getElementById("ddl_unitOfMeasure").value;
            var unitValue = document.getElementById("tb_item_unitValue").value.replace(".", "").replace(".", "").replace(".", "");
            var netValue = quantity * unitValue;


            _item_jsonList[index].Description = description;
            _item_jsonList[index].Quantity = quantity;
            _item_jsonList[index].UnitOfMeasure = unitOfMeasure;
            _item_jsonList[index].UnitValue = unitValue;

            
            loadTableItems();


            var a_addItem = document.getElementById("a_addItem");
            var a_saveItem = document.getElementById("a_saveItem");
            var a_cancelItem = document.getElementById("a_cancelItem");
            a_addItem.style.display = "block";
            a_saveItem.style.display = "none";
            a_cancelItem.style.display = "none";


            document.getElementById("tr_itemList_" + index).style.color = "Red";
            document.getElementById("tr_itemList_" + index).style.fontWeight = "Bold";
            window.setTimeout(function () {
                document.getElementById("tr_itemList_" + index).style.color = "Black";
                document.getElementById("tr_itemList_" + index).style.fontWeight = "100";
            },3000);
            

            document.getElementById("tb_item_description").value = "";
            document.getElementById("tb_item_quantity").value = "";
            document.getElementById("ddl_unitOfMeasure").selectedIndex = 0;
            document.getElementById("tb_item_unitValue").value = "";
        }
    }
    
    //----------------------------------------------------------------- Cancelar edición de item
    function a_cancelItem_onClick() {

        document.getElementById("hf_itemToEdit_rowNum").value = "";

        document.getElementById("tb_item_description").value = "";
        document.getElementById("tb_item_quantity").value = "";
        document.getElementById("ddl_unitOfMeasure").selectedIndex = 0;
        document.getElementById("tb_item_unitValue").value = "";

        var a_addItem = document.getElementById("a_addItem");
        var a_saveItem = document.getElementById("a_saveItem");
        var a_cancelItem = document.getElementById("a_cancelItem");
        a_addItem.style.display = "block";
        a_saveItem.style.display = "none";
        a_cancelItem.style.display = "none";

    }

    //----------------------------------------------------------------- Click en ícono eliminar de un Item de la tabla
    function a_itemList_item_delete_onClick(id) {
        $("#div_confirmItemDeletion").dialog({
            resizable: false,
            height: 180,
            dialogClass: "noclose",
            modal: true,
            title: "Mensaje",
            buttons: {
                "Si": function () {

                    var index = id.split("#")[1];

                    _item_jsonList.splice(index, 1);

                    loadTableItems();

                    $(this).dialog("close");
                },
                "Cancelar": function () {
                    $(this).dialog("close");
                }
            }
             });
    }
    
    //----------------------------------------------------------------- Limpia la tabla e inserta todos los datos de la lista Json y los Totales
    function loadTableItems() {

        loadingItemList_ToModel();

        deletingItems_fromTable();
        insertingItems_toTable();
        insertingTotals();

    }
    //----------------------------------------------------------------- Limpia la tabla e inserta todos los datos de la lista Json y los Totales
    function loadingItemList_ToModel() {
        document.getElementById("tb_itemList").value = JSON.stringify(_item_jsonList);        
    }
    //------------------------------------------------------------------Eliminar todos los items de la tabla
    function deletingItems_fromTable() {
        $(".rowToDelete").remove();
    }
    //------------------------------------------------------------------Añadir todo el JSON con items a la tabla
    function insertingItems_toTable() {

        var i = 0;
        
        for (i; i < _item_jsonList.length; i++) {

            var itemNumber = i + 1;

            var description = _item_jsonList[i].Description;
            var quantity = _item_jsonList[i].Quantity;
            var unitOfMeasure = _item_jsonList[i].UnitOfMeasure;
            var unitValue = _item_jsonList[i].UnitValue;
            var netValue = quantity * unitValue;

            $("#table_itemList")
               .append($("" +
                   "<tr id='tr_itemList_" + i + "' class='rowToDelete'>" +
                       "<td class='td_itemList_itemNum td_itemList_item_common'> " + itemNumber + " </td>" +
                       "<td id='td_itemList_description_" + i + "' class='td_itemList_description td_itemList_item_leftBorder td_itemList_item_common'> " + description + " </td>" +
                       "<td id='td_itemList_quantity_" + i + "' class='td_itemList_quantity td_itemList_item_leftBorder td_itemList_item_common'> " + thousandSeparator(quantity) + " " + unitOfMeasure + " </td>" +
                       "<td id='td_itemList_unitValue_" + i + "' class='td_itemList_unitValue td_itemList_item_leftBorder td_itemList_item_common'> $ " + thousandSeparator(unitValue) + " </td>" +
                       "<td id='td_itemList_netValue_" + i + "' class='td_itemList_netValue td_itemList_item_leftBorder td_itemList_item_common'> <span>$ " + thousandSeparator(netValue) + "</span> </td>" +
                       "<td class='td_itemList_controls td_itemList_item_leftBorder'> " +
                           "<a id='a_itemList_item_edit_#" + i + "' title='Modificar' style='cursor:pointer' onclick='a_itemList_item_edit_onClick(this.id)'>" +
                               "<img class='img_item_editIcon' />" +
                           "</a>" +
                           "&nbsp" +
                           "<a id='a_itemList_item_delete_#" + i + "' title='Quitar' style='cursor:pointer' onclick='a_itemList_item_delete_onClick(this.id)'>" +
                               "<img class='img_item_deleteIcon' />" +
                           "</a>" +
                       " </td>" +
                   "</tr>")
               );
            
        }

    }

    function insertingTotals() {
        var ivaPercentage = document.getElementById("tb_ivaPercentage").value;
                
        var finalNetValue = 0;
        var finalIVAValue = 0;
        var finalTotalValue = 0;

        var i = 0;
        for (i; i < _item_jsonList.length; i++) {
            var netValue = _item_jsonList[i].Quantity * _item_jsonList[i].UnitValue;
            finalNetValue = finalNetValue + netValue;
        }

        finalIVAValue = roundNumber(ivaPercentage * finalNetValue / 100);
        finalTotalValue = finalNetValue + finalIVAValue;

        document.getElementById("lbl_itemList_totals_finalNetValue").innerHTML = "$ " + thousandSeparator(finalNetValue);
        document.getElementById("lbl_itemList_totals_finalIVAValue").innerHTML = "$ " + thousandSeparator(finalIVAValue);
        document.getElementById("lbl_itemList_totals_finalTotalValue").innerHTML = "$ " + thousandSeparator(finalTotalValue);        
    }
    

    //----------------------------------------------------------------- Click en ícono eliminar de un Item de la tabla
    function btn_delete_onClick(id) {
        $("#div_confirmPurchaseOrderDeletion").dialog({
            resizable: false,
            height: 180,
            width: 465,
            dialogClass: "noclose",
            modal: true,
            title: "Mensaje",
            buttons: {
                "Si": function () {

                    var purchOrderId = document.getElementById("tb_purchaseOrderId").value;
                    var purchaseOrderDeleteUrl = '@Url.Action("PurchaseOrderDelete", "PurchaseOrder")';

                    window.location = purchaseOrderDeleteUrl + "?purchOrderId=" + purchOrderId;

                    $(this).dialog("close");
                },
                "Cancelar": function () {
                    $(this).dialog("close");
                }
            }
        });
    }


    function btn_goBack_onClick() {
        var goBackPoint = '@ViewBag.GoBackPoint';

        if (goBackPoint == "PurchaseOrderList")
            window.location.href = '@Url.Action("PurchaseOrderList", "PurchaseOrder")';
        else if (goBackPoint == "AdvancedSearch")
            window.location.href = '@Url.Action("PurchaseOrderList", "PurchaseOrder")';
        else if (goBackPoint == "WorkOrderEdit")
            window.location.href = '@Url.Action("WorkOrderEdit", "WorkOrder", New With {.workId = 1})';
        else if (goBackPoint == "WorkOrderList")
            window.location.href = '@Url.Action("WorkOrderList", "WorkOrder")';
        else
            window.location.href = '@Url.Action("PurchaseOrderList", "PurchaseOrder")';
    }



</script>

<div id="div_createdInfo">
    Última modificación: @Model.CreatedDate por @Model.CreatedBy
</div>

<div id="div_confirmPurchaseOrderDeletion" style="display:none;"  >   
    ¿Desea realmente eliminar la orden de compra? 
</div>
<div id="div_confirmItemDeletion" style="display:none;"  >   
    ¿Desea eliminar item? 
</div>

<form id="form_purchaseOrderAdd" method="post" action="PurchaseOrderAdd">

    <div id="div_supplierInfo" class="purchaseOrderAddWidth">
        <table id="table_supplierInfo">
            <tr class="tr_supplierInfo_rows">
                <td class="td_supplierInfo_1rst">Empresa</td>
                <td style="width:1%;">:</td>
                <td class="td_supplierInfo_3rd">@ViewBag.PurchaseOrderAdd_SupplierName</td>
                <td class="td_supplierInfo_halfSeparator"></td>
                <td class="td_supplierInfo_1rst">Ciudad</td>
                <td style="width:1%;">:</td>
                <td class="td_supplierInfo_3rd">@ViewBag.PurchaseOrderAdd_City</td>
            </tr>
        
            <tr class="tr_supplierInfo_rows">
                <td class="td_supplierInfo_1rst">Rut</td>
                <td style="width:1%;">:</td>
                <td class="td_supplierInfo_3rd">@ViewBag.PurchaseOrderAdd_Rut</td>            
                <td class="td_supplierInfo_halfSeparator"></td>
                <td class="td_supplierInfo_1rst">Email</td>
                <td style="width:1%;">:</td>
                <td class="td_supplierInfo_3rd">@ViewBag.PurchaseOrderAdd_Email</td>
            </tr>
            <tr class="tr_supplierInfo_rows">
                <td class="td_supplierInfo_1rst">Giro</td>
                <td style="width:1%;">:</td>
                <td class="td_supplierInfo_3rd">@ViewBag.PurchaseOrderAdd_ServiceType</td>
                <td class="td_supplierInfo_halfSeparator"></td>
                <td class="td_supplierInfo_1rst">Fono</td>
                <td style="width:1%;">:</td>
                <td class="td_supplierInfo_3rd">@ViewBag.PurchaseOrderAdd_Phone</td>
            </tr>
            <tr class="tr_supplierInfo_rows">
                <td class="td_supplierInfo_1rst">Dirección</td>
                <td style="width:1%;">:</td>
                <td class="td_supplierInfo_3rd">@ViewBag.PurchaseOrderAdd_Adderss</td>
                <td class="td_supplierInfo_halfSeparator"></td>
                <td class="td_supplierInfo_1rst"></td>
                <td style="width:1%;"></td>
                <td class="td_supplierInfo_3rd"></td>
            </tr>
        </table>
    </div>


    <div id="div_purchaseOrderDetails" class="purchaseOrderAddWidth">
    
        <table id="table_purchaseOrderDetails">
            <tr class="tr_purchaseOrderDetails_rows">
                <td class="td_purchaseOrderDetails_1rst">
                    N° Orden de Compra
                </td>
                <td class="td_purchaseOrderDetails_2nd">:</td>
                <td class="td_purchaseOrderDetails_3rd" style="text-align:center;">
                    @Html.ValueFor(Function(model) model.PurchaseOrderNumber)             
                </td>
                <td class="td_purchaseOrderDetails_halfSeparator"></td>
                <td class="td_purchaseOrderDetails_1rst_right" rowspan="5">
                    Observaciones
                </td>
                <td class="td_purchaseOrderDetails_2nd" rowspan="5">:</td>
                <td class="td_purchaseOrderDetails_3rd" rowspan="5">
                    @Html.TextAreaFor(Function(model) model.Observations, New With {.id = "tb_observations", .maxLength = "200", .style = "height:120px; width:80%;"})
                </td>
            </tr>
            <tr class="tr_purchaseOrderDetails_rows">
                <td class="td_purchaseOrderDetails_1rst">
                    Nombre de Contacto
                    <span class="span_requiredIcon">*</span>
                </td>
                <td class="td_purchaseOrderDetails_2nd">:</td>
                <td class="td_purchaseOrderDetails_3rd">
                    @Html.TextBoxFor(Function(model) model.ContactName, New With {.id = "tb_contactName", .maxLength = "100", .style = "width:180px;"})                
                </td>
                <td class="td_purchaseOrderDetails_halfSeparator"></td>
            </tr>
            <tr class="tr_purchaseOrderDetails_rows">
                <td class="td_purchaseOrderDetails_1rst">
                    Destino
                </td>
                <td class="td_purchaseOrderDetails_2nd">:</td>
                <td class="td_purchaseOrderDetails_3rd">
                    @Html.TextBoxFor(Function(model) model.WorkDestiny, New With {.id = "tb_workDestiny", .maxLength = "100", .style = "width:180px;"})                
                </td>
            </tr>
            <tr class="tr_purchaseOrderDetails_rows">
                <td class="td_purchaseOrderDetails_1rst">
                    Tipo de Trabajo
                    <span class="span_requiredIcon">*</span>
                </td>
                <td class="td_purchaseOrderDetails_2nd">:</td>
                <td class="td_purchaseOrderDetails_3rd">
                    @Html.DropDownListFor(Function(model) model.WorkType, New SelectList(ViewBag.PurchaseOrderAdd_WorkTypePO_list), New With {.id = "ddl_workType", .style = "width:90%;height:24px;"})
                </td>
                <td class="td_purchaseOrderDetails_halfSeparator"></td>
            </tr>
            <tr class="tr_purchaseOrderDetails_rows">
                <td class="td_purchaseOrderDetails_1rst">
                    Forma de Pago
                    <span class="span_requiredIcon">*</span>
                </td>
                <td class="td_purchaseOrderDetails_2nd">:</td>
                <td class="td_purchaseOrderDetails_3rd">
                    @Html.DropDownListFor(Function(model) model.MethodOfPayment, New SelectList(ViewBag.PurchaseOrderAdd_MethodOfPayment_list), New With {.id = "ddl_methodOfPayment", .style = "width:90%;height:24px;"})
                </td>
                <td class="td_purchaseOrderDetails_halfSeparator"></td>
            </tr>
            <tr class="tr_purchaseOrderDetails_rows">
                <td class="td_purchaseOrderDetails_1rst">
                    Fecha de Validez
                    <span class="span_requiredIcon">*</span>
                </td>
                <td class="td_purchaseOrderDetails_2nd">:</td>
                <td class="td_purchaseOrderDetails_3rd" style="text-align:center;">
                    @Html.TextBoxFor(Function(model) model.ValidityDate, New With {.id = "tb_validityDate", .maxLength = "10", .style = "width:120px; text-align:center;", .onchange = "setDateFormat(this.id)"})
                </td>
                <td class="td_purchaseOrderDetails_halfSeparator"></td>
            </tr>
        </table>
    </div>


    <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ITEMS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->


    <div id="div_itemsDetails" class="purchaseOrderAddWidth">

        <div id="div_item">
            <table id="table_item">
                <tr class="tr_item_rows">
                    <td class="td_item_1rst">
                        Descripción
                    </td>
                    <td style="width:1%; vertical-align:top;">:</td>
                    <td class="td_item_3rd">
                        <textarea id="tb_item_description" maxlength="3000" style="width:100%; height:45px;"></textarea>
                    </td>
                    <td></td>
                </tr>
                <tr class="tr_item_rows">
                    <td class="td_item_1rst">
                        Cantidad
                    </td>
                    <td style="width:1%; vertical-align:top;">:</td>
                    <td class="td_item_3rd">
                        <input id="tb_item_quantity" maxlength="10" style="width:100px; text-align:right;" onkeyup="checkIfNumber_thousandSeparator(this.id, event)" onkeypress="tb_item_onKeyPress(event)" />
                        <select id="ddl_unitOfMeasure"></select>
                    </td>
                    <td></td>
                </tr>
                <tr class="tr_item_rows">
                    <td class="td_item_1rst">
                        Valor unitario
                    </td>
                    <td style="width:1%; vertical-align:top;">:</td>
                    <td style="padding-left:16px; vertical-align:top;">
                        $ <input id="tb_item_unitValue" maxlength="10" style="width:120px; text-align:right;" onkeyup="checkIfNumber_thousandSeparator(this.id, event)" onkeypress="tb_item_onKeyPress(event)" />
                    </td>
                    <td style="width:100px; height:1%;">                    
                        <a id="a_addItem"  title="Agregar" style="cursor:pointer" onclick="a_addItem_onClick()">
                            <img class="img_plusIcon"  />
                        </a>
                        <div style="float:left;">
                            <a id="a_saveItem" title="Guardar" style="cursor:pointer; display:none;" onclick="a_saveItem_onClick()">
                                <img  class="img_okIcon"  />
                            </a>
                        </div>
                        <div style="float:left;">
                            <a id="a_cancelItem" title="Cancelar" style="cursor:pointer;  display:none;" onclick="a_cancelItem_onClick()">
                                <img  class="img_cancelIcon"  />
                            </a>
                        </div>
                    </td>
                </tr>
            </table>
            <div id="tb_item_message" style="text-align:center;">
                <label id="lbl_item_message"></label>
            </div>

        </div>
        <div style="height:3px;"></div>
        <div id="div_itemList">
            <table id="table_itemList" cellspacing="0">
                <tr class="tr_itemList_title">
                    <td class="td_itemList_title_itemNum">
                        N°
                    </td>
                    <td class="td_itemList_title_description">
                        Descripción
                    </td>
                    <td class="td_itemList_title_quantity">
                        Cantidad
                    </td>
                    <td class="td_itemList_title_unitValue">
                        Valor Unit.
                    </td>
                    <td class="td_itemList_title_netValue">
                        Valor Neto
                    </td>
                    <td class="td_itemList_title_controls">

                    </td>
                </tr>
                <tbody>
                    <!-- ------------------------------------ ITEMS A INSERTAR ------------------------------------->
                </tbody>
                <tr>
                    <td>
                        <br /><br />
                    </td>
                    <td class="td_itemList_item_leftBorder">
                    
                    </td>
                    <td class="td_itemList_item_leftBorder">
                    
                    </td>
                    <td class="td_itemList_item_leftBorder">
                    
                    </td>
                    <td class="td_itemList_item_leftBorder">
                    </td>
                    <td class="td_itemList_item_leftBorder">
                    
                    </td>                
                </tr>               
            
            
                       
                <tr class="">
                    <td class="td_itemList_totals_netRow">
                    
                    </td>
                    <td class="td_itemList_totals_netRow">
                    
                    </td>
                    <td class="td_itemList_totals_netRow">
                    
                    </td>
                    <td class="td_itemList_totals_netRow td_itemList_totalsStr">
                        Neto :
                    </td>
                    <td class="td_itemList_totals_netRow td_itemList_totals">
                        <label id="lbl_itemList_totals_finalNetValue"></label>
                    </td>
                    <td class="td_itemList_totals_netRow">

                    </td>                
                </tr>    
                <tr>
                    <td>
                    
                    </td>
                    <td>
                    
                    </td>
                    <td>
                    
                    </td>
                    <td class="td_itemList_totalsStr">
                        IVA :
                    </td>
                    <td class="td_itemList_totals">
                        <label id="lbl_itemList_totals_finalIVAValue"></label>
                    </td>
                    <td>

                    </td>                
                </tr>
                <tr>
                    <td>
                    
                    </td>
                    <td>
                    
                    </td>
                    <td>
                    
                    </td>
                    <td class="td_itemList_totalsStr">
                        Total :
                    </td>
                    <td class="td_itemList_totals">
                        <label id="lbl_itemList_totals_finalTotalValue"></label>
                    </td>
                    <td>

                    </td>                
                </tr>
            </table>
        </div>

    </div>
    <br />
    <div id="div_purchaseOrderAdd_buttonsContainer" style="width:100%; text-align:right; ">
        
        <button type="button" id="btn_goBack" onclick="btn_goBack_onClick()" class="btn_common btn_common_color2 btn_common_size1">Volver</button>
        &nbsp
        <button type="button" id="btn_delete" class="btn_common btn_common_color_delete btn_common_size2" onclick="btn_delete_onClick()">Eliminar</button>
        &nbsp
        <button type="button" id="btn_save" class="btn_common btn_common_color1 btn_common_size2" onclick="btn_save_onClick()">Guardar</button>
        &nbsp        
        <button type="button" id="btn_download" class="btn_common btn_common_color1 btn_common_size2" onclick="window.location = '@Url.Action("GeneratePurchaseOrderPDF", "PurchaseOrder", New With {.purchOrderId = Model.PurchaseOrder_Id})'">Descargar</button>
    </div>
    
    
    @Html.TextBoxFor(Function(model) model.Item_list, New With {.id = "tb_itemList", .style = "display:none;"})
    @Html.TextBoxFor(Function(model) model.SupplierRut, New With {.id = "tb_supplierRut_model", .style = "display:none;"})
    @Html.TextBoxFor(Function(model) model.PurchaseOrder_Id, New With {.id = "tb_purchaseOrderId", .style = "display:none;"})
    @Html.TextBoxFor(Function(model) model.IVAPercentage, New With {.id = "tb_ivaPercentage", .style = "display:none;"})

</form>




<div style="visibility:hidden;">
    <input id="hf_itemToEdit_rowNum" type="hidden" />
</div>
