﻿@ModelType TopWork.Supplier
@Code
    ViewData("AppNode") = "Registros > Proveedores > Agregar"
End Code


<style>
    #table_supplier {
        width:100%;
    }
    #div_formContainer {
        width:680px;
    }
    .td_items {
        height:30px;
    }
</style>

<script>



</script>


<form method="post" action="SupplierAdd">

    <div id="div_formContainer" style="overflow:hidden;">

        <table id="table_supplier">
            <tr>
                <td class="td_items">
                    Rut
                    <span class="span_requiredIcon">*</span>
                </td>
                <td>:</td>
                <td>
                    @Html.TextBoxFor(Function(model) model.RutNumber, New With {.id = "tb_rutNumber", .maxLength = "8", .style = "width: 120px; text-align:right; text-transform: lowercase;", .onKeyDown = "checkIfNumber(this.id, event)"})
                    &nbsp
                    -
                    &nbsp
                    @Html.TextBoxFor(Function(model) model.RutNValidator, New With {.maxLength = "1", .style = "width: 30px; text-align:right;"})
                </td>
            </tr>
            <tr>
                <td class="td_items">
                    Nombre o Razón Social
                    <span class="span_requiredIcon">*</span>
                </td>
                <td>:</td>
                <td>
                    @Html.TextBoxFor(Function(model) model.Name, New With {.maxLength = "200", .style = "width: 380px;"})
                </td>
            </tr>
            <tr>
                <td class="td_items">
                    Giro o Tipo de Servicio
                    <span class="span_requiredIcon">*</span>
                </td>
                <td>:</td>
                <td>
                    @Html.TextBoxFor(Function(model) model.ServiceType, New With {.maxLength = "200", .style = "width: 400px;"})
                </td>
            </tr>
            <tr>
                <td class="td_items">
                    Teléfono de Contacto
                </td>
                <td>:</td>
                <td>
                    @Html.TextBoxFor(Function(model) model.Phone, New With {.maxLength = "50", .style = "width: 250px;"})
                </td>
            </tr>
            <tr>
                <td class="td_items">
                    Email
                </td>
                <td>:</td>
                <td>
                    @Html.TextBoxFor(Function(model) model.Email, New With {.maxLength = "150", .type = "email", .style = "width: 280px;"})
                </td>
            </tr>
            <tr>
                <td class="td_items">
                    Dirección
                    <span class="span_requiredIcon">*</span>
                </td>
                <td>:</td>
                <td>
                     @Html.TextBoxFor(Function(model) model.Address, New With {.maxLength = "200", .style = "width: 320px;"})
                </td>
            </tr>
            <tr>
                <td class="td_items">
                    Ciudad
                    <span class="span_requiredIcon">*</span>
                </td>
                <td>:</td>
                <td>
                     @Html.TextBoxFor(Function(model) model.City, New With {.maxLength = "50", .style = "width: 270px;"})
                </td>
            </tr>
        </table>

        
        <br />
        <br />

        <div id="div_mainButtonsContainer">
            <button type="button" id="btn_goBack" onclick="window.location.href='@Url.Action("SupplierList", "Register")'" class="btn_common btn_common_color2 btn_common_size1">Volver</button>
            &nbsp
            <button type="submit" id="btn_create" class="btn_common btn_common_color1 btn_common_size2">Crear</button>
        </div>



    </div>
    


</form>
