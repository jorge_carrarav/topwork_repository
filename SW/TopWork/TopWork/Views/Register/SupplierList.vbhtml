﻿@ModelType IEnumerable(of TopWork.SupplierDataProvider.Supplier_List_Formatted)
@Code
    ViewData("AppNode") = "Registros > Proveedores"
    
    Dim grid = New WebGrid(Model, canPage:=True, rowsPerPage:=20)
    grid.Pager(WebGridPagerModes.All)
End Code

<style>
    #div_controlsContainer
    {
    }
    #table_searchControls {
        width:100%;
    }
    #lbl_searchResult {
        color:blue;
    }
    .td_rut {
        width:9%;
        text-align:right;
    }
    .td_name {
        width:20%;
        text-align:left;
    }
    .td_phone {
        width:12%;
    }
    .td_email {
        width:18%;
    }
    .td_address {
        text-align:left;
    }
    .td_city {
        width:12%;
    }

    .td_searchItem {
        width:260px;
        height:30px;
    }
    .td_searchButton {
        padding-left:10px;
        width:30px;
        text-align:center;   
    }
    .td_searchResult {
        text-align:right;
    }

    .tb_search {
        width:150px;
    }

    .webGrid_table {
        width:100%;
    }

    #lbl_searchByRut_autocomplete {
        position:absolute;
        width:250px;
    }
    #lbl_searchByRut_autocomplete .ui-autocomplete {
        background-color: #FCFDD2;
        text-align:right;
        position:absolute;
        width:250px;
    }

    #lbl_searchByName_autocomplete {
        position:absolute;
        width:250px;
    }
    #lbl_searchByName_autocomplete .ui-autocomplete {
        background-color: #FCFDD2;
    }
    

</style>
<script>


    $(document).ready(function () {
        var rad_searchByRut = document.getElementById("rad_searchByRut");
        var tb_searchByRut = document.getElementById("tb_searchByRut");
        var rad_searchByName = document.getElementById("rad_searchByName");
        var tb_searchByName = document.getElementById("tb_searchByName");



        if (document.URL.split("?rut=").length > 1) {
            rad_searchByRut.checked = true;
            rad_searchByName.checked = false;

            tb_searchByRut.disabled = false;
            tb_searchByName.disabled = true;

            tb_searchByRut.value = document.URL.split("?rut=")[1].split("&")[0]
        }
        else if (document.URL.split("?name=").length > 1) {
            rad_searchByRut.checked = false;
            rad_searchByName.checked = true;

            tb_searchByRut.disabled = true;
            tb_searchByName.disabled = false;
            tb_searchByName.value = document.URL.split("?name=")[1].split("&")[0].replace("+", " ")
        }
        else {
            rad_searchByRut.checked = true;
            rad_searchByName.checked = false;

            tb_searchByRut.disabled = false;
            tb_searchByName.disabled = true;
        }

    });

    $(function () {

        var rut_list_viewbag = [@Html.Raw(ViewBag.RutList)];
        
        $("#tb_searchByRut").autocomplete({
            source: rut_list_viewbag,
            appendTo: "#lbl_searchByRut_autocomplete"
        });
        
        var name_list_viewbag = [@Html.Raw(ViewBag.NameList)];
        
        $("#tb_searchByName").autocomplete({
            source: name_list_viewbag,
            appendTo: "#lbl_searchByName_autocomplete"
        });


    });


    function rad_searchByRut_onChange() {
        radioSearch_onChange();
    }
    function rad_searchByName_onChange() {
        radioSearch_onChange();
    }

    function radioSearch_onChange() {
        if (rad_searchByRut.checked == true) {
            tb_searchByRut.disabled = false;
            tb_searchByName.disabled = true;
            tb_searchByName.value = "";
        }
        else if (rad_searchByName.checked == true) {
            tb_searchByRut.disabled = true;
            tb_searchByRut.value = "";
            tb_searchByName.disabled = false;
        }
    }
    function lbl_searchByRut_onClick() {

        rad_searchByRut.checked = true;
        rad_searchByName.checked = false;

        tb_searchByRut.disabled = false;
        tb_searchByName.disabled = true;

        tb_searchByName.value = "";
    }
    function lbl_searchByName_onClick() {

        rad_searchByRut.checked = false;
        rad_searchByName.checked = true;

        tb_searchByRut.disabled = true;
        tb_searchByName.disabled = false;

        tb_searchByRut.value = "";
    }


    function img_search_onClick() {
        if (tb_searchByRut.value != "")
            window.location = "SupplierList?rut=" + tb_searchByRut.value;
        else if (tb_searchByName.value != "")
            window.location = "SupplierList?name=" + tb_searchByName.value
        else
            window.location = "SupplierList"
    }

    function tb_searchKeyPress(e) {
        if (e.keyCode == 13) {
            var a_search = document.getElementById("a_search");
            a_search.click();
        }
    }

</script>

@section ButtonContainer
    <button type="button" class="btn_common btn_common_color1 btn_common_size2" onclick="window.location.href='@Url.Action("SupplierAdd", "Register")'">Agregar</button>
End section


    <div id="div_controlsContainer">

        <table id="table_searchControls">
            <tr>
                <td class="td_searchItem">
                    <input id="rad_searchByRut" name="search" type="radio" onchange="rad_searchByRut_onChange()" />
                    <label id="lbl_searchByRut" onclick="lbl_searchByRut_onClick()">Rut</label>
                    <div id="lbl_searchByRut_autocomplete"></div>
                    &nbsp
                    <input id="tb_searchByRut" class="tb_search" maxlength="10" onkeypress="tb_searchKeyPress(event)" type="text" />
                </td>
                <td class="td_searchItem">
                     <input id="rad_searchByName" name="search" type="radio" onchange="rad_searchByName_onChange()"  />
                    <label id="lbl_searchByName" onclick="lbl_searchByName_onClick()">Nombre</label>
                    <div id="lbl_searchByName_autocomplete"></div>
                    &nbsp
                    <input id="tb_searchByName" class="tb_search" maxlength="150" onkeypress="tb_searchKeyPress(event)" type="text" />      
                </td>
                <td class="td_searchButton">                    
                    <a id="a_search" title="Buscar" style="cursor:pointer;" onclick="img_search_onClick()" >
                        <img class="img_magnifyIcon" />
                    </a>   
                </td>
                <td class="td_searchResult">
                    <label id="lbl_searchResult">
                        @ViewBag.SearchResult
                    </label>
                </td>
            </tr>
        </table>
     
        <div id="div_content">
            @grid.GetHtml(
           tableStyle:="webGrid_table",
           headerStyle:="webGrid_header",
           footerStyle:="webGrid_footer",
           alternatingRowStyle:="webGrid_alternating_row",
           rowStyle:="",
           columns:=grid.Columns(
               grid.Column(
                   columnName:="Rut",
                   header:="Rut",
                   style:="td_rut"
                   ),
               grid.Column(
                   columnName:="Name",
                    header:="Nombre",
                    style:="td_name"
                    ),
               grid.Column(
                   columnName:="Phone",
                   header:="Teléfono",
                   style:="td_phone"
                   ),
               grid.Column(
                   columnName:="Email",
                   header:="Email",
                   style:="td_email"
                   ),
               grid.Column(
                   columnName:="Address",
                   header:="Dirección",
                   style:="td_address"
                   ),
               grid.Column(
                   columnName:="City",
                   header:="Ciudad",
                   style:="td_city"
                   ),
               grid.Column(
                   header:="",
                   style:="webGrid_col_controls",
                   format:=@@<div> 
                                <a title="Editar" href="@Url.Action("SupplierEdit", "Register", New With {.supplierId = item.Supplier_Id})">
                                    <img class="img_editIcon" />
                                </a>
                             </div>
                   )
           ))
           
        </div>

    </div>