﻿@Code
    ViewData("AppNode") = "Trabajos > Búsqueda Avanzada"
    
    Dim grid = New WebGrid(Model, canPage:=True, rowsPerPage:=15)
    grid.Pager(WebGridPagerModes.All)
End Code


<style>
    #div_controlsContainer
    {
    }
    #table_searchControls {
        width:100%;
    }
    #lbl_searchResult {
        color:blue;
    }

    #div_searchContainer {
        text-align:center;
        margin-top:-40px;
    }


    .td_creationDate {
        width:10%;
    }
    .td_clientName {
        text-align:left;
        width:18%;
    }
    .tb_workWorkDestiny {
        text-align:right;
        width:10%;
    }
    .td_workType {
        width:15%;
    }
    .td_finishDate {
        width:10%;
    }
    .td_woNumber {
        text-align:right;
        width:6%;
    }
    .tb_quotationNumberVersion {
        text-align:right;
        width:6%;
    }
    .td_status {
        width:15%;
    }
    
    .td_statusFilter {
        width:70%
    }
    
    .td_searchResult {
        text-align:right;
    }

    .tb_search {
        text-align:right;
        width:150px;
    }

    .webGrid_table {
        width:100%;
    }
    
</style>


<script>

    $(document).ready(function () {


        if (document.URL.split("woNum=").length > 1) {
            tb_searchByWONum.value = document.URL.split("woNum=")[1].split("&")[0]
        }

        

    checkWorkFinishDate();

    });

    function checkWorkFinishDate() {

        var finishDateCellIndex = 5;

        var table_workList = document.getElementById("table_workList");

        for (var i = 1; i < table_workList.rows.length; i++) {

            var finishDate_str = table_workList.rows[i].cells[finishDateCellIndex].innerHTML;

            var day = finishDate_str.split('-')[0];
            var month = finishDate_str.split('-')[1];
            var year = finishDate_str.split('-')[2];

            var finishDate = new Date(month + "/" + day + "/" + year);
            var currentDate = new Date();

            var diff = Math.floor((currentDate - finishDate) / (1000 * 60 * 60 * 24));
            if (diff == -1) //Diff arroja la cantidad de días que quedan.
            {
                table_workList.rows[i].style.backgroundColor = "#F7E79A";
            }

        }

    }




    function img_search_onClick() {
        updateParameters();
    }

    function tb_searchByWONum_onKeyPress(e) {

        if (e.keyCode == 13) {
            var a_img_magnifyIcon = document.getElementById("a_img_magnifyIcon");
            a_img_magnifyIcon.click();
        }
    }
    function tb_searchByWONum_onKeyDown(e) {
        if (!IsNumber(e))
            tb_searchByWONum.value = "";
    }
    
    function tb_searchByWONum_onKeyUp() {

        if (tb_searchByWONum.value != "") {

            cb_ws_b.disabled = true;
            cb_ws_c.disabled = true;
            cb_ws_d.disabled = true;
            cb_ws_e.disabled = true;
        }
        else {

            cb_ws_b.disabled = false;
            cb_ws_c.disabled = false;
            cb_ws_d.disabled = false;
            cb_ws_e.disabled = false;
        }

    }


    function updateParameters(e) {

        var ws_b = 0;
        var ws_c = 0;
        var ws_d = 0;
        var ws_e = 0;

        
        if (cb_ws_b.checked)
            ws_b = 1;
        if (cb_ws_c.checked)
            ws_c = 1;
        if (cb_ws_d.checked)
            ws_d = 1;
        if (cb_ws_e.checked)
            ws_e = 1;

        var parameters;
        parameters = "woNum=" + tb_searchByWONum.value;
        parameters += "&ws_b=" + ws_b;
        parameters += "&ws_c=" + ws_c;
        parameters += "&ws_d=" + ws_d;
        parameters += "&ws_e=" + ws_e;


        window.location = '@Url.Action("WorkOrderList", "WorkOrder")' + "?" + parameters;
    }



    function lbl_ws_b_onClick() {
        if (tb_searchByWONum.value == "") {
            if (cb_ws_b.checked)
                cb_ws_b.checked = false;
            else
                cb_ws_b.checked = true;

            updateParameters();
        }
    }
    function lbl_ws_c_onClick() {
        if (tb_searchByWONum.value == "") {
            if (cb_ws_c.checked)
                cb_ws_c.checked = false;
            else
                cb_ws_c.checked = true;

            updateParameters();
        }
    }
    function lbl_ws_d_onClick() {
        if (tb_searchByWONum.value == "") {
            if (cb_ws_d.checked)
                cb_ws_d.checked = false;
            else
                cb_ws_d.checked = true;

            updateParameters();
        }
    }
    function lbl_ws_e_onClick() {
        if (tb_searchByWONum.value == "") {
            if (cb_ws_e.checked)
                cb_ws_e.checked = false;
            else
                cb_ws_e.checked = true;

            updateParameters();
        }
    }

    
</script>



@section ButtonContainer
    
    <button type="button" class="btn_common btn_common_color2 btn_common_size3" onclick="window.location.href='@Url.Action("WorkAdvancedSearch", "WorkOrder")'">Volver</button>
    
End section



<div>

    <br />
        
    <div style="text-align:center;">
        <label id="lbl_searchResult">
            @ViewBag.SearchResult
        </label>
    </div>

    <br />
     

    <div id="div_content">
        @grid.GetHtml(
        htmlAttributes:=New With {.id = "table_workList"},
        tableStyle:="webGrid_table",
        headerStyle:="webGrid_header",
        footerStyle:="webGrid_footer",
        alternatingRowStyle:="webGrid_alternating_row",
        rowStyle:="",
        columns:=grid.Columns(
            grid.Column(
                columnName:="CreationDate",
                header:="Fecha Creación",
                style:="td_creationDate"
                ),
            grid.Column(
                columnName:="ClientName",
                header:="Cliente",
                style:="td_clientName"
                ),
            grid.Column(
                columnName:="WorkOrderNumber",
                header:="N° O.T.",
                style:="td_woNumber"
                ),
            grid.Column(
                columnName:="WorkDestiny",
                header:="Destino",
                style:="tb_workWorkDestiny"
                ),
            grid.Column(
                columnName:="WorkTypeQ",
                header:="Tipo de Trabajo",
                style:="td_workType"
                ),
            grid.Column(
                columnName:="FinishDate",
                header:="Fecha Término",
                style:="td_finishDate"
                ),
            grid.Column(
                columnName:="QuotationNumber",
                header:="N° Cot",
                style:="tb_quotationNumberVersion",
                format:=@@<a title="Cotización" href="@Url.Action("QuotationReview", "Quotation", New With {.quotId = item.Quotation_Id, .fromNode = "WorkOrderList"})">@item.QuotationNumber</a>
                ),
            grid.Column(
                columnName:="Status",
                header:="Estado",
                style:="td_status"
                ),
            grid.Column(
                header:="Doc.",
                style:="webGrid_col_document",
                format:=@@<a title="Documento" href="@Url.Action("GenerateWorkOrderPDF", "WorkOrder", New With {.workId = item.WorkOrder_Id})">
                            <img class="img_downloadDocumentIcon"/>
                        </a>
                ),
            grid.Column(
                header:="",
                style:="webGrid_col_controls",
                format:=@@<a title="Editar" href="@Url.Action("WorkOrderEdit", "WorkOrder", New With {.workId = item.WorkOrder_Id})">
                            <img class="img_editIcon" />
                        </a>
                )
        ))


    </div>
        

</div>

