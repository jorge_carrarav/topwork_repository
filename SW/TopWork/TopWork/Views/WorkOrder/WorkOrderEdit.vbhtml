﻿@ModelType TopWork.WorkOrderDataProvider.WorkOrder_AddEdit_Formatted
@Code
    ViewData("AppNode") = "Trabajos > Editar"
End Code


@Styles.Render("~/Styles/WorkPage")

<script>

    var _item_jsonList = [];

    var _activeTab = '@ViewBag.activeTab';


    $(document).ready(function () {


        /*-----------------------------Inserting items to table----------*/
        var itemList_str = document.getElementById("tb_itemList").value;
        if (itemList_str != "") {
            _item_jsonList = JSON.parse(itemList_str);
            insertingItems_toTable();
        }

        /*---------------------------------------------------------------*/
        //esto activa el tab correspondiente tras el refresh, se ubica aquí para ejecutarse después de llenar el jsonList
        if (_activeTab == "" || _activeTab == "1") {
            document.getElementById("tab_a_items").click();
        }

        /*---------------------------------------------------------------*/

        $(function () {
            $("#div_tabs").tabs();
        });

        $("#tb_finishDate").datepicker({
            changeMonth: true,
            changeYear: true
        });


        $("#div_message").dialog({
            resizable: false,
            height: 200,
            width: 400,
            dialogClass: "noclose",
            autoOpen: false,
            modal: true,
            title: "Mensaje",
            buttons: {
                "Cerrar": function () {
                    $(this).dialog("close");
                }
            }
        });

    });


    function tab_items_onClick() {
        /*---------Ajustando tamaño de Tab de ITEMS al tamaño de lista de items.*/
        var tabHeight = document.getElementById("table_items_itemList").clientHeight + 120;

        if (tabHeight < 300)
            tabHeight = 300;

        document.getElementById("div_tabs").style.height = tabHeight.toString() + "px";

        document.getElementById("hf_activeTab").value = "1";

    }
    function tab_materials_onClick() {
        /*---------Ajustando tamaño de Tab de MATERIALS al tamaño de lista de materiales.*/
        var tabHeight = document.getElementById("table_woMaterials_materialList").clientHeight + 340;
        if (tabHeight < 300)
            tabHeight = 300;
        document.getElementById("div_tabs").style.height = tabHeight.toString() + "px";

        document.getElementById("hf_activeTab").value = "2";
    }
    function tab_wayBills_onClick() {
        /*---------Ajustando tamaño de Tab de GUIAS DE DESPACHO al tamaño de lista de guías de despacho.*/
        var tabHeight = document.getElementById("div_wayBillItems_wayBillList").clientHeight + 110;

        if (tabHeight < 300)
            tabHeight = 300;
        document.getElementById("div_tabs").style.height = tabHeight.toString() + "px";

        document.getElementById("hf_activeTab").value = "3";
    }

    function tab_invoices_onClick() {
        /*---------Ajustando tamaño de Tab de GUIAS DE DESPACHO al tamaño de lista de guías de despacho.*/
        var tabHeight = document.getElementById("div_invoiceItems_invoiceList").clientHeight + 110;

        if (tabHeight < 300)
            tabHeight = 300;
        document.getElementById("div_tabs").style.height = tabHeight.toString() + "px";

        document.getElementById("hf_activeTab").value = "4";
    }



    function tb_quotationNumber_onKeyPress(e) {
        if (e.keyCode == 13) {
            var a_clientNameSelection = document.getElementById("a_clientNameSelection");
            a_quotationNumberSelection.click();
        }
    }

    function tb_quotationNumber_onKeyUp(e) {
        var tb_quotationNumber = document.getElementById("tb_quotationNumber");

        if (!IsNumber_or_point(e))
            tb_quotationNumber.value = "";
    }


    function a_quotationNumberSelection_onClick() {
        var quotationNumber = document.getElementById("tb_quotationNumber").value;
        window.location.href = '@Url.Action("WorkOrderAdd", "WorkOrder")?quotNum=' + quotationNumber;
}


function tb_purchaseOrderNumber_onKeyUp(e) {

    var tb_purchaseOrderNumber = document.getElementById("tb_purchaseOrderNumber");

    if (!IsNumber(e))
        tb_purchaseOrderNumber.value = "";
}

//----Añadir todo el JSON con items a la tabla
function insertingItems_toTable() {

    var i = 0;

    for (i; i < _item_jsonList.length; i++) {

        var itemNumber = i + 1;

        var description = _item_jsonList[i].Description;
        var quantity = _item_jsonList[i].Quantity;


        $("#table_items_itemList")
           .append($("" +
               "<tr id='tr_woDetails_itemList_" + i + "'>" +
                   "<td class='td_woDetails_itemList_itemNum td_woDetails_itemList_item_common'> " + itemNumber + " </td>" +
                   "<td id='td_woDetails_itemList_description_" + i + "' class='td_woDetails_itemList_description td_woDetails_itemList_item_leftBorder td_woDetails_itemList_item_common'> " + description + " </td>" +
                   "<td id='td_woDetails_itemList_quantity_" + i + "' class='td_woDetails_itemList_quantity td_woDetails_itemList_item_leftBorder td_woDetails_itemList_item_common'> " + thousandSeparator(quantity) + " </td>" +
               "</tr>")
           );

    }

}

//Guardar OT
function btn_work_save_onClick() {

    $("#div_loading").dialog("open");


    if (workOrder_ValidatingFields()) {

        var workAdd_url = '@Url.Action("WorkOrderEdit", "WorkOrder")';

        var myForm = $("#form_workEdit");


        $.ajax({
            type: "POST",
            url: workAdd_url,
            data: myForm.serialize(),
            datatype: "html",
            success: function (workId) {

                var WorkOrderEdit_url = '@Url.Action("WorkOrderEdit", "WorkOrder")';
                var _activeTab = document.getElementById("hf_activeTab").value;
                window.location = WorkOrderEdit_url + "?workId=" + workId + "&result=SuccessEdition" + "&activeTab=" + _activeTab;

            }
        });
    }
    else {
        $("#div_loading").dialog("close");
    }

}


    //--- Ventana de confirmación para eliminar OT
    function btn_workOrder_delete_onClick() {
        $("#div_workOrder_confirmDeletion").dialog({
            resizable: false,
            height: 200,
            width: 400,
            dialogClass: "noclose",
            modal: true,
            title: "Mensaje",
            buttons: {
                "Si": function () {

                    var workId = document.getElementById("tb_workId").value;
                    var workDeleteUrl = '@Url.Action("WorkDelete", "WorkOrder")';

                window.location = workDeleteUrl + "?workId=" + workId;

                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
}

//Valida los los campos de la OT
function workOrder_ValidatingFields() {

    var tb_quotationNumber = document.getElementById("tb_quotationNumber");
    var tb_finishDate = document.getElementById("tb_finishDate");

    if (tb_finishDate.value == "") {
        document.getElementById("lbl_message").innerHTML = "Debe seleccionar una Fecha de Término";
        $("#div_message").dialog("open");
        return false;
    }
    else if (_invoices_jsonList.length > 0) {
        var purchaseOrderNumber = document.getElementById("tb_purchaseOrderNumber").value;
        if (purchaseOrderNumber == "") {
            document.getElementById("lbl_message").innerHTML = "Este trabajo posee facturas. Debe ingresar un N° Orden de Compra.";
            $("#div_message").dialog("open");
            return false;
        }
    }


    return true
}

function btn_goBack_onClick() {

    var goBackPoint = '@ViewBag.GoBackPoint';

    if (goBackPoint == "QuotationEdit")
        window.location.href = '@Url.Action("QuotationEdit", "Quotation", New With {.quotId = ViewBag.QuotationId})';
    else if (goBackPoint == "QuotationList")
        window.location.href = '@Url.Action("QuotationList", "Quotation", New With {.wip = 1})';
    else
        window.location.href = '@Url.Action("WorkOrderList", "WorkOrder", New With {.wip = 1})';
}




    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    MATERIALS   >>>>>>>>>>>>>>>>>>>>>>>>
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


    var _materials_jsonList = [];

    $(document).ready(function () {

        /*-----------------------------Inserting materials to table----------*/
        var materialList_str = document.getElementById("tb_materialList").value;

        if (materialList_str != "") {
            _materials_jsonList = JSON.parse(materialList_str);
            insertingMaterials_toTable();
        }
        materialTotalCalculator();
        /*---------------------------------------------------------------*/
        //esto activa el tab correspondiente tras el refresh, se ubica aquí para ejecutarse después de llenar el jsonList
        if (_activeTab == "2") {
            document.getElementById("tab_a_materials").click();
        }
        /*---------------------------------------------------------------*/

        document.getElementById("a_addMaterial").style.display = "block";


        bindUnitsOfMeasureSelect();
    });


    function bindUnitsOfMeasureSelect() {

        var unitOfMeasures_list = "@(ViewBag.UnitsOfMeasure)".split(",");

        var ddl_unitOfMeasure = document.getElementById("ddl_unitOfMeasure");

        var option = document.createElement("option");
        option.text = "---";
        ddl_unitOfMeasure.add(option);

        for (var i = 0; i < unitOfMeasures_list.length; i++) {

            option = document.createElement("option");
            option.text = unitOfMeasures_list[i];
            ddl_unitOfMeasure.add(option);

        }

    }

//Checkea si se presionó ENTER, si es así, se clickea el botón que corresponda
function tb_material_quantity_onKeyUp(id, e) {

    checkIfNumber_thousandSeparator(id, e);

    var lbl_material_netValue = document.getElementById("lbl_material_netValue");
    var unitValue = removePoints(document.getElementById("tb_material_unitValue").value);
    var quantity = removePoints(document.getElementById("tb_material_quantity").value);

    if (quantity != "" && unitValue != "") {
        lbl_material_netValue.innerHTML = thousandSeparator(parseInt(quantity) * parseInt(unitValue));
    }
    else {
        lbl_material_netValue.innerHTML = "";
    }

    if (e.keyCode == 13) {

        var a_addMaterial = document.getElementById("a_addMaterial");
        var a_saveMaterial = document.getElementById("a_saveMaterial");


        if (a_addMaterial.style.display == "block") {

            a_addMaterial.click();
        }
        else if (a_saveMaterial.style.display == "block") {
            a_saveMaterial.click();
        }
    }
}

//Checkea si se presionó ENTER, si es así, se clickea el botón que corresponda
function tb_material_unitValue_onKeyUp(id, e) {

    checkIfNumber_thousandSeparator(id, e)

    var lbl_material_netValue = document.getElementById("lbl_material_netValue");
    var unitValue = removePoints(document.getElementById("tb_material_unitValue").value);
    var quantity = removePoints(document.getElementById("tb_material_quantity").value);

    if (quantity != "" && unitValue != "") {
        lbl_material_netValue.innerHTML = thousandSeparator(parseInt(quantity) * parseInt(unitValue));
    }
    else {
        lbl_material_netValue.innerHTML = "";
    }

    if (e.keyCode == 13) {

        var a_addMaterial = document.getElementById("a_addMaterial");
        var a_saveMaterial = document.getElementById("a_saveMaterial");


        if (a_addMaterial.style.display == "block") {

            a_addMaterial.click();
        }
        else if (a_saveMaterial.style.display == "block") {
            a_saveMaterial.click();
        }
    }
}


//----Añadir item al listado de items
function a_addMaterial_onClick() {

    if (checkMaterialInsertion()) {
        addingMaterial();
        tab_materials_onClick();
    }

}

//----Añadir itmaterial al listado de materiales JSON
function addingMaterial() {

    var description = document.getElementById("tb_material_description").value;
    var quantity = removePoints(document.getElementById("tb_material_quantity").value);
    var unitOfMeasure = document.getElementById("ddl_unitOfMeasure").value;
    var netValue = removePoints(document.getElementById("lbl_material_netValue").innerHTML);
    var ivaPercentage = document.getElementById("tb_ivaPercentage").value;
    

    _materials_jsonList.push(
        {
            "Description": description,
            "Quantity": quantity,
            "UnitOfMeasure": unitOfMeasure,
            "NetValue": netValue,
            "IVAPercentage": ivaPercentage
        }
    );
    
    loadMaterialsTable();

    document.getElementById("tb_material_description").value = "";
    document.getElementById("tb_material_quantity").value = "";
    document.getElementById("ddl_unitOfMeasure").selectedIndex = 0;
    document.getElementById("tb_material_unitValue").value = "";
    document.getElementById("lbl_material_netValue").innerHTML = "";
}

//--- Click en el ícono Editar de un Item
function a_materialList_material_edit_onClick(id) {

    var index = id.split("#")[1];
    document.getElementById("hf_materialToEdit_rowNum").value = index;


    document.getElementById("tb_material_description").value = _materials_jsonList[index].Description;
    document.getElementById("tb_material_quantity").value = thousandSeparator(_materials_jsonList[index].Quantity);
    document.getElementById("ddl_unitOfMeasure").value = _materials_jsonList[index].UnitOfMeasure;


    document.getElementById("tb_material_unitValue").value = thousandSeparator(parseInt(_materials_jsonList[index].NetValue) / parseInt(_materials_jsonList[index].Quantity));
    document.getElementById("lbl_material_netValue").innerHTML = thousandSeparator(_materials_jsonList[index].NetValue);


    var a_addMaterial = document.getElementById("a_addMaterial");
    var a_saveMaterial = document.getElementById("a_saveMaterial");
    var a_cancelMaterial = document.getElementById("a_cancelMaterial");

    a_addMaterial.style.display = "none";
    a_saveMaterial.style.display = "block";
    a_cancelMaterial.style.display = "block";

}

// Guardar item modificado
function a_saveMaterial_onClick() {

    if (checkMaterialInsertion()) {

        var index = document.getElementById("hf_materialToEdit_rowNum").value;

        var description = document.getElementById("tb_material_description").value;
        var quantity = removePoints(document.getElementById("tb_material_quantity").value);
        var unitOfMeasure = document.getElementById("ddl_unitOfMeasure").value;
        var netValue = removePoints(document.getElementById("lbl_material_netValue").innerHTML);
        var ivaPercentage = removePoints(document.getElementById("tb_ivaPercentage").value);


        _materials_jsonList[index].Description = description;
        _materials_jsonList[index].Quantity = quantity;
        _materials_jsonList[index].UnitOfMeasure = unitOfMeasure;
        _materials_jsonList[index].NetValue = netValue;
        _materials_jsonList[index].IVAPercentage = ivaPercentage;

        loadMaterialsTable();


        var a_addMaterial = document.getElementById("a_addMaterial");
        var a_saveMaterial = document.getElementById("a_saveMaterial");
        var a_cancelMaterial = document.getElementById("a_cancelMaterial");
        a_addMaterial.style.display = "block";
        a_saveMaterial.style.display = "none";
        a_cancelMaterial.style.display = "none";


        document.getElementById("tr_materialList_" + index).style.color = "Red";
        document.getElementById("tr_materialList_" + index).style.fontWeight = "Bold";
        window.setTimeout(function () {
            document.getElementById("tr_materialList_" + index).style.color = "Black";
            document.getElementById("tr_materialList_" + index).style.fontWeight = "100";
        }, 3000);


        document.getElementById("tb_material_description").value = "";
        document.getElementById("tb_material_quantity").value = "";
        document.getElementById("ddl_unitOfMeasure").selectedIndex = 0;
        document.getElementById("tb_material_unitValue").value = "";
        document.getElementById("lbl_material_netValue").innerHTML = "";
    }
}

// Cancelar edición de item
function a_cancelMaterial_onClick() {

    document.getElementById("hf_materialToEdit_rowNum").value = "";

    document.getElementById("tb_material_description").value = "";
    document.getElementById("tb_material_quantity").value = "";
    document.getElementById("ddl_unitOfMeasure").selectedIndex = 0;
    document.getElementById("tb_material_unitValue").value = "";
    document.getElementById("lbl_material_netValue").innerHTML = "";

    var a_addMaterial = document.getElementById("a_addMaterial");
    var a_saveMaterial = document.getElementById("a_saveMaterial");
    var a_cancelMaterial = document.getElementById("a_cancelMaterial");
    a_addMaterial.style.display = "block";
    a_saveMaterial.style.display = "none";
    a_cancelMaterial.style.display = "none";

}

// Click en ícono eliminar de un Item de la tabla
function a_materialList_material_delete_onClick(id) {
    $("#div_material_confirmDeletion").dialog({
        resizable: false,
        height: 200,
        width: 400,
        dialogClass: "noclose",
        modal: true,
        title: "Mensaje",
        buttons: {
            "Si": function () {

                var index = id.split("#")[1];

                _materials_jsonList.splice(index, 1);

                loadMaterialsTable();

                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
}

// Limpia la tabla e inserta todos los datos de la lista Json y los Totales
function loadMaterialsTable() {

    loadingMaterialsList_ToModel();

    deletingMaterials_fromTable();
    insertingMaterials_toTable();

    materialTotalCalculator();
}
//Calculador de montos totales para los materiales
function materialTotalCalculator() {

    var ivaPercentage = document.getElementById("tb_ivaPercentage").value;
    var netValueTotal = 0;

    for (var i = 0; i < _materials_jsonList.length; i++) {
        netValueTotal = netValueTotal + parseInt(_materials_jsonList[i].NetValue);
    }


    var ivaValueTotal = 0;
    var totalValueTotal = 0;

    ivaValueTotal = roundNumber(ivaPercentage * netValueTotal / 100);
    totalValueTotal = netValueTotal + ivaValueTotal;

    document.getElementById("lbl_material_finalNetValue").innerHTML = "$ " + thousandSeparator(netValueTotal);
    document.getElementById("lbl_material_finalIVAValue").innerHTML = "$ " + thousandSeparator(ivaValueTotal);
    document.getElementById("lbl_material_finalTotalValue").innerHTML = "$ " + thousandSeparator(totalValueTotal);

}
// Limpia la tabla e inserta todos los datos de la lista Json y los Totales
function loadingMaterialsList_ToModel() {
    document.getElementById("tb_materialList").value = JSON.stringify(_materials_jsonList);
}
//Eliminar todos los items de la tabla
function deletingMaterials_fromTable() {
    $(".materials_rowToDelete").remove();
}
//Añadir todo el JSON con items a la tabla
function insertingMaterials_toTable() {

    for (var i = 0; i < _materials_jsonList.length; i++) {

        var materialNumber = i + 1;

        var description = _materials_jsonList[i].Description;
        var quantity = _materials_jsonList[i].Quantity;
        var unitOfMeasure = " " + _materials_jsonList[i].UnitOfMeasure;
        var netValue = _materials_jsonList[i].NetValue;

        $("#table_woMaterials_materialList")
           .append($("" +
               "<tr id='tr_materialList_" + i + "' class='materials_rowToDelete'>" +
                   "<td class='td_materialList_materialNum td_materialList_material_common'> " + materialNumber + " </td>" +
                   "<td id='td_materialList_description" + i + "' class='td_materialList_description td_materialList_material_leftBorder td_materialList_material_common'> " + description + " </td>" +
                   "<td id='td_materialList_quantity" + i + "' class='td_materialList_quantity td_materialList_material_leftBorder td_materialList_material_common'> " + thousandSeparator(quantity) + unitOfMeasure + "</td>" +
                   "<td id='td_materialList_netValue" + i + "' class='td_materialList_netValue td_materialList_material_leftBorder td_materialList_material_common'> $" + thousandSeparator(netValue) + "</td>" +
                   "<td class='td_materialList_controls td_materialList_material_leftBorder'> " +
                       "<a id='a_materialList_material_edit_#" + i + "' title='Modificar' style='cursor:pointer' onclick='a_materialList_material_edit_onClick(this.id)'>" +
                           "<img class='img_item_editIcon' />" +
                       "</a>" +
                       "&nbsp" +
                       "<a id='a_materialList_material_delete_#" + i + "' title='Quitar' style='cursor:pointer' onclick='a_materialList_material_delete_onClick(this.id)'>" +
                           "<img class='img_item_deleteIcon' />" +
                       "</a>" +
                   " </td>" +
               "</tr>")
           );

    }

}

//Revisar que los campos hayan sido completados antes de ser insertados.
function checkMaterialInsertion() {
    var tb_material_message = document.getElementById("tb_material_message")
    var lbl_material_message = document.getElementById("lbl_material_message");

    var tb_material_description = document.getElementById("tb_material_description");
    var tb_material_quantity = document.getElementById("tb_material_quantity");
    var ddl_unitOfMeasure = document.getElementById("ddl_unitOfMeasure");
    var tb_material_unitValue = document.getElementById("tb_material_unitValue");

    if (tb_material_description.value != "") {

        if (tb_material_quantity.value != "") {

            if (ddl_unitOfMeasure.selectedIndex != 0) {
                                
                if (tb_material_unitValue.value != "") {

                    tb_material_message.style.height = "0px";
                    lbl_material_message.style.visibility = "Hidden";

                    return true;
                }
                else {
                    tb_material_message.style.height = "20px";
                    lbl_material_message.style.visibility = "Visible";
                    lbl_material_message.innerHTML = "Debe ingresar un valor unitario"
                }
            }
            else {
                tb_material_message.style.height = "20px";
                lbl_material_message.style.visibility = "Visible";
                lbl_material_message.innerHTML = "Debe ingresar una unidad de medida"
            }
        }
        else {
            tb_material_message.style.height = "20px";
            lbl_material_message.style.visibility = "Visible";
            lbl_material_message.innerHTML = "Debe ingresar una cantidad"
        }
    }
    else {
        tb_material_message.style.height = "20px";
        lbl_material_message.style.visibility = "Visible";
        lbl_material_message.innerHTML = "Debe ingresar un material"
    }

    window.setTimeout(function () {
        tb_material_message.style.height = "0px";
        lbl_material_message.style.visibility = "Hidden";
    }, 3000);

    return false;
}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    WAY BILL   >>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


var _wayBills_jsonList = [];
var _wayBill_dbItems_jsonList = [];
var _wayBillForm_items_jsonList = [];
var _wayBill_missingItems_jsonList = [];


var _editingWayBillNumber = -1;
var _wayBillItemsInEdition_toRemove_from_missingItems;
var _wayBillIInEdition_index;
var _wayBillCreatedOrEdited_number = 0;

$(document).ready(function () {

    /*-----------------------------Inserting way bills to jsonList and Table----------*/
    var wayBillList_str = document.getElementById("tb_wayBillList").value;
    if (wayBillList_str != "") {
        _wayBills_jsonList = JSON.parse(wayBillList_str);
        loadWayBillsTable();
    }
    /*---------------------------------------------------------------*/
    /*-----------------------------Inserting way bills items to jsonList----------*/
    var wayBill_DBItemsList_str = document.getElementById("tb_wayBill_DBItemsList").value;
    if (wayBill_DBItemsList_str != "")
        _wayBill_dbItems_jsonList = JSON.parse(wayBill_DBItemsList_str);

    //esto activa el tab correspondiente tras el refresh, se ubica aquí para ejecutarse después de llenar el jsonList
    if (_activeTab == "3") {
        document.getElementById("tab_a_wayBills").click();
    }
    /*---------------------------------------------------------------*/

    buildWayBill_missingItemsList(); //Construye la lista de items faltantes para agregar a una guía

    checkWayBillMissingItems_showHideCreateButton(); //Esconde el botón Nueva Guía si no hay items disponibles para agregar


    $("#tb_wayBillForm_date").datepicker({
        changeMonth: true,
        changeYear: true
    });


});

//Elimina todas las guías de la lista de guías y las inserta nuevamente.
function loadWayBillsTable() {

    loadingWayBillsList_ToModel();
    removeAllWayBills_fromTable();
    insertingWayBills_toTable();

    checkWayBillMissingItems_showHideCreateButton(); //Esconde el botón Nueva Guía si no hay items disponibles para agregar
}
//Mover la jsonLista de Guías finales en el atributo del modelo para guardarlo en Base de Dates
function loadingWayBillsList_ToModel() {
    document.getElementById("tb_wayBillList").value = JSON.stringify(_wayBills_jsonList);
}
//Elimina todas las guías de la lista de guías
function removeAllWayBills_fromTable() {
    $(".wayBill_rowToDelete").remove();
}

//Ordena la lista de Guías de Despacho
function sortWayBillList() {
    var finalList = [];

    for (var i = 0; i < _wayBills_jsonList.length; i++) {
        finalList.push(Number(_wayBills_jsonList[i].WayBillNumber));
    }

    finalList.sort(function (a, b) {
        return a - b;
    });

    var finalWayBillList = new Array();

    for (var i = 0; i < finalList.length; i++) {

        var wbNumber = finalList[i];

        var index = _wayBills_jsonList.length;
        while (index--) {
            if (_wayBills_jsonList[index].WayBillNumber == wbNumber) break; //Busca el número de guía 
        }
        if (index != -1) {
            var wayBillNumber = _wayBills_jsonList[index].WayBillNumber;
            var wayBillDate = _wayBills_jsonList[index].WayBillDate;
            var comment = _wayBills_jsonList[index].Comment;

            var wayBillProdOrServ_jsonList = _wayBills_jsonList[index].ItemsAndQuantity_list;

            finalWayBillList.push({
                "WayBillNumber": wayBillNumber,
                "WayBillDate": wayBillDate,
                "Comment": comment,
                "ItemsAndQuantity_list": wayBillProdOrServ_jsonList
            });
        }

    }

    _wayBills_jsonList = finalWayBillList;

}

//Inserta guías de despacho a la lista de guías
function insertingWayBills_toTable() {

    sortWayBillList();

    for (var i = 0; i < _wayBills_jsonList.length; i++) {

        var wayBillNumber = _wayBills_jsonList[i].WayBillNumber;
        var wayBillDate = _wayBills_jsonList[i].WayBillDate;
        var comment = _wayBills_jsonList[i].Comment;

        var wayBillProdOrServ_jsonList = _wayBills_jsonList[i].ItemsAndQuantity_list;

        var newWayBill_styleCode = "";
        var newWayBill_classCode = "";

        if (wayBillNumber == _wayBillCreatedOrEdited_number) {
            newWayBill_styleCode = "style='border:3px solid yellow;'";
            newWayBill_classCode = "newWayBill";
        }
        $("#div_wayBillItems_wayBillList")
           .append($("" +
                "<div id='div_wayBillItems_wayBill_" + i + "' class='div_wayBillItems_wayBill wayBill_rowToDelete " + newWayBill_classCode + "' " + newWayBill_styleCode + "> " +
                    "<table class='table_wayBillItem_wayBill_header'>" +
                        "<tr>" +
                            "<td class='td_wayBillItem_wayBill_title'>" +
                                "Guía N° " +
                                wayBillNumber +
                            "</td>" +
                            "<td class='td_wayBillItem_wayBill_controls'>" +
                                "<a id='a_wayBillList_wayBill_edit_#" + i + "' title='Modificar' style='cursor:pointer' onclick='a_wayBillList_wayBill_edit_onClick(this.id)'>" +
                                    "<img class='img_waybill_editIcon'/>" +
                                "</a>" +
                                "&nbsp" +
                                "&nbsp" +
                                "<a id='a_wayBillList_wayBill_delete_#" + i + "' title='Quitar' style='cursor:pointer' onclick='a_wayBillList_wayBill_delete_onClick(this.id)'>" +
                                    "<img class='img_waybill_deleteIcon'/>" +
                                "</a>" +
                                "&nbsp" +
                            "</td>" +
                        "</tr>" +
                    "</table>" +
                    "<div class='div_wayBillItem_wayBill_content'>" +
                        "<div style='height:10px;'></div>" +
                        "<table id='' class='table_wayBillItems_details' cellspacing='0'>" +
                            "<tr>" +
                                "<td class='td_wayBillItems_details_1rsCol'>" +
                                    "Fecha" +
                                "</td>" +
                                "<td style='width:1%;'>:</td>" +
                                "<td class='td_wayBillItems_details_3rdCol'>" +
                                    wayBillDate +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td class='td_wayBillItems_details_1rsCol td_wayBillItem_details_comment'>" +
                                    "Comentario" +
                                "</td>" +
                                "<td style='width:1%;' class='td_wayBillItem_details_comment'>:</td>" +
                                "<td class='td_wayBillItems_details_3rdCol td_wayBillItem_details_comment'>" +
                                    comment +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td colspan='3'>" +
                                "</td>" +
                            "</tr>" +
                        "</table>" +
                        "<table id='table_wayBillItems_items_" + i + "' class='table_wayBillItems_items' cellspacing='0'>" +
                            "<tr>" +
                                "<td class='td_wayBillItem_wayBill_item_title td_wayBillItem_items_title'>" +
                                    "Item" +
                                "</td>" +
                                "<td class='td_wayBillItem_wayBill_quantity_title td_wayBillItem_items_title'>" +
                                    "Cantidad" +
                                "</td>" +
                            "</tr>" +
                        "</table>" +
                    "</div>" +
                "</div>"
               )
           );

        //esto crea un salto de linea al final de cada guía
        for (var j = 0; j < wayBillProdOrServ_jsonList.length; j++) {
            $("#table_wayBillItems_items_" + i).append($("" +
                    "<tr>" +
                        "<td class='td_wayBillItems_items_description td_wayBillItems_row'>" +
                            wayBillProdOrServ_jsonList[j].Description +
                        "</td>" +
                        "<td class='td_wayBillItems_items_quantity td_wayBillItems_row'>" +
                            thousandSeparator(wayBillProdOrServ_jsonList[j].Quantity) +
                        "</td>" +
                    "</tr>"
                )
            );
        }


        $("#div_wayBillItems_wayBillList")
            .append($("" +
            "<br class='wayBill_rowToDelete' />" +
            "<br class='wayBill_rowToDelete' />"
            ));

    }

    setTimeout(function () {
        if (document.getElementsByClassName("newWayBill").length != 0) {
            document.getElementsByClassName("newWayBill")[0].style.border = 'none';
        }
    }, 3000);

}

//Botón que abre la ventana para crear nueva Guía de Despacho
function btn_wayBill_create_onClick() {

    if (_wayBill_missingItems_jsonList.length == 0) {
        document.getElementById("lbl_message").innerHTML = "Todos los items poseen guía de despacho.";
        $("#div_message").dialog("open");
    }
    else {

        bindWayBillForm_selectors();

        $("#div_wayBillForm").dialog({
            width: 600,
            modal: true,
            buttons: {
                "Crear nueva": function () {
                    btn_wayBillForm_save();
                },
                Cancelar: function () {
                    $("#div_wayBillForm").dialog("close");
                }
            },
            close: function () {
                btn_wayBillForm_close();
            }
        });
    }

}

//Editando una Guía de Despacho
function a_wayBillList_wayBill_edit_onClick(id) {


    var index = id.split("#")[1];
    var wayBillNumber = _wayBills_jsonList[index].WayBillNumber.toString()
    if (_invoice_dbWBNumbers_jsonList.indexOf(wayBillNumber) != -1) { //Revisa si existe una factura asociada, en ese caso no podrá editarse ni eliminarse

        document.getElementById("lbl_message").innerHTML = "Existe una factura asociada a este número de guía. No se puede editar.";
        $("#div_message").dialog("open");

    }
    else {

        _wayBillIInEdition_index = index;

        var number = _wayBills_jsonList[index].WayBillNumber;

        _editingWayBillNumber = number;

        var date = _wayBills_jsonList[index].WayBillDate;
        var comment = _wayBills_jsonList[index].Comment;

        var itemJsonList = _wayBills_jsonList[index].ItemsAndQuantity_list;

        document.getElementById("tb_wayBillForm_number").value = number;
        document.getElementById("tb_wayBillForm_date").value = date;
        document.getElementById("tb_wayBillForm_comment").value = comment;


        _wayBillForm_items_jsonList = cloneObject(itemJsonList); //Se pasan los items a editar a la lista de items que irán en la tabla de items del formulario
        _wayBillItemsInEdition_toRemove_from_missingItems = cloneObject(itemJsonList); //En caso de cancelar, se eliminarán de la lista de faltantes, los mismos que se editaban



        addWayBillItems_to_missingItems(itemJsonList);
        bindWayBillForm_selectors();
        loadWayBillFormTable();


        $("#div_wayBillForm").dialog({
            width: 600,
            modal: true,
            buttons: {
                "Guardar": function () {
                    btn_wayBillForm_save();
                },
                Cancelar: function () {
                    removeWayBillItemsToEdit_from_missingItems(itemJsonList);
                    $("#div_wayBillForm").dialog("close");
                }
            },
            close: function () {

                btn_wayBillForm_close();
            }
        });
    }
}

//Botón guardar nueva guía
function btn_wayBillForm_save() {
    if (wayBillForm_ValidatingFields()) {
        createNewWayBill();

        if (_editingWayBillNumber != -1)
            _wayBills_jsonList.splice(_wayBillIInEdition_index, 1); //Elimino la guía de despacho para crearla nuevamente al recrear la tabla

        sortWayBillList();
        loadWayBillsTable();
        tab_wayBills_onClick();

        $("#div_wayBillForm").dialog("close");

    }

}

//Si ya no quedan items, entonces quitar botón Agregar
function checkWayBillMissingItems_showHideCreateButton() {
    /*if (_wayBill_missingItems_jsonList.length == 0)
        document.getElementById("btn_wayBill_create").style.visibility = 'Hidden';
    else
        document.getElementById("btn_wayBill_create").style.visibility = 'Visible';*/
}

//Al cerrar la ventana de nueva guía
function btn_wayBillForm_close() {
    document.getElementById("tb_wayBillForm_number").value = "";
    document.getElementById("tb_wayBillForm_date").value = "";
    document.getElementById("tb_wayBillForm_comment").value = "";

    _wayBillForm_items_jsonList = new Array();

    removeWayBillFormItems_fromTable();
    removeAllWayBillForm_selectors();


    _wayBillCreatedOrEdited_number = 0;

    _editingWayBillNumber = -1;
}


//Eliminar una Guía de Despacho
function a_wayBillList_wayBill_delete_onClick(id) {
    var index = id.split("#")[1];

    var wayBillNumber = _wayBills_jsonList[index].WayBillNumber.toString()
    if (_invoice_dbWBNumbers_jsonList.indexOf(wayBillNumber) != -1) { //Revisa si existe una factura asociada, en ese caso no podrá editarse ni eliminarse

        document.getElementById("lbl_message").innerHTML = "Existe una factura asociada a este número de guía. No se puede eliminar.";
        $("#div_message").dialog("open");

    }
    else {

        $("#div_wayBill_confirmDeletion").dialog({
            resizable: false,
            height: 200,
            width: 400,
            dialogClass: "noclose",
            modal: true,
            title: "Mensaje",
            buttons: {
                "Si": function () {

                    var itemJsonList = _wayBills_jsonList[index].ItemsAndQuantity_list;

                    addWayBillItems_to_missingItems(itemJsonList);

                    _wayBills_jsonList.splice(index, 1);
                    loadWayBillsTable();


                    document.getElementById("tab_a_wayBills").click();

                    $(this).dialog("close");
                },
                "Cancelar": function () {
                    $(this).dialog("close");
                }
            }
        });
    }

}

//Agrega los items de la Guía que está siendo editada a los items faltantes. 
function addWayBillItems_to_missingItems(items_jsonList) {

    for (var i = 0; i < items_jsonList.length; i++) {

        var missingIndex = _wayBill_missingItems_jsonList.length;
        while (missingIndex--) {
            if (_wayBill_missingItems_jsonList[missingIndex].Description == items_jsonList[i].Description) break; //Busca cada descripción de cada item a agregar en la lista de Missing, si no lo encuentra, lo agregará al Missing_jsonList
        }
        if (missingIndex != -1) {
            var missingQuantity = _wayBill_missingItems_jsonList[missingIndex].Quantity;
            _wayBill_missingItems_jsonList[missingIndex].Quantity = Number(missingQuantity) + Number(items_jsonList[i].Quantity);
        }
        else { //Si no lo encuentra, lo agregará al jsonList
            _wayBill_missingItems_jsonList.push({
                "Description": items_jsonList[i].Description,
                "Quantity": items_jsonList[i].Quantity
            });
        }

    }

}

//Elimina los items de la guía que está siendo editada de la lista de items faltantes.
function removeWayBillItemsToEdit_from_missingItems(items_jsonList) {

    if (typeof items_jsonList !== 'undefined') {

        var itemDescToRemove_fromMissingIWBNumbers = [];

        for (var i = 0; i < items_jsonList.length; i++) {

            var missingIndex = _wayBill_missingItems_jsonList.length;
            while (missingIndex--) {
                if (_wayBill_missingItems_jsonList[missingIndex].Description == items_jsonList[i].Description) break; //Busca cada descripción de cada item a agregar en la lista de Missing, si no lo encuentra, lo agregará al Missing_jsonList
            }
            if (missingIndex != -1) {
                var missingQuantity = _wayBill_missingItems_jsonList[missingIndex].Quantity;
                _wayBill_missingItems_jsonList[missingIndex].Quantity = missingQuantity - items_jsonList[i].Quantity;


                if (_wayBill_missingItems_jsonList[missingIndex].Quantity == 0) {
                    var desc = _wayBill_missingItems_jsonList[missingIndex].Description;
                    itemDescToRemove_fromMissingIWBNumbers.push(desc);
                }

            }
        }

        //Eliminar los items que no tengan ya cantidad porque han sido agregados a una guía completamente
        for (var i = 0; i < itemDescToRemove_fromMissingIWBNumbers.length; i++) {
            var desc = itemDescToRemove_fromMissingIWBNumbers[i];

            var indexToRemove = _wayBill_missingItems_jsonList.length;
            while (indexToRemove--) {
                if (_wayBill_missingItems_jsonList[indexToRemove].Description == desc) break;  //Busca el index del item a eliminar
            }
            if (indexToRemove != -1) //index != -1 -> index encontrado
            {
                _wayBill_missingItems_jsonList.splice(indexToRemove, 1);
            }

        }
        _wayBillItemsInEdition_toRemove_from_missingItems = new Array();
    }
}

//Añate items a la lista de items sin guía de despacho
function buildWayBill_missingItemsList() {

    _wayBill_missingItems_jsonList = cloneObject(_item_jsonList); //cloneObject <- Es una funcion dentro del archivo tools.js


    var totalItemsLength = _item_jsonList.length;

    var wbDBItems_list = _wayBill_dbItems_jsonList;

    var itemDescToRemove_fromMissingIWBNumbers = new Array();

    for (var i = 0 ; i < totalItemsLength ; i++) {

        var wbDBItemIndex = wbDBItems_list.length;

        while (wbDBItemIndex--) {
            if (wbDBItems_list[wbDBItemIndex].Description == _wayBill_missingItems_jsonList[i].Description) break; //Busca si existe una guía de despacho para el item I
        }
        if (wbDBItemIndex != -1) { //si itemIndex es distinto de -1 significa existe una guía de despacho con este item
            var wbItem_readyQuantity = wbDBItems_list[wbDBItemIndex].Quantity;
            var wbItem_totalQuantity = _wayBill_missingItems_jsonList[i].Quantity;

            _wayBill_missingItems_jsonList[i].Quantity = wbItem_totalQuantity - wbItem_readyQuantity;

            if (_wayBill_missingItems_jsonList[i].Quantity == 0) {
                var desc = _wayBill_missingItems_jsonList[i].Description;
                itemDescToRemove_fromMissingIWBNumbers.push(desc);
            }
        }
    }
    //Eliminar los items que no tengan ya cantidad porque han sido agregados a una o varias guías completamente
    for (var i = 0; i < itemDescToRemove_fromMissingIWBNumbers.length; i++) {
        var desc = itemDescToRemove_fromMissingIWBNumbers[i];

        var indexToRemove = _wayBill_missingItems_jsonList.length;
        while (indexToRemove--) {
            if (_wayBill_missingItems_jsonList[indexToRemove].Description == desc) break;  //Busca el index del item a eliminar
        }
        if (indexToRemove != -1) //index != -1 -> index encontrado
        {
            _wayBill_missingItems_jsonList.splice(indexToRemove, 1);
        }

    }

}
//Insertando Items sin Guía de Despacho al selector
function bindWayBillForm_selectors() {

    removeAllWayBillForm_selectors();

    for (var i = 0; i < _wayBill_missingItems_jsonList.length; i++) {

        var desc = _wayBill_missingItems_jsonList[i].Description;


        var itemInFormIndex = _wayBillForm_items_jsonList.length;
        while (itemInFormIndex--) {
            if (_wayBillForm_items_jsonList[itemInFormIndex].Description == desc) break; //Buscar si los items sin guías están siendo insertados en una nueva guía.
        }


        var mustBeShown = true;
        if (itemInFormIndex != -1) {//Si es != -1 es porque un item sin Guía fue insertado en la tabla de nueva Guía
            if (_wayBillForm_items_jsonList[itemInFormIndex].Quantity == _wayBill_missingItems_jsonList[i].Quantity) //Significa que la cantidad es igual a la faltante, entonces no se mostrará como opción.
                mustBeShown = false;
        }
        if (mustBeShown) {
            //-----AÑADIENDO ITEMS AL SELECTOR
            var selectItem_description = document.getElementById("sel_wayBillForm_items_description");

            var option_desc = document.createElement("option");
            option_desc.text = desc;
            selectItem_description.add(option_desc);
        }
    }
}
//Eliminar todos los items de los selectores 
function removeAllWayBillForm_selectors() {

    document.getElementById("sel_wayBillForm_items_description").options.length = 0;
    document.getElementById("sel_wayBillForm_items_quantity").options.length = 0;

    var selectItem_description = document.getElementById("sel_wayBillForm_items_description");

    var option_desc = document.createElement("option");
    option_desc.text = "---";
    selectItem_description.add(option_desc);


    var selectItem_quantity = document.getElementById("sel_wayBillForm_items_quantity");

    var option_quant = document.createElement("option");
    option_quant.text = "---";
    selectItem_quantity.add(option_quant);

}

//Al cambiar de fecha, el foco será el campo de comment
function tb_wayBillForm_date_onChange(id) {
    setDateFormat(id);
    $('#tb_wayBillForm_comment').focus();
}
//al hacer click en el botón + (mas) para añadir un item a la lista de items de la nueva guía
function a_wayBillForm_addWayBill_onClick() {

    if (document.getElementById("sel_wayBillForm_items_description").selectedIndex != 0) {

        var description = document.getElementById("sel_wayBillForm_items_description").value;
        var quantity = document.getElementById("sel_wayBillForm_items_quantity").value;

        var itemIndex = _wayBillForm_items_jsonList.length;
        while (itemIndex--) {
            if (_wayBillForm_items_jsonList[itemIndex].Description == description) break; //Revisa si el item fue agregado con anterioridad
        }
        if (itemIndex != -1) { //si es != -1 significa que el item ya había sido agregado anteriormente y sumará las cantidades
            _wayBillForm_items_jsonList[itemIndex].Quantity = Number(_wayBillForm_items_jsonList[itemIndex].Quantity) + Number(quantity);
        }
        else {
            _wayBillForm_items_jsonList.push(
                {
                    "Description": description,
                    "Quantity": quantity
                }
            );
        }
        bindWayBillForm_selectors();
        loadWayBillFormTable();

        document.getElementById("sel_wayBillForm_items_description").selectedIndex = 0;
        document.getElementById("sel_wayBillForm_items_quantity").selectedIndex = 0;

    }
}
//elimina un item de la lista de items de la nueva guía
function a_wayBillFormItemList_delete_onClick(id) {

    var index = id.split("#")[1];
    _wayBillForm_items_jsonList.splice(index, 1);

    bindWayBillForm_selectors();
    loadWayBillFormTable();
}

// Limpia la tabla de items de la nueva guía de despacho y los ingresa nuevamente pero actualizados.
function loadWayBillFormTable() {
    removeWayBillFormItems_fromTable();
    insertingWayBillFormItems_toTable();
}

//Inserta items a la lista de items de la nueva guía
function insertingWayBillFormItems_toTable() {

    for (var i = 0; i < _wayBillForm_items_jsonList.length; i++) {

        var description = _wayBillForm_items_jsonList[i].Description;
        var quantity = _wayBillForm_items_jsonList[i].Quantity;
        $("#table_wayBillForm_items")
           .append($("" +
               "<tr class='wayBillForm_rowToDelete'>" +
                   "<td class='td_wayBillFormItemList_description'> " + description + " </td>" +
                   "<td class='td_wayBillFormItemList_quantity'> " + thousandSeparator(quantity) + " </td>" +
                   "<td class='td_wayBillFormItemList_controls'> " +
                       "<a id='a_wayBillFormItemList_delete_#" + i + "' title='Quitar' style='cursor:pointer' onclick='a_wayBillFormItemList_delete_onClick(this.id)'>" +
                           "<img class='img_item_deleteIcon' />" +
                       "</a>" +
                   " </td>" +
               "</tr>")
           );
    }

}


//Eliminar todos los items de una guía de despacho de la tabla | form
function removeWayBillFormItems_fromTable() {
    $(".wayBillForm_rowToDelete").remove();
}

//Al elegir un item del selector, cargará la cantidad en el 2do selector
function sel_wayBillForm_items_description_onChange() {

    if (document.getElementById("sel_wayBillForm_items_description").selectedIndex != 0) {

        var desc = document.getElementById("sel_wayBillForm_items_description").value;


        var itemIndex = _wayBill_missingItems_jsonList.length;
        while (itemIndex--) {
            if (removeNewLine_trim(_wayBill_missingItems_jsonList[itemIndex].Description) == removeNewLine_trim(desc)) break; //Busca la descripción en la lista de Missing para ubicar la cantidad faltante
        }
        var missingQuantity = 0;
        if (itemIndex != -1) {
            missingQuantity = _wayBill_missingItems_jsonList[itemIndex].Quantity;
        }


        var itemInFormIndex = _wayBillForm_items_jsonList.length;
        while (itemInFormIndex--) {
            if (_wayBillForm_items_jsonList[itemInFormIndex].Description == desc) break;  //Revisa si el item ya fue agregado en la lista de items de la nueva guía y resta la cantidad para mostrar sólo el faltante
        }
        if (itemInFormIndex != -1) //significa que el item fue agregado en la misma guía
        {
            missingQuantity = missingQuantity - _wayBillForm_items_jsonList[itemInFormIndex].Quantity;
        }

        var selectItem_quantity = document.getElementById("sel_wayBillForm_items_quantity");
        selectItem_quantity.length = 0;
        var option_quant = document.createElement("option");


        for (var i = missingQuantity; i >= 1; i--) {
            option_quant = document.createElement("option");
            option_quant.text = i;
            selectItem_quantity.add(option_quant);
        }
    }
    else {
        var selectItem_quantity = document.getElementById("sel_wayBillForm_items_quantity");
        var option_quant = document.createElement("option");

        option_quant.text = "---";
        selectItem_description.add(option_quant);

    }
}

function wayBillForm_ValidatingFields() {
    var number = document.getElementById("tb_wayBillForm_number").value;
    var date = document.getElementById("tb_wayBillForm_date").value;


    if (number == "") {
        alert("Debe ingresar un número de guía");
        return false;
    }
    else {
        var index = _wayBills_jsonList.length;

        while (index--) {
            if (_wayBills_jsonList[index].WayBillNumber == number) break;  //Revisa que el número de Guía no exista previamente
        }

        if (index != -1) //significa el número de guía ya existe
        {
            if (_editingWayBillNumber != -1) {
                if (number != _editingWayBillNumber) {
                    alert("Este número de guía ya existe");
                    return false;
                }
            }
            else {
                alert("Este número de guía ya existe");
                return false;
            }
        }

    }
    if (date == "") {
        alert("Debe seleccionar una Fecha");
        return false;
    }
    if (_wayBillForm_items_jsonList.length == 0) {
        alert("La guía debe poseer al menos 1 item");
        return false;
    }
    return true
}

//Crea una nueva guía de despacho
function createNewWayBill() {

    var number = document.getElementById("tb_wayBillForm_number").value;
    var date = document.getElementById("tb_wayBillForm_date").value.replace("/", "-").replace("/", "-");
    var comment = document.getElementById("tb_wayBillForm_comment").value;

    var itemsAndQuantity = _wayBillForm_items_jsonList;

    _wayBills_jsonList.push({
        "WayBillNumber": number,
        "WayBillDate": date,
        "Comment": comment,
        "ItemsAndQuantity_list": itemsAndQuantity
    });

    _wayBillCreatedOrEdited_number = number;

    removeWayBillFormItems_from_missingItems();

}

//Quitar items de nueva guía de la lista de items faltantes
function removeWayBillFormItems_from_missingItems() {


    var itemDescToRemove_fromMissingIWBNumbers = [];

    for (var i = 0; i < _wayBillForm_items_jsonList.length; i++) {

        var missingIndex = _wayBill_missingItems_jsonList.length;

        while (missingIndex--) {
            if (_wayBill_missingItems_jsonList[missingIndex].Description == _wayBillForm_items_jsonList[i].Description) break;  //Revisa que el item a agregar se encuentre en missing item para poder descontar la cantidad agregada
        }
        if (missingIndex != -1) //significa que el item fue encontrado y se quitará la cantidad agregada
        {
            _wayBill_missingItems_jsonList[missingIndex].Quantity = _wayBill_missingItems_jsonList[missingIndex].Quantity - _wayBillForm_items_jsonList[i].Quantity;

            if (_wayBill_missingItems_jsonList[missingIndex].Quantity == 0) {
                var desc = _wayBill_missingItems_jsonList[missingIndex].Description;
                itemDescToRemove_fromMissingIWBNumbers.push(desc);
            }
        }
    }
    //Eliminar los items que no tengan ya cantidad porque han sido agregados a una guía completamente
    for (var i = 0; i < itemDescToRemove_fromMissingIWBNumbers.length; i++) {
        var desc = itemDescToRemove_fromMissingIWBNumbers[i];

        var indexToRemove = _wayBill_missingItems_jsonList.length;
        while (indexToRemove--) {
            if (_wayBill_missingItems_jsonList[indexToRemove].Description == desc) break;  //Busca el index del item a eliminar
        }
        if (indexToRemove != -1) //index != -1 -> index encontrado
        {
            _wayBill_missingItems_jsonList.splice(indexToRemove, 1);
        }

    }

}




//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    INVOICE   >>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>



var _invoices_jsonList = [];
var _invoice_dbWBNumbers_jsonList = [];
var _invoiceForm_wbNumbers_jsonList = [];
var _invoiceForm_missingWBNumbers_jsonList = [];


var _editingInvoiceNumber = -1;
var _invoiceItemsInEdition_toRemove_from_missingItems;
var _invoiceIInEdition_index;
var _invoiceCreatedOrEdited_number = 0;
var _purchaseOrderNumber = "";
$(document).ready(function () {

    /*-----------------------------Inserting way bills to jsonList and Table----------*/
    var invoiceList_str = document.getElementById("tb_invoiceList").value;
    if (invoiceList_str != "") {
        _invoices_jsonList = JSON.parse(invoiceList_str);
        insertingInvoices_toTable();
    }
    /*--------------------------------------------------------------------------*/
    /*-----------------------------Inserting way bills items to jsonList----------*/
    var invoice_DBWBNumberList_str = document.getElementById("tb_invoice_DBWBNumberList").value;
    if (invoice_DBWBNumberList_str != "")
        _invoice_dbWBNumbers_jsonList = JSON.parse(invoice_DBWBNumberList_str);
    /*--------------------------------------------------------------------------*/
    //esto activa el tab correspondiente tras el refresh, se ubica aquí para ejecutarse después de llenar el jsonList
    if (_activeTab == "4") {
        document.getElementById("tab_a_invoices").click();
    }
    /*--------------------------------------------------------------------------*/
    _purchaseOrderNumber = '@Model.PurchaseOrderNumber';

    buildInvoice_missingWBNumbersList(); //Construye lista de guías para agregar en una factura

    checkInvoiceMissingWBNumbers_showHideCreateButton(); //Esconde el botón Nueva Factura si no hay guías disponibles para agregar

    $("#tb_invoiceForm_date").datepicker({
        changeMonth: true,
        changeYear: true
    });

});


//Si ya no quedan items, entonces quitar botón Agregar
function checkInvoiceMissingWBNumbers_showHideCreateButton() {

    /*if (_invoiceForm_missingWBNumbers_jsonList.length == 0) {
        
        document.getElementById("btn_invoice_create").style.visibility = 'Hidden';
    }
    else {
        document.getElementById("div_noWayBillsForInvoice").style.display = 'None';
        document.getElementById("btn_invoice_create").style.visibility = 'Visible';
    }
    */


}

//Elimina todas las guías de la lista de guías y las inserta nuevamente.
function loadInvoicesTable() {

    loadingInvoicesList_ToModel();
    removeAllInvoices_fromTable();
    insertingInvoices_toTable();

    checkInvoiceMissingWBNumbers_showHideCreateButton(); //Esconde el botón Nueva Factura si no hay guías disponibles para agregar
}
//Mover la jsonLista de Guías finales en el atributo del modelo para guardarlo en Base de Dates
function loadingInvoicesList_ToModel() {
    document.getElementById("tb_invoiceList").value = JSON.stringify(_invoices_jsonList);
}
//Elimina todas las guías de la lista de guías
function removeAllInvoices_fromTable() {
    $(".invoice_rowToDelete").remove();
}

//Ordena la lista de Guías de Despacho
function sortInvoiceList() {
    var finalList = [];

    for (var i = 0; i < _invoices_jsonList.length; i++) {
        finalList.push(Number(_invoices_jsonList[i].InvoiceNumber));
    }

    finalList.sort(function (a, b) {
        return a - b;
    });

    var finalInvoiceList = new Array();

    for (var i = 0; i < finalList.length; i++) {

        var invNumber = finalList[i];

        var index = _invoices_jsonList.length;
        while (index--) {
            if (_invoices_jsonList[index].InvoiceNumber == invNumber) break; //Busca el número de factura
        }
        if (index != -1) {
            var invoiceNumber = _invoices_jsonList[index].InvoiceNumber;
            var invoiceDate = _invoices_jsonList[index].InvoiceDate;
            var isPaid = _invoices_jsonList[index].IsPaid;
            var comment = _invoices_jsonList[index].Comment;
            var netValue = _invoices_jsonList[index].NetValue;

            var invoiceWayBillNumbers_jsonList = _invoices_jsonList[index].WayBillNumber_list;

            finalInvoiceList.push({
                "InvoiceNumber": invoiceNumber,
                "InvoiceDate": invoiceDate,
                "Comment": comment,
                "IsPaid": isPaid,
                "NetValue": netValue,
                "WayBillNumber_list": invoiceWayBillNumbers_jsonList
            });
        }

    }

    _invoices_jsonList = finalInvoiceList;

}

//Inserta guías de despacho a la lista de guías
function insertingInvoices_toTable() {

    sortInvoiceList();

    for (var i = 0; i < _invoices_jsonList.length; i++) {

        var invoiceNumber = _invoices_jsonList[i].InvoiceNumber;
        var invoiceDate = _invoices_jsonList[i].InvoiceDate;
        var isPaid = _invoices_jsonList[i].IsPaid;
        var comment = _invoices_jsonList[i].Comment;

        var ivaPercentage = document.getElementById("tb_ivaPercentage").value;

        var netValue = parseInt(removePoints(_invoices_jsonList[i].NetValue));
        var ivaValue = parseInt(roundNumber(ivaPercentage * netValue / 100));
        var totalValue = netValue + ivaValue;

        var isPaid_iconClassCode = "";
        if (isPaid == true)
            isPaid_iconClassCode = "img_invoice_trueIcon";
        else
            isPaid_iconClassCode = "img_invoice_falseIcon";

        var invoiceWayBillNumbers_jsonList = _invoices_jsonList[i].WayBillNumber_list;

        var newInvoice_styleCode = "";
        var newInvoice_classCode = "";

        if (invoiceNumber == _invoiceCreatedOrEdited_number) {
            newInvoice_styleCode = "style='border:3px solid yellow;'";
            newInvoice_classCode = "newInvoice";
        }
        $("#div_invoiceItems_invoiceList")
           .append($("" +
                "<div id='div_invoiceItems_invoice_" + i + "' class='div_invoiceItems_invoice invoice_rowToDelete " + newInvoice_classCode + "' " + newInvoice_styleCode + "> " +
                    "<table class='table_invoiceItem_invoice_header'>" +
                        "<tr>" +
                            "<td class='td_invoiceItem_invoice_title'>" +
                                "Factura N° " +
                                invoiceNumber +
                            "</td>" +
                            "<td class='td_invoiceItem_invoice_controls'>" +
                                "<a id='a_invoiceList_invoice_edit_#" + i + "' title='Modificar' style='cursor:pointer' onclick='a_invoiceList_invoice_edit_onClick(this.id)'>" +
                                    "<img class='img_invoice_editIcon'/>" +
                                "</a>" +
                                "&nbsp" +
                                "&nbsp" +
                                "<a id='a_invoiceList_invoice_delete_#" + i + "' title='Quitar' style='cursor:pointer' onclick='a_invoiceList_invoice_delete_onClick(this.id)'>" +
                                    "<img class='img_invoice_deleteIcon'/>" +
                                "</a>" +
                                "&nbsp" +
                            "</td>" +
                        "</tr>" +
                    "</table>" +
                    "<div class='div_invoiceItem_invoice_content'>" +
                        "<div style='height:10px;'></div>" +
                        "<table id='' class='table_invoiceItems_details' cellspacing='0'>" +
                            "<tr>" +
                                "<td class='td_invoiceItems_details_1rsCol'>" +
                                    "Fecha" +
                                "</td>" +
                                "<td style='width:1%;'>:</td>" +
                                "<td class='td_invoiceItems_details_3rdCol'>" +
                                    invoiceDate +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td class='td_invoiceItems_details_1rsCol td_invoiceItem_details_comment'>" +
                                    "Comentario" +
                                "</td>" +
                                "<td style='width:1%;' class='td_invoiceItem_details_comment'>:</td>" +
                                "<td class='td_invoiceItems_details_3rdCol td_invoiceItem_details_comment'>" +
                                    comment +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td class='td_invoiceItems_details_1rsCol td_invoiceItem_details_comment'>" +
                                    "Pagada" +
                                "</td>" +
                                "<td style='width:1%;' class='td_invoiceItem_details_comment'>:</td>" +
                                "<td class='td_invoiceItems_details_3rdCol td_invoiceItem_details_isPaid'>" +
                                     "<img class='" + isPaid_iconClassCode + "'/>" +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td class='td_invoiceItems_details_1rsCol'>" +
                                    "Monto Neto" +
                                "</td>" +
                                "<td style='width:1%;'>:</td>" +
                                "<td class='td_invoiceItems_details_3rdCol' style='text-align:right;'>" +
                                     "$ " + thousandSeparator(netValue) +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td class='td_invoiceItems_details_1rsCol'>" +
                                    "Monto I.V.A." +
                                "</td>" +
                                "<td style='width:1%;'>:</td>" +
                                "<td class='td_invoiceItems_details_3rdCol' style='text-align:right;'>" +
                                    "$ " + thousandSeparator(ivaValue) +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td class='td_invoiceItems_details_1rsCol'>" +
                                    "Monto Total" +
                                "</td>" +
                                "<td style='width:1%;'>:</td>" +
                                "<td class='td_invoiceItems_details_3rdCol'  style='text-align:right;'>" +
                                    "$ " + thousandSeparator(totalValue) +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td colspan='3'>" +
                                "</td>" +
                            "</tr>" +
                        "</table>" +
                        "<table id='table_invoiceItems_items_" + i + "' class='table_invoiceItems_items' cellspacing='0'>" +
                            "<tr>" +
                                "<td class='td_invoiceItem_invoice_item_title td_invoiceItem_items_title'>" +
                                    "N° de Guías de Despacho" +
                                "</td>" +
                            "</tr>" +
                        "</table>" +
                    "</div>" +
                "</div>"
               )
           );

        //esto crea un salto de linea al final de cada guía
        for (var j = 0; j < invoiceWayBillNumbers_jsonList.length; j++) {
            $("#table_invoiceItems_items_" + i).append($("" +
                    "<tr>" +
                        "<td class='td_invoiceItems_items_wayBill td_invoiceItems_row'>" +
                            invoiceWayBillNumbers_jsonList[j] +
                        "</td>" +
                    "</tr>"
                )
            );
        }


        $("#div_invoiceItems_invoiceList")
            .append($("" +
            "<br class='invoice_rowToDelete' />" +
            "<br class='invoice_rowToDelete' />"
            ));

    }

    setTimeout(function () {
        if (document.getElementsByClassName("newInvoice").length != 0) {
            document.getElementsByClassName("newInvoice")[0].style.border = 'none';
        }
    }, 3000);

}

//Botón que abre la ventana para crear nueva Guía de Despacho
function btn_invoice_create_onClick() {

    if (_purchaseOrderNumber == "") {
        document.getElementById("lbl_message").innerHTML = "Debe ingresar el N° de Orden de Compra para continuar";
        $("#div_message").dialog("open");
    }
    else if (_invoiceForm_missingWBNumbers_jsonList.length == 0) {
        document.getElementById("lbl_message").innerHTML = "No existen números de Guía para asociar.";
        $("#div_message").dialog("open");
    }
    else {

        bindInvoiceForm_selectors(); //Genera la lista de n° de guías en el selector

        $("#div_invoiceForm").dialog({
            width: 600,
            modal: true,
            buttons: {
                "Crear nueva": function () {
                    btn_invoiceForm_save();
                },
                Cancelar: function () {

                    $("#div_invoiceForm").dialog("close");
                }
            },
            close: function () {
                btn_invoiceForm_close();
            }
        });
    }

}

//Editando una Guía de Despacho
function a_invoiceList_invoice_edit_onClick(id) {

    var index = id.split("#")[1];
    _invoiceIInEdition_index = index;

    var invNumber = _invoices_jsonList[index].InvoiceNumber;
    _editingInvoiceNumber = invNumber;

    var date = _invoices_jsonList[index].InvoiceDate;
    var isPaid = _invoices_jsonList[index].IsPaid;
    var comment = _invoices_jsonList[index].Comment;
    var netValue = _invoices_jsonList[index].NetValue;

    var wbNumbersJsonList = _invoices_jsonList[index].WayBillNumber_list;

    document.getElementById("tb_invoiceForm_number").value = invNumber;
    document.getElementById("tb_invoiceForm_date").value = date;
    document.getElementById("cb_invoiceForm_isPaid").checked = isPaid;
    document.getElementById("tb_invoiceForm_comment").value = comment;
    document.getElementById("tb_invoiceForm_netValue").value = netValue;
    invoiceValuesCalculator(netValue);

    _invoiceForm_wbNumbers_jsonList = cloneObject(wbNumbersJsonList); //Se cargan en la tabla del formulario, los n°de guías asociadas a la factura
    _invoiceItemsInEdition_toRemove_from_missingItems = cloneObject(wbNumbersJsonList); //Se guardan los números de guía  a editar en caso de cancelar, para volver todo al estado original

    bindInvoiceForm_selectors(); //Rellena el selector con números de guía
    loadInvoiceFormTable(); //Recrea la tabla de número de guías en el formulario

    $("#div_invoiceForm").dialog({
        width: 600,
        modal: true,
        buttons: {
            "Guardar": function () {
                btn_invoiceForm_save();
            },
            Cancelar: function () {

                removeInvoiceWBNumbersToEdit_from_missingWBNumbers(_invoiceItemsInEdition_toRemove_from_missingItems);
                $("#div_invoiceForm").dialog("close");
            }
        },
        close: function () {
            btn_invoiceForm_close();
        }
    });

}

//Botón guardar nueva guía
function btn_invoiceForm_save() {

    if (invoiceForm_ValidatingFields()) {
        createNewInvoice();

        if (_editingInvoiceNumber != -1)
            _invoices_jsonList.splice(_invoiceIInEdition_index, 1); //Elimino la guía de despacho para crearla nuevamente posteriormente

        sortInvoiceList();
        loadInvoicesTable();
        tab_invoices_onClick();

        $("#div_invoiceForm").dialog("close");
    }

}


//Al cerrar la ventana de nueva guía
function btn_invoiceForm_close() {
    document.getElementById("tb_invoiceForm_number").value = "";
    document.getElementById("tb_invoiceForm_date").value = "";
    document.getElementById("tb_invoiceForm_comment").value = "";
    document.getElementById("tb_invoiceForm_netValue").value = "";
    document.getElementById("lbl_invoiceForm_ivaValue").innerHTML = "---";
    document.getElementById("lbl_invoiceForm_totalValue").innerHTML = "---";

    _invoiceForm_wbNumbers_jsonList = new Array();

    removeInvoiceFormWBNumbers_fromTable();
    removeAllInvoiceForm_selectors();


    _editingInvoiceNumber = -1;
}


//Eliminar una Guía de Despacho
function a_invoiceList_invoice_delete_onClick(id) {
    var index = id.split("#")[1];

    $("#div_invoice_confirmDeletion").dialog({
        resizable: false,
        height: 200,
        width: 400,
        dialogClass: "noclose",
        modal: true,
        title: "Mensaje",
        buttons: {
            "Si": function () {

                var itemJsonList = _invoices_jsonList[index].WayBillNumber_list;

                addInvoiceWBNumbers_to_missingWBNumbers(itemJsonList);

                _invoices_jsonList.splice(index, 1);
                loadInvoicesTable();

                document.getElementById("tab_a_invoices").click();

                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

}

//Agrega los items de la Factura que está siendo editada a los items faltantes. 
function addInvoiceWBNumbers_to_missingWBNumbers(wbNumbers_jsonList) {

    for (var i = 0; i < wbNumbers_jsonList.length; i++) {

        var missingIndex = _invoiceForm_missingWBNumbers_jsonList.indexOf(wbNumbers_jsonList[i]); //busca el número de guía para agregarlo si no lo encuentra.

        if (missingIndex == -1) { //Si no lo encuentra, lo agregará al jsonList
            _invoiceForm_missingWBNumbers_jsonList.push(wbNumbers_jsonList[i]);
        }

    }

}

//Elimina los items de la guía que está siendo editada de la lista de items faltantes.
function removeInvoiceWBNumbersToEdit_from_missingWBNumbers(wbNumbers_jsonList) {

    if (typeof wbNumbers_jsonList !== 'undefined') {

        for (var i = 0; i < wbNumbers_jsonList.length ; i++) {
            var indexToRemove = _invoiceForm_missingWBNumbers_jsonList.indexOf(wbNumbers_jsonList[i]);
            if (indexToRemove != -1)
                _invoiceForm_missingWBNumbers_jsonList.splice(indexToRemove, 1);
        }

        _invoiceWBNumbersInEdition_toRemove_from_missingWBNumbers = new Array();
    }
}

//Añate items a la lista de items sin guía de despacho
function buildInvoice_missingWBNumbersList() {


    //Copio los números de guía existentes a la lista de faltantes
    for (var i = 0; i < _wayBills_jsonList.length; i++) {
        _invoiceForm_missingWBNumbers_jsonList.push(_wayBills_jsonList[i].WayBillNumber.toString());
    }

    //resto de la lista de faltantes los números de guías que estén en DB
    for (var i = 0; i < _invoice_dbWBNumbers_jsonList.length; i++) {
        var index = _invoiceForm_missingWBNumbers_jsonList.indexOf(_invoice_dbWBNumbers_jsonList[i]);
        if (index != -1)
            _invoiceForm_missingWBNumbers_jsonList.splice(index, 1);
    }


}
//Insertando Items sin Guía de Despacho al selector
function bindInvoiceForm_selectors() {

    removeAllInvoiceForm_selectors();

    for (var i = 0; i < _invoiceForm_missingWBNumbers_jsonList.length; i++) {

        var wbNumber = _invoiceForm_missingWBNumbers_jsonList[i];

        if (_invoiceForm_wbNumbers_jsonList.indexOf(wbNumber) == -1) {

            //-----AÑADIENDO ITEMS AL SELECTOR
            var select_wayBillNumbers = document.getElementById("sel_invoiceForm_wayBillNumber");

            var option_wbNumber = document.createElement("option");
            option_wbNumber.text = wbNumber;
            select_wayBillNumbers.add(option_wbNumber);
        }
    }

    

}
//Eliminar todos los items de los selectores 
function removeAllInvoiceForm_selectors() {

    document.getElementById("sel_invoiceForm_wayBillNumber").options.length = 0;

    var selectItem_wayBill = document.getElementById("sel_invoiceForm_wayBillNumber");

    var option_desc = document.createElement("option");
    option_desc.text = "---";
    selectItem_wayBill.add(option_desc);

}


//Al cambiar de fecha, el foco será el campo de comment
function tb_invoiceForm_date_onChange(id) {
    setDateFormat(id);
    $('#tb_invoiceForm_comment').focus();
}

//Calcula el monto IVA y el Total
function tb_invoiceForm_netValue_onKeyUp(id, e) {

    checkIfNumber_thousandSeparator(id, e);

    var ivaPercentage = document.getElementById("tb_ivaPercentage").value;
    var netValue = parseInt(removePoints(document.getElementById("tb_invoiceForm_netValue").value));

    var ivaValue = parseInt(roundNumber(ivaPercentage * netValue / 100));
    var totalValue = netValue + ivaValue;

    if (isNaN(ivaValue)) {
        document.getElementById("lbl_invoiceForm_ivaValue").innerHTML = "---";
        document.getElementById("lbl_invoiceForm_totalValue").innerHTML = "---";
    }
    else {
        document.getElementById("lbl_invoiceForm_ivaValue").innerHTML = thousandSeparator(ivaValue);
        document.getElementById("lbl_invoiceForm_totalValue").innerHTML = thousandSeparator(totalValue);
    }
}

//Calcula el monto IVA y el Total
function invoiceValuesCalculator(netValue) {

    var ivaPercentage = document.getElementById("tb_ivaPercentage").value;
    var netValue = parseInt(removePoints(netValue));

    var ivaValue = parseInt(roundNumber(ivaPercentage * netValue / 100));
    var totalValue = netValue + ivaValue;

    if (isNaN(ivaValue)) {
        document.getElementById("lbl_invoiceForm_ivaValue").innerHTML = "---";
        document.getElementById("lbl_invoiceForm_totalValue").innerHTML = "---";
    }
    else {
        document.getElementById("tb_invoiceForm_netValue").value = thousandSeparator(netValue);
        document.getElementById("lbl_invoiceForm_ivaValue").innerHTML = thousandSeparator(ivaValue);
        document.getElementById("lbl_invoiceForm_totalValue").innerHTML = thousandSeparator(totalValue);
    }
}

//al hacer click en el botón + (mas) para añadir un item a la lista de items de la nueva guía
function a_invoiceForm_addInvoice_onClick() {

    if (document.getElementById("sel_invoiceForm_wayBillNumber").selectedIndex != 0) {

        var wayBillNumber = document.getElementById("sel_invoiceForm_wayBillNumber").value;

        _invoiceForm_wbNumbers_jsonList.push(wayBillNumber); //Ingresa a la tabla del formulario
        
        bindInvoiceForm_selectors();
        loadInvoiceFormTable();

        document.getElementById("sel_invoiceForm_wayBillNumber").selectedIndex = 0;

    }
}
//elimina un item de la lista de items de la nueva guía
function a_invoiceFormItemList_delete_onClick(id) {

    var index = id.split("#")[1];
    var wayBillNumber = _invoiceForm_wbNumbers_jsonList[index].toString()
    _invoiceForm_wbNumbers_jsonList.splice(index, 1); //Se quita el n° de guía de la tabla del formulario

    _invoiceForm_missingWBNumbers_jsonList.push(wayBillNumber);


    bindInvoiceForm_selectors();
    loadInvoiceFormTable();
}

// Limpia la tabla de items de la nueva guía de despacho y los ingresa nuevamente pero actualizados.
function loadInvoiceFormTable() {
    removeInvoiceFormWBNumbers_fromTable();
    insertingInvoiceFormWBNumbers_toTable();
}

//Inserta items a la lista de items de la nueva guía
function insertingInvoiceFormWBNumbers_toTable() {

    for (var i = 0; i < _invoiceForm_wbNumbers_jsonList.length; i++) {

        var wayBillNumber = _invoiceForm_wbNumbers_jsonList[i];
        $("#table_invoiceForm_wbNumbers")
           .append($("" +
               "<tr class='invoiceForm_rowToDelete'>" +
                   "<td class='td_invoiceFormWBNumberList_wayBillNumber'> " + wayBillNumber + " </td>" +
                   "<td class='td_invoiceFormWBNumberList_controls'> " +
                       "<a id='a_invoiceFormItemList_delete_#" + i + "' title='Quitar' style='cursor:pointer' onclick='a_invoiceFormItemList_delete_onClick(this.id)'>" +
                           "<img class='img_item_deleteIcon' />" +
                       "</a>" +
                   " </td>" +
               "</tr>")
        );
    }

}


//Eliminar todos los números de guía de despacho de una factura de la tabla | form
function removeInvoiceFormWBNumbers_fromTable() {
    $(".invoiceForm_rowToDelete").remove();
}

//Validador de campos de una nueva factura
function invoiceForm_ValidatingFields() {
    var invNumber = document.getElementById("tb_invoiceForm_number").value;
    var date = document.getElementById("tb_invoiceForm_date").value;
    var netValue = document.getElementById("tb_invoiceForm_netValue").value;

    if (invNumber == "") {
        alert("Debe ingresar un número de factura");
        return false;
    }
    else {
        var index = _invoices_jsonList.length;
        while (index--) {
            if (_invoices_jsonList[index].InvoiceNumber == invNumber) break;
        }
        if (index != -1) //significa el número de guía ya existe
        {
            if (_editingInvoiceNumber != -1) {
                if (invNumber != _editingInvoiceNumber) {
                    alert("Este número de factura ya existe");
                    return false;
                }
            }
            else {
                alert("Este número de factura ya existe");
                return false;
            }
        }

    }
    if (date == "") {
        alert("Debe seleccionar una Fecha");
        return false;
    }
    if (netValue == "") {
        alert("Debe ingresar un monto neto");
        return false;
    }
    if (_invoiceForm_wbNumbers_jsonList.length == 0) {
        alert("La factura debe poseer al menos 1 guía de despacho");
        return false;
    }
    return true
}

//Crea una nueva guía de despacho
function createNewInvoice() {


    var number = document.getElementById("tb_invoiceForm_number").value;
    var date = document.getElementById("tb_invoiceForm_date").value.replace("/", "-").replace("/", "-");
    var comment = document.getElementById("tb_invoiceForm_comment").value;
    var isPaid = document.getElementById("cb_invoiceForm_isPaid").checked;
    var netValue = removePoints(document.getElementById("tb_invoiceForm_netValue").value);
    var WayBillNumber_list = _invoiceForm_wbNumbers_jsonList;

    _invoices_jsonList.push({
        "InvoiceNumber": number,
        "InvoiceDate": date,
        "Comment": comment,
        "IsPaid": isPaid,
        "NetValue": netValue,
        "WayBillNumber_list": WayBillNumber_list
    });

    _invoiceCreatedOrEdited_number = number;

    removeInvoiceFormWBNumbers_from_missingWBNumbers();


}

//Quitar items de nueva guía de la lista de items faltantes
function removeInvoiceFormWBNumbers_from_missingWBNumbers() {

    for (var i = 0; i < _invoiceForm_wbNumbers_jsonList.length; i++) {

        var indexToRemove = _invoiceForm_missingWBNumbers_jsonList.indexOf(_invoiceForm_wbNumbers_jsonList[i]);

        if (indexToRemove != -1) {

            _invoiceForm_missingWBNumbers_jsonList.splice(indexToRemove, 1);

        }
    }

}
    

</script>


<form id="form_workEdit" method="post" action="WorkOrderEdit">
    
<div id="div_createdInfo">
    Creado el @Model.CreatedDate por @Model.CreatedBy
</div>
    
<div id="div_message" style="display:none;">
    <label id="lbl_message"></label>
</div>


<div id="div_workOrder_confirmDeletion" style="display:none;"  >   
    ¿Desea realmente eliminar este Trabajo? 
</div>
    
<div id="div_material_confirmDeletion" style="display:none;"  >   
    ¿Desea eliminar este Material? 
</div>
<div id="div_wayBill_confirmDeletion" style="display:none;"  >   
    ¿Desea eliminar esta Guía de Despacho? 
</div>
<div id="div_invoice_confirmDeletion" style="display:none;"  >   
    ¿Desea eliminar esta Factura? 
</div>

    <table style="width:100%;">
        <tr>
            <td id="td_leftSide">

                <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> GENERAL  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->
                <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> GENERAL  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->
                <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> GENERAL  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->


                <table id="table_woDetails" align="center">
                    <tr>
                        <td class="td_woDetails_1rst">
                            N° Orden de Trabajo
                        </td>
                        <td style="width:1%;">:</td>
                        <td class="td_woDetails_3rd">
                            @Html.ValueFor(Function(model) model.WorkOrderNumber)
                        </td>
                    </tr>
                    <tr>
                        <td class="td_woDetails_1rst">
                            N° Cotización
                        </td>
                        <td style="width:1%;">:</td>
                        <td class="td_woDetails_3rd">
                            <a title="Cotización" style="color:blue;" href="@Url.Action("QuotationReview", "Quotation", New With {.quotId = Model.Quotation_Id, .fromNode = "WorkOrderEdit"})">@Model.QuotationNumber</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_woDetails_1rst">
                            Cliente
                        </td>
                        <td style="width:1%;">:</td>
                        <td class="td_woDetails_3rd">
                            @Html.ValueFor(Function(model) model.ClientName)
                        </td>
                    </tr>
                    <tr>
                        <td class="td_woDetails_1rst">
                            Tipo de Trabajo
                        </td>
                        <td style="width:1%;">:</td>
                        <td class="td_woDetails_3rd">
                            @Html.ValueFor(Function(model) model.WorkTypeQ)
                        </td>
                    </tr>
                    <tr>
                        <td class="td_woDetails_1rst">
                            Fecha de Término
                            <span class="span_requiredIcon">*</span>
                        </td>
                        <td style="width:1%;">:</td>
                        <td class="td_woDetails_3rd">
                            @Html.TextBoxFor(Function(model) model.FinishDate, New With {.id = "tb_finishDate", .maxLength = "10", .style = "width:120px; text-align:center;", .onchange = "setDateFormat(this.id)"})
                        </td>
                    </tr>
                    <tr>
                        <td class="td_woDetails_1rst">
                            N° Orden de Compra
                        </td>
                        <td style="width:1%;">:</td>
                        <td class="td_woDetails_3rd">
                            @Html.TextBoxFor(Function(model) model.PurchaseOrderNumber, New With {.id = "tb_purchaseOrderNumber", .maxLength = "10", .style = "width:150px; text-align:center;", .onkeyup = "tb_purchaseOrderNumber_onKeyUp(event)"})           
                        </td>
                    </tr>
                    <tr>
                        <td class="td_woDetails_1rst" style="vertical-align:top; padding-top:5px;">
                            Observaciones
                        </td>
                        <td style="width:1%;vertical-align:top; padding-top:5px;">:</td>
                        <td class="td_woDetails_3rd">
                            @Html.TextAreaFor(Function(model) model.Observations, New With {.id = "tb_observations", .maxLength = "200", .style = "height:100px; width:80%;"})

                        </td>
                    </tr>
                    <tr>
                        <td class="td_woDetails_1rst" style="vertical-align:top; padding-top:5px;">
                            Estado
                        </td>
                        <td style="width:1%;vertical-align:top; padding-top:5px;">:</td>
                        <td class="td_woDetails_3rd">
                            <img class="img_statusIcon" src="~/Images/Sites/Icons/point_@(ViewBag.StatusIconColor).png" />
                            @ViewBag.StatusName
                        </td>
                    </tr>
                </table>

            </td>
            <td id="td_rightSide">

                <div id="div_tabs" style="height:400px; " >
                    <ul>
                        <li id="tab_items" onclick="tab_items_onClick()"><a id="tab_a_items" href="#div_items_tab" class="tab_title">Items</a></li>
                        <li id="tab_materials" onclick="tab_materials_onClick()"><a id="tab_a_materials" href="#div_materials_tab" class="tab_title">Materiales</a></li>
                        <li id="tab_wayBills" onclick="tab_wayBills_onClick()"><a id="tab_a_wayBills" href="#div_wayBills_tab" class="tab_title">Guías de Despacho</a></li>
                        <li id="tab_invoices" onclick="tab_invoices_onClick()"><a id="tab_a_invoices"  href="#div_invoices_tab" class="tab_title">Facturas</a></li>
                    </ul>
                    
                <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ITEMS  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->
                <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ITEMS  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->
                <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ITEMS  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->
                    
                    
                    <div id="div_items_tab" class="tab_body">

                        <br />

                        <table id="table_items_itemList" cellspacing="0">
                            <tr class="tr_items_itemList_title">
                                <td class="td_itemList_title_itemNum">
                                    N°
                                </td>
                                <td class="td_itemList_title_description">
                                    Descripción
                                </td>
                                <td class="td_itemList_title_quantity">
                                    Cantidad
                                </td>                    
                            </tr>
                            <tbody>
                                <!-- ------------------------------------ ITEMS A INSERTAR ------------------------------------->
                            </tbody>
                            <tr>
                                <td>
                                    <br /><br />
                                </td>
                                <td class="td_woDetails_itemList_item_leftBorder">
                    
                                </td>
                                <td class="td_woDetails_itemList_item_leftBorder">
                    
                                </td>
           
                            </tr>               

                        </table>
         
                    </div>
                    
                    <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> MATERIALS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->
                    <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> MATERIALS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->
                    <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> MATERIALS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->

                    <div id="div_materials_tab" class="tab_body" >
        
                        <br />

                        <table id="table_woMaterials_materials">
                            <tr class="tr_material_rows">
                                <td class="td_material_1rst">
                                    Descripción
                                </td>
                                <td style="width:1%; vertical-align:top;">:</td>
                                <td class="td_material_3rd">
                                    <textarea id="tb_material_description" maxlength="200" style="width:100%; height:45px;"></textarea>
                                </td>
                                <td></td>
                            </tr>
                            <tr class="tr_material_rows">
                                <td class="td_material_1rst">
                                    Cantidad
                                </td>
                                <td style="width:1%; vertical-align:top;">:</td>
                                <td class="td_material_3rd">
                                    <input id="tb_material_quantity" maxlength="10" style="width:100px; text-align:right;" onkeyup="tb_material_quantity_onKeyUp(this.id, event)" />
                                    <select id="ddl_unitOfMeasure"></select>
                                </td>
                                <td></td>
                            </tr>                        
                            <tr class="tr_material_rows">
                                <td class="td_material_1rst">
                                    Valor Unitario
                                </td>
                                <td style="width:1%; vertical-align:top;">:</td>
                                <td class="td_material_3rd">
                                    $ <input id="tb_material_unitValue" maxlength="10" style="width:120px; text-align:right;" onkeyup="tb_material_unitValue_onKeyUp(this.id, event)" />
                                </td>
                                <td></td>
                            </tr>
                            <tr class="tr_material_rows">
                                <td class="td_material_1rst">
                                    Valor Neto
                                </td>
                                <td style="width:1%; vertical-align:top;">:</td>
                                <td class="td_material_3rd">
                                    <div style="width:10px; float:left;">$</div>
                                    <div style="width:135px; text-align:right;">
                                         <label id="lbl_material_netValue"></label>
                                    </div>
                                </td>
                                <td style="width:1%; height:1%;">                    
                                    <a id="a_addMaterial" title="Agregar" style="cursor:pointer" onclick="a_addMaterial_onClick()">
                                        <img class="img_plusIcon"  />
                                    </a>
                                    <a id="a_saveMaterial" title="Guardar" style="cursor:pointer; display:none;" onclick="a_saveMaterial_onClick()">
                                        <img class="img_okIcon"  />
                                    </a>
                                    <a id="a_cancelMaterial" title="Cancelar" style="cursor:pointer;  display:none;" onclick="a_cancelMaterial_onClick()">
                                        <img class="img_cancelIcon"  />
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <div id="tb_material_message" style="text-align:center;">
                            <label id="lbl_material_message"></label>
                        </div>

                        <br />

                        <table id="table_woMaterials_materialList" cellspacing="0">
                            <tr class="tr_materialList_title">
                                <td class="td_materialList_title_materialNum">
                                    N°
                                </td>
                                <td class="td_materialList_title_description">
                                    Material
                                </td>
                                <td class="td_materialList_title_quantity">
                                    Cantidad
                                </td>
                                <td class="td_materialList_title_netValue">
                                    Valor Neto
                                </td>
                                <td class="td_materialList_title_controls">

                                </td>
                            </tr>
                            <tbody>
                                <!-- ------------------------------------ MATERIALS A INSERTAR ------------------------------------->
                            </tbody>
                            <tr>
                                <td>
                                    <br /><br />
                                </td>
                                <td class="td_materialList_material_leftBorder">                    
                                </td>
                                <td class="td_materialList_material_leftBorder">
                                </td>
                                <td class="td_materialList_material_leftBorder">
                                </td>
                                <td class="td_materialList_material_leftBorder">
                    
                                </td>
                            </tr>
                        </table>
                        <table style="width:100%; text-align:right;">
                            <tr>
                                <td style="width:75%">Neto : </td>
                                <td>
                                    <label id="lbl_material_finalNetValue"></label>
                                </td>
                                <td style="width:10%;"></td>
                            </tr>
                            <tr>
                                <td>IVA :</td>
                                <td>
                                    <label id="lbl_material_finalIVAValue"></label>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Total :</td>
                                <td>
                                    <label id="lbl_material_finalTotalValue"></label>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </div>

                    <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> GUÍAS DE DESPACHO >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->
                    <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> GUÍAS DE DESPACHO >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->
                    <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> GUÍAS DE DESPACHO >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->

                    <div id="div_wayBills_tab" class="tab_body">
            

                        <div style="text-align:right;">
                            <button type="button" id="btn_wayBill_create" class="btn_common btn_common_color3 btn_common_size3" onclick="btn_wayBill_create_onClick()">
                                Nueva Guía
                            </button>
                        </div>

                        <br />

                        <div id="div_wayBillItems_wayBillList" cellspacing="0" >

                        </div>
                        
                        <!------------------------------------------------------------------------------------------------------------------------>
                        <!--------------------------------------------------------------  MODAL WINDOW. NEW WAY BILL  ---------------------------->
                        <!------------------------------------------------------------------------------------------------------------------------>
                        <div id="div_wayBillForm" title="Nueva Guía de Despacho" style="display:none;">
                            <table id="table_wayBillForm">
                                <tr>
                                    <td class="table_wayBillForm_1rstCol">
                                        N° Guía
                                    </td>
                                    <td class="table_wayBillForm_2ndCol">
                                        :
                                    </td>
                                    <td class="table_wayBillForm_3rdCol">
                                        <input id="tb_wayBillForm_number" onkeyup="checkIfNumber(this.id, event)" style="width:110px; text-align:center;" type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="table_wayBillForm_1rstCol">
                                        Fecha
                                    </td>
                                    <td class="table_wayBillForm_2ndCol">
                                        :
                                    </td>
                                    <td class="table_wayBillForm_3rdCol">
                                        <input id="tb_wayBillForm_date" onchange="tb_wayBillForm_date_onChange(this.id)" maxlength="10" type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="table_wayBillForm_1rstCol">
                                        Comentario
                                    </td>
                                    <td class="table_wayBillForm_2ndCol">
                                        :
                                    </td>
                                    <td class="table_wayBillForm_3rdCol">
                                        <textarea id="tb_wayBillForm_comment"></textarea>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table id="table_wayBillForm_items">
                                <tr>
                                    <td id="table_wayBillForm_items_items_title" class="table_wayBillForm_items_title">
                                        Item
                                    </td>
                                    <td id="table_wayBillForm_items_quantity_title" class="table_wayBillForm_items_title">
                                        Cantidad
                                    </td>
                                    <td id="table_wayBillForm_items_controls_title" class="table_wayBillForm_items_title">
                
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">
                                        <select id="sel_wayBillForm_items_description" onchange="sel_wayBillForm_items_description_onChange()" style="width:100%;">
                                            <option>---</option>
                                        </select>
                                    </td>
                                    <td style="text-align:center;">
                                        <select id="sel_wayBillForm_items_quantity" style="width:100%;">
                                            <option>---</option>
                                        </select>
                                    </td>
                                    <td style="width:1%; text-align:center;">
                                        <a id="a_wayBillForm_addWayBill" title="Agregar" style="cursor:pointer" onclick="a_wayBillForm_addWayBill_onClick()">
                                            <img class="img_plusIcon" />
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                         
                    </div>
                    
                    <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    FACTURAS    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->
                    <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    FACTURAS    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->
                    <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    FACTURAS    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->
                    <div id="div_invoices_tab" class="tab_body">
          
                        
                        <div style="text-align:right;">
                            <button type="button" id="btn_invoice_create" class="btn_common btn_common_color3 btn_common_size3" onclick="btn_invoice_create_onClick()">
                                Nueva Factura
                            </button>
                        </div>
                        <div id="div_noWayBillsForInvoice" style="text-align:center; display:none;">
                            No hay guías de despacho para vincular a una factura.
                        </div>

                        <br />

                        <div id="div_invoiceItems_invoiceList" cellspacing="0" >

                        </div>
                        
                        <!------------------------------------------------------------------------------------------------------------------------>
                        <!--------------------------------------------------------------  MODAL WINDOW. NEW INVOICE  ---------------------------->
                        <!------------------------------------------------------------------------------------------------------------------------>
                        <div id="div_invoiceForm" title="Nueva Factura" style="display:none; ">
                            <table id="table_invoiceForm" >
                                <tr>
                                    <td class="table_invoiceForm_1rstCol">
                                        N° Factura
                                    </td>
                                    <td class="table_invoiceForm_2ndCol">
                                        :
                                    </td>
                                    <td class="table_invoiceForm_3rdCol">
                                        <input id="tb_invoiceForm_number" onkeyup="checkIfNumber(this.id, event)" style="width:110px; text-align:center;" type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="table_invoiceForm_1rstCol">
                                        Fecha
                                    </td>
                                    <td class="table_invoiceForm_2ndCol">
                                        :
                                    </td>
                                    <td class="table_invoiceForm_3rdCol">
                                        <input id="tb_invoiceForm_date" onchange="tb_invoiceForm_date_onChange(this.id)" maxlength="10" type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="table_invoiceForm_1rstCol">
                                        Pagada
                                    </td>
                                    <td class="table_invoiceForm_2ndCol">
                                        :
                                    </td>
                                    <td class="table_invoiceForm_3rdCol">
                                        <input id="cb_invoiceForm_isPaid" type="checkbox" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="table_invoiceForm_1rstCol">
                                        Comentario
                                    </td>
                                    <td class="table_invoiceForm_2ndCol">
                                        :
                                    </td>
                                    <td class="table_invoiceForm_3rdCol">
                                        <textarea id="tb_invoiceForm_comment"></textarea>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <!----------------- MONTOS--------------------->
                            <table class="table_invoiceForm">
                                 <tr>
                                    <td class="table_invoiceForm_1rstCol">
                                        Monto Neto
                                    </td>
                                    <td class="table_invoiceForm_2ndCol">
                                        :
                                    </td>
                                    <td class="table_invoiceForm_3rdCol" style="text-align:right; padding-right:40px;">
                                        $ <input id="tb_invoiceForm_netValue" onkeyup="tb_invoiceForm_netValue_onKeyUp(this.id, event)"   style="width:100px; text-align:right;" type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="table_invoiceForm_1rstCol">
                                        Monto I.V.A.
                                    </td>
                                    <td class="table_invoiceForm_2ndCol">
                                        :
                                    </td>
                                    <td class="table_invoiceForm_3rdCol" style="text-align:right; padding-right:45px;">
                                        $ <label id="lbl_invoiceForm_ivaValue">---</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="table_invoiceForm_1rstCol">
                                        Monto Total
                                    </td>
                                    <td class="table_invoiceForm_2ndCol">
                                        :
                                    </td>
                                    <td class="table_invoiceForm_3rdCol" style="text-align:right; padding-right:45px;">
                                        $ <label id="lbl_invoiceForm_totalValue">---</label>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table id="table_invoiceForm_wbNumbers">
                                <tr>
                                    <td id="table_invoiceForm_wbNumbers_items_title" class="table_invoiceForm_wbNumbers_title">
                                        N° de Guías de Despacho
                                    </td>
                                    <td id="table_invoiceForm_wbNumbers_controls_title" class="table_invoiceForm_wbNumbers_title">
                
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">
                                        <select id="sel_invoiceForm_wayBillNumber" style="width:230px;">
                                            <option>---</option>
                                        </select>
                                    </td>
                                    <td style="width:1%; text-align:center;">
                                        <a id="a_invoiceForm_addInvoice" title="Agregar" style="cursor:pointer" onclick="a_invoiceForm_addInvoice_onClick()">
                                            <img class="img_plusIcon" />
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>

                </div>

            </td>
        </tr>
    </table>

    <br />
    
    
    <div id="div_workAdd_buttonsContainer"  style="width:100%; text-align:right;">
        <button type="button" id="btn_goBack" onclick="btn_goBack_onClick()" class="btn_common btn_common_color2 btn_common_size1">Volver</button>
        &nbsp
        <button type="button" id="btn_workOrder_delete" class="btn_common btn_common_color_delete btn_common_size2" onclick="btn_workOrder_delete_onClick()">Eliminar</button>
        &nbsp
        <button type="button" id="btn_workOrder_save" class="btn_common btn_common_color1 btn_common_size2" onclick="btn_work_save_onClick()">Guardar</button>
        &nbsp        
        <button type="button" id="btn_workOrder_download" class="btn_common btn_common_color1 btn_common_size2" onclick="window.location = '@Url.Action("GenerateWorkOrderPDF", "WorkOrder", New With {.workId = Model.WorkOrder_Id})'">Descargar</button>
    </div>

    
    
    @Html.TextBoxFor(Function(model) model.Quotation_Id, New With {.id = "tb_quotationId", .style = "display:none;"})
    @Html.TextBoxFor(Function(model) model.WorkOrder_Id, New With {.id = "tb_workId", .style = "display:none;"})
    @Html.TextBoxFor(Function(model) model.Item_list, New With {.id = "tb_itemList", .style = "display:none;"})
    @Html.TextBoxFor(Function(model) model.Material_list, New With {.id = "tb_materialList", .style = "display:none;"})
    @Html.TextBoxFor(Function(model) model.WayBill_list, New With {.id = "tb_wayBillList", .style = "display:none;"})
    @Html.TextBoxFor(Function(model) model.WayBill_DBItems_list, New With {.id = "tb_wayBill_DBItemsList", .style = "display:none;"})
    @Html.TextBoxFor(Function(model) model.Invoice_list, New With {.id = "tb_invoiceList", .style = "display:none;"})
    @Html.TextBoxFor(Function(model) model.Invoice_DBWBNumbers_list, New With {.id = "tb_invoice_DBWBNumberList", .style = "display:none;"})
    @Html.TextBoxFor(Function(model) model.IVAPercentage, New With {.id = "tb_ivaPercentage", .style = "display:none;"})


</form>


<div style="visibility:hidden;">
    <input id="hf_activeTab" type="hidden" />
    <input id="hf_materialToEdit_rowNum" type="hidden" />
</div>