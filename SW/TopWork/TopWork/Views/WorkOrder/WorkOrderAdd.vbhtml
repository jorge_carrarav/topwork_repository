﻿@ModelType TopWork.WorkOrderDataProvider.WorkOrder_AddEdit_Formatted
@Code
    ViewData("AppNode") = "Trabajos > Agregar"
End Code

<style>

    #div_createdInfo {
        font-style:italic;
        color:darkblue;
        position:absolute;
        right:1%;
        top:1%;        
    }

    #div_tabs {
        background-color:#F4F4F4;
        font-size:16px;

    }

    #div_tabs input {
        font-size:15px;
        font-family: "Arial", Times, serif;
    }
    #div_tabs textarea {
        font-size:15px;
        font-family: "Arial", Times, serif;
    }
    #div_tabs select {
        font-size:15px;
        font-family: "Arial", Times, serif;
    }

    #td_leftSide {
        width:40%;
        vertical-align:top;
    }
    #td_rightSide {
        width:60%;
    }


    .tab_title {
    }
    .tab_body {
        font-family: "Arial", Times, serif;
    }
    .td_woDetails_1rst {
        width:50%;
        height:26px;
    }

    .td_woDetails_3rd {
        width:50%;
        text-align:center;
    }


    /* -------------------------------------------------------------ItemList */
    #table_woDetails {
        width:90%;
    }

    #div_itemList {
        padding:10px;
        width:50%;
        height:300px;
        margin:auto;
    }
    #table_items_itemList {
        background-color:white;
        border:1px solid gray;
        margin:auto;
        width:80%;
    }

    .tr_item_rows {
        height:25px;
    }
    .tr_items_itemList_title {
        background-color: #DCE6F1;
        text-align:center;
        height:30px;
    }
    .td_itemList_title_itemNum {
        width:7%;
        font-weight:bold;
    }
    .td_itemList_title_description {
        font-weight:bold;
    }
    .td_itemList_title_quantity {
        width: 15%;
        font-weight:bold;

    }
    
    
    .td_itemList_itemNum {
        text-align:center;
    }
    .td_itemList_description {
        text-align:left;
        padding-left:5px;
        padding-right:5px;
    }
    .td_itemList_quantity {
        text-align:center;
    }


    .td_itemList_item_leftBorder {        
        border-left:solid 1px gray;
    }
    .td_itemList_item_common {
        padding-bottom:10px;
        padding-left:5px;
        padding-right:5px;
    }
        
/* ---------------------------------------------------------------------- */


</style>


<script>

    var _item_jsonList = [];

    $(document).ready(function () {
        
       
        $(function () {
            
            $("#div_tabs").tabs();


            /*-----------------------------Inserting items to table----------*/
            var itemList_str = document.getElementById("tb_itemList").value;
            if (itemList_str != "") {
                _item_jsonList = JSON.parse(itemList_str);
                insertingItems_toTable();
            }
            /*---------------------------------------------------------------*/


            var quotNumber = '@ViewBag.QuotationNumber';

            if (quotNumber == "") {

                document.getElementById("btn_create").disabled = true;
                document.getElementById("tb_finishDate").disabled = true;
                document.getElementById("tb_purchaseOrderNumber").disabled = true;
                document.getElementById("tb_observations").disabled = true;

            }
            
        });

        
        $(function () {
            var tabHeight = document.getElementById("table_items_itemList").clientHeight + 120;
            
            if (tabHeight < 350)
                tabHeight = 350;

            document.getElementById("div_tabs").style.height = tabHeight.toString() + "px";
        });
        

        $("#tb_finishDate").datepicker({
            changeMonth: true,
            changeYear: true
        });

    });

    function tb_quotationNumber_onKeyPress(e) {
        if (e.keyCode == 13) {
            var a_clientNameSelection = document.getElementById("a_clientNameSelection");
            a_quotationNumberSelection.click();
        }
    }
        

    function a_quotationNumberSelection_onClick() {
        var quotationNumber = document.getElementById("tb_quotationNumber").value;
        window.location.href = '@Url.Action("WorkOrderAdd", "WorkOrder")?quotNum=' + quotationNumber;
    }


    function tb_purchaseOrderNumber_onKeyUp(e) {

        var tb_purchaseOrderNumber = document.getElementById("tb_purchaseOrderNumber");

        if (!IsNumber(e))
            tb_purchaseOrderNumber.value = "";
    }
    

    //------------------------------------------------------------------Añadir todo el JSON con items a la tabla
    function insertingItems_toTable() {

        var i = 0;

        for (i; i < _item_jsonList.length; i++) {

            var itemNumber = i + 1;

            var description = _item_jsonList[i].Description;
            var quantity = _item_jsonList[i].Quantity;


            $("#table_items_itemList")
               .append($("" +
                   "<tr id='tr_itemList_" + i + "' class='rowToDelete'>" +
                       "<td class='td_itemList_itemNum td_itemList_item_common'> " + itemNumber + " </td>" +
                       "<td id='td_itemList_description_" + i + "' class='td_itemList_description td_itemList_item_leftBorder td_itemList_item_common'> " + description + " </td>" +
                       "<td id='td_itemList_quantity_" + i + "' class='td_itemList_quantity td_itemList_item_leftBorder td_itemList_item_common'> " + thousandSeparator(quantity) + " </td>" +
                   "</tr>")
               );
        }

    }

    function btn_create_onClick() {

        $("#div_loading").dialog("open");

        if (validatingFields()) {

            var workAdd_url = '@Url.Action("WorkOrderAdd", "WorkOrder")';

            var myForm = $("#form_workAdd");

            $.ajax({
                type: "POST",
                url: workAdd_url,
                data: myForm.serialize(),
                datatype: "html",
                success: function (workId) {

                    var workEdit_url = '@Url.Action("WorkOrderEdit", "WorkOrder")';

                    window.location = workEdit_url + "?workId=" + workId + "&result=SuccessCreation";

                }
            });

        }
        else {
            $("#div_loading").dialog("close");
        }
    }
    

    function validatingFields() {

        var tb_quotationNumber = document.getElementById("tb_quotationNumber");
        var tb_finishDate = document.getElementById("tb_finishDate");

        if (tb_finishDate.value == "") {
            alert("Debe seleccionar una Fecha de Término");
            return false;
        }
        
        return true
    }


    function btn_goBack_onClick() {
        var goBackPoint = '@ViewBag.GoBackPoint'

        if (goBackPoint == "QuotationEdit")
            window.location.href = '@Url.Action("QuotationEdit", "Quotation", New With {.quotId = ViewBag.QuotationId})';
        else
            window.location.href = '@Url.Action("WorkOrderList", "WorkOrder", New With {.wip = 1})';
    }

</script>


<form id="form_workAdd" method="post" action="WorkOrderAdd">
    

    <table style="width:100%;">
        <tr>
            <td id="td_leftSide">

                <table id="table_woDetails" align="center">
                      <tr>
                          <td class="td_woDetails_1rst">
                              N° Orden de Trabajo
                          </td>
                          <td style="width:1%;">:</td>
                          <td class="td_woDetails_3rd">
                              ---
                          </td>
                      </tr>
                      <tr>
                          <td class="td_woDetails_1rst">
                              N° Cotización
                          </td>
                          <td style="width:1%;">:</td>
                          <td class="td_woDetails_3rd">
                                  
                          @Html.TextBoxFor(Function(model) model.QuotationNumber, New With {.id = "tb_quotationNumber", .maxLength = "10", .style = "width:150px; text-align:center;", .onkeypress = "tb_quotationNumber_onKeyPress(event)", .onkeyup = "checkIfNumber_or_point(this.id, event)"})           

                            <a id="a_quotationNumberSelection" title="Cargar" style="cursor:pointer" onclick="a_quotationNumberSelection_onClick()">
                                <img class="img_goIcon" />
                            </a>
                  
                          </td>
                      </tr>
                      <tr>
                          <td class="td_woDetails_1rst">
                              Cliente
                          </td>
                          <td style="width:1%;">:</td>
                          <td class="td_woDetails_3rd">
                              @Html.ValueFor(Function(model) model.ClientName)
                          </td>
                      </tr>
                      <tr>
                          <td class="td_woDetails_1rst">
                              Tipo de Trabajo
                          </td>
                          <td style="width:1%;">:</td>
                          <td class="td_woDetails_3rd">
                              @Html.ValueFor(Function(model) model.WorkTypeQ)
                          </td>
                      </tr>
                      <tr>
                          <td class="td_woDetails_1rst">
                              Fecha de Término
                                <span class="span_requiredIcon">*</span>
                          </td>
                          <td style="width:1%;">:</td>
                          <td class="td_woDetails_3rd">
                              @Html.TextBoxFor(Function(model) model.FinishDate, New With {.id = "tb_finishDate", .maxLength = "10", .style = "width:120px; text-align:center;", .onchange = "setDateFormat(this.id)"})
                          </td>
                      </tr>
                      <tr>
                          <td class="td_woDetails_1rst">
                              N° Orden de Compra
                          </td>
                          <td style="width:1%;">:</td>
                          <td class="td_woDetails_3rd">
                              @Html.TextBoxFor(Function(model) model.PurchaseOrderNumber, New With {.id = "tb_purchaseOrderNumber", .maxLength = "10", .style = "width:150px; text-align:center;",  .onkeyup = "tb_purchaseOrderNumber_onKeyUp(event)"})           
                          </td>
                      </tr>
                      <tr>
                          <td class="td_woDetails_1rst" style="vertical-align:top; padding-top:5px;">
                              Observaciones
                          </td>
                          <td style="width:1%;vertical-align:top; padding-top:5px;">:</td>
                          <td class="td_woDetails_3rd">
                              @Html.TextAreaFor(Function(model) model.Observations, New With {.id = "tb_observations", .maxLength = "200", .style = "height:100px; width:80%;"})

                          </td>
                      </tr>
                      <tr>
                          <td class="td_woDetails_1rst" style="vertical-align:top; padding-top:5px;">
                              Estado
                          </td>
                          <td style="width:1%;vertical-align:top; padding-top:5px;">:</td>
                          <td class="td_woDetails_3rd">                                 
                                <img class="img_statusIcon" src="~/Images/Sites/Icons/point_gray.png" />
                          </td>
                      </tr>
                  </table>

            </td>
            <td id="td_rightSide">
                  <div id="div_tabs" style="height:300px; " >
                        <ul>
                            <li><a href="#tab_items" class="tab_title">Items</a></li>
                            <li><a id="tab_materials" href="#div_materials" style="pointer-events:none;"  class="tab_title">Materiales</a></li>
                            <li><a href="#div_wayBills" style="pointer-events:none;"  class="tab_title">Guías de Despacho</a></li>
                            <li><a href="#div_invoices" style="pointer-events:none;" class="tab_title">Facturas</a></li>
                        </ul>

                     <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ITEMS  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->
                        <div id="div_items_tab" class="tab_body">
                            
                            <br />
                            <br />

                            <table id="table_items_itemList" cellspacing="0">
                                <tr class="tr_items_itemList_title">
                                    <td class="td_itemList_title_itemNum">
                                        N°
                                    </td>
                                    <td class="td_itemList_title_description">
                                        Descripción
                                    </td>
                                    <td class="td_itemList_title_quantity">
                                        Cantidad
                                    </td>                    
                                </tr>
                                <tbody>
                                    <!-- ------------------------------------ ITEMS A INSERTAR ------------------------------------->
                                </tbody>
                                <tr>
                                    <td>
                                        <br /><br />
                                    </td>
                                    <td class="td_woDetails_itemList_item_leftBorder">
                    
                                    </td>
                                    <td class="td_woDetails_itemList_item_leftBorder">
                    
                                    </td>
           
                                </tr>               

                            </table>         
                        </div>      
                        <div id="div_materials_tab" class="tab_body">
                        </div>
                        <div id="div_wayBills_tab" class="tab_body">
                        </div>
                        <div id="div_invoices_tab" class="tab_body">
                        </div>

                    </div>


            </td>
        </tr>
    </table>
      
    <br />


    
    <div id="div_quotationAdd_buttonsContainer" style="width:100%; float:left; text-align:right;">
        <button type="button" id="btn_goBack" onclick="btn_goBack_onClick()" class="btn_common btn_common_color2 btn_common_size1">Volver</button>
        &nbsp
        <button type="button" id="btn_create" class="btn_common btn_common_color1 btn_common_size2" onclick="btn_create_onClick()">Crear</button>
    </div>

    
    @Html.TextBoxFor(Function(model) model.Quotation_Id, New With {.id = "tb_quotationId", .style = "display:none;"})
    @Html.TextBoxFor(Function(model) model.Item_list, New With {.id = "tb_itemList", .style = "display:none;"})

</form>

