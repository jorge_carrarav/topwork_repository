﻿function IsBasicKey(event) {
    var key = window.event ? event.keyCode : event.which;
    
    if (key == 8 || //Borrar
        key == 9 || //Tab
        key == 13 || //Enter
        key == 27 || //Escape
        key == 20 || //Mayúscula
        key == 35 || //Inicio
        key == 36 || //Fin
        key == 46 || //Suprimir
        key == 37 || //Izquierda
        key == 38 || //Arriba
        key == 39 || //Derecha
        key == 40) //Abajo
        return true
    else
        return false;
}
function IsNumber(event) {

    var key = window.event ? event.keyCode : event.which;
    
    if (IsBasicKey(event))
        return true;
    if (key == 144 || key == 17) { // 17 -> Control
        return true;
    }
    else if (key > 47 && key < 58) { //Numérico teclado superior
        return true;
    }
    else if (key > 95 && key < 106) { //Numérico teclado derecha
        return true;
    }
    else {
        return false;
    }

}


function IsNumber_or_negative(event) {

    var key = window.event ? event.keyCode : event.which;

    if (key == 189)  //signo negativo
        return true;
    else
        return IsNumber(event);

}

function IsNumber_or_point(event) {

    var key = window.event ? event.keyCode : event.which;
    
    if (key == 110 || key == 190)  //Punto
        return true;
    else
        return IsNumber(event);

}

function IsNumber_or_dash(event) {

    var key = window.event ? event.keyCode : event.which;

    if (event.keyCode == 189)
        return true;
    else
        return IsNumber(event);
    
}

function thousandSeparator(value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function roundNumber(number) {
    return Math.round(number);
}

function sortJsonArrayByKey_number(array, key) {
    return array.sort(function (a, b) {
        var x = Number(a[key]);
        var y = Number(b[key]);

        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        
    });

}

function cloneObject(obj) {
    if (obj == null || typeof (obj) != 'object')
        return obj;

    var cloneResult = obj.constructor(); // changed

    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            cloneResult[key] = cloneObject(obj[key]);
        }
    }
    return cloneResult;
}




//Revisa que la tecla presionada sea un Número y la cambia
function checkIfNumber_thousandSeparator(id, e) {
    var objectToCheck = document.getElementById(id);
    var quantity = objectToCheck.value.replace(".", "").replace(".", "").replace(".", "");

    objectToCheck.value = thousandSeparator(quantity);

    if (!IsNumber(e))
        objectToCheck.value = "";
}


//Revisa que la tecla presionada sea un Número y la cambia
function checkIfNumberOrNegative_thousandSeparator(id, e) {
    var objectToCheck = document.getElementById(id);
    var quantity = objectToCheck.value.replace(".", "").replace(".", "").replace(".", "");

    objectToCheck.value = thousandSeparator(quantity);

    if (!IsNumber_or_negative(e))
        objectToCheck.value = "";
}

//Revisa que la tecla presionada sea un Número y la cambia
function checkIfNumber_or_point(id, e) {
    var objectToCheck = document.getElementById(id);
    var quantity = objectToCheck.value.replace(".", "").replace(".", "").replace(".", "");

    if (!IsNumber_or_point(e)) {
        
        objectToCheck.value = "";
    }

}

//Se usa con keyUp | onkeyup="checkIfNumber_thousandSeparator(this.id, event)"
function checkIfNumber(id, e) {
    var objectToCheck = document.getElementById(id);
    
    if (!IsNumber(e)) {
    
        objectToCheck.value = "";

    }
    
}


//Se usa con keyUp | onkeyup="checkIfNumber_thousandSeparator(this.id, event)"
function checkIfNumber_or_dash(id, e) {
    var objectToCheck = document.getElementById(id);

    if (!IsNumber_or_dash(e))
        objectToCheck.value = "";
}

function removeNewLine_trim(txt) {
    return txt.replace(/(\r\n|\n|\r)/gm, "").toString().trim();
}
function removePoints(txt) {
    return txt.replace(".", "").replace(".", "").replace(".", "").replace(".", "").replace(".", "").replace(".", "").replace(".", "").replace(".", "").replace(".", "").replace(".", "").replace(".", "").replace(".", "").replace(".", "").replace(".", "");
}

function setDateFormat(id) {
    document.getElementById(id).value = document.getElementById(id).value.replace("/", "-").replace("/", "-");
}

function getFormatDate(dateTxt) {
    dateTxt = dateTxt.toString().replace("/", "-").toString().replace("/", "-");
    
    var day = dateTxt.split("-")[0];
    var month = dateTxt.split("-")[1];
    var year = dateTxt.split("-")[2];

    if (day.length == 1)
        day = "0" + day;
    if (month.length == 1)
        month = "0" + month;

    return day + "-" + month + "-" + year;

}