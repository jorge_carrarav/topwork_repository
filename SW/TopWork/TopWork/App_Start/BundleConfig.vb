﻿Imports System.Web
Imports System.Web.Optimization

Public Class BundleConfig
    ' For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
    Public Shared Sub RegisterBundles(ByVal bundles As BundleCollection)


        bundles.Add(New ScriptBundle("~/Scripts/jquery").Include(
                    "~/Scripts/jquery.js",
                    "~/Scripts/jquery-ui.js"))

        bundles.Add(New ScriptBundle("~/Scripts/zmjquery").Include("~/Scripts/zm/zm.jquery.js"))
        bundles.Add(New ScriptBundle("~/Scripts/Tools").Include("~/Scripts/Tools.js"))
        bundles.Add(New ScriptBundle("~/Scripts/JQuery_Settings").Include("~/Scripts/JQuery_Settings.js"))

        bundles.Add(New ScriptBundle("~/Scripts/WorkPage").Include("~/Scripts/WorkPage_1_general.js",
                                                                   "~/Scripts/WorkPage_2_material.js",
                                                                   "~/Scripts/WorkPage_3_waybill.js",
                                                                   "~/Scripts/WorkPage_4_invoice.js"))


        bundles.Add(New StyleBundle("~/Styles/Sites").Include("~/Styles/Sites.css"))
        bundles.Add(New StyleBundle("~/Styles/Layout").Include("~/Styles/Layout.css"))
        bundles.Add(New StyleBundle("~/Styles/Login").Include("~/Styles/Login.css"))
        bundles.Add(New StyleBundle("~/Styles/jquery").Include("~/Styles/jquery-ui.css"))
        bundles.Add(New StyleBundle("~/Styles/WebGridTables").Include("~/Styles/WebGridTables.css"))
        bundles.Add(New StyleBundle("~/Styles/WorkPage").Include("~/Styles/WorkPage.css"))


        bundles.Add(New StyleBundle("~/Styles/ZMStyle").Include("~/Styles/zm_style/quarry.css"))


    End Sub
End Class