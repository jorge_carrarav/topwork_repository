﻿Imports Microsoft.Web.WebPages.OAuth

Imports System.Data.Entity
Imports System.Data.Entity.Infrastructure
Imports System.Threading
Imports WebMatrix.WebData


Public Class AuthConfig

    Private Shared _initializer As SimpleMembershipInitializer
    Private Shared _initializerLock As New Object
    Private Shared _isInitialized As Boolean

    Public Shared Sub RegisterAuth()

        LazyInitializer.EnsureInitialized(_initializer, _isInitialized, _initializerLock)


        ' To let users of this site log in using their accounts from other sites such as Microsoft, Facebook, and Twitter,
        ' you must update this site. For more information visit http://go.microsoft.com/fwlink/?LinkID=252166

        ' OAuthWebSecurity.RegisterMicrosoftClient(
        '     clientId:="",
        '     clientSecret:="")

        ' OAuthWebSecurity.RegisterTwitterClient(
        '     consumerKey:="",
        '     consumerSecret:="")

        ' OAuthWebSecurity.RegisterFacebookClient(
        '     appId:="",
        '     appSecret:="")

        ' OAuthWebSecurity.RegisterGoogleClient()
    End Sub

    Private Class SimpleMembershipInitializer
        Public Sub New()
            Database.SetInitializer(Of UsersContext)(Nothing)

            Try
                Using context As New UsersContext()
                    If Not context.Database.Exists() Then
                        ' Create the SimpleMembership database without Entity Framework migration schema
                        CType(context, IObjectContextAdapter).ObjectContext.CreateDatabase()
                    End If
                End Using

                WebSecurity.InitializeDatabaseConnection("TopWorkConnection", "UserProfile", "UserId", "UserName", autoCreateTables:=True)
            Catch ex As Exception
                Throw New InvalidOperationException("The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588", ex)
            End Try
        End Sub
    End Class

End Class