﻿Imports System.Security.Cryptography
Public Class Tools : Inherits Context


    Function GetResultMessage(ByVal node As String, ByVal result As String) As String

        If (node = "Quotation") Then
            If (result = "SuccessCreation") Then
                Return "Cotización creada con éxito"
            ElseIf (result = "SuccessEdition") Then
                Return "Cotización guardada con éxito"
            ElseIf (result = "SuccessDeletion") Then
                Return "Cotización eliminada con éxito"
            ElseIf (result = "CurrentVersion") Then
                Return "Versión actual"
            End If
        ElseIf (node = "PurchaseOrder") Then
            If (result = "SuccessCreation") Then
                Return "Orden de Compra creada con éxito"
            ElseIf (result = "SuccessEdition") Then
                Return "Orden de Compra guardada con éxito"
            ElseIf (result = "SuccessDeletion") Then
                Return "Orden de Compra eliminada con éxito"
            End If
        ElseIf (node = "WorkOrder") Then
            If (result = "SuccessCreation") Then
                Return "Orden Trabajo creado con éxito"
            ElseIf (result = "SuccessEdition") Then
                Return "Orden de Trabajo guardado con éxito"
            ElseIf (result = "SuccessDeletion") Then
                Return "Orden de Trabajo eliminado con éxito"
            End If
        ElseIf (node = "Supplier") Then
            If (result = "SuccessSupplierCreation") Then
                Return "Proveedor creado con éxito."
            End If

        ElseIf (node = "Client") Then

            If (result = "SuccessClientCreation") Then
                Return "Cliente creado con éxito."
            ElseIf (result = "ClientHasQuotationsImpossibleToDelete") Then
                Return "El cliente posee cotizaciones. Imposible eliminar."
            End If

        ElseIf (node = "WorkType") Then

            If (result = "SuccessWorkTypeCreation") Then
                Return "Tipo de trabajo creado con éxito."
            ElseIf (result = "WorkTypeHasQuotationsImpossibleToDelete") Then
                Return "El tipo de trabajo se ha usado en cotizaciones. Imposible eliminar."
            End If

        ElseIf (node = "MethodOfPayment") Then

            If (result = "SuccessMethodOfPaymentCreation") Then
                Return "Forma de pago creada con éxito."
            ElseIf (result = "MethodOfPaymentHasQuotationsImpossibleToDelete") Then
                Return "La forma de pago se ha usado en cotizaciones. Imposible eliminar."
            End If

        ElseIf (node = "User") Then

            If (result = "CannotDeleteAdmin") Then
                Return "No es posible eliminar usuario administrador."
            ElseIf (result = "SuccessUserCreation") Then
                Return "Usuario creado con éxito."
            ElseIf (result = "UserHasQuotationsImpossibleToDelete") Then
                Return "El usuario ha creado cotizaciones. Imposible eliminar."
            End If

        End If

        If (result = "SuccessDeletion") Then
            Return "Eliminado con éxito."
        ElseIf (result = "ErrorTryingToDelete") Then
            Return "Error al intentar eliminar."
        End If




    End Function

    Public Function IsValidRut(ByVal rutNumber As Integer, ByVal rutNValidator As String) As Boolean

        Dim rutNumber_str = rutNumber.ToString

        Dim result As Integer
        Dim counter As Integer = 2

        Dim i As Integer

        For i = rutNumber_str.Length - 1 To 0 Step -1

            If counter > 7 Then
                counter = 2
            End If

            result += Integer.Parse(rutNumber_str(i).ToString()) * counter
            counter += 1
        Next

        Dim dv As Integer = 11 - (result Mod 11)
        Dim nVal As String = dv.ToString()

        If nVal = "10" Then
            nVal = "K"
        End If

        If nVal = "11" Then
            nVal = "0"
        End If

        If nVal = rutNValidator.ToUpper Then
            Return True
        Else
            Return False
        End If

    End Function


    Function EncryptData(ByVal data As String) As String

        Dim encryption As String = String.Empty
        Dim encode As Byte() = New Byte(data.Length - 1) {}
        encode = Encoding.UTF8.GetBytes(data)
        encryption = Convert.ToBase64String(encode)
        Return encryption

    End Function

    Function ThousandSeparator(ByVal value As String) As String

        Dim value_int As Long = Convert.ToInt64(value)

        Return String.Format("{0:#,##0}", value_int).Replace(",", ".")


    End Function

    Function GetFormattedDate(ByVal oldDate As DateTime) As String

        Dim day As String = oldDate.Day
        Dim month As String = oldDate.Month
        Dim year As String = oldDate.Year

        If day.Length = 1 Then
            day = "0" + day
        End If
        If month.Length = 1 Then
            month = "0" + month
        End If

        Return day + "-" + month + "-" + year

    End Function

    Function GetCurrentIVAPercentage() As Double


        Dim currentIVAPercentage As Double = (From c In _context.AppConfiguration
                                       Select c.IVAPercentage).FirstOrDefault()

        Return currentIVAPercentage

    End Function

End Class
