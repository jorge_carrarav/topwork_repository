﻿Imports TopWork.ProductOrServiceDataProvider

Public Class QuotationProductOrServiceDataProvider : Inherits Context



    Function InsertQuotProdOrServ(ByVal newQuotProdOrServ As QuotationProductOrService)

        _context.QuotationProductOrService.Add(newQuotProdOrServ)
        _context.SaveChanges()

    End Function

    Function GetAllProductOrServiceQ_Item_by_quotId_version(ByVal quotId As Integer, ByVal version As String) As List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item)


        Dim prodOrServDP As New ProductOrServiceQDataProvider
        Dim unitOfMeasureDP As New UnitOfMeasureQDataProvider

        Dim query As List(Of QuotationProductOrService) = (From qps In _context.QuotationProductOrService
                                                           Where qps.Quotation_id = quotId _
                                                           And qps.QuotaionVersion = version
                                                           Order By qps.ItemNumber Ascending
                                                           Select qps).ToList

        Dim prodOrServ_item_list As New List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item)

        For Each quotProdServ As QuotationProductOrService In query

            Dim prodOrServ_item As New ProductOrServiceQDataProvider.ProductOrServiceQ_Item

            prodOrServ_item.ProductOrServiceQ_Id = quotProdServ.ProductOrServiceQ_Id
            prodOrServ_item.ItemNumber = quotProdServ.ItemNumber
            prodOrServ_item.Description = prodOrServDP.GetProductOrServiceQ_by_id(quotProdServ.ProductOrServiceQ_Id).Description.Trim()
            prodOrServ_item.Quantity = quotProdServ.Quantity
            prodOrServ_item.UnitValue = quotProdServ.UnitValue
            prodOrServ_item.UnitOfMeasure = unitOfMeasureDP.GetUnitOfMeasureQ_by_id(quotProdServ.UnitOfMeasureQ_Id).ShortName

            prodOrServ_item_list.Add(prodOrServ_item)

        Next

        Return prodOrServ_item_list

    End Function

    Function GetAllProductOrServiceQ_Item_by_quotId_version_quotProdOrServList(ByVal quotId As Integer, ByVal version As String, ByVal quotProdOrServList As List(Of QuotationProductOrService)) As List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item)


        Dim prodOrServDP As New ProductOrServiceQDataProvider

        Dim query As List(Of QuotationProductOrService) = (From qps In quotProdOrServList
                                                           Where qps.Quotation_id = quotId _
                                                           And qps.QuotaionVersion = version
                                                           Order By qps.ItemNumber Ascending
                                                           Select qps).ToList

        Dim prodOrServ_item_list As New List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item)

        For Each quotProdServ As QuotationProductOrService In query

            Dim prodOrServ_item As New ProductOrServiceQDataProvider.ProductOrServiceQ_Item

            prodOrServ_item.ProductOrServiceQ_Id = quotProdServ.ProductOrServiceQ_Id
            prodOrServ_item.ItemNumber = quotProdServ.ItemNumber
            prodOrServ_item.Description = prodOrServDP.GetProductOrServiceQ_by_id(quotProdServ.ProductOrServiceQ_Id).Description.Trim()
            prodOrServ_item.Quantity = quotProdServ.Quantity
            prodOrServ_item.UnitValue = quotProdServ.UnitValue

            prodOrServ_item_list.Add(prodOrServ_item)

        Next

        Return prodOrServ_item_list

    End Function

    Function GetAllProductOrService_by_quotId_version(ByVal quotId As Integer, ByVal version As String) As List(Of QuotationProductOrService)


        Dim prodOrServDP As New ProductOrServiceQDataProvider

        Dim quotProdOrServ_list As List(Of QuotationProductOrService) = (From qps In _context.QuotationProductOrService
                                                           Where qps.Quotation_id = quotId _
                                                           And qps.QuotaionVersion = version
                                                           Order By qps.ItemNumber Ascending
                                                           Select qps).ToList


        Return quotProdOrServ_list

    End Function

    Function GetQuotationValues_by_quotation(ByVal quotation As Quotation)

        Dim prodOrServ_item_list As List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item) = Me.GetAllProductOrServiceQ_Item_by_quotId_version(quotation.Quotation_id, quotation.Version)

        Dim netValue_total As Integer = 0

        For Each item As ProductOrServiceQDataProvider.ProductOrServiceQ_Item In prodOrServ_item_list

            Dim netValue_int As Integer = item.Quantity * item.UnitValue
            netValue_total += netValue_int

        Next

        Dim ivaPercentage As Double = quotation.IVAPercentage
        Dim ivaValue As Integer = Math.Round(netValue_total * ivaPercentage / 100)
        Dim totalValue As Integer = netValue_total + ivaValue

        Dim quotation_values As New ValuesModel

        quotation_values.NetValue = netValue_total
        quotation_values.IVAValue = ivaValue
        quotation_values.TotalValue = totalValue

        Return quotation_values

    End Function


    Function GetQuotationValues_by_quotation_quotProdOrServList(ByVal quotation As Quotation, ByVal quotProdOrServList As List(Of QuotationProductOrService))

        Dim prodOrServ_item_list As List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item) = Me.GetAllProductOrServiceQ_Item_by_quotId_version_quotProdOrServList(quotation.Quotation_id, quotation.Version, quotProdOrServList)

        Dim netValue_total As Integer = 0

        For Each item As ProductOrServiceQDataProvider.ProductOrServiceQ_Item In prodOrServ_item_list

            Dim netValue_int As Integer = item.Quantity * item.UnitValue
            netValue_total += netValue_int

        Next

        Dim ivaPercentage As Double = quotation.IVAPercentage
        Dim ivaValue As Integer = Math.Round(netValue_total * ivaPercentage / 100)
        Dim totalValue As Integer = netValue_total + ivaValue

        Dim quotation_values As New ValuesModel

        quotation_values.NetValue = netValue_total
        quotation_values.IVAValue = ivaValue
        quotation_values.TotalValue = totalValue

        Return quotation_values

    End Function



End Class
