﻿Public Class SupplierDataProvider : Inherits Context


    Function GetAllSuppliers()

        Dim query As List(Of Supplier)

        query = (From p In _context.Supplier
                Order By p.RutNumber.Length, p.RutNumber Ascending
                Select p).ToList


        Dim formatted_list As New List(Of Supplier_List_Formatted)

        For Each supp As Supplier In query

            Dim newProv As New Supplier_List_Formatted

            newProv.Supplier_Id = supp.Supplier_Id
            newProv.Rut = New Tools().ThousandSeparator(supp.RutNumber) + "-" + supp.RutNValidator
            newProv.Name = supp.Name
            newProv.ServiceType = supp.ServiceType
            newProv.Phone = supp.Phone
            newProv.Email = supp.Email
            newProv.Address = supp.Address
            newProv.City = supp.City

            formatted_list.Add(newProv)

        Next

        Return formatted_list

    End Function


    Function GetActiveSupplier_by_rutNumber(ByVal rutNumber As String) As Supplier


        Dim query As Supplier = (From p In _context.Supplier
                                 Where p.RutNumber = rutNumber _
                                    And p.Active = True
                                 Select p).FirstOrDefault()
        Return query

    End Function

    Function GetActiveSupplier_by_name(ByVal name As String) As Supplier


        Dim query As Supplier = (From p In _context.Supplier
                                 Where p.Name.ToLower = name.ToLower _
                                 And p.Active = True
                                 Select p).FirstOrDefault()
        Return query

    End Function

    Function GetActiveSupplier_by_supplierId(ByVal supplierId As Integer) As Supplier


        Dim query As Supplier = (From s In _context.Supplier
                                 Where s.Supplier_Id = supplierId _
                                 And s.Active = True
                                 Select s).FirstOrDefault()
        Return query

    End Function

    Function GetAllSuppliers_by_rutNumber_active(ByVal rutNumber As String) As List(Of Supplier_List_Formatted)


        Dim query As List(Of Supplier) = (From p In _context.Supplier
                                        Where p.RutNumber.Contains(rutNumber) _
                                        And p.Active = True
                                        Order By p.RutNumber.Length, p.RutNumber Ascending
                                        Select p).ToList


        Dim formatted_list As New List(Of Supplier_List_Formatted)

        For Each supp As Supplier In query

            Dim newProv As New Supplier_List_Formatted

            newProv.Supplier_Id = supp.Supplier_Id
            newProv.Rut = New Tools().ThousandSeparator(supp.RutNumber) + "-" + supp.RutNValidator
            newProv.Name = supp.Name
            newProv.ServiceType = supp.ServiceType
            newProv.Phone = supp.Phone
            newProv.Email = supp.Email
            newProv.Address = supp.Address
            newProv.City = supp.City

            formatted_list.Add(newProv)

        Next

        Return formatted_list

    End Function

    Function GetAllActiveSuppliers_formattedForList() As List(Of Supplier_List_Formatted)


        Dim query As List(Of Supplier) = (From s In _context.Supplier
                                             Where s.Active = True
                                            Order By s.RutNumber.Length, s.RutNumber Ascending
                                            Select s).ToList

        Dim formatted_list As New List(Of Supplier_List_Formatted)

        For Each sup As Supplier In query

            Dim newSupp As New Supplier_List_Formatted

            newSupp.Supplier_Id = sup.Supplier_Id
            newSupp.Rut = New Tools().ThousandSeparator(sup.RutNumber) + "-" + sup.RutNValidator
            newSupp.Name = sup.Name
            newSupp.ServiceType = sup.ServiceType
            newSupp.Phone = sup.Phone
            newSupp.Email = sup.Email
            newSupp.Address = sup.Address
            newSupp.City = sup.City

            formatted_list.Add(newSupp)

        Next

        Return formatted_list

    End Function

    Function GetAllActiveSuppliers_by_name(ByVal name As String) As List(Of Supplier_List_Formatted)


        Dim query As List(Of Supplier) = (From p In _context.Supplier
                                            Where p.Name.ToLower.Contains(name.ToLower) _
                                            And p.Active = True
                                            Order By p.RutNumber.Length, p.RutNumber Ascending
                                            Select p).ToList

        Dim formatted_list As New List(Of Supplier_List_Formatted)

        For Each supp As Supplier In query

            Dim newProv As New Supplier_List_Formatted

            newProv.Supplier_Id = supp.Supplier_Id
            newProv.Rut = New Tools().ThousandSeparator(supp.RutNumber) + "-" + supp.RutNValidator
            newProv.Name = supp.Name
            newProv.ServiceType = supp.ServiceType
            newProv.Phone = supp.Phone
            newProv.Email = supp.Email
            newProv.Address = supp.Address
            newProv.City = supp.City

            formatted_list.Add(newProv)

        Next

        Return formatted_list

    End Function

    Function InsertSupplier(ByVal newSupplier As Supplier) As Integer



        _context.Supplier.Add(newSupplier)
        _context.SaveChanges()

        Return newSupplier.Supplier_Id

    End Function

    Function UpdateSupplier(ByVal newSupplier As Supplier)



        Dim oldSupplier As Supplier = (From p In _context.Supplier
                                       Where p.Supplier_Id = newSupplier.Supplier_Id
                                       Select p).FirstOrDefault()

        oldSupplier.RutNumber = newSupplier.RutNumber
        oldSupplier.RutNValidator = newSupplier.RutNValidator
        oldSupplier.Name = newSupplier.Name
        oldSupplier.ServiceType = newSupplier.ServiceType
        oldSupplier.Phone = newSupplier.Phone
        oldSupplier.Email = newSupplier.Email
        oldSupplier.Address = newSupplier.Address
        oldSupplier.City = newSupplier.City

        _context.SaveChanges()

    End Function

    Function DeleteSupplier_by_id(ByVal suppId As Integer)


        Dim query = (From w In _context.Supplier
                     Where w.Supplier_Id = suppId
                     Select w).FirstOrDefault()

        query.Active = False

        _context.SaveChanges()
    End Function

    Function GetSupplier_by_id(ByVal suppId As Integer) As Supplier

        Dim query As Supplier = (From p In _context.Supplier
                                 Where p.Supplier_Id = suppId
                                 Select p).FirstOrDefault()
        Return query

    End Function

    Function GetSupplier_by_rutNumber(ByVal rutNumber As String) As Supplier

        Dim query As Supplier = (From p In _context.Supplier
                                 Where p.RutNumber = rutNumber
                                 Select p).FirstOrDefault()
        Return query

    End Function

    Function GetSupplier_by_rutNumber_butNotThisId(ByVal rutNumber As String, ByVal butNotThisId As Integer) As Supplier

        Dim query As Supplier = (From p In _context.Supplier
                                 Where p.RutNumber = rutNumber _
                                 And p.Supplier_Id <> butNotThisId
                                 Select p).FirstOrDefault()
        Return query

    End Function

    Function GetSupplier_by_name(ByVal name As String) As Supplier

        Dim query As Supplier = (From p In _context.Supplier
                                 Where p.Name.ToLower = name.ToLower
                                 Select p).FirstOrDefault()
        Return query

    End Function

    Function GetSupplier_by_name_butNotThisId(ByVal name As String, ByVal butNotThisId As Integer) As Supplier


        Dim query As Supplier = (From p In _context.Supplier
                                 Where p.Name.ToLower = name.ToLower _
                                 And p.Supplier_Id <> butNotThisId
                                 Select p).FirstOrDefault()
        Return query

    End Function


    Public Class Supplier_List_Formatted

        Public Property Supplier_Id As Integer
        Public Property Rut As String
        Public Property Name As String
        Public Property ServiceType As String
        Public Property Phone As String
        Public Property Email As String
        Public Property Address As String
        Public Property City As String

    End Class


End Class
