﻿Public Class ClientDataProvider : Inherits Context




    Function InsertClient(ByVal newClient As Client) As Integer

        _context.Client.Add(newClient)
        _context.SaveChanges()

        Return newClient.Client_Id

    End Function

    Function UpdateClient(ByVal newClient As Client)


        Dim oldClient As Client = (From p In _context.Client
                                       Where p.Client_Id = newClient.Client_Id
                                       Select p).FirstOrDefault()

        oldClient.RutNumber = newClient.RutNumber
        oldClient.RutNValidator = newClient.RutNValidator
        oldClient.Name = newClient.Name
        oldClient.ServiceType = newClient.ServiceType
        oldClient.Phone = newClient.Phone
        oldClient.EMail = newClient.EMail
        oldClient.Address = newClient.Address
        oldClient.City = newClient.City

        _context.SaveChanges()

    End Function

    Function DeleteClient_by_id(ByVal clieId As Integer)


        Dim query = (From w In _context.Client
                     Where w.Client_Id = clieId
                     Select w).FirstOrDefault()

        query.Active = False
        _context.SaveChanges()
    End Function


    Function ClientHasQuotations_by_id(ByVal clientId As Integer)


        Dim query = (From q In _context.Quotation
                     Where q.Client_Id = clientId _
                     And q.Active
                     Select q).FirstOrDefault()

        If (query IsNot Nothing) Then
            Return True
        Else
            Return False

        End If

    End Function

    Function GetActiveClient_by_clientId(ByVal clientId As Integer) As Client


        Dim query As Client = (From c In _context.Client
                                 Where c.Client_Id = clientId _
                                 And c.Active = True
                                 Select c).FirstOrDefault()
        Return query

    End Function

    Function GetClient_by_quotationId(ByVal quotId As Integer) As Client


        Dim query As Client = (From c In _context.Client
                               Join q In _context.Quotation On q.Client_Id Equals c.Client_Id
                                 Where q.Quotation_id = quotId
                                 Select c).FirstOrDefault()
        Return query

    End Function

    Function GetActiveClient_by_rutNumber(ByVal rutNumber As String) As Client


        Dim query As Client = (From p In _context.Client
                                 Where p.RutNumber = rutNumber _
                                    And p.Active = True
                                 Select p).FirstOrDefault()
        Return query

    End Function

    Function GetActiveClient_by_rutNumber_butNotThisId(ByVal rutNumber As String, ByVal butNotThisId As Integer) As Client


        Dim query As Client = (From p In _context.Client
                                 Where p.RutNumber = rutNumber _
                                 And p.Client_Id <> butNotThisId _
                                 And p.Active = True
                                 Select p).FirstOrDefault()
        Return query

    End Function

    Function GetActiveClient_by_name(ByVal name As String) As Client


        Dim query As Client = (From p In _context.Client
                                 Where p.Name.ToLower = name.ToLower _
                                 And p.Active = True
                                 Select p).FirstOrDefault()
        Return query

    End Function

    Function GetAllActiveClients() As List(Of Client)



        Dim clients_list As List(Of Client) = (From c In _context.Client
                                               Where c.Active = True
                                               Select c).ToList()

        Return clients_list

    End Function

    Function GetClient_by_name_butNotThisId(ByVal name As String, ByVal butNotThisId As Integer) As Client


        Dim query As Client = (From p In _context.Client
                                 Where p.Name.ToLower = name.ToLower _
                                 And p.Client_Id <> butNotThisId
                                 Select p).FirstOrDefault()
        Return query

    End Function


    Function GetAllActiveClients_formattedForList() As List(Of Client_List_Formatted)


        Dim query As List(Of Client)

        query = (From c In _context.Client
                 Where c.Active = True
                Order By c.RutNumber.Length, c.RutNumber Ascending
                Select c).ToList

        Dim formatted_list As New List(Of Client_List_Formatted)

        For Each clie As Client In query

            Dim newClien As New Client_List_Formatted

            newClien.Client_Id = clie.Client_Id
            newClien.Rut = New Tools().ThousandSeparator(clie.RutNumber) + "-" + clie.RutNValidator
            newClien.Name = clie.Name
            newClien.ServiceType = clie.ServiceType
            newClien.Phone = clie.Phone
            newClien.Email = clie.EMail
            newClien.Address = clie.Address
            newClien.City = clie.City

            formatted_list.Add(newClien)

        Next

        Return formatted_list

    End Function

    Function GetAllClients_by_rutNumber(ByVal rutNumber As String) As List(Of Client_List_Formatted)


        Dim query As List(Of Client) = (From c In _context.Client
                                        Where c.RutNumber.Contains(rutNumber) _
                                        And c.Active = True
                                        Order By c.RutNumber.Length, c.RutNumber Ascending
                                        Select c).ToList

        Dim formatted_list As New List(Of Client_List_Formatted)

        For Each clie As Client In query

            Dim newProv As New Client_List_Formatted

            newProv.Client_Id = clie.Client_Id
            newProv.Rut = New Tools().ThousandSeparator(clie.RutNumber) + "-" + clie.RutNValidator
            newProv.Name = clie.Name
            newProv.ServiceType = clie.ServiceType
            newProv.Phone = clie.Phone
            newProv.Email = clie.EMail
            newProv.Address = clie.Address
            newProv.City = clie.City

            formatted_list.Add(newProv)

        Next

        Return formatted_list

    End Function


    Function GetAllClients_by_name(ByVal name As String) As List(Of Client_List_Formatted)


        Dim query As List(Of Client) = (From c In _context.Client
                                        Where c.Name.ToLower.Contains(name.ToLower) _
                                        And c.Active = True
                                        Order By c.RutNumber.Length, c.RutNumber Ascending
                                        Select c).ToList

        Dim formatted_list As New List(Of Client_List_Formatted)

        For Each clie As Client In query

            Dim newProv As New Client_List_Formatted

            newProv.Client_Id = clie.Client_Id
            newProv.Rut = New Tools().ThousandSeparator(clie.RutNumber) + "-" + clie.RutNValidator
            newProv.Name = clie.Name
            newProv.ServiceType = clie.ServiceType
            newProv.Phone = clie.Phone
            newProv.Email = clie.EMail
            newProv.Address = clie.Address
            newProv.City = clie.City

            formatted_list.Add(newProv)

        Next

        Return formatted_list

    End Function


    Public Class Client_List_Formatted


        Public Property Client_Id As Integer
        Public Property Rut As String
        Public Property Name As String
        Public Property ServiceType As String
        Public Property Phone As String
        Public Property Email As String
        Public Property Address As String
        Public Property City As String

    End Class


End Class
