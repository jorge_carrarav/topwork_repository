﻿Public Class UnitOfMeasurePODataProvider : Inherits Context



    Function GetUnitOfMeasurePO_by_shortName(ByVal shortName As String) As UnitOfMeasurePO

        Dim unitOfMeasure As UnitOfMeasurePO = (From u In _context.UnitOfMeasurePO
                                                      Where u.ShortName = shortName
                                                      Select u).FirstOrDefault()

        Return unitOfMeasure

    End Function

    Function GetUnitOfMeasurePO_by_fullName(ByVal fullName As String) As UnitOfMeasurePO

        Dim unitOfMeasure As UnitOfMeasurePO = (From u In _context.UnitOfMeasurePO
                                                      Where u.FullName = fullName
                                                      Select u).FirstOrDefault()

        Return unitOfMeasure

    End Function

    Function GetUnitOfMeasurePO_by_id(ByVal unitOfMeasId As Integer) As UnitOfMeasurePO

        Dim unitOfMeasure As UnitOfMeasurePO = (From u In _context.UnitOfMeasurePO
                                                      Where u.UnitOfMeasurePO_Id = unitOfMeasId
                                                      Select u).FirstOrDefault()

        Return unitOfMeasure

    End Function

    Function GetAllActiveUnitOfMeasurePO() As List(Of UnitOfMeasurePO)

        Dim unitOfMeasure_list As List(Of UnitOfMeasurePO) = (From u In _context.UnitOfMeasurePO
                                                                      Where u.Active = True
                                                                      Select u).ToList

        Return unitOfMeasure_list

    End Function

    Function GetAllActiveUnitOfMeasurePO_shortName() As List(Of String)

        Dim unitOfMeas_shortName_list As List(Of String) = (From u In _context.UnitOfMeasurePO
                                                                      Where u.Active = True
                                                                      Order By u.ShortName Ascending
                                                                      Select u.ShortName).ToList

        Return unitOfMeas_shortName_list

    End Function


End Class
