﻿Public Class ProductOrServiceQDataProvider : Inherits Context

    Function InsertProductOrServiceQ(ByVal prodOrServ As ProductOrServiceQ)

        _context.ProductOrServiceQ.Add(prodOrServ)
        _context.SaveChanges()

    End Function


    Function GetAllProductOrServiceQ() As List(Of ProductOrServiceQ)

        Dim prodOrServ_list As List(Of ProductOrServiceQ) = (From p In _context.ProductOrServiceQ
                                                            Select p).ToList()

        Return prodOrServ_list

    End Function


    Function GetProductOrServiceQ_by_id(ByVal prodOrServId As Integer) As ProductOrServiceQ

        Dim query = (From p In _context.ProductOrServiceQ
                     Where p.ProductOrServiceQ_Id = prodOrServId
                     Select p).FirstOrDefault()

        Return query

    End Function

    Function GetProductOrServiceQ_by_description(ByVal description As String) As ProductOrServiceQ



        Dim query = (From p In _context.ProductOrServiceQ
                     Where p.Description = description
                     Select p).FirstOrDefault()

        Return query

    End Function


    Public Class ProductOrServiceQ_Item

        Public Property ProductOrServiceQ_Id As String
        Public Property ItemNumber As String
        Public Property Description As String
        Public Property Quantity As Integer
        Public Property UnitValue As Integer
        Public Property UnitOfMeasure As String

    End Class

End Class
