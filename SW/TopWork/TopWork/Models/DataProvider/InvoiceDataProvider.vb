﻿Public Class InvoiceDataProvider : Inherits Context




    Function InsertInvoice(ByVal newInvoice As Invoice)

        _context.Invoice.Add(newInvoice)
        _context.SaveChanges()

        Return newInvoice.Invoice_Id

    End Function

    Function GetAllInvoices() As List(Of Invoice)

        Dim invoice_list As List(Of Invoice) = (From i In _context.Invoice
                                                Select i).ToList()

        Return invoice_list

    End Function

    Function AreAllInvoicePaid_by_workId(ByVal workId As Integer) As Boolean

        Dim invoice_list As List(Of Invoice) = (From i In _context.Invoice
                                                Where i.WorkOrder_Id = workId
                                                Select i).ToList

        If (invoice_list.Count = 0) Then

            Return False

        Else

            Dim paidInvoices_list As List(Of Invoice) = (From i In invoice_list
                                                         Where i.IsPaid = False _
                                                         And i.WorkOrder_Id = workId
                                                         Select i).ToList()

            If (paidInvoices_list.Count > 0) Then
                Return False
            Else
                Return True
            End If

        End If

    End Function


    Function GetAllInvoices_addEdit_by_workId(ByVal workId As Integer) As List(Of Invoice_AddEdit_Formatted)

        Dim tool As New Tools

        Dim invoice_list As List(Of Invoice) = (From w In _context.Invoice
                                                 Where w.WorkOrder_Id = workId
                                                 Select w).ToList()
        Dim invoiceWayBills_list As List(Of InvoiceWayBill) = (From i In _context.InvoiceWayBill
                                                                Select i).ToList()
        Dim wayBills_list As List(Of WayBill) = (From i In _context.WayBill
                                                Select i).ToList()
        Dim prodOrServ_list As List(Of ProductOrServiceQ) = (From p In _context.ProductOrServiceQ
                                                            Select p).ToList()


        Dim invoiceAddEditFormatted_list As New List(Of InvoiceDataProvider.Invoice_AddEdit_Formatted)


        For Each inv As Invoice In invoice_list

            Dim invoiceAddEditFormatted As New InvoiceDataProvider.Invoice_AddEdit_Formatted
            invoiceAddEditFormatted.InvoiceNumber = inv.InvoiceNumber
            invoiceAddEditFormatted.InvoiceDate = tool.GetFormattedDate(inv.InvoiceDate)
            invoiceAddEditFormatted.IsPaid = inv.IsPaid
            If (inv.Comment Is Nothing) Then
                invoiceAddEditFormatted.Comment = ""
            Else
                invoiceAddEditFormatted.Comment = inv.Comment

            End If

            invoiceAddEditFormatted.NetValue = inv.NetValue.ToString

            Dim invWayBills_thisInvoice_list As List(Of InvoiceWayBill) = (From iw In invoiceWayBills_list
                                                                            Where iw.Invoice_Id = inv.Invoice_Id
                                                                            Select iw).ToList()

            Dim invoiceWayBillsItem_list As New List(Of String)

            For Each invWayBill In invWayBills_thisInvoice_list

                Dim wbNumber As String = (From w In wayBills_list
                                        Where w.WayBill_Id = invWayBill.WayBill_Id
                                        Select w.WayBillNumber).FirstOrDefault()

                invoiceWayBillsItem_list.Add(wbNumber)

            Next

            invoiceAddEditFormatted.WayBillNumber_list = invoiceWayBillsItem_list

            invoiceAddEditFormatted_list.Add(invoiceAddEditFormatted)
        Next

        Return invoiceAddEditFormatted_list

    End Function


    Function DeleteAllInvoices_by_workId(ByVal workId As Integer)

        Dim invoice_list As List(Of Invoice) = (From w In _context.Invoice
                                                Where w.WorkOrder_Id = workId
                                                Select w).ToList()


        For Each invoice As Invoice In invoice_list

            Dim invoiceWayBills_list As List(Of InvoiceWayBill) = (From i In _context.InvoiceWayBill
                                                                    Where i.Invoice_Id = invoice.Invoice_Id
                                                                    Select i).ToList()

            For Each invWayBills As InvoiceWayBill In invoiceWayBills_list

                _context.InvoiceWayBill.Remove(invWayBills)

            Next

            _context.Invoice.Remove(invoice)

        Next

        _context.SaveChanges()

    End Function


    Public Class Invoice_AddEdit_Formatted

        Public Property InvoiceNumber As Integer
        Public Property InvoiceDate As String
        Public Property IsPaid As Boolean
        Public Property Comment As String
        Public Property NetValue As String
        Public Property WayBillNumber_list As List(Of String)

    End Class

End Class
