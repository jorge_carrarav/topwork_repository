﻿Public Class WorkOrderDataProvider : Inherits Context


    Function InsertWorkOrder(ByVal newWorkOrder As WorkOrder)
        If (newWorkOrder.Observation Is Nothing) Then
            newWorkOrder.Observation = ""
        End If

        _context.WorkOrder.Add(newWorkOrder)
        _context.SaveChanges()

    End Function

    Function UpdateWorkOrder(ByVal newWO As WorkOrder)

        Dim oldWO As WorkOrder = (From wo In _context.WorkOrder
                                    Where wo.WorkOrder_Id = newWO.WorkOrder_Id
                                    Select wo).FirstOrDefault()

        If (newWO.Observation Is Nothing) Then
            newWO.Observation = ""
        End If

        oldWO.FinishDate = newWO.FinishDate
        oldWO.PurchaseOrderNumber = newWO.PurchaseOrderNumber
        oldWO.Observation = newWO.Observation

        _context.SaveChanges()

    End Function

    Function GetAllWorkOrders()

        Dim work_list As List(Of WorkOrder) = (From wo In _context.WorkOrder
                                                    Select wo).ToList

        Return work_list

    End Function

    Function GetNewWorkOrderNumber() As Integer

        Dim wo_list = (From q In _context.WorkOrder
                       Select q).ToList

        Dim wo2_list As IEnumerable = wo_list.OrderBy(Function(wo) wo.WorkOrderNumber.Length).ThenBy(Function(wo) wo.WorkOrderNumber).Reverse

        Dim query As Integer = (From q In wo2_list
                                Select q.WorkOrderNumber).FirstOrDefault

        Return query + 1

    End Function

    Function GetActiveWorkOrder_by_quotId(ByVal quotId As Integer) As WorkOrder

        Dim work As WorkOrder = (From w In _context.WorkOrder
                                    Where w.Quotation_id = quotId _
                                    And w.Active = True
                                    Select w).FirstOrDefault


        Return work

    End Function

    Function GetQuotation_by_workId(ByVal workId As Integer) As Quotation

        Dim quotation As Quotation = (From q In _context.Quotation
                                      Join w In _context.WorkOrder On w.Quotation_id Equals q.Quotation_id
                                       Where w.WorkOrder_Id = workId
                                       Select q).FirstOrDefault()

        Return quotation

    End Function

    Function GetWorkOrder_by_Id(ByVal workId As Integer) As WorkOrder

        Dim query As WorkOrder = (From w In _context.WorkOrder
                                    Where w.WorkOrder_Id = workId _
                                    Select w).FirstOrDefault

        Return query

    End Function

    Function GetAllActiveWorkOrders_formattedForList_by_quotNumberVersion(ByVal quotNumberVersion As String) As List(Of WorkOrder_List_Formatted)


        Dim quotVersion As String = ""
        If (quotNumberVersion.Split(".").Count > 1) Then
            quotVersion = quotNumberVersion.Split(".")(1)
        End If
        Dim quotNumber = quotNumberVersion.Split(".")(0)


        Dim work_list As List(Of WorkOrder) = (From w In _context.WorkOrder
                                          Join q In _context.Quotation On q.Quotation_id Equals w.Quotation_id
                                            Where w.Active = True _
                                            And q.QuotationNumber = quotNumber
                                            Order By w.CreationDate Descending
                                            Select w).ToList()

        Return GetAllWorkOrders_formattedForList_by_workOrderList(work_list)

    End Function


    Function GetAllActiveWorkOrders_formattedForList_by_workOrderNumber(ByVal workOrderNumber As String) As List(Of WorkOrder_List_Formatted)

        Dim work_list As List(Of WorkOrder)
        If (workOrderNumber = -1) Then
            work_list = (From w In _context.WorkOrder
                        Where w.Active = True _
                        Order By w.CreationDate Descending
                        Select w).ToList()
        Else
            work_list = (From w In _context.WorkOrder
                        Where w.Active = True _
                        And w.WorkOrderNumber = workOrderNumber
                        Order By w.CreationDate Descending
                        Select w).ToList()

        End If


        Return GetAllWorkOrders_formattedForList_by_workOrderList(work_list)

    End Function

    Function GetAllActiveWorkOrders_formattedForList_by_purchaseOrderNumber(ByVal purchaseOrderNumber As String) As List(Of WorkOrder_List_Formatted)


        Dim work_list As List(Of WorkOrder) = (From w In _context.WorkOrder
                                            Where w.Active = True _
                                            And w.PurchaseOrderNumber = purchaseOrderNumber
                                            Order By w.CreationDate Descending
                                            Select w).ToList()

        Return GetAllWorkOrders_formattedForList_by_workOrderList(work_list)

    End Function

    Function GetAllActiveWorkOrders_formattedForList_by_clientName(ByVal clientName As String) As List(Of WorkOrder_List_Formatted)


        Dim work_list As List(Of WorkOrder) = (From w In _context.WorkOrder
                                          Join q In _context.Quotation On q.Quotation_id Equals w.Quotation_id
                                          Join c In _context.Client On c.Client_Id Equals q.Client_Id
                                            Where w.Active = True _
                                            And c.Name = clientName
                                            Order By w.CreationDate Descending
                                            Select w).ToList()

        Return GetAllWorkOrders_formattedForList_by_workOrderList(work_list)

    End Function

    Function GetAllActiveWorkOrders_formattedForList_by_clientRutNumber(ByVal rutNumber As String) As List(Of WorkOrder_List_Formatted)

        Dim numberOnly As String = rutNumber.Split("-")(0)

        Dim work_list As List(Of WorkOrder) = (From w In _context.WorkOrder
                                          Join q In _context.Quotation On q.Quotation_id Equals w.Quotation_id
                                          Join c In _context.Client On c.Client_Id Equals q.Client_Id
                                            Where w.Active = True _
                                            And c.RutNumber = numberOnly
                                            Order By w.CreationDate Descending
                                            Select w).ToList()

        Return GetAllWorkOrders_formattedForList_by_workOrderList(work_list)

    End Function

    Function GetAllActiveWorkOrders_formattedForList_by_wayBillNumber(ByVal wayBillNumber As String) As List(Of WorkOrder_List_Formatted)


        Dim work_list As List(Of WorkOrder) = (From w In _context.WorkOrder
                                          Join wb In _context.WayBill On wb.WorkOrder_Id Equals w.WorkOrder_Id
                                            Where w.Active = True _
                                            And wb.WayBillNumber = wayBillNumber
                                            Order By w.CreationDate Descending
                                            Select w).ToList()

        Return GetAllWorkOrders_formattedForList_by_workOrderList(work_list)

    End Function

    Function GetAllActiveWorkOrders_formattedForList_by_invoice(ByVal invoiceNumber As String) As List(Of WorkOrder_List_Formatted)


        Dim work_list As List(Of WorkOrder) = (From w In _context.WorkOrder
                                          Join i In _context.Invoice On i.WorkOrder_Id Equals w.WorkOrder_Id
                                            Where w.Active = True _
                                            And i.InvoiceNumber = invoiceNumber
                                            Order By w.CreationDate Descending
                                            Select w).ToList()

        Return GetAllWorkOrders_formattedForList_by_workOrderList(work_list)

    End Function

    Function GetAllWorkOrders_formattedForList_by_workOrderList(ByVal work_list As List(Of WorkOrder))

        Dim quotationDP As New QuotationDataProvider
        Dim workStatusDP As New WorkStatusDataProvider
        Dim tool As New Tools

        Dim allClients_list As List(Of Client) = (From c In _context.Client
                                          Select c).ToList

        Dim allQuotations_list As List(Of Quotation) = (From q In _context.Quotation
                                                        Select q).ToList

        Dim allWorkTypes_list As List(Of WorkTypeQ) = (From wt In _context.WorkTypeQ
                                                 Select wt).ToList

        Dim wo_list_formatted As New List(Of WorkOrder_List_Formatted)


        For Each work As WorkOrder In work_list

            Dim newWorkOrder As New WorkOrder_List_Formatted
            Dim quot As Quotation = (From q In allQuotations_list
                                     Where q.Quotation_id = work.Quotation_id
                                     Select q).FirstOrDefault

            newWorkOrder.WorkOrder_Id = work.WorkOrder_Id
            newWorkOrder.Quotation_Id = work.Quotation_id
            newWorkOrder.CreationDate = tool.GetFormattedDate(work.CreationDate)
            newWorkOrder.ClientName = (From c In allClients_list
                                 Where c.Client_Id = quot.Client_Id
                                 Select c.Name).FirstOrDefault()
            newWorkOrder.WorkDestiny = quot.WorkDestiny
            newWorkOrder.WorkTypeQ = (From wt In allWorkTypes_list
                            Where wt.WorkTypeQ_Id = quot.WorkTypeQ_Id
                            Select wt.TypeName).FirstOrDefault()
            If (work.FinishDate IsNot Nothing) Then
                newWorkOrder.FinishDate = tool.GetFormattedDate(work.FinishDate)
            End If
            newWorkOrder.WorkOrderNumber = work.WorkOrderNumber

            Dim quotNumber As String = quot.QuotationNumber
            If (quot.Version <> 1) Then
                quotNumber += "." + quot.Version
            End If

            newWorkOrder.QuotationNumber = quotNumber
            newWorkOrder.Status = workStatusDP.GetWorkStatus_by_quotId_by_version(quot.Quotation_id, quot.Version).StatusName

            wo_list_formatted.Add(newWorkOrder)

        Next

        Return wo_list_formatted

    End Function

    Function GetActiveWorkOrder_formattedForAddEdit_by_quotNumberVersion(ByVal quotNumberVersion As String) As WorkOrder_AddEdit_Formatted

        Dim quotProdOrServDP As New QuotationProductOrServiceDataProvider
        Dim workStatusDP As New WorkStatusDataProvider

        Dim quotNumber As String = quotNumberVersion.Split(".")(0)
        Dim version As String = 1

        If (quotNumberVersion.Split(".").Count > 1) Then
            version = quotNumberVersion.Split(".")(1)
        End If

        Dim quot As Quotation = (From q In _context.Quotation
                                  Where q.QuotationNumber = quotNumber _
                                  And q.Version = version _
                                  And q.Active = True
                                  Select q).FirstOrDefault

        Dim work As WorkOrder = (From w In _context.WorkOrder
                                      Where w.Quotation_id = quot.Quotation_id _
                                      And w.Active = True
                                      Select w).FirstOrDefault

        Dim allClients_list As List(Of Client) = (From c In _context.Client
                                                  Where c.Active = True
                                                  Select c).ToList()

        Dim allWorkTypes_list As List(Of WorkTypeQ) = (From w In _context.WorkTypeQ
                                                     Select w).ToList


        If (quot IsNot Nothing) Then

            Dim prodOrServ_item_list As List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item) = quotProdOrServDP.GetAllProductOrServiceQ_Item_by_quotId_version(quot.Quotation_id, version)
            Dim jss As New System.Web.Script.Serialization.JavaScriptSerializer

            Dim work_formatted As New WorkOrder_AddEdit_Formatted

            Dim quotNumberVersión = quotNumber
            If (version <> 1) Then
                quotNumberVersión += "." + version
            End If

            work_formatted.Quotation_Id = quot.Quotation_id
            work_formatted.QuotationNumber = quotNumberVersión
            work_formatted.ClientName = (From c In allClients_list
                                              Where c.Client_Id = quot.Client_Id
                                              Select c.Name).FirstOrDefault
            work_formatted.WorkTypeQ = (From w In allWorkTypes_list
                                            Where w.WorkTypeQ_Id = quot.WorkTypeQ_Id
                                            Select w.TypeName).FirstOrDefault
            work_formatted.Item_list = jss.Serialize(prodOrServ_item_list)
            work_formatted.StatusName = workStatusDP.GetWorkStatus_by_quotId_by_version(quot.Quotation_id, version).StatusName


            If (work IsNot Nothing) Then

                work_formatted.WorkOrder_Id = work.WorkOrder_Id
                work_formatted.WorkOrderNumber = ""
                work_formatted.FinishDate = ""
                work_formatted.PurchaseOrderNumber = ""
                work_formatted.Observations = ""
                work_formatted.Material_list = ""
                work_formatted.WayBill_list = ""
                work_formatted.Invoice_list = ""

            End If

            Return work_formatted

        End If


    End Function

    Function GetWorkOrder_formattedForAddEdit_by_workId(ByVal workId As Integer) As WorkOrder_AddEdit_Formatted

        Dim work As WorkOrder = (From w In _context.WorkOrder
                         Where w.WorkOrder_Id = workId
                         Select w).FirstOrDefault

        Return GetWorkOrder_formattedForAddEdit_by_workOrder(work)

    End Function

    Function GetWorkOrder_formattedForAddEdit_by_workOrderNumber(ByVal workOrderNum As Integer) As WorkOrder_AddEdit_Formatted

        Dim work As WorkOrder = (From w In _context.WorkOrder
                          Where w.WorkOrderNumber = workOrderNum
                          Select w).FirstOrDefault

        Return GetWorkOrder_formattedForAddEdit_by_workOrder(work)

    End Function

    Function GetWorkOrder_formattedForAddEdit_by_workOrder(ByVal work As WorkOrder) As WorkOrder_AddEdit_Formatted


        Dim jss As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim userDP As New UserDataProvider
        Dim quotProdOrServDP As New QuotationProductOrServiceDataProvider
        Dim workMaterialDP As New WorkOrderMaterialDataProvider
        Dim wayBillDP As New WayBillDataProvider
        Dim wayBillProdOrServDP As New WayBillProductOrServiceDataProvider
        Dim invoiceDP As New InvoiceDataProvider
        Dim invoiceWayBillDP As New InvoiceWayBIllDataProvider
        Dim tool As New Tools

        Dim quot As Quotation = (From q In _context.Quotation
                                  Where q.Quotation_id = work.Quotation_id
                                  Select q).FirstOrDefault


        Dim allClients_list As List(Of Client) = (From c In _context.Client
                                                  Where c.Active = True
                                                  Select c).ToList()

        Dim allWorkTypes_list As List(Of WorkTypeQ) = (From w In _context.WorkTypeQ
                                                     Select w).ToList


        Dim prodOrServ_item_list As List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item) = quotProdOrServDP.GetAllProductOrServiceQ_Item_by_quotId_version(quot.Quotation_id, quot.Version)


        Dim work_formatted As New WorkOrder_AddEdit_Formatted

        Dim quotNumber As String = quot.QuotationNumber
        If (quot.Version <> 1) Then
            quotNumber += "." + quot.Version
        End If

        work_formatted.Quotation_Id = quot.Quotation_id
        work_formatted.QuotationNumber = quotNumber
        work_formatted.IVAPercentage = quot.IVAPercentage

        work_formatted.ClientName = (From c In allClients_list
                                          Where c.Client_Id = quot.Client_Id
                                          Select c.Name).FirstOrDefault
        work_formatted.WorkTypeQ = (From w In allWorkTypes_list
                                        Where w.WorkTypeQ_Id = quot.WorkTypeQ_Id
                                        Select w.TypeName).FirstOrDefault
        work_formatted.Item_list = jss.Serialize(prodOrServ_item_list)


        work_formatted.WorkOrder_Id = work.WorkOrder_Id
        work_formatted.WorkOrderNumber = work.WorkOrderNumber
        work_formatted.FinishDate = tool.GetFormattedDate(work.FinishDate)
        If (work.PurchaseOrderNumber = 0) Then
            work_formatted.PurchaseOrderNumber = ""
        Else
            work_formatted.PurchaseOrderNumber = work.PurchaseOrderNumber
        End If

        work_formatted.Observations = work.Observation

        work_formatted.CreatedDate = tool.GetFormattedDate(work.CreationDate)


        '---------------------------------------------------------------------------------- LISTA DE MATERIALES
        Dim material_item_list As List(Of MaterialWODataProvider.MaterialWO_Item) = workMaterialDP.GetAllMaterialWO_Item_by_workId(work.WorkOrder_Id)
        work_formatted.Material_list = jss.Serialize(material_item_list)

        '---------------------------------------------------------------------------------- LISTA DE GUÍAS DE DESPACHO
        Dim wayBills_list As List(Of WayBillDataProvider.WayBill_AddEdit_Formatted) = wayBillDP.GetAllWayBills_addEdit_by_workId(work.WorkOrder_Id)
        work_formatted.WayBill_list = jss.Serialize(wayBills_list)

        Dim wayBill_dbItems_list As List(Of WayBillProductOrServiceDataProvider.WayBillProductOrService_Item) = wayBillProdOrServDP.GetAllWayBillProdOrServ_item_by_workId(work.WorkOrder_Id)
        work_formatted.WayBill_DBItems_list = jss.Serialize(wayBill_dbItems_list)

        '---------------------------------------------------------------------------------- LISTA DE FACTURAS
        Dim invoices_list As List(Of InvoiceDataProvider.Invoice_AddEdit_Formatted) = invoiceDP.GetAllInvoices_addEdit_by_workId(work.WorkOrder_Id)
        work_formatted.Invoice_list = jss.Serialize(invoices_list)

        Dim Invoice_DBWBNumbers_list As List(Of String) = invoiceWayBillDP.GetAllInvoiceWayBills_by_workId(work.WorkOrder_Id)
        work_formatted.Invoice_DBWBNumbers_list = jss.Serialize(Invoice_DBWBNumbers_list)

        '------------------------------------------------------------------------------------------------------------------

        Dim userInfo As UserInfo = userDP.GetUserInfo_by_id(work.User_Id)

        If (userInfo.LastName.Split(" ").Count = 0) Then
            work_formatted.CreatedBy = userInfo.FirstName
        ElseIf (userInfo.LastName.Split(" ").Count = 1) Then
            work_formatted.CreatedBy = userInfo.FirstName + " " + userInfo.LastName.Split(" ")(0).Substring(0, 1) + ". "
        ElseIf (userInfo.LastName.Split(" ").Count = 2) Then
            work_formatted.CreatedBy = userInfo.FirstName + " " + userInfo.LastName.Split(" ")(0).Substring(0, 1) + ". " + userInfo.LastName.Split(" ")(1).Substring(0, 1) + "."
        End If



        Return work_formatted


    End Function


    Function DesactivateWorkOrder(ByVal woId As Integer)

        Dim work As WorkOrder = (From q In _context.WorkOrder
                                    Where q.WorkOrder_Id = woId
                                    Select q).FirstOrDefault()

        work.Active = False

        _context.SaveChanges()

    End Function

    Function GetAllActiveWorkOrders()

        Dim work_list As List(Of WorkOrder) = (From w In _context.WorkOrder
                                          Where w.Active = True
                                          Order By w.CreationDate Descending
                                          Select w).ToList()

        Return work_list

    End Function

    Function GetAllActiveWorkOrders_formattedForReport() As List(Of Work_Report_Formatted)

        Dim clientDP As New ClientDataProvider
        Dim quotationDP As New QuotationDataProvider
        Dim workTypeDP As New WorkTypeQDataProvider
        Dim quotProdOrServDP As New QuotationProductOrServiceDataProvider
        Dim workStatusDP As New WorkStatusDataProvider
        Dim tool As New Tools

        Dim allWorkOrder_list As List(Of WorkOrder) = Me.GetAllActiveWorkOrders()
        Dim allClient_list As List(Of Client) = clientDP.GetAllActiveClients()
        Dim allQuotation_list As List(Of Quotation) = quotationDP.GetAllActiveQuotations()
        Dim allWorkTypeQ_list As List(Of WorkTypeQ) = workTypeDP.GetAllActiveWorkTypeQ


        Dim allWorkOrders_list_formatted As New List(Of Work_Report_Formatted)

        Dim quotProdOrServList As List(Of QuotationProductOrService) = (From qps In _context.QuotationProductOrService
                                                                        Select qps).ToList

        For Each work As WorkOrder In allWorkOrder_list


            Dim quotation As Quotation = (From q In allQuotation_list
                                          Where q.Quotation_id = work.Quotation_id
                                          Select q).FirstOrDefault
            Dim client As Client = (From c In allClient_list
                                    Where c.Client_Id = quotation.Client_Id
                                    Select c).FirstOrDefault

            Dim quotNumberVersion As String = quotation.QuotationNumber
            Dim quotVersion As Integer = quotation.Version
            If (quotVersion <> 1) Then
                quotNumberVersion = quotNumberVersion + "." + quotVersion
            End If

            Dim quotation_values As ValuesModel = quotProdOrServDP.GetQuotationValues_by_quotation_quotProdOrServList(quotation, quotProdOrServList)


            Dim newWorkOrderReport As New Work_Report_Formatted

            newWorkOrderReport.CreationDate = tool.GetFormattedDate(work.CreationDate)
            newWorkOrderReport.ClientRut = tool.ThousandSeparator(client.RutNumber) + "-" + client.RutNValidator
            newWorkOrderReport.ClientName = client.Name
            newWorkOrderReport.WorkTypeQ = (From wt In allWorkTypeQ_list
                                      Where wt.WorkTypeQ_Id = quotation.WorkTypeQ_Id
                                      Select wt.TypeName).FirstOrDefault

            newWorkOrderReport.WONumber = work.WorkOrderNumber
            newWorkOrderReport.QuotNumber = quotNumberVersion
            If (work.PurchaseOrderNumber = 0) Then
                newWorkOrderReport.PONumber = ""
            Else
                newWorkOrderReport.PONumber = work.PurchaseOrderNumber
            End If
            newWorkOrderReport.QuotNetValue = tool.ThousandSeparator(quotation_values.NetValue)
            newWorkOrderReport.QuotIVAValue = tool.ThousandSeparator(quotation_values.IVAValue)
            newWorkOrderReport.QuotTotalValue = tool.ThousandSeparator(quotation_values.TotalValue)
            newWorkOrderReport.WorkStatus = workStatusDP.GetWorkStatus_by_quotId_by_version(quotation.Quotation_id, quotation.Version).StatusName

            allWorkOrders_list_formatted.Add(newWorkOrderReport)

        Next

        Return allWorkOrders_list_formatted

    End Function


    Function GetAllActiveWorks_formattedForReport_by_utilitiesReportModel(ByVal utilitiesReportModel As ReportDataProvider.UtilitiesReportModel) As List(Of Work_Report_Formatted)



        Dim clientDP As New ClientDataProvider
        Dim quotationDP As New QuotationDataProvider
        Dim workTypeDP As New WorkTypeQDataProvider
        Dim quotProdOrServDP As New QuotationProductOrServiceDataProvider
        Dim workStatusDP As New WorkStatusDataProvider
        Dim woDP As New WorkOrderDataProvider
        Dim invoiceDP As New InvoiceDataProvider
        Dim workMaterialDP As New WorkOrderMaterialDataProvider
        Dim tool As New Tools

        Dim invoice_list As List(Of Invoice) = invoiceDP.GetAllInvoices()

        Dim allWorkOrder_list As List(Of WorkOrder) = Me.GetAllActiveWorkOrders()
        Dim allClient_list As List(Of Client) = clientDP.GetAllActiveClients()
        Dim allQuotation_list As List(Of Quotation) = quotationDP.GetAllActiveQuotations()
        Dim allWorkTypeQ_list As List(Of WorkTypeQ) = workTypeDP.GetAllActiveWorkTypeQ
        Dim allWorkOrderMaterial_list As List(Of WorkOrderMaterial) = workMaterialDP.GetAllWorkMaterial()


        '---------------------------------------------------------------------------------------------------FILTRO ----------------------------------------
        '---------------------------------------------------------------------------------------------------date
        Dim filter_dateFrom As DateTime = New Date(Date.Today.Year, 1, 1)
        Dim filter_dateTo As DateTime = Date.Today
        utilitiesReportModel.DateFrom = utilitiesReportModel.DateFrom.Replace("/", "-")
        utilitiesReportModel.DateTo = utilitiesReportModel.DateTo.Replace("/", "-")
        Dim from_day = utilitiesReportModel.DateFrom.Split("-")(0)
        Dim from_month = utilitiesReportModel.DateFrom.Split("-")(1)
        Dim from_year = utilitiesReportModel.DateFrom.Split("-")(2)
        Dim to_day = utilitiesReportModel.DateTo.Split("-")(0)
        Dim to_month = utilitiesReportModel.DateTo.Split("-")(1)
        Dim to_year = utilitiesReportModel.DateTo.Split("-")(2)
        filter_dateFrom = New Date(from_year, from_month, from_day)
        filter_dateTo = New Date(to_year, to_month, to_day)

        '---------------------------------------------------------------------------------------------------Client
        Dim filter_clientId As Integer = -1
        If (Not String.IsNullOrEmpty(utilitiesReportModel.ClientRut) And utilitiesReportModel.ClientRut <> "Todos") Then
            filter_clientId = clientDP.GetActiveClient_by_rutNumber(utilitiesReportModel.ClientRut.Split("-")(0)).Client_Id
        End If
        If (Not String.IsNullOrEmpty(utilitiesReportModel.ClientName) And utilitiesReportModel.ClientName <> "Todos") Then
            filter_clientId = clientDP.GetActiveClient_by_name(utilitiesReportModel.ClientName).Client_Id
        End If

        '---------------------------------------------------------------------------------------------------Montos
        Dim filter_valueType As String = utilitiesReportModel.ValueType
        Dim filter_valueFrom As Integer = -1000000000
        Dim filter_valueTo As Integer = 1000000000

        If (utilitiesReportModel.ValueFrom IsNot Nothing And utilitiesReportModel.ValueFrom <> "Todos") Then
            filter_valueFrom = Convert.ToInt32(utilitiesReportModel.ValueFrom.Replace(".", ""))
        End If
        If (utilitiesReportModel.ValueTo IsNot Nothing And utilitiesReportModel.ValueTo <> "Todos") Then
            filter_valueTo = Convert.ToInt32(utilitiesReportModel.ValueTo.Replace(".", ""))
        End If

        '---------------------------------------------------------------------------------------------------Tipo de Trabajo
        Dim filter_workTypeId As Integer = -1
        If (Not String.IsNullOrEmpty(utilitiesReportModel.WorkTypeQ) And utilitiesReportModel.WorkTypeQ <> "Todos") Then
            filter_workTypeId = workTypeDP.GetActiveWorkTypeQ_by_typeName(utilitiesReportModel.WorkTypeQ).WorkTypeQ_Id
        End If

        '---------------------------------------------------------------------------------------------------Estado de Trabajo
        Dim filter_workStatus As String = utilitiesReportModel.WorkStatus
        '---------------------------------------------------------------------------------------------------Con O.C.?
        Dim filter_withPONumber As String = utilitiesReportModel.WithPONumber
        '---------------------------------------------------------------------------------------------------Numero de Guía de Despacho

        Dim filter_wayBillNumber As String = utilitiesReportModel.WayBillNumber
        Dim woNumber_list_by_wbNumber As New List(Of String)
        If (utilitiesReportModel.WayBillNumber <> "Todos") Then
            woNumber_list_by_wbNumber = woDP.GetAllActiveWorkOrderNumber_by_wayBillNumber(Convert.ToInt32(utilitiesReportModel.WayBillNumber))
        End If

        '---------------------------------------------------------------------------------------------------Número de Factura
        Dim filter_invoiceNumber As String = utilitiesReportModel.InvoiceNumber
        Dim woNumber_list_by_invNumber As New List(Of String)
        If (utilitiesReportModel.InvoiceNumber <> "Todos") Then
            woNumber_list_by_invNumber = woDP.GetAllActiveWorkOrderNumber_by_invoiceNumber(Convert.ToInt32(utilitiesReportModel.InvoiceNumber))
        End If


        '---------------------------------------------------------------------------------------------------------------------------------------

        Dim quotProdOrServList As List(Of QuotationProductOrService) = (From qps In _context.QuotationProductOrService
                                                                        Select qps).ToList()


        Dim allWorkOrders_list_formatted As New List(Of Work_Report_Formatted)

        For Each work As WorkOrder In allWorkOrder_list

            Dim shouldContinue As Boolean = True


            If (filter_withPONumber = "Si" And work.PurchaseOrderNumber = 0 Or filter_withPONumber = "No" And work.PurchaseOrderNumber <> 0) Then 'WITH PO NUMBER
                shouldContinue = False
            End If
            If (shouldContinue) Then

                If (filter_dateFrom <= work.CreationDate And filter_dateTo.AddDays(1) > work.CreationDate) Then 'DATE FROM - DATE TO


                    Dim quotation As Quotation = (From q In allQuotation_list
                                                  Where q.Quotation_id = work.Quotation_id
                                                  Select q).FirstOrDefault

                    If (filter_clientId <> -1 And filter_clientId <> quotation.Client_Id) Then 'CLIENT ID
                        shouldContinue = False
                    End If
                    If (shouldContinue) Then

                        If (filter_workTypeId <> -1 And filter_workTypeId <> quotation.WorkTypeQ_Id) Then 'WORK TYPE ID
                            shouldContinue = False
                        End If
                        If (shouldContinue) Then

                            If (filter_wayBillNumber <> "Todos" And Not woNumber_list_by_wbNumber.Contains(work.WorkOrderNumber)) Then 'WAY BILL NUMBER
                                shouldContinue = False
                            End If
                            If (shouldContinue) Then

                                If (filter_invoiceNumber <> "Todos" And Not woNumber_list_by_invNumber.Contains(work.WorkOrderNumber)) Then 'INVOICE NUMBER
                                    shouldContinue = False
                                End If
                                If (shouldContinue) Then


                                    Dim client As Client = (From c In allClient_list
                                                            Where c.Client_Id = quotation.Client_Id
                                                            Select c).FirstOrDefault


                                    Dim quotNumberVersion As String = quotation.QuotationNumber.ToString()
                                    Dim quotVersion As Integer = quotation.Version
                                    If (quotVersion <> 1) Then
                                        quotNumberVersion = quotNumberVersion.ToString() + "." + quotVersion.ToString()
                                    End If


                                    Dim quotation_values As ValuesModel = quotProdOrServDP.GetQuotationValues_by_quotation_quotProdOrServList(quotation, quotProdOrServList)
                                    Dim invo_values As ValuesModel = Me.GetInvoiceValues_by_workId_invoiceList(work.WorkOrder_Id, invoice_list)
                                    Dim paidInvo_values As ValuesModel = Me.GetPaidInvoiceValues_by_workId_invoiceList(work.WorkOrder_Id, invoice_list)
                                    Dim material_values As ValuesModel = workMaterialDP.GetMaterialValues_by_workId_allWorkMaterialList(work.WorkOrder_Id, allWorkOrderMaterial_list)
                                    Dim workExpectedUtilities As Integer = quotation_values.NetValue - material_values.NetValue
                                    Dim workRealUtilities As Integer = paidInvo_values.NetValue - material_values.NetValue


                                    If (filter_valueType = "Cot. Monto Neto" And (filter_valueFrom > quotation_values.NetValue Or filter_valueTo < quotation_values.NetValue)) Then 'QUOT. NET VALUE
                                        shouldContinue = False
                                    End If
                                    If (shouldContinue) Then
                                        If (filter_valueType = "Cot. Monto IVA" And (filter_valueFrom > quotation_values.IVAValue Or filter_valueTo < quotation_values.IVAValue)) Then 'QUOT. IVA VALUE
                                            shouldContinue = False
                                        End If
                                        If (shouldContinue) Then
                                            If (filter_valueType = "Cot. Monto Total" And (filter_valueFrom > quotation_values.TotalValue Or filter_valueTo < quotation_values.TotalValue)) Then 'QUOT. TOTAL VALUE
                                                shouldContinue = False
                                            End If
                                            If (shouldContinue) Then
                                                If (filter_valueType = "Fac. Monto Neto" And (filter_valueFrom > invo_values.NetValue Or filter_valueTo < invo_values.NetValue)) Then 'INVO. TOTAL VALUE
                                                    shouldContinue = False
                                                End If
                                                If (shouldContinue) Then
                                                    If (filter_valueType = "Fac. Monto IVA" And (filter_valueFrom > invo_values.IVAValue Or filter_valueTo < invo_values.IVAValue)) Then 'INVO. TOTAL VALUE
                                                        shouldContinue = False
                                                    End If
                                                    If (shouldContinue) Then
                                                        If (filter_valueType = "Fac. Monto Total" And (filter_valueFrom > invo_values.TotalValue Or filter_valueTo < invo_values.TotalValue)) Then 'INVO. TOTAL VALUE
                                                            shouldContinue = False
                                                        End If
                                                        If (shouldContinue) Then
                                                            If (filter_valueType = "Fac. Pag. Monto Neto" And (filter_valueFrom > paidInvo_values.NetValue Or filter_valueTo < paidInvo_values.NetValue)) Then 'PAID INVO. TOTAL VALUE
                                                                shouldContinue = False
                                                            End If
                                                            If (shouldContinue) Then
                                                                If (filter_valueType = "Fac. Pag. Monto IVA" And (filter_valueFrom > paidInvo_values.IVAValue Or filter_valueTo < paidInvo_values.IVAValue)) Then 'PAID INVO. TOTAL VALUE
                                                                    shouldContinue = False
                                                                End If
                                                                If (shouldContinue) Then
                                                                    If (filter_valueType = "Fac. Pag. Monto Total" And (filter_valueFrom > paidInvo_values.TotalValue Or filter_valueTo < paidInvo_values.TotalValue)) Then 'PAID INVO. TOTAL VALUE
                                                                        shouldContinue = False
                                                                    End If
                                                                    If (shouldContinue) Then
                                                                        If (filter_valueType = "Mat. Monto Neto" And (filter_valueFrom > material_values.NetValue Or filter_valueTo < material_values.NetValue)) Then 'INVO. TOTAL VALUE
                                                                            shouldContinue = False
                                                                        End If
                                                                        If (shouldContinue) Then
                                                                            If (filter_valueType = "Mat. Monto IVA" And (filter_valueFrom > material_values.IVAValue Or filter_valueTo < material_values.IVAValue)) Then 'INVO. TOTAL VALUE
                                                                                shouldContinue = False
                                                                            End If
                                                                            If (shouldContinue) Then
                                                                                If (filter_valueType = "Mat. Monto Total" And (filter_valueFrom > material_values.TotalValue Or filter_valueTo < material_values.TotalValue)) Then 'INVO. TOTAL VALUE
                                                                                    shouldContinue = False
                                                                                End If
                                                                                If (shouldContinue) Then
                                                                                    If (filter_valueType = "Utilidades Esperadas" And (filter_valueFrom > workExpectedUtilities Or filter_valueTo < workExpectedUtilities)) Then 'INVO. TOTAL VALUE
                                                                                        shouldContinue = False
                                                                                    End If
                                                                                    If (shouldContinue) Then
                                                                                        If (filter_valueType = "Utilidades Reales" And (filter_valueFrom > workRealUtilities Or filter_valueTo < workRealUtilities)) Then 'INVO. TOTAL VALUE
                                                                                            shouldContinue = False
                                                                                        End If

                                                                                        Dim newWorkOrderReport As New Work_Report_Formatted

                                                                                        newWorkOrderReport.CreationDate = tool.GetFormattedDate(work.CreationDate)
                                                                                        newWorkOrderReport.ClientRut = tool.ThousandSeparator(client.RutNumber) + "-" + client.RutNValidator
                                                                                        newWorkOrderReport.ClientName = client.Name
                                                                                        newWorkOrderReport.WorkTypeQ = (From wt In allWorkTypeQ_list
                                                                                                                  Where wt.WorkTypeQ_Id = quotation.WorkTypeQ_Id
                                                                                                                  Select wt.TypeName).FirstOrDefault

                                                                                        newWorkOrderReport.WONumber = work.WorkOrderNumber
                                                                                        newWorkOrderReport.QuotNumber = quotNumberVersion
                                                                                        If (work.PurchaseOrderNumber = 0) Then
                                                                                            newWorkOrderReport.PONumber = ""
                                                                                        Else
                                                                                            newWorkOrderReport.PONumber = work.PurchaseOrderNumber
                                                                                        End If


                                                                                        newWorkOrderReport.QuotNetValue = "$ " + tool.ThousandSeparator(quotation_values.NetValue)
                                                                                        newWorkOrderReport.QuotIVAValue = "$ " + tool.ThousandSeparator(quotation_values.IVAValue)
                                                                                        newWorkOrderReport.QuotTotalValue = "$ " + tool.ThousandSeparator(quotation_values.TotalValue)

                                                                                        newWorkOrderReport.InvoNetValue = "$ " + tool.ThousandSeparator(invo_values.NetValue)
                                                                                        newWorkOrderReport.InvoIVAValue = "$ " + tool.ThousandSeparator(invo_values.IVAValue)
                                                                                        newWorkOrderReport.InvoTotalValue = "$ " + tool.ThousandSeparator(invo_values.TotalValue)

                                                                                        newWorkOrderReport.PaidInvoNetValue = "$ " + tool.ThousandSeparator(paidInvo_values.NetValue)
                                                                                        newWorkOrderReport.PaidInvoIVAValue = "$ " + tool.ThousandSeparator(paidInvo_values.IVAValue)
                                                                                        newWorkOrderReport.PaidInvoTotalValue = "$ " + tool.ThousandSeparator(invo_values.TotalValue)

                                                                                        newWorkOrderReport.MatNetValue = "$ " + tool.ThousandSeparator(material_values.NetValue)
                                                                                        newWorkOrderReport.MatIVAValue = "$ " + tool.ThousandSeparator(material_values.IVAValue)
                                                                                        newWorkOrderReport.MatTotalValue = "$ " + tool.ThousandSeparator(material_values.TotalValue)

                                                                                        newWorkOrderReport.ExpectedUtilities = "$ " + tool.ThousandSeparator(workExpectedUtilities)
                                                                                        newWorkOrderReport.RealUtilities = "$ " + tool.ThousandSeparator(workRealUtilities)

                                                                                        newWorkOrderReport.WorkStatus = workStatusDP.GetWorkStatus_by_quotId_by_version(quotation.Quotation_id, quotation.Version).StatusName

                                                                                        If (filter_workStatus <> "Todos" And filter_workStatus <> newWorkOrderReport.WorkStatus) Then
                                                                                            shouldContinue = False
                                                                                        End If
                                                                                        If (shouldContinue) Then

                                                                                            allWorkOrders_list_formatted.Add(newWorkOrderReport)

                                                                                        End If

                                                                                    End If
                                                                                End If
                                                                            End If
                                                                        End If
                                                                    End If
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Next

        Return allWorkOrders_list_formatted

    End Function

    Function GetAllActiveWorkOrderNumber_by_wayBillNumber(ByVal wayBillNumber As Integer) As List(Of String)

        Dim wayBill_list As List(Of WayBill) = (From wb In _context.WayBill
                                                Where wb.WayBillNumber = wayBillNumber
                                                Select wb).ToList

        Dim allWorkOrder_list As List(Of WorkOrder) = New WorkOrderDataProvider().GetAllActiveWorkOrders()
        Dim woNumber_list As New List(Of String)

        For Each wayBill As WayBill In wayBill_list
            Dim work As WorkOrder = (From w In allWorkOrder_list
                                Where w.WorkOrder_Id = wayBill.WorkOrder_Id
                                Select w).FirstOrDefault()

            woNumber_list.Add(work.WorkOrderNumber)
        Next

        Return woNumber_list

    End Function

    Function GetAllActiveWorkOrderNumber_by_invoiceNumber(ByVal invoiceNumber As Integer) As List(Of String)

        Dim invoice_list As List(Of Invoice) = (From wb In _context.Invoice
                                                Where wb.InvoiceNumber = invoiceNumber
                                                Select wb).ToList

        Dim allWorkOrder_list As List(Of WorkOrder) = New WorkOrderDataProvider().GetAllActiveWorkOrders()
        Dim woNumber_list As New List(Of String)

        For Each invoice As Invoice In invoice_list
            Dim work As WorkOrder = (From w In allWorkOrder_list
                                Where w.WorkOrder_Id = invoice.WorkOrder_Id
                                Select w).FirstOrDefault()

            woNumber_list.Add(work.WorkOrderNumber)
        Next

        Return woNumber_list

    End Function


    Function GetInvoiceValues_by_workId_invoiceList(ByVal workId As Integer, ByVal invoiceList As List(Of Invoice)) As ValuesModel


        Dim netValue_total As Integer = 0
        Dim ivaValue_total As Integer = 0

        Dim invo_list As List(Of Invoice) = (From i In invoiceList
                                            Where i.WorkOrder_Id = workId
                                            Select i).ToList()

        For Each invo As Invoice In invo_list

            netValue_total += invo.NetValue
            Dim ivaPercentage As Double = invo.IVAPercentage

            ivaValue_total += Math.Round(invo.NetValue * ivaPercentage / 100)

        Next

        Dim totalValue As Integer = netValue_total + ivaValue_total

        Dim quotation_values As New ValuesModel

        quotation_values.NetValue = netValue_total
        quotation_values.IVAValue = ivaValue_total
        quotation_values.TotalValue = totalValue

        Return quotation_values

    End Function

    Function GetPaidInvoiceValues_by_workId_invoiceList(ByVal workId As Integer, ByVal invoiceList As List(Of Invoice)) As ValuesModel


        Dim netValue_total As Integer = 0
        Dim ivaValue_total As Integer = 0

        Dim invo_list As List(Of Invoice) = (From i In invoiceList
                                            Where i.WorkOrder_Id = workId _
                                            And i.IsPaid = True
                                            Select i).ToList()

        For Each invo As Invoice In invo_list

            netValue_total += invo.NetValue
            Dim ivaPercentage As Double = invo.IVAPercentage

            ivaValue_total += Math.Round(invo.NetValue * ivaPercentage / 100)

        Next

        Dim totalValue As Integer = netValue_total + ivaValue_total

        Dim quotation_values As New ValuesModel

        quotation_values.NetValue = netValue_total
        quotation_values.IVAValue = ivaValue_total
        quotation_values.TotalValue = totalValue

        Return quotation_values

    End Function

    Public Class WorkOrder_List_Formatted


        Public Property WorkOrder_Id As Integer
        Public Property Quotation_Id As String
        Public Property CreationDate As String
        Public Property ClientName As String
        Public Property WorkDestiny As String
        Public Property WorkTypeQ As String
        Public Property FinishDate As String
        Public Property QuotationNumber As String
        Public Property WorkOrderNumber As String
        Public Property Status As String

    End Class

    Public Class WorkOrder_AddEdit_Formatted

        Public Property WorkOrder_Id As Integer
        Public Property Quotation_Id As String
        Public Property CreatedBy As String
        Public Property CreatedDate As String
        Public Property WorkOrderNumber As String
        Public Property QuotationNumber As String
        Public Property ClientName As String
        Public Property WorkTypeQ As String
        Public Property FinishDate As String
        Public Property PurchaseOrderNumber As String
        Public Property Observations As String
        Public Property StatusName As String
        Public Property Item_list As String
        Public Property Material_list As String
        Public Property WayBill_list As String
        Public Property WayBill_DBItems_list As String
        Public Property Invoice_list As String
        Public Property Invoice_DBWBNumbers_list As String
        Public Property IVAPercentage As String

    End Class

    Public Class Work_Report_Formatted

        Public Property CreationDate As String
        Public Property ClientRut As String
        Public Property ClientName As String
        Public Property WorkTypeQ As String
        Public Property WONumber As String
        Public Property QuotNumber As String
        Public Property PONumber As String
        Public Property WorkStatus As String
        Public Property QuotNetValue As String
        Public Property QuotIVAValue As String
        Public Property QuotTotalValue As String
        Public Property InvoNetValue As String
        Public Property InvoIVAValue As String
        Public Property InvoTotalValue As String
        Public Property PaidInvoNetValue As String
        Public Property PaidInvoIVAValue As String
        Public Property PaidInvoTotalValue As String
        Public Property MatNetValue As String
        Public Property MatIVAValue As String
        Public Property MatTotalValue As String
        Public Property ExpectedUtilities As String
        Public Property RealUtilities As String

    End Class





End Class
