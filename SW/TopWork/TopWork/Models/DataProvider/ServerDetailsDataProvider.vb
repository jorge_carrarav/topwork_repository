﻿Public Class ServerDetailsDataProvider : Inherits Context



    Public Function InsertDetails(ByVal details As ServerDetails)

        _context.ServerDetails.Add(details)
        _context.SaveChanges()

    End Function

    Public Function GetServerDetails() As ServerDetails



        Dim query As ServerDetails = (From s In _context.ServerDetails
                                      Order By s.ServerDetails_Id Descending
                                      Select s).FirstOrDefault()

        Return query

    End Function

End Class
