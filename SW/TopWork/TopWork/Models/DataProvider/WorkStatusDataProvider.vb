﻿Public Class WorkStatusDataProvider : Inherits Context





    Function InsertStatus(ByVal id As Integer, ByVal name As String)

        Dim newQuotStatus As New WorkStatus
        newQuotStatus.WorkStatus_Id = id
        newQuotStatus.StatusName = name

        _context.WorkStatus.Add(newQuotStatus)
        _context.SaveChanges()

    End Function

    Public Function GetStatus_by_id(ByVal id As Integer) As WorkStatus

        Dim workStatus As WorkStatus = (From q In _context.WorkStatus
                                        Where q.WorkStatus_Id = id
                                        Select q).FirstOrDefault()

        Return workStatus

    End Function

    Public Function GetAllWorkStatus() As List(Of WorkStatus)

        Dim workStatus_list As List(Of WorkStatus) = (From w In _context.WorkStatus
                                                    Select w).ToList()

        Return workStatus_list

    End Function

    Function GetWorkStatus_by_quotId_by_version(ByVal quotId As Integer, ByVal version As Integer) As WorkStatus

        Dim quotProdOrServDP As New QuotationProductOrServiceDataProvider
        Dim wayBillProdOrServDP As New WayBillProductOrServiceDataProvider
        Dim wayBillDP As New WayBillDataProvider
        Dim invoiceWayBillsDP As New InvoiceWayBIllDataProvider
        Dim invoiceDP As New InvoiceDataProvider

        Dim workStatusId As Integer

        Dim work As WorkOrder = (From w In _context.WorkOrder
                            Where w.Active = True _
                            And w.Quotation_id = quotId
                            Select w).FirstOrDefault


        If (work Is Nothing) Then '----------------------------------------------------------------------------------------------------------------------------------SIN O.T.
            workStatusId = 1
        Else
            Dim wayBills_list As List(Of WayBill) = (From w In _context.WayBill
                                                     Where w.WorkOrder_Id = work.WorkOrder_Id
                                                     Select w).ToList()

            Dim quotProdOrServ_list As List(Of QuotationProductOrService) = quotProdOrServDP.GetAllProductOrService_by_quotId_version(quotId, version)
            Dim wayBillProdOrServ_list As List(Of WayBillProductOrService) = wayBillProdOrServDP.GetAllWayBillProdOrServ_by_workId(work.WorkOrder_Id)

            If (Not Me.ComparingQuotationItems_vs_WayBillItems(quotProdOrServ_list, wayBillProdOrServ_list)) Then '-------------------------------------------------------------EN PROCESO | faltan guías de despacho
                workStatusId = 2
            Else
                Dim wayBillNumber_list As List(Of Integer) = wayBillDP.GetAllWayBillNumbers_by_workId(work.WorkOrder_Id)
                Dim invoiceWayBillNumber_list As List(Of Integer) = invoiceWayBillsDP.GetAllInvoiceWayBillNumbers_by_workId(work.WorkOrder_Id)

                If (Not ComparingWayBillNumber_vs_invoiceWayBillNumber(wayBillNumber_list, invoiceWayBillNumber_list)) Then '----------------------------------------TRABAJO ENTREGADO, pero no facturado
                    workStatusId = 3
                Else

                    Dim areAllInvoicesPaid As Boolean = invoiceDP.AreAllInvoicePaid_by_workId(work.WorkOrder_Id)

                    If (Not areAllInvoicesPaid) Then 'TRABAJO FACTURADO | Existe factura para todas las guías pero no están todas pagadas

                        workStatusId = 4

                    Else

                        workStatusId = 5

                    End If

                End If


            End If

        End If

        Dim ws As WorkStatus = (From w In _context.WorkStatus
                                Where w.WorkStatus_Id = workStatusId
                                Select w).FirstOrDefault()

        Return ws

    End Function

    Function ComparingWayBillNumber_vs_invoiceWayBillNumber(ByVal wayBillNumber_list As List(Of Integer), ByVal invoiceWayBillNumber_list As List(Of Integer))

        Dim areEquals = True

        Dim count = 0
        While count < wayBillNumber_list.Count And areEquals

            Dim wayBillNumber As Integer = wayBillNumber_list(count)

            If (invoiceWayBillNumber_list.IndexOf(wayBillNumber) = -1) Then
                areEquals = False
            End If

            count += 1
        End While

        Return areEquals

    End Function

    Function ComparingQuotationItems_vs_WayBillItems(ByVal quotProdOrServ_list As List(Of QuotationProductOrService), ByVal wayBillProdOrServ_list As List(Of WayBillProductOrService))

        Dim areEquals = True

        Dim count As Integer = 0
        While (count < quotProdOrServ_list.Count And areEquals)

            Dim itemId As Integer = quotProdOrServ_list(count).ProductOrServiceQ_Id
            Dim totalQuantity As Integer = quotProdOrServ_list(count).Quantity

            Dim wbItemQuantity As Integer = (From w In wayBillProdOrServ_list
                                                             Where w.ProductOrServiceQ_Id = itemId
                                                             Select w.Quantity).Sum()


            If (totalQuantity <> wbItemQuantity) Then
                areEquals = False
            End If
            count += 1
        End While

        Return areEquals

    End Function



End Class
