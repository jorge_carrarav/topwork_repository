﻿
Public Class AppCodeDataProvider : Inherits Context

    'Code 2 es el que importa
    'Code 1 = ServerName + DomainName + OSVersion + ProcessorCount + ExpirationDate
    'Code 2 = hash(hash 1)

    Function GetCode1_usingDataBaseDetails() As String

        Dim tool As New Tools

        Dim serverDetails As ServerDetails = (From s In _context.ServerDetails
                                              Order By s.ServerDetails_Id Descending
                                              Select s).FirstOrDefault()


        Dim expirationDate As DateTime = New AppConfigurationDataProvider().GetAppConfiguration().AppExpirationDate

        Dim encr_1 As String = serverDetails.ServerName + serverDetails.DomainName + serverDetails.OSVersion + serverDetails.ProcessorCount + expirationDate
        encr_1 = tool.EncryptData(encr_1)

        Return encr_1

    End Function


    Function GetCode1_forThisServer() As String

        Dim tool As New Tools


        Dim serverName As String = System.Environment.MachineName
        Dim osVersion As String = System.Environment.OSVersion.VersionString
        Dim processorCount As String = System.Environment.ProcessorCount
        Dim domainName As String = System.Environment.UserDomainName

        Dim expirationDate As DateTime = New AppConfigurationDataProvider().GetAppConfiguration().AppExpirationDate



        Dim encr_1 As String = serverName + osVersion + processorCount + domainName + expirationDate.ToString
        encr_1 = tool.EncryptData(encr_1)

        Return encr_1

    End Function

    Function GetCode2_forThisServer() As String

        Dim code_1 = GetCode1_forThisServer()
        Return GetCode2_by_code1(code_1)

    End Function

    Function GetCode2_by_code1(ByVal code1 As String) As String
        Dim tool As New Tools

        Return tool.EncryptData(code1)

    End Function

    Function CheckAppCode_by_dbCode(ByVal dbCode As String) As String

        Dim code_1 As String = GetCode1_forThisServer()

        Dim code_2 As String = GetCode2_by_code1(code_1)

        If (dbCode = code_2) Then
            Return True
        Else
            Return False
        End If

    End Function


End Class
