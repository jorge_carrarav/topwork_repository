﻿Public Class QuotationHistoryDataProvider : Inherits Context



    Function InsertQuotationHistory(ByVal newQuotVistory As QuotationHistory)

        _context.QuotationHistory.Add(newQuotVistory)
        _context.SaveChanges()

    End Function

    Function GetQuotHistory_by_id(ByVal quotHistoryId As Integer) As QuotationHistory



        Dim query As QuotationHistory = (From qh In _context.QuotationHistory
                                         Where qh.QuotationHistory_Id
                                         Select qh).FirstOrDefault()

        Return query

    End Function

    Function GetQuotHistory_by_quotNum_version(ByVal quotNumber As Integer, ByVal quotVersion As Integer) As QuotationHistory



        Dim query As QuotationHistory = (From qh In _context.QuotationHistory
                                         Where qh.QuotationNumber = quotNumber _
                                         And qh.Version = quotVersion
                                         Select qh).FirstOrDefault()

        Return query

    End Function


End Class
