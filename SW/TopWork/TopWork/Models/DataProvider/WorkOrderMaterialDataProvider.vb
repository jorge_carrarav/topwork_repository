﻿Imports TopWork.MaterialWODataProvider

Public Class WorkOrderMaterialDataProvider : Inherits Context


    Function InsertWorkMaterial(ByVal newWorkMaterial As WorkOrderMaterial)

        _context.WorkOrderMaterial.Add(newWorkMaterial)
        _context.SaveChanges()

    End Function

    Function DeleteAllWorkMaterial_by_workId(ByVal workOrderId As Integer)

        Dim workMaterial_list As List(Of WorkOrderMaterial) = (From wom In _context.WorkOrderMaterial
                                                                    Where wom.WorkOrder_Id = workOrderId
                                                                    Select wom).ToList()

        For Each woMaterial As WorkOrderMaterial In workMaterial_list

            _context.WorkOrderMaterial.Remove(woMaterial)

        Next

        _context.SaveChanges()

    End Function


    Function GetMaterialValues_by_workId_allWorkMaterialList(ByVal workOrderId As Integer, ByVal allWorkMaterial_list As List(Of WorkOrderMaterial)) As ValuesModel

        Dim workMaterial_list As List(Of WorkOrderMaterial) = (From w In allWorkMaterial_list
                                                        Where w.WorkOrder_Id = workOrderId
                                                        Select w).ToList

        Dim materialNetValue As Integer = 0
        Dim materialIVAValue As Integer = 0
        Dim materialTotalValue As Integer = 0
        Dim unitIVAValue As Integer = 0
        For Each workMaterial As WorkOrderMaterial In workMaterial_list

            materialNetValue = materialNetValue + workMaterial.NetValue
            unitIVAValue = (workMaterial.NetValue * workMaterial.IVAPercentage) / 100
            materialIVAValue = materialIVAValue + unitIVAValue

        Next

        materialTotalValue = materialNetValue + materialIVAValue

        Dim materialValues As New ValuesModel

        materialValues.NetValue = materialNetValue
        materialValues.IVAValue = materialIVAValue
        materialValues.TotalValue = materialTotalValue

        Return materialValues

    End Function

    Function GetAllMaterialWO_Item_by_workId(ByVal workOrderId As Integer) As List(Of MaterialWODataProvider.MaterialWO_Item)

        Dim unitOfMeasDP As New UnitOfMeasureQDataProvider
        Dim materialDP As New MaterialWODataProvider


        Dim workMaterial_list As List(Of WorkOrderMaterial) = (From wom In _context.WorkOrderMaterial
                                                    Where wom.WorkOrder_Id = workOrderId _
                                                    Order By wom.ItemNumber Ascending
                                                    Select wom).ToList

        Dim material_item_list As New List(Of MaterialWO_Item)

        For Each workMat As WorkOrderMaterial In workMaterial_list

            Dim material_item As New MaterialWODataProvider.MaterialWO_Item

            material_item.MaterialWO_Id = workMat.MaterialWO_Id
            material_item.ItemNumber = workMat.ItemNumber
            material_item.Description = materialDP.GetMaterialWO_by_id(workMat.MaterialWO_Id).Description
            material_item.Quantity = workMat.Quantity
            material_item.UnitOfMeasure = unitOfMeasDP.GetUnitOfMeasureQ_by_id(workMat.UnitOfMeasure_Id).ShortName
            material_item.NetValue = workMat.NetValue
            material_item.IVAPercentage = workMat.IVAPercentage

            material_item_list.Add(material_item)

        Next

        Return material_item_list

    End Function


    Function GetAllWorkMaterial() As List(Of WorkOrderMaterial)

        Dim workMaterial_list = (From wm In _context.WorkOrderMaterial
                                 Select wm).ToList()

        Return workMaterial_list

    End Function





End Class
