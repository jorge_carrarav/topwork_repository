﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Globalization
Imports System.Data.Entity

Imports WebMatrix.WebData
Public Class UserDataProvider : Inherits Context


    Function GetUserInfo_by_id(ByVal userId As Integer) As UserInfo

        Dim query = (From u In _context.UserInfo
                     Where u.UserId = userId
                     Select u).FirstOrDefault()

        Return query

    End Function

    Function GetUserName_by_id(ByVal userId As Integer) As String

        Dim username As String = New WebMatrix.WebData.SimpleMembershipProvider().GetUserNameFromId(userId)

        Return username

    End Function

    Function InsertUserInformation(ByVal userId As Integer, ByVal firstName As String, ByVal lasftName As String, ByVal active As Boolean)



        Dim newUserInfo As New UserInfo
        newUserInfo.UserId = userId
        newUserInfo.FirstName = firstName
        newUserInfo.LastName = lasftName
        newUserInfo.Active = active

        _context.UserInfo.Add(newUserInfo)
        _context.SaveChanges()

    End Function


    Function GetUserId_byFullName(ByVal fullName As String) As Integer

        Dim user As UserInfo = (From q In _context.UserInfo
                                 Where q.FirstName + " " + q.LastName = fullName
                                 Select q).FirstOrDefault

        If (user IsNot Nothing) Then
            Return user.UserId
        Else
            Return -1
        End If

    End Function


    Function UpdateUserInformation(ByVal userId As Integer, ByVal firstName As String, ByVal lasftName As String, ByVal active As Boolean)



        Dim newUserInfo = (From u In _context.UserInfo
                          Where u.UserId = userId
                          Select u).FirstOrDefault

        newUserInfo.FirstName = firstName
        newUserInfo.LastName = lasftName
        newUserInfo.Active = active

        _context.SaveChanges()

    End Function

    Function DeleteUser(ByVal userId As Integer)



        Dim userInfo = (From u In _context.UserInfo
                          Where u.UserId = userId
                          Select u).FirstOrDefault


        Dim provider As SimpleMembershipProvider = Membership.Provider
        Dim username As String = provider.GetUserNameFromId(userInfo.UserId)

        Dim userRole As String = Roles.GetRolesForUser(username).FirstOrDefault()
        Roles.RemoveUserFromRole(username, userRole)

        _context.UserInfo.Remove(userInfo)
        _context.SaveChanges()

        provider.DeleteAccount(username)
        provider.DeleteUser(username, True)

    End Function

    Function UserHasQuotations_by_id(ByVal userId As Integer)


        Dim query = (From q In _context.Quotation
                     Where q.User_Id = userId
                     Select q).FirstOrDefault()

        If (query IsNot Nothing) Then
            Return True
        Else
            Return False

        End If

    End Function

    Function GetAllUserCompleteInfo() As List(Of UserCompleteInfoModel)


        Dim userInfo_list As List(Of UserInfo) = (From uProf In _context.UserInfo
                                                                  Select uProf).ToList()

        Dim userCompleteInfo_list As New List(Of UserCompleteInfoModel)

        For Each user As UserInfo In userInfo_list

            Dim providerUserKey As Object = user.UserId

            Dim provider As SimpleMembershipProvider = Membership.Provider

            Dim username As String = provider.GetUserNameFromId(user.UserId)

            userCompleteInfo_list.Add(New UserCompleteInfoModel With {
                                          .UserId = user.UserId,
                                          .UserName = username,
                                          .FirstName = user.FirstName,
                                          .LastName = user.LastName,
                                          .Active = user.Active})

        Next

        Dim userCompleteInfo_list_ordered As List(Of UserCompleteInfoModel) = (From u In userCompleteInfo_list
                                                                               Order By u.FirstName
                                                                               Select u).ToList

        Return userCompleteInfo_list_ordered

    End Function

    Function GetAllUserCompleteInfo_noAdmin() As List(Of UserCompleteInfoModel)



        Dim userCompleteInfo_list As New List(Of UserCompleteInfoModel)

        For Each userCompleteInfo As UserCompleteInfoModel In Me.GetAllUserCompleteInfo
            If (userCompleteInfo.UserName <> "admin") Then
                userCompleteInfo_list.Add(userCompleteInfo)
            End If
        Next


        Return userCompleteInfo_list

    End Function

    Public Class UserCompleteInfoModel

        Public Property UserId As Integer
        Public Property UserName As String
        Public Property FirstName As String
        Public Property LastName As String
        Public Property Active As Boolean

    End Class


End Class
