﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Globalization
Imports System.Data.Entity

Imports WebMatrix.WebData
Public Class WorkTypeQDataProvider : Inherits Context


    Function CheckIfExists_by_name_docType(ByVal typeName As String) As Boolean

        Dim query = (From w In _context.WorkTypeQ
                     Where w.TypeName = typeName _
                     Select w).FirstOrDefault()
        If (query IsNot Nothing) Then
            Return True
        Else
            Return False
        End If

    End Function

    Function CheckIfExists_by_name_butNotThisId_docType(ByVal typeName As String, ByVal typeId As Integer) As Boolean


        Dim query = (From w In _context.WorkTypeQ
                     Where w.TypeName = typeName _
                     And w.WorkTypeQ_Id <> typeId _
                     Select w).FirstOrDefault()
        If (query IsNot Nothing) Then
            Return True
        Else
            Return False
        End If

    End Function

    Function GetActiveWorkTypeQ_by_workTypeId(ByVal typeId As Integer) As WorkTypeQ

        Dim workType = (From w In _context.WorkTypeQ
                     Where w.WorkTypeQ_Id = typeId _
                     And w.Active = True
                     Select w).FirstOrDefault()

        Return workType

    End Function
    Function GetWorkTypeQ_by_workTypeId(ByVal typeId As Integer) As WorkTypeQ

        Dim workType = (From w In _context.WorkTypeQ
                     Where w.WorkTypeQ_Id = typeId
                     Select w).FirstOrDefault()

        Return workType

    End Function
    Function GetActiveWorkTypeQ_by_workId(ByVal workId As Integer) As WorkTypeQ

        Dim workType = (From wt In _context.WorkTypeQ
                     Join q In _context.Quotation On q.WorkTypeQ_Id Equals wt.WorkTypeQ_Id
                     Join w In _context.WorkOrder On q.Quotation_id Equals w.Quotation_id
                     Where w.WorkOrder_Id = workId _
                     And w.Active = True
                     Select wt).FirstOrDefault()

        Return workType

    End Function

    Function GetActiveWorkTypeQ_by_typeName(ByVal typeName As String) As WorkTypeQ


        Dim query = (From w In _context.WorkTypeQ
                     Where w.TypeName = typeName _
                     And w.Active = True _
                     Select w).FirstOrDefault()

        Return query

    End Function

    Function InsertWorkTypeQ(ByVal workTypeModel As WorkTypeQ) As WorkTypeQ

        Dim newType As New WorkTypeQ
        newType.TypeName = workTypeModel.TypeName
        newType.Active = workTypeModel.Active

        _context.WorkTypeQ.Add(newType)
        _context.SaveChanges()

        Return newType
    End Function

    Function GetAllActiveWorkTypeQ() As List(Of WorkTypeQ)

        Dim query As List(Of WorkTypeQ) = (From w In _context.WorkTypeQ
                                          Where w.Active = True
                                          Order By w.TypeName Ascending
                                        Select w).ToList()

        Return query

    End Function
    Function GetAllWorkTypeQ() As List(Of WorkTypeQ)

        Dim query As List(Of WorkTypeQ) = (From w In _context.WorkTypeQ
                                          Order By w.TypeName Ascending
                                        Select w).ToList()

        Return query

    End Function

    Function UpdateWorkTypeQ_by_model(ByVal model As WorkTypeQ)


        Dim query = (From w In _context.WorkTypeQ
                     Where w.WorkTypeQ_Id = model.WorkTypeQ_Id
                     Select w).FirstOrDefault()

        query.TypeName = model.TypeName
        query.Active = model.Active

        _context.SaveChanges()

    End Function

    Function DeleteWorkTypeQ_by_id(ByVal typeId As Integer)


        Dim query = (From w In _context.WorkTypeQ
                     Where w.WorkTypeQ_Id = typeId
                     Select w).FirstOrDefault()

        _context.WorkTypeQ.Remove(query)
        _context.SaveChanges()

    End Function

    Function WorkTypeQHasQuotations_by_id(ByVal typeId As Integer)


        Dim query = (From q In _context.Quotation
                     Where q.WorkTypeQ_Id = typeId
                     Select q).FirstOrDefault()

        If (query IsNot Nothing) Then
            Return True
        Else
            Return False

        End If

    End Function

    Function GetAllActiveWorkTypeQName() As List(Of String)

        Dim query As List(Of String) = (From q In _context.WorkTypeQ
                                        Where q.Active = True _
                                        Order By q.TypeName
                                        Select q.TypeName).ToList()

        Return query

    End Function

End Class
