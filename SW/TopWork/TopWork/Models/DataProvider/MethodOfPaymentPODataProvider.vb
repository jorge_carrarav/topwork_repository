﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Globalization
Imports System.Data.Entity

Imports WebMatrix.WebData
Public Class MethodOfPaymentPODataProvider : Inherits Context


    Function CheckIfExists_by_name_docType(ByVal methodName As String) As Boolean


        Dim query = (From w In _context.MethodOfPaymentPO
                     Where w.MethodName = methodName _
                     Select w).FirstOrDefault()
        If (query IsNot Nothing) Then
            Return True
        Else
            Return False
        End If

    End Function

    Function CheckIfExists_by_name_butNotThisId_docType(ByVal methodName As String, ByVal id As Integer) As Boolean


        Dim query = (From w In _context.MethodOfPaymentPO
                     Where w.MethodName = methodName _
                     And w.MethodOfPaymentPO_Id <> id _
                     Select w).FirstOrDefault()
        If (query IsNot Nothing) Then
            Return True
        Else
            Return False
        End If

    End Function

    Function GetMethodOfPaymentPO_by_id(ByVal methodId As Integer) As MethodOfPaymentPO


        Dim query = (From w In _context.MethodOfPaymentPO
                     Where w.MethodOfPaymentPO_Id = methodId
                     Select w).FirstOrDefault()

        Return query

    End Function

    Function GetMethodOfPaymentPO_by_methodName_docType(ByVal methodName As String) As MethodOfPaymentPO


        Dim query = (From w In _context.MethodOfPaymentPO
                     Where w.MethodName = methodName _
                     Select w).FirstOrDefault()

        Return query

    End Function

    Function GetAllMethodOfPaymentPO() As List(Of MethodOfPaymentPO)

        Dim query = (From w In _context.MethodOfPaymentPO
                     Order By w.MethodName Ascending
                     Select w).ToList()

        Return query

    End Function

    Function InsertMethodOfPaymentPO(ByVal newMethod As MethodOfPaymentPO) As MethodOfPaymentPO


        _context.MethodOfPaymentPO.Add(newMethod)
        _context.SaveChanges()

        Return newMethod

    End Function

    Function UpdateMethodOfPaymentPO_by_model(ByVal model As MethodOfPaymentPO)


        Dim query = (From w In _context.MethodOfPaymentPO
                     Where w.MethodOfPaymentPO_Id = model.MethodOfPaymentPO_Id
                     Select w).FirstOrDefault()

        query.MethodName = model.MethodName
        query.Active = model.Active
        _context.SaveChanges()

    End Function

    Function DeleteMethodOfPaymentPO_by_id(ByVal methodId As Integer)


        Dim query = (From w In _context.MethodOfPaymentPO
                     Where w.MethodOfPaymentPO_Id = methodId
                     Select w).FirstOrDefault()

        _context.MethodOfPaymentPO.Remove(query)
        _context.SaveChanges()

    End Function


    Function GetAllActiveMethodOfPaymentPOName() As List(Of String)

        Dim query = (From w In _context.MethodOfPaymentPO
                     Where w.Active = True _
                     Order By w.MethodName Ascending
                     Select w.MethodName).ToList()

        Return query

    End Function


End Class
