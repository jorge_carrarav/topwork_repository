﻿Imports TopWork.UserDataProvider

Public Class QuotationDataProvider : Inherits Context



    Function InsertQuotation(ByVal newQuot As Quotation)

        If (newQuot.ContactName Is Nothing) Then
            newQuot.ContactName = ""
        End If
        If (newQuot.WorkDestiny Is Nothing) Then
            newQuot.WorkDestiny = ""
        End If
        If (newQuot.Observation Is Nothing) Then
            newQuot.Observation = ""
        End If

        _context.Quotation.Add(newQuot)
        _context.SaveChanges()

    End Function

    Function UpdateQuotation(ByVal newQuot As Quotation)


        Dim oldQuot As Quotation = (From q In _context.Quotation
                                    Where q.Quotation_id = newQuot.Quotation_id
                                    Select q).FirstOrDefault()

        oldQuot.ContactName = newQuot.ContactName
        oldQuot.MethodOfPaymentQ_Id = newQuot.MethodOfPaymentQ_Id
        oldQuot.WorkTypeQ_Id = newQuot.WorkTypeQ_Id
        oldQuot.WorkDestiny = newQuot.WorkDestiny
        oldQuot.ValidityDate = newQuot.ValidityDate
        oldQuot.Observation = newQuot.Observation

        If (oldQuot.ContactName Is Nothing) Then
            oldQuot.ContactName = ""
        End If
        If (oldQuot.WorkDestiny Is Nothing) Then
            oldQuot.WorkDestiny = ""
        End If
        If (oldQuot.Observation Is Nothing) Then
            oldQuot.Observation = ""
        End If

        _context.SaveChanges()

    End Function

    Function DesactivateQuotation(ByVal quotId As Integer)

        Dim quot As Quotation = (From q In _context.Quotation
                                    Where q.Quotation_id = quotId
                                    Select q).FirstOrDefault()

        quot.Active = False

        _context.SaveChanges()

    End Function


    Function GetAllActiveQuotations()

        Dim quotation_list As List(Of Quotation) = (From q In _context.Quotation
                                                    Where q.Active = True
                                                    Order By q.CreationDate Descending
                                                    Select q).ToList

        Return quotation_list

    End Function

    Function InserNewQuotationVersion(ByVal newQuot As Quotation)


        Dim quotHistoryDP As New QuotationHistoryDataProvider

        Dim oldQuot As Quotation = (From q In _context.Quotation
                                    Where q.Quotation_id = newQuot.Quotation_id
                                    Select q).FirstOrDefault()

        Dim newQuotHistory As New QuotationHistory
        newQuotHistory.Client_Id = oldQuot.Client_Id
        newQuotHistory.Quotation_id = oldQuot.Quotation_id
        newQuotHistory.User_Id = oldQuot.User_Id
        newQuotHistory.WorkTypeQ_Id = oldQuot.WorkTypeQ_Id
        newQuotHistory.MethodOfPaymentQ_Id = oldQuot.MethodOfPaymentQ_Id
        newQuotHistory.ContactName = oldQuot.ContactName
        newQuotHistory.QuotationNumber = oldQuot.QuotationNumber
        newQuotHistory.Version = oldQuot.Version
        newQuotHistory.WorkDestiny = oldQuot.WorkDestiny
        newQuotHistory.ValidityDate = oldQuot.ValidityDate
        newQuotHistory.Observation = oldQuot.Observation
        newQuotHistory.CreationDate = oldQuot.CreationDate
        newQuotHistory.IVAPercentage = oldQuot.IVAPercentage

        'Copiar Quotation a QuotationHistory
        quotHistoryDP.InsertQuotationHistory(newQuotHistory)




        'Guardar cambio en Quotation y asignar Version
        oldQuot.MethodOfPaymentQ_Id = newQuot.MethodOfPaymentQ_Id
        oldQuot.WorkTypeQ_Id = newQuot.WorkTypeQ_Id
        If (newQuot.WorkDestiny Is Nothing) Then
            oldQuot.WorkDestiny = ""
        Else
            oldQuot.WorkDestiny = newQuot.WorkDestiny
        End If
        oldQuot.ValidityDate = newQuot.ValidityDate

        If (newQuot.Observation Is Nothing) Then
            oldQuot.Observation = ""
        Else
            oldQuot.Observation = newQuot.Observation
        End If

        oldQuot.Version = newQuot.Version
        oldQuot.CreationDate = newQuot.CreationDate
        oldQuot.User_Id = newQuot.User_Id

        _context.SaveChanges()

    End Function

    Function GetAllQuotationVersionsButNoLast_by_quotId(ByVal quotId As Integer) As List(Of QuotationOldVersion_Formatted)



        Dim quotList As List(Of QuotationHistory) = (From q In _context.QuotationHistory
                                    Where q.Quotation_id = quotId
                                    Order By q.Version.Length, q.Version Ascending
                                    Select q).ToList

        quotList.Reverse()

        Dim quotOldVersion_list As New List(Of QuotationOldVersion_Formatted)
        Dim userDP As New UserDataProvider

        For Each quotVersion As QuotationHistory In quotList

            Dim oldVersion As New QuotationOldVersion_Formatted

            oldVersion.Quotation_Num = quotVersion.QuotationNumber
            oldVersion.Version = quotVersion.Version
            oldVersion.CreatedDate = quotVersion.CreationDate

            Dim userInfo As UserInfo = userDP.GetUserInfo_by_id(quotVersion.User_Id)

            If (userInfo.LastName.Split(" ").Count = 0) Then
                oldVersion.CreatedBy = userInfo.FirstName
            ElseIf (userInfo.LastName.Split(" ").Count = 1) Then
                oldVersion.CreatedBy = userInfo.FirstName + " " + userInfo.LastName.Split(" ")(0).Substring(0, 1) + ". "
            ElseIf (userInfo.LastName.Split(" ").Count = 2) Then
                oldVersion.CreatedBy = userInfo.FirstName + " " + userInfo.LastName.Split(" ")(0).Substring(0, 1) + ". " + userInfo.LastName.Split(" ")(1).Substring(0, 1) + "."
            End If

            quotOldVersion_list.Add(oldVersion)

        Next

        Return quotOldVersion_list

    End Function


    Function GetAllActiveQuotations_formattedForList_by_quotNumVersion_focusDate(ByVal quotNumberVersion As String, ByVal mustBeExact As Boolean, ByVal focusDate As DateTime) As List(Of Quotation_List_Formatted)

        Dim quotVersion As String = ""
        If (quotNumberVersion.Split(".").Count > 1) Then
            quotVersion = quotNumberVersion.Split(".")(1)
        End If
        Dim quotNumber = quotNumberVersion.Split(".")(0)


        Dim quotations_list As List(Of Quotation)

        If (quotNumberVersion = -1) Then
            quotations_list = (From q In _context.Quotation
                                  Where q.Active = True _
                                  And q.CreationDate > focusDate
                                  Order By q.CreationDate Descending
                                  Select q).ToList()
        Else
            If (mustBeExact) Then
                If (quotVersion = "") Then
                    quotations_list = (From q In _context.Quotation
                                      Where q.Active = True _
                                      And q.QuotationNumber = quotNumber _
                                      And q.CreationDate > focusDate
                                      Select q).ToList()
                Else
                    quotations_list = (From q In _context.Quotation
                                      Where q.Active = True _
                                      And q.QuotationNumber = quotNumber _
                                      And q.Version = quotVersion _
                                      And q.CreationDate > focusDate
                                      Select q).ToList()
                End If
            Else
                If (quotVersion = "") Then
                    quotations_list = (From q In _context.Quotation
                                      Where q.Active = True _
                                      And q.QuotationNumber.Contains(quotNumber) _
                                      And q.CreationDate > focusDate
                                      Select q).ToList()
                Else
                    quotations_list = (From q In _context.Quotation
                                      Where q.Active = True _
                                      And q.QuotationNumber.Contains(quotNumber) _
                                      And q.Version = quotVersion _
                                      And q.CreationDate > focusDate
                                      Select q).ToList()
                End If
            End If


        End If


        Dim quotationList As List(Of Quotation_List_Formatted) = Me.GetAllQuotations_formattedForList_by_quotationList(quotations_list)

        Return quotationList

    End Function

    Function GetAllActiveQuotations_formattedForList_by_workOrderNumber(ByVal workOrderNumber As Integer) As List(Of Quotation_List_Formatted)

        Dim allQuotations_list As List(Of Quotation) = (From q In _context.Quotation
                                                          Join w In _context.WorkOrder On w.Quotation_id Equals q.Quotation_id
                                                          Where q.Active = True _
                                                          And w.WorkOrderNumber = workOrderNumber
                                                          Order By q.CreationDate Descending
                                                          Select q).ToList()

        Return Me.GetAllQuotations_formattedForList_by_quotationList(allQuotations_list)

    End Function

    Function GetAllActiveQuotations_formattedForList_by_purchaseOrderNumber(ByVal purchaseOrderNumber As Integer) As List(Of Quotation_List_Formatted)

        Dim allQuotations_list As List(Of Quotation) = (From q In _context.Quotation
                                                          Join w In _context.WorkOrder On w.Quotation_id Equals q.Quotation_id
                                                          Where q.Active = True _
                                                          And w.PurchaseOrderNumber = purchaseOrderNumber
                                                          Order By q.CreationDate Descending
                                                          Select q).ToList()

        Return Me.GetAllQuotations_formattedForList_by_quotationList(allQuotations_list)

    End Function

    Function GetAllActiveQuotations_formattedForList_by_clientName(ByVal clientName As String) As List(Of Quotation_List_Formatted)

        Dim allQuotations_list As List(Of Quotation) = (From q In _context.Quotation
                                                          Join c In _context.Client On c.Client_Id Equals q.Client_Id
                                                          Where q.Active = True _
                                                          And c.Name = clientName
                                                          Order By q.CreationDate Descending
                                                          Select q).ToList()

        Return Me.GetAllQuotations_formattedForList_by_quotationList(allQuotations_list)

    End Function

    Function GetAllActiveQuotations_formattedForList_by_clientRutNumber(ByVal rutNumber As String) As List(Of Quotation_List_Formatted)

        Dim numberOnly As String = rutNumber.Split("-")(0)

        Dim allQuotations_list As List(Of Quotation) = (From q In _context.Quotation
                                                          Join c In _context.Client On c.Client_Id Equals q.Client_Id
                                                          Where q.Active = True _
                                                          And c.RutNumber = numberOnly
                                                          Order By q.CreationDate Descending
                                                          Select q).ToList()


        Return Me.GetAllQuotations_formattedForList_by_quotationList(allQuotations_list)

    End Function

    Function GetAllActiveQuotations_formattedForList_by_wayBillNumber(ByVal wayBillNumber As String) As List(Of Quotation_List_Formatted)

        Dim allQuotations_list As List(Of Quotation) = (From q In _context.Quotation
                                                      Join w In _context.WorkOrder On w.Quotation_id Equals q.Quotation_id
                                                      Join wb In _context.WayBill On wb.WorkOrder_Id Equals w.WorkOrder_Id
                                                      Where q.Active = True _
                                                      And wb.WayBillNumber = wayBillNumber
                                                      Order By q.CreationDate Descending
                                                      Select q).ToList()


        Return Me.GetAllQuotations_formattedForList_by_quotationList(allQuotations_list)

    End Function

    Function GetAllActiveQuotations_formattedForList_by_invoiceNumber(ByVal invoiceNumber As String) As List(Of Quotation_List_Formatted)

        Dim allQuotations_list As List(Of Quotation) = (From q In _context.Quotation
                                                          Join w In _context.WorkOrder On w.Quotation_id Equals q.Quotation_id
                                                          Join i In _context.Invoice On i.WorkOrder_Id Equals w.WorkOrder_Id
                                                          Where q.Active = True _
                                                          And i.InvoiceNumber = invoiceNumber
                                                          Order By q.CreationDate Descending
                                                          Select q).ToList()


        Return Me.GetAllQuotations_formattedForList_by_quotationList(allQuotations_list)

    End Function

    Function GetAllQuotations_formattedForList_by_quotationList(ByVal quotation_list As List(Of Quotation)) As List(Of Quotation_List_Formatted)

        Dim woStatusDP As New WorkStatusDataProvider
        Dim woDP As New WorkOrderDataProvider
        Dim tool As New Tools

        Dim allClients_list As List(Of Client) = (From c In _context.Client
                                                       Select c).ToList

        Dim allWorkTypeQ_list As List(Of WorkTypeQ) = (From wt In _context.WorkTypeQ
                                                 Select wt).ToList

        Dim allWorkOrders_list As List(Of WorkOrder) = (From wo In _context.WorkOrder
                                                 Select wo).ToList




        Dim allWO_list As List(Of WorkOrder) = woDP.GetAllWorkOrders()

        Dim quot_list_formatted As New List(Of Quotation_List_Formatted)

        For Each quot In quotation_list

            Dim newQuot As New Quotation_List_Formatted

            Dim qVersion As String = ""
            If (quot.Version <> 1) Then
                qVersion = "." + quot.Version
            End If

            Dim work As WorkOrder = (From wo In allWorkOrders_list
                                Where wo.Active = True _
                                And wo.Quotation_id = quot.Quotation_id
                                Select wo).FirstOrDefault()

            newQuot.Quotation_Id = quot.Quotation_id
            newQuot.CreationDate = tool.GetFormattedDate(quot.CreationDate)
            newQuot.ClientName = (From c In allClients_list
                                 Where c.Client_Id = quot.Client_Id
                                 Select c.Name).FirstOrDefault()
            newQuot.QuotationNumberVersion = quot.QuotationNumber + qVersion
            newQuot.WorkTypeQ = (From wt In allWorkTypeQ_list
                                Where wt.WorkTypeQ_Id = quot.WorkTypeQ_Id
                                Select wt.TypeName).FirstOrDefault()
            newQuot.ValidityDate = tool.GetFormattedDate(quot.ValidityDate)
            newQuot.NetValue = "$ " + tool.ThousandSeparator(GetNetValue_by_quotId_version(quot.Quotation_id, quot.Version))

            If (work IsNot Nothing) Then
                newQuot.WorkOrder_Id = work.WorkOrder_Id
                newQuot.WorkOrderNumber = work.WorkOrderNumber
            Else
                newQuot.WorkOrder_Id = ""
                newQuot.WorkOrderNumber = ""
            End If

            newQuot.Status = woStatusDP.GetWorkStatus_by_quotId_by_version(quot.Quotation_id, quot.Version).StatusName

            quot_list_formatted.Add(newQuot)

        Next

        Return quot_list_formatted


    End Function

    Function GetActiveQuotation_formattedForAddEdit_by_quotId(ByVal quotId As String) As Quotation_AddEdit_Formatted

        Dim quotationDP As New QuotationDataProvider
        Dim clientDP As New ClientDataProvider
        Dim prodOrServDP As New ProductOrServiceQDataProvider
        Dim quotProdOrServDP As New QuotationProductOrServiceDataProvider
        Dim quotationStatusDP As New WorkStatusDataProvider
        Dim workTypeQDP As New WorkTypeQDataProvider
        Dim methodOfPaymentDP As New MethodOfPaymentQDataProvider
        Dim appConfigDP As New AppConfigurationDataProvider
        Dim userDP As New UserDataProvider
        Dim woStatusDP As New WorkStatusDataProvider
        Dim tool As New Tools

        Dim quotation As Quotation = quotationDP.GetActiveQuotation_by_quotId(quotId)
        Dim client As Client = clientDP.GetActiveClient_by_clientId(quotation.Client_Id)


        Dim quotAddEditModel As New Quotation_AddEdit_Formatted

        Dim quotationNumberVersion As String = quotation.QuotationNumber
        If (quotation.Version > 1) Then
            quotationNumberVersion += "." + quotation.Version
        End If


        quotAddEditModel.Quotation_Id = quotId
        quotAddEditModel.ClientRut = client.RutNumber
        quotAddEditModel.QuotationNumberVersion = quotationNumberVersion
        quotAddEditModel.ContactName = quotation.ContactName
        quotAddEditModel.WorkDestiny = quotation.WorkDestiny
        quotAddEditModel.WorkTypeQ = workTypeQDP.GetActiveWorkTypeQ_by_workTypeId(quotation.WorkTypeQ_Id).TypeName
        quotAddEditModel.MethodOfPayment = methodOfPaymentDP.GetMethodOfPaymentQ_by_id(quotation.MethodOfPaymentQ_Id).MethodName
        quotAddEditModel.ValidityDate = tool.GetFormattedDate(quotation.ValidityDate.ToString())
        quotAddEditModel.StatusName = woStatusDP.GetWorkStatus_by_quotId_by_version(quotation.Quotation_id, quotation.Version).StatusName
        quotAddEditModel.Observations = quotation.Observation
        quotAddEditModel.Version = quotation.Version
        quotAddEditModel.IVAPercentage = quotation.IVAPercentage


        Dim userInfo As UserInfo = userDP.GetUserInfo_by_id(quotation.User_Id)

        If (userInfo.LastName.Split(" ").Count = 0) Then
            quotAddEditModel.CreatedBy = userInfo.FirstName
        ElseIf (userInfo.LastName.Split(" ").Count = 1) Then
            quotAddEditModel.CreatedBy = userInfo.FirstName + " " + userInfo.LastName.Split(" ")(0).Substring(0, 1) + ". "
        ElseIf (userInfo.LastName.Split(" ").Count = 2) Then
            quotAddEditModel.CreatedBy = userInfo.FirstName + " " + userInfo.LastName.Split(" ")(0).Substring(0, 1) + ". " + userInfo.LastName.Split(" ")(1).Substring(0, 1) + "."
        End If

        quotAddEditModel.CreatedDate = tool.GetFormattedDate(quotation.CreationDate)


        Return quotAddEditModel

    End Function


    Function GetNetValue_by_quotId_version(ByVal quotId As Integer, ByVal version As String) As Integer



        Dim quotProdOrServ_list As List(Of QuotationProductOrService) = (From qps In _context.QuotationProductOrService
                                                                    Where qps.Quotation_id = quotId _
                                                                    And qps.QuotaionVersion = version
                                                                    Select qps).ToList
        Dim netValue As Integer = 0

        For Each quotProdOrServ As QuotationProductOrService In quotProdOrServ_list

            netValue += (quotProdOrServ.Quantity * quotProdOrServ.UnitValue)

        Next

        Return netValue

    End Function



    Function GetAllActiveQuotations_formattedForReport_by_quotationReportModel(ByVal quotationsReportModel As ReportDataProvider.QuotationsReportModel) As List(Of Quotation_Report_Formatted)

        Dim clientDP As New ClientDataProvider
        Dim quotationDP As New QuotationDataProvider
        Dim workTypeDP As New WorkTypeQDataProvider
        Dim quotProdOrServDP As New QuotationProductOrServiceDataProvider
        Dim workStatusDP As New WorkStatusDataProvider
        Dim woDP As New WorkOrderDataProvider
        Dim userDP As New UserDataProvider

        Dim workMaterialDP As New WorkOrderMaterialDataProvider
        Dim tool As New Tools


        Dim allQuotations_list As List(Of Quotation) = Me.GetAllActiveQuotations()
        Dim allWorkOrder_list As List(Of WorkOrder) = woDP.GetAllActiveWorkOrders()
        Dim allClient_list As List(Of Client) = clientDP.GetAllActiveClients()
        Dim allWorkTypeQ_list As List(Of WorkTypeQ) = workTypeDP.GetAllActiveWorkTypeQ
        Dim allUsers_list As List(Of UserCompleteInfoModel) = userDP.GetAllUserCompleteInfo
        Dim allWorkOrderMaterial_list As List(Of WorkOrderMaterial) = workMaterialDP.GetAllWorkMaterial()


        '---------------------------------------------------------------------------------------------------FILTRO ----------------------------------------
        '---------------------------------------------------------------------------------------------------date
        Dim filter_dateFrom As DateTime = New Date(Date.Today.Year, 1, 1)
        Dim filter_dateTo As DateTime = Date.Today
        quotationsReportModel.DateFrom = quotationsReportModel.DateFrom.Replace("/", "-")
        quotationsReportModel.DateTo = quotationsReportModel.DateTo.Replace("/", "-")
        Dim from_day = quotationsReportModel.DateFrom.Split("-")(0)
        Dim from_month = quotationsReportModel.DateFrom.Split("-")(1)
        Dim from_year = quotationsReportModel.DateFrom.Split("-")(2)
        Dim to_day = quotationsReportModel.DateTo.Split("-")(0)
        Dim to_month = quotationsReportModel.DateTo.Split("-")(1)
        Dim to_year = quotationsReportModel.DateTo.Split("-")(2)
        filter_dateFrom = New Date(from_year, from_month, from_day)
        filter_dateTo = New Date(to_year, to_month, to_day)




        '---------------------------------------------------------------------------------------------------Client
        Dim filter_clientId As Integer = -1
        If (Not String.IsNullOrEmpty(quotationsReportModel.ClientRut) And quotationsReportModel.ClientRut <> "Todos") Then
            filter_clientId = clientDP.GetActiveClient_by_rutNumber(quotationsReportModel.ClientRut.Split("-")(0)).Client_Id
        End If
        If (Not String.IsNullOrEmpty(quotationsReportModel.ClientName) And quotationsReportModel.ClientName <> "Todos") Then
            filter_clientId = clientDP.GetActiveClient_by_name(quotationsReportModel.ClientName).Client_Id
        End If

        '---------------------------------------------------------------------------------------------------Montos
        Dim filter_valueType As String = quotationsReportModel.ValueType
        Dim filter_valueFrom As Integer = -1000000000
        Dim filter_valueTo As Integer = 1000000000

        If (quotationsReportModel.ValueFrom IsNot Nothing And quotationsReportModel.ValueFrom <> "Todos") Then
            filter_valueFrom = Convert.ToInt64(quotationsReportModel.ValueFrom.Replace(".", ""))
        End If
        If (quotationsReportModel.ValueTo IsNot Nothing And quotationsReportModel.ValueTo <> "Todos") Then
            filter_valueTo = Convert.ToInt64(quotationsReportModel.ValueTo.Replace(".", ""))
        End If

        '---------------------------------------------------------------------------------------------------Tipo de Trabajo
        Dim filter_workTypeId As Integer = -1
        If (Not String.IsNullOrEmpty(quotationsReportModel.WorkTypeQ) And quotationsReportModel.WorkTypeQ <> "Todos") Then
            filter_workTypeId = workTypeDP.GetActiveWorkTypeQ_by_typeName(quotationsReportModel.WorkTypeQ).WorkTypeQ_Id
        End If

        '---------------------------------------------------------------------------------------------------Estado de Trabajo
        Dim filter_workStatus As String = quotationsReportModel.WorkStatus


        '---------------------------------------------------------------------------------------------------Estado de Trabajo
        Dim filter_createdById As Integer = userDP.GetUserId_byFullName(quotationsReportModel.CreatedBy)

        '---------------------------------------------------------------------------------------------------------------------------------------

        Dim quotProdOrServList As List(Of QuotationProductOrService) = (From qps In _context.QuotationProductOrService
                                                                        Select qps).ToList()


        Dim allWorkOrders_list_formatted As New List(Of Quotation_Report_Formatted)

        Dim itemNumber As Integer = 0
        For Each quot As Quotation In allQuotations_list

            Dim shouldContinue As Boolean = True


            If (filter_dateFrom <= quot.CreationDate And filter_dateTo.AddDays(1) > quot.CreationDate) Then 'DATE FROM - DATE TO


                If (filter_clientId <> -1 And filter_clientId <> quot.Client_Id) Then 'CLIENT ID
                    shouldContinue = False
                End If
                If (shouldContinue) Then

                    If (filter_workTypeId <> -1 And filter_workTypeId <> quot.WorkTypeQ_Id) Then 'WORK TYPE ID
                        shouldContinue = False
                    End If
                    If (shouldContinue) Then


                        If (filter_createdById <> -1 And filter_createdById <> quot.User_Id) Then 'WORK TYPE ID
                            shouldContinue = False
                        End If
                        If (shouldContinue) Then


                            Dim client As Client = (From c In allClient_list
                                                    Where c.Client_Id = quot.Client_Id
                                                    Select c).FirstOrDefault

                            Dim wo As WorkOrder = (From c In allWorkOrder_list
                                                    Where c.Quotation_id = quot.Quotation_id
                                                    Select c).FirstOrDefault

                            Dim quotNumberVersion As String = quot.QuotationNumber.ToString()
                            Dim quotVersion As Integer = quot.Version
                            If (quotVersion <> 1) Then
                                quotNumberVersion = quotNumberVersion.ToString() + "." + quotVersion.ToString()
                            End If


                            Dim quotation_values As ValuesModel = quotProdOrServDP.GetQuotationValues_by_quotation_quotProdOrServList(quot, quotProdOrServList)

                            If (filter_valueType = "Cot. Monto Neto" And (filter_valueFrom > quotation_values.NetValue Or filter_valueTo < quotation_values.NetValue)) Then 'QUOT. NET VALUE
                                shouldContinue = False
                            End If
                            If (shouldContinue) Then
                                If (filter_valueType = "Cot. Monto IVA" And (filter_valueFrom > quotation_values.IVAValue Or filter_valueTo < quotation_values.IVAValue)) Then 'QUOT. IVA VALUE
                                    shouldContinue = False
                                End If
                                If (shouldContinue) Then
                                    If (filter_valueType = "Cot. Monto Total" And (filter_valueFrom > quotation_values.TotalValue Or filter_valueTo < quotation_values.TotalValue)) Then 'QUOT. TOTAL VALUE
                                        shouldContinue = False
                                    End If
                                    If (shouldContinue) Then


                                        Dim user As UserCompleteInfoModel = (From q In allUsers_list
                                                                          Where q.UserId = quot.User_Id
                                                                          Select q).FirstOrDefault()

                                        itemNumber += 1

                                        Dim newQuotationData As New Quotation_Report_Formatted

                                        newQuotationData.ItemNumber = itemNumber
                                        newQuotationData.CreationDate = tool.GetFormattedDate(quot.CreationDate)
                                        newQuotationData.ClientRut = tool.ThousandSeparator(client.RutNumber) + "-" + client.RutNValidator
                                        newQuotationData.ClientName = client.Name
                                        newQuotationData.WorkType = (From wt In allWorkTypeQ_list
                                                                    Where wt.WorkTypeQ_Id = quot.WorkTypeQ_Id
                                                                    Select wt.TypeName).FirstOrDefault

                                        newQuotationData.QuotNumber = quotNumberVersion
                                        If (wo IsNot Nothing) Then
                                            newQuotationData.WONumber = wo.WorkOrderNumber
                                        Else
                                            newQuotationData.WONumber = ""
                                        End If

                                        newQuotationData.CreatedBy = user.FirstName + " " + user.LastName

                                        newQuotationData.QuotNetValue = "$ " + tool.ThousandSeparator(quotation_values.NetValue)
                                        newQuotationData.QuotIVAValue = "$ " + tool.ThousandSeparator(quotation_values.IVAValue)
                                        newQuotationData.QuotTotalValue = "$ " + tool.ThousandSeparator(quotation_values.TotalValue)

                                        newQuotationData.WorkStatus = workStatusDP.GetWorkStatus_by_quotId_by_version(quot.Quotation_id, quot.Version).StatusName

                                        If (filter_workStatus <> "Todos" And filter_workStatus <> newQuotationData.WorkStatus) Then
                                            shouldContinue = False
                                        End If
                                        If (shouldContinue) Then

                                            allWorkOrders_list_formatted.Add(newQuotationData)

                                        End If

                                    End If
                                End If
                            End If

                        End If
                    End If
                End If

            End If

        Next

        Return allWorkOrders_list_formatted

    End Function



    Function GetActiveQuotation_by_quotId(ByVal quotId As Integer) As Quotation

        Dim quot As Quotation = (From q In _context.Quotation
                                  Where q.Quotation_id = quotId _
                                  And q.Active = True
                                  Select q).FirstOrDefault

        Return quot

    End Function

    Function GetActiveQuotation_by_quotNumberVersion(ByVal quotNumberVersion As String) As Quotation

        Dim quotNumber As String = quotNumberVersion.Split(".")(0)
        Dim version As String = 1

        If (quotNumberVersion.Split(".").Count > 1) Then
            version = quotNumberVersion.Split(".")(1)
        End If



        Dim quot As Quotation = (From q In _context.Quotation
                                  Where q.QuotationNumber = quotNumber _
                                  And q.Version = version _
                                  And q.Active = True
                                  Select q).FirstOrDefault

        Return quot

    End Function

    Function GetOldQuotation_by_quotNum_quotVersion(ByVal quotNum As String, ByVal quotVersion As String) As QuotationHistory



        Dim oldVersion As QuotationHistory = (From qh In _context.QuotationHistory
                                  Where qh.QuotationNumber = quotNum _
                                  And qh.Version = quotVersion
                                  Select qh).FirstOrDefault

        Return oldVersion

    End Function


    Function GetNewQuotationNumber() As Integer

        Dim quot_list = (From q In _context.Quotation
                       Select q).ToList

        Dim quot2_list As IEnumerable = quot_list.OrderBy(Function(quot) quot.QuotationNumber.Length).ThenBy(Function(quot) quot.QuotationNumber).Reverse

        Dim query As Integer = (From q In quot2_list
                                Select q.QuotationNumber).FirstOrDefault

        Return query + 1

    End Function



    Public Class Quotation_List_Formatted

        Public Property Quotation_Id As String
        Public Property WorkOrder_Id As String
        Public Property CreationDate As String
        Public Property ClientName As String
        Public Property WorkDestiny As String
        Public Property QuotationNumberVersion As String
        Public Property WorkTypeQ As String
        Public Property Version As String
        Public Property ValidityDate As String
        Public Property NetValue As String
        Public Property WorkOrderNumber As String
        Public Property Status As String

    End Class


    Public Class Quotation_AddEdit_Formatted

        Public Property Quotation_Id As String
        Public Property WorkOrder_Id As String
        Public Property QuotationNumberVersion As String
        Public Property ContactName As String
        Public Property WorkDestiny As String
        Public Property WorkTypeQ As String
        Public Property MethodOfPayment As String
        Public Property ValidityDate As String
        Public Property StatusName As String
        Public Property Observations As String
        Public Property Item_list As String
        Public Property CreatedBy As String
        Public Property CreatedDate As String
        Public Property WorkOrderNumber As String
        Public Property ClientRut As String
        Public Property Version As String
        Public Property IVAPercentage As String
        Public Property OldVersionsInfo As String

    End Class

    Public Class QuotationOldVersion_Formatted

        Public Property Quotation_Num As String
        Public Property Version As String
        Public Property CreatedBy As String
        Public Property CreatedDate As String

    End Class


    Public Class Quotation_Report_Formatted


        Public Property ItemNumber As String
        Public Property CreationDate As String
        Public Property ClientRut As String
        Public Property ClientName As String
        Public Property WorkType As String
        Public Property QuotNumber As String
        Public Property WONumber As String
        Public Property WorkStatus As String
        Public Property CreatedBy As String
        Public Property QuotNetValue As String
        Public Property QuotIVAValue As String
        Public Property QuotTotalValue As String

    End Class



End Class
