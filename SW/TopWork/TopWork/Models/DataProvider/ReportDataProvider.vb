﻿Public Class ReportDataProvider



    Public Class QuotationsReportModel

        Public Property DateFrom As String
        Public Property DateTo As String
        Public Property ClientRut As String
        Public Property ClientName As String
        Public Property ValueFrom As String
        Public Property ValueTo As String
        Public Property ValueType As String
        Public Property WorkTypeQ As String
        Public Property WorkStatus As String
        Public Property CreatedBy As String
        Public Property OrderBy As String

    End Class

    Public Class UtilitiesReportModel

        Public Property DateFrom As String
        Public Property DateTo As String
        Public Property ClientRut As String
        Public Property ClientName As String
        Public Property WorkTypeQ As String
        Public Property WayBillNumber As String
        Public Property InvoiceNumber As String
        Public Property WithPONumber As String
        Public Property ValueFrom As String
        Public Property ValueTo As String
        Public Property ValueType As String
        Public Property WorkStatus As String
        Public Property OrderBy As String

    End Class

End Class
