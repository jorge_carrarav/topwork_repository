﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Globalization
Imports System.Data.Entity

Imports WebMatrix.WebData
Public Class MethodOfPaymentQDataProvider : Inherits Context


    Function CheckIfExists_by_name_docType(ByVal methodName As String, ByVal docType As String) As Boolean


        Dim query = (From w In _context.MethodOfPaymentQ
                     Where w.MethodName = methodName _
                     Select w).FirstOrDefault()
        If (query IsNot Nothing) Then
            Return True
        Else
            Return False
        End If

    End Function

    Function CheckIfExists_by_name_butNotThisId_docType(ByVal methodName As String, ByVal id As Integer) As Boolean


        Dim query = (From w In _context.MethodOfPaymentQ
                     Where w.MethodName = methodName _
                     And w.MethodOfPaymentQ_Id <> id _
                     Select w).FirstOrDefault()
        If (query IsNot Nothing) Then
            Return True
        Else
            Return False
        End If

    End Function

    Function GetMethodOfPaymentQ_by_id(ByVal methodId As Integer) As MethodOfPaymentQ


        Dim query = (From w In _context.MethodOfPaymentQ
                     Where w.MethodOfPaymentQ_Id = methodId
                     Select w).FirstOrDefault()

        Return query

    End Function

    Function GetMethodOfPaymentQ_by_methodName(ByVal methodName As String) As MethodOfPaymentQ


        Dim query = (From w In _context.MethodOfPaymentQ
                     Where w.MethodName = methodName _
                     Select w).FirstOrDefault()

        Return query

    End Function

    Function GetAllMethodOfPaymentQ() As List(Of MethodOfPaymentQ)

        Dim query = (From w In _context.MethodOfPaymentQ
                     Order By w.MethodName Ascending
                     Select w).ToList()

        Return query

    End Function

    Function InsertMethodOfPaymentQ(ByVal newMethod As MethodOfPaymentQ) As MethodOfPaymentQ


        _context.MethodOfPaymentQ.Add(newMethod)
        _context.SaveChanges()

        Return newMethod

    End Function

    Function UpdateMethodOfPaymentQ_by_model(ByVal model As MethodOfPaymentQ)


        Dim query = (From w In _context.MethodOfPaymentQ
                     Where w.MethodOfPaymentQ_Id = model.MethodOfPaymentQ_Id
                     Select w).FirstOrDefault()

        query.MethodName = model.MethodName
        query.Active = model.Active
        _context.SaveChanges()

    End Function

    Function DeleteMethodOfPaymentQ_by_id(ByVal methodId As Integer)


        Dim query = (From w In _context.MethodOfPaymentQ
                     Where w.MethodOfPaymentQ_Id = methodId
                     Select w).FirstOrDefault()

        _context.MethodOfPaymentQ.Remove(query)
        _context.SaveChanges()

    End Function

    Function MethodOfPaymentQHasQuotations_by_id(ByVal methodId As Integer)


        Dim query = (From q In _context.Quotation
                     Where q.MethodOfPaymentQ_Id = methodId
                     Select q).FirstOrDefault()

        If (query IsNot Nothing) Then
            Return True
        Else
            Return False

        End If

    End Function


    Function GetAllActiveMethodOfPaymentQName() As List(Of String)

        Dim query = (From w In _context.MethodOfPaymentQ
                     Where w.Active = True _
                     Order By w.MethodName Ascending
                     Select w.MethodName).ToList()

        Return query

    End Function


End Class
