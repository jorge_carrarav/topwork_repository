﻿Public Class ProductOrServicePODataProvider : Inherits Context



    Function InsertProductOrServicePO(ByVal prodOrServ As ProductOrServicePO)

        _context.ProductOrServicePO.Add(prodOrServ)
        _context.SaveChanges()

    End Function


    Function GetAllProductOrServicePO() As List(Of ProductOrServicePO)

        Dim prodOrServ_list As List(Of ProductOrServicePO) = (From p In _context.ProductOrServicePO
                                                            Select p).ToList()

        Return prodOrServ_list

    End Function


    Function GetProductOrServicePO_by_id(ByVal prodOrServId As Integer) As ProductOrServicePO

        Dim query = (From p In _context.ProductOrServicePO
                     Where p.ProductOrServicePO_Id = prodOrServId
                     Select p).FirstOrDefault()

        Return query

    End Function

    Function GetProductOrServicePO_by_description(ByVal description As String) As ProductOrServicePO



        Dim query = (From p In _context.ProductOrServicePO
                     Where p.Description = description
                     Select p).FirstOrDefault()

        Return query

    End Function


    Public Class ProductOrServicePO_Item

        Public Property ProductOrServicePO_Id As String
        Public Property ItemNumber As String
        Public Property Description As String
        Public Property Quantity As Integer
        Public Property UnitValue As Integer
        Public Property UnitOfMeasure As String

    End Class

End Class
