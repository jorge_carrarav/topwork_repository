﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Globalization
Imports System.Data.Entity

Imports WebMatrix.WebData
Public Class WorkTypePODataProvider : Inherits Context



    Function CheckIfExists_by_name_docType(ByVal typeName As String) As Boolean

        Dim query = (From w In _context.WorkTypePO
                     Where w.TypeName = typeName _
                     Select w).FirstOrDefault()
        If (query IsNot Nothing) Then
            Return True
        Else
            Return False
        End If

    End Function

    Function CheckIfExists_by_name_butNotThisId_docType(ByVal typeName As String, ByVal typeId As Integer) As Boolean


        Dim query = (From w In _context.WorkTypePO
                     Where w.TypeName = typeName _
                     And w.WorkTypePO_Id <> typeId _
                     Select w).FirstOrDefault()
        If (query IsNot Nothing) Then
            Return True
        Else
            Return False
        End If

    End Function

    Function GetActiveWorkTypePO_by_workTypeId(ByVal typeId As Integer) As WorkTypePO

        Dim workType = (From w In _context.WorkTypePO
                     Where w.WorkTypePO_Id = typeId _
                     And w.Active = True
                     Select w).FirstOrDefault()

        Return workType

    End Function
    Function GetWorkTypePO_by_workTypeId(ByVal typeId As Integer) As WorkTypePO

        Dim workType = (From w In _context.WorkTypePO
                     Where w.WorkTypePO_Id = typeId
                     Select w).FirstOrDefault()

        Return workType

    End Function

    Function GetActiveWorkTypePO_by_typeName(ByVal typeName As String) As WorkTypePO

        Dim query = (From w In _context.WorkTypePO
                     Where w.TypeName = typeName _
                     And w.Active = True _
                     Select w).FirstOrDefault()

        Return query

    End Function

    Function InsertWorkTypePO(ByVal workTypeModel As WorkTypePO) As WorkTypePO

        Dim newType As New WorkTypePO
        newType.TypeName = workTypeModel.TypeName
        newType.Active = workTypeModel.Active

        _context.WorkTypePO.Add(newType)
        _context.SaveChanges()

        Return newType
    End Function

    Function GetAllActiveWorkTypePO() As List(Of String)

        Dim query As List(Of String) = (From w In _context.WorkTypePO
                                          Where w.Active = True
                                          Order By w.TypeName Ascending
                                        Select w.TypeName).ToList()

        Return query

    End Function
    Function GetAllWorkTypePOs() As List(Of WorkTypePO)

        Dim query As List(Of WorkTypePO) = (From w In _context.WorkTypePO
                                          Order By w.TypeName Ascending
                                        Select w).ToList()

        Return query

    End Function

    Function UpdateWorkTypePO_by_model(ByVal model As WorkTypePO)

        Dim query = (From w In _context.WorkTypePO
                     Where w.WorkTypePO_Id = model.WorkTypePO_Id
                     Select w).FirstOrDefault()

        query.TypeName = model.TypeName
        query.Active = model.Active

        _context.SaveChanges()

    End Function

    Function DeleteWorkTypePO_by_id(ByVal typeId As Integer)


        Dim query = (From w In _context.WorkTypePO
                     Where w.WorkTypePO_Id = typeId
                     Select w).FirstOrDefault()

        _context.WorkTypePO.Remove(query)
        _context.SaveChanges()

    End Function



End Class
