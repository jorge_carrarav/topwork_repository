﻿Public Class UnitOfMeasureQDataProvider : Inherits Context



    Function GetUnitOfMeasureQ_by_shortName(ByVal shortName As String) As UnitOfMeasureQ

        Dim unitOfMeasure As UnitOfMeasureQ = (From u In _context.UnitOfMeasureQ
                                                      Where u.ShortName = shortName
                                                      Select u).FirstOrDefault()

        Return unitOfMeasure

    End Function

    Function GetUnitOfMeasureQ_by_fullName(ByVal fullName As String) As UnitOfMeasureQ

        Dim unitOfMeasure As UnitOfMeasureQ = (From u In _context.UnitOfMeasureQ
                                                      Where u.FullName = fullName
                                                      Select u).FirstOrDefault()

        Return unitOfMeasure

    End Function

    Function GetUnitOfMeasureQ_by_id(ByVal unitOfMeasId As Integer) As UnitOfMeasureQ

        Dim unitOfMeasure As UnitOfMeasureQ = (From u In _context.UnitOfMeasureQ
                                                      Where u.UnitOfMeasureQ_Id = unitOfMeasId
                                                      Select u).FirstOrDefault()

        Return unitOfMeasure

    End Function

    Function GetAllActiveUnitOfMeasureQ() As List(Of UnitOfMeasureQ)

        Dim unitOfMeasure_list As List(Of UnitOfMeasureQ) = (From u In _context.UnitOfMeasureQ
                                                                      Where u.Active = True
                                                                      Select u).ToList

        Return unitOfMeasure_list

    End Function

    Function GetAllActiveUnitOfMeasureQ_shortName() As List(Of String)

        Dim unitOfMeas_shortName_list As List(Of String) = (From u In _context.UnitOfMeasureQ
                                                                      Where u.Active = True
                                                                      Order By u.ShortName Ascending
                                                                      Select u.ShortName).ToList

        Return unitOfMeas_shortName_list

    End Function


End Class
