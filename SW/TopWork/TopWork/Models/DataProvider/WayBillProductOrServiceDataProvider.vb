﻿Imports TopWork.Context

Public Class WayBillProductOrServiceDataProvider : Inherits Context


    Function InsertWayBillProductOrService(ByVal wbProdOrServ As WayBillProductOrService)

        _context.WayBillProductOrService.Add(wbProdOrServ)
        _context.SaveChanges()

    End Function


    Public Function GetAllWayBillProdOrServ_item_by_workId(ByVal workId As Integer) As List(Of WayBillProductOrService_Item)

        Dim prodOrServ_list As List(Of ProductOrServiceQ) = (From p In _context.ProductOrServiceQ
                                                            Select p).ToList()

        Dim wayBillProdOrServ_list As List(Of WayBillProductOrService) = Me.GetAllWayBillProdOrServ_by_workId(workId)

        Dim wayBillProdOrServ_item_list As New List(Of WayBillProductOrService_Item)


        For Each wbProdOrServ In wayBillProdOrServ_list

            Dim wbProdOrServ_Item As New WayBillProductOrService_Item

            Dim desc As String = (From p In prodOrServ_list
                                    Where p.ProductOrServiceQ_Id = wbProdOrServ.ProductOrServiceQ_Id
                                    Select p.Description).FirstOrDefault()

            Dim quant As Integer = wbProdOrServ.Quantity

            'Reviso si ya fue añadido el item previamente. De ser así, sumo las cantidades, de lo contrario añado el item completamente
            Dim addedItem As WayBillProductOrService_Item = (From i In wayBillProdOrServ_item_list
                                                             Where i.Description = desc
                                                             Select i).FirstOrDefault()


            If (Not addedItem Is Nothing) Then

                addedItem.Quantity = addedItem.Quantity + quant

            Else
                wbProdOrServ_Item.Description = desc
                wbProdOrServ_Item.Quantity = quant

                wayBillProdOrServ_item_list.Add(wbProdOrServ_Item)
            End If

        Next

        Return wayBillProdOrServ_item_list

    End Function

    Public Function GetAllWayBillProdOrServ_by_workId(ByVal workId As Integer) As List(Of WayBillProductOrService)

        Dim wayBill_list As List(Of WayBill) = (From w In _context.WayBill
                                       Where w.WorkOrder_Id = workId
                                       Select w).ToList()

        Dim wbProdOrServ_list As New List(Of WayBillProductOrService)

        For Each wayBill As WayBill In wayBill_list
            Dim wbProdOrServ_temp_list As List(Of WayBillProductOrService) = (From p In _context.WayBillProductOrService
                                                                           Where p.WayBill_Id = wayBill.WayBill_Id
                                                                           Select p).ToList
            For Each wbProdOrServ As WayBillProductOrService In wbProdOrServ_temp_list
                wbProdOrServ_list.Add(wbProdOrServ)
            Next

        Next


        Return wbProdOrServ_list
    End Function


    Public Class WayBillProductOrService_Item

        Public Property Description As String
        Public Property Quantity As Integer

    End Class


End Class
