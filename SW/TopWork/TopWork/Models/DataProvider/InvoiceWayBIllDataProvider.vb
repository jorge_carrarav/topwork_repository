﻿Public Class InvoiceWayBIllDataProvider : Inherits Context


    Function InsertInvoiceWayBills(ByVal wbProdOrServ As InvoiceWayBill)

        _context.InvoiceWayBill.Add(wbProdOrServ)
        _context.SaveChanges()

    End Function


    Function GetAllInvoiceWayBillNumbers_by_workId(ByVal workId As Integer) As List(Of Integer)

        Dim invWayBill_list As List(Of Integer) = (From i In _context.Invoice
                                                   Join iw In _context.InvoiceWayBill On iw.Invoice_Id Equals i.Invoice_Id
                                                   Join w In _context.WayBill On w.WayBill_Id Equals iw.WayBill_Id
                                                   Where i.WorkOrder_Id = workId
                                                   Select w.WayBillNumber).ToList()

        Return invWayBill_list

    End Function

    Public Function GetAllInvoiceWayBills_by_workId(ByVal workId As Integer) As List(Of String)

        Dim invoice_list As List(Of Invoice) = (From i In _context.Invoice
                                                Where i.WorkOrder_Id = workId
                                                 Select i).ToList()

        Dim allInvoiceWayBills As List(Of InvoiceWayBill) = (From i In _context.InvoiceWayBill
                                                              Select i).ToList()

        Dim invWayBill_list As New List(Of String)

        For Each Invoice As Invoice In invoice_list

            Dim invWayBill_temp_list As List(Of Integer) = (From iw In _context.InvoiceWayBill
                                                            Join i In _context.WayBill On i.WayBill_Id Equals iw.WayBill_Id
                                                             Where iw.Invoice_Id = Invoice.Invoice_Id
                                                             Select i.WayBillNumber).ToList()

            For Each invWBNumber As Integer In invWayBill_temp_list

                invWayBill_list.Add(invWBNumber.ToString())

            Next

        Next

        Return invWayBill_list

    End Function


End Class
