﻿Public Class AppConfigurationDataProvider : Inherits Context




    Public Function InsertAppConfiguration(ByVal configuration As AppConfiguration)

        _context.AppConfiguration.Add(configuration)
        _context.SaveChanges()

    End Function

    Public Function UpdateAppConfiguration(ByVal newConfiguration As AppConfiguration)


        Dim oldConfiguration As AppConfiguration = (From c In _context.AppConfiguration
                                                    Where c.AppConfiguration_Id = newConfiguration.AppConfiguration_Id
                                                    Select c).FirstOrDefault

        oldConfiguration.IVAPercentage = newConfiguration.IVAPercentage
        oldConfiguration.AppStartDate = newConfiguration.AppStartDate
        oldConfiguration.AppExpirationDate = newConfiguration.AppExpirationDate
        oldConfiguration.AppCode = newConfiguration.AppCode

        _context.SaveChanges()

    End Function


    Public Function GetAppConfiguration() As AppConfiguration



        Dim query As AppConfiguration = (From c In _context.AppConfiguration
                                         Order By c.AppConfiguration_Id Descending
                                         Select c).FirstOrDefault()

        Return query


    End Function



End Class
