﻿Public Class WayBillDataProvider : Inherits Context




    Function InsertWayBill(ByVal newWayBill As WayBill)

        _context.WayBill.Add(newWayBill)
        _context.SaveChanges()

        Return newWayBill.WayBill_Id

    End Function

    Function GetAllWayBillNumbers_by_workId(ByVal workId As Integer) As List(Of Integer)

        Dim wayBillNumber_list As List(Of Integer) = (From w In _context.WayBill
                                                Where w.WorkOrder_Id = workId
                                                Select w.WayBillNumber).ToList()

        Return wayBillNumber_list

    End Function

    Function GetAllWayBills_by_workId(ByVal workId As Integer) As List(Of WayBill)

        Dim wayBill_list As List(Of WayBill) = (From w In _context.WayBill
                                                Where w.WorkOrder_Id = workId
                                                Select w).ToList()

        Return wayBill_list

    End Function


    Function GetAllActiveWayBills_formattedForList_by_wayBillNumber(ByVal wayBillNumber As Integer) As List(Of WayBill_List_Formatted)

        Dim wayBill_list As List(Of WayBill)

        If (wayBillNumber = -1) Then
            wayBill_list = (From w In _context.WayBill
                        Where w.Active = True _
                        Order By w.CreationDate Descending
                        Select w).ToList()
        Else
            wayBill_list = (From w In _context.WayBill
                        Where w.Active = True _
                        And w.WayBillNumber = wayBillNumber
                        Order By w.CreationDate Descending
                        Select w).ToList()

        End If


        Return GetAllWayBills_formattedForList_by_wayBillList(wayBill_list)

    End Function

    Function GetAllWayBills_formattedForList_by_wayBillList(ByVal wayBill_list As List(Of WayBill))

        Dim quotationDP As New QuotationDataProvider
        Dim workStatusDP As New WorkStatusDataProvider
        Dim tool As New Tools

        Dim allClients_list As List(Of Client) = (From c In _context.Client
                                          Select c).ToList

        Dim allQuotations_list As List(Of Quotation) = (From q In _context.Quotation
                                                        Select q).ToList
        Dim allWorkOrders_list As List(Of WorkOrder) = (From q In _context.WorkOrder
                                                        Select q).ToList
        Dim allWorkTypes_list As List(Of WorkTypeQ) = (From wt In _context.WorkTypeQ
                                                 Select wt).ToList

        Dim wayBill_list_formatted As New List(Of WayBill_List_Formatted)


        For Each wayBill As WayBill In wayBill_list

            Dim newWayBill As New WayBill_List_Formatted


            newWayBill.WayBillId = wayBill.WayBill_Id
            newWayBill.CreationDate = tool.GetFormattedDate(wayBill.CreationDate)
            newWayBill.ClientName = (From c In allClients_list
                                     Join q In allQuotations_list On q.Client_Id Equals c.Client_Id
                                     Join wo In allWorkOrders_list On wo.Quotation_id Equals q.Quotation_id
                                     Where c.Client_Id = q.Client_Id
                                     Select c.Name).FirstOrDefault()
            newWayBill.WayBillNumber = wayBill.WayBillNumber

            wayBill_list_formatted.Add(newWayBill)

        Next

        Return wayBill_list_formatted

    End Function


    Function GetAllWayBills_addEdit_by_workId(ByVal workId As Integer) As List(Of WayBill_AddEdit_Formatted)

        Dim tool As New Tools

        Dim wayBill_list As List(Of WayBill) = (From w In _context.WayBill
                                                 Where w.WorkOrder_Id = workId
                                                 Select w).ToList()
        Dim wayBillProdOrServ_list As List(Of WayBillProductOrService) = (From p In _context.WayBillProductOrService
                                                                          Select p).ToList()
        Dim prodOrServ_list As List(Of ProductOrServiceQ) = (From p In _context.ProductOrServiceQ
                                                            Select p).ToList()


        Dim wayBillAddEditFormatted_list As New List(Of WayBillDataProvider.WayBill_AddEdit_Formatted)


        For Each wb As WayBill In wayBill_list

            Dim wbAddEditFormatted As New WayBillDataProvider.WayBill_AddEdit_Formatted
            wbAddEditFormatted.WayBillNumber = wb.WayBillNumber
            wbAddEditFormatted.WayBillDate = tool.GetFormattedDate(wb.WayBillDate)
            If (wb.Comment Is Nothing) Then
                wbAddEditFormatted.Comment = ""
            Else
                wbAddEditFormatted.Comment = wb.Comment
            End If

            Dim wayBillProdOrServ_thisWayBill_list As List(Of WayBillProductOrService) = (From p In wayBillProdOrServ_list
                                                                                           Where p.WayBill_Id = wb.WayBill_Id
                                                                                           Select p).ToList()
            Dim wayBillProdOrServItem_list As New List(Of WayBillProductOrServiceDataProvider.WayBillProductOrService_Item)

            For Each wayBillProdOrServ In wayBillProdOrServ_thisWayBill_list

                Dim wayBillProdOrServItem As New WayBillProductOrServiceDataProvider.WayBillProductOrService_Item
                Dim desc As String = (From p In prodOrServ_list
                                             Where p.ProductOrServiceQ_Id = wayBillProdOrServ.ProductOrServiceQ_Id
                                             Select p.Description).FirstOrDefault()

                wayBillProdOrServItem.Description = desc
                wayBillProdOrServItem.Quantity = wayBillProdOrServ.Quantity

                wayBillProdOrServItem_list.Add(wayBillProdOrServItem)

            Next

            wbAddEditFormatted.ItemsAndQuantity_list = wayBillProdOrServItem_list


            wayBillAddEditFormatted_list.Add(wbAddEditFormatted)
        Next

        Return wayBillAddEditFormatted_list

    End Function


    Function DeleteAllWayBills_by_workId(ByVal workId As Integer)

        Dim wayBill_list As List(Of WayBill) = (From w In _context.WayBill
                                                Where w.WorkOrder_Id = workId
                                                Select w).ToList()

        Dim wayBillProdOrServ_list As List(Of WayBillProductOrService)

        For Each wayBill As WayBill In wayBill_list

            wayBillProdOrServ_list = (From p In _context.WayBillProductOrService
                                        Where p.WayBill_Id = wayBill.WayBill_Id
                                        Select p).ToList()

            For Each wbProdOrServ As WayBillProductOrService In wayBillProdOrServ_list

                _context.WayBillProductOrService.Remove(wbProdOrServ)

            Next

            _context.WayBill.Remove(wayBill)

        Next

        _context.SaveChanges()

    End Function





    Public Class WayBill_List_Formatted

        Public Property WayBillId As Integer
        Public Property CreationDate As String
        Public Property ClientName As String
        Public Property WayBillNumber As Integer
        Public Property WorkOrderNumber As String
        Public Property Status As String

    End Class

    Public Class WayBill_AddEdit_Formatted

        Public Property WayBillNumber As Integer
        Public Property WayBillDate As String
        Public Property Comment As String
        Public Property ItemsAndQuantity_list As List(Of WayBillProductOrServiceDataProvider.WayBillProductOrService_Item)

    End Class

End Class
