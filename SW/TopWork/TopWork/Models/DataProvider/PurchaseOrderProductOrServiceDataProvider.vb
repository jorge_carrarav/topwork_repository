﻿Imports TopWork.ProductOrServiceDataProvider

Public Class PurchaseOrderProductOrServiceDataProvider : Inherits Context


    Function InsertPurchaseOrderProdOrServ(ByVal newPurchOderProdOrServ As PurchaseOrderProductOrService)

        _context.PurchaseOrderProductOrService.Add(newPurchOderProdOrServ)
        _context.SaveChanges()

    End Function

    Function GetAllProductOrService_Item_by_purchOrderId(ByVal purchOrderId As Integer) As List(Of ProductOrServicePODataProvider.ProductOrServicePO_Item)


        Dim prodOrServPODP As New ProductOrServicePODataProvider
        Dim unitOfMeasureDP As New UnitOfMeasurePODataProvider

        Dim query As List(Of PurchaseOrderProductOrService) = (From pops In _context.PurchaseOrderProductOrService
                                                           Where pops.PurchaseOrder_id = purchOrderId _
                                                           Order By pops.ItemNumber Ascending
                                                           Select pops).ToList

        Dim prodOrServ_item_list As New List(Of ProductOrServicePODataProvider.ProductOrServicePO_Item)

        For Each purchOrderProdServ As PurchaseOrderProductOrService In query

            Dim prodOrServ_item As New ProductOrServicePODataProvider.ProductOrServicePO_Item

            prodOrServ_item.ProductOrServicePO_Id = purchOrderProdServ.ProductOrServicePO_Id
            prodOrServ_item.ItemNumber = purchOrderProdServ.ItemNumber
            prodOrServ_item.Description = prodOrServPODP.GetProductOrServicePO_by_id(purchOrderProdServ.ProductOrServicePO_Id).Description.Trim()
            prodOrServ_item.Quantity = purchOrderProdServ.Quantity
            prodOrServ_item.UnitValue = purchOrderProdServ.UnitValue
            prodOrServ_item.UnitOfMeasure = unitOfMeasureDP.GetUnitOfMeasurePO_by_id(purchOrderProdServ.UnitOfMeasurePO_Id).ShortName

            prodOrServ_item_list.Add(prodOrServ_item)

        Next

        Return prodOrServ_item_list

    End Function

    Function GetAllProductOrService_Item_by_purchOrderId_purchOrderProdOrServList(ByVal purchOrderId As Integer, ByVal purchOrderProdOrServList As List(Of PurchaseOrderProductOrService)) As List(Of ProductOrServicePODataProvider.ProductOrServicePO_Item)


        Dim prodOrServPODP As New ProductOrServicePODataProvider

        Dim query As List(Of PurchaseOrderProductOrService) = (From pops In purchOrderProdOrServList
                                                               Where pops.PurchaseOrder_id = purchOrderId _
                                                               Order By pops.ItemNumber Ascending
                                                               Select pops).ToList

        Dim prodOrServ_item_list As New List(Of ProductOrServicePODataProvider.ProductOrServicePO_Item)

        For Each purchOrderProdServ As PurchaseOrderProductOrService In query

            Dim prodOrServ_item As New ProductOrServicePODataProvider.ProductOrServicePO_Item

            prodOrServ_item.ProductOrServicePO_Id = purchOrderProdServ.ProductOrServicePO_Id
            prodOrServ_item.ItemNumber = purchOrderProdServ.ItemNumber
            prodOrServ_item.Description = prodOrServPODP.GetProductOrServicePO_by_id(purchOrderProdServ.ProductOrServicePO_Id).Description.Trim()
            prodOrServ_item.Quantity = purchOrderProdServ.Quantity
            prodOrServ_item.UnitValue = purchOrderProdServ.UnitValue

            prodOrServ_item_list.Add(prodOrServ_item)

        Next

        Return prodOrServ_item_list

    End Function

    Function GetAllProductOrService_by_purchOrderId(ByVal purchOrderId As Integer) As List(Of PurchaseOrderProductOrService)

        Dim prodOrServPODP As New ProductOrServicePODataProvider

        Dim purchOrderProdOrServ_list As List(Of PurchaseOrderProductOrService) = (From pops In _context.PurchaseOrderProductOrService
                                                                                   Where pops.PurchaseOrder_id = purchOrderId _
                                                                                   Order By pops.ItemNumber Ascending
                                                                                   Select pops).ToList


        Return purchOrderProdOrServ_list

    End Function

    Function GetPurchOrderValues_by_purchOrder(ByVal purchOrder As PurchaseOrder)

        Dim prodOrServ_item_list As List(Of ProductOrServicePODataProvider.ProductOrServicePO_Item) = Me.GetAllProductOrService_Item_by_purchOrderId(purchOrder.PurchaseOrder_Id)

        Dim netValue_total As Integer = 0

        For Each item As ProductOrServicePODataProvider.ProductOrServicePO_Item In prodOrServ_item_list

            Dim netValue_int As Integer = item.Quantity * item.UnitValue
            netValue_total += netValue_int

        Next

        Dim ivaPercentage As Double = purchOrder.IVAPercentage
        Dim ivaValue As Integer = Math.Round(netValue_total * ivaPercentage / 100)
        Dim totalValue As Integer = netValue_total + ivaValue

        Dim purchOrder_values As New ValuesModel

        purchOrder_values.NetValue = netValue_total
        purchOrder_values.IVAValue = ivaValue
        purchOrder_values.TotalValue = totalValue

        Return purchOrder_values

    End Function


    Function GetPurchaseOrderValues_by_purchOrder_purchOrderProdOrServList(ByVal purchOrder As PurchaseOrder, ByVal purchOrderProdOrServList As List(Of PurchaseOrderProductOrService))

        Dim prodOrServ_item_list As List(Of ProductOrServicePODataProvider.ProductOrServicePO_Item) = Me.GetAllProductOrService_Item_by_purchOrderId_purchOrderProdOrServList(purchOrder.PurchaseOrder_Id, purchOrderProdOrServList)

        Dim netValue_total As Integer = 0

        For Each item As ProductOrServicePODataProvider.ProductOrServicePO_Item In prodOrServ_item_list

            Dim netValue_int As Integer = item.Quantity * item.UnitValue
            netValue_total += netValue_int

        Next

        Dim ivaPercentage As Double = purchOrder.IVAPercentage
        Dim ivaValue As Integer = Math.Round(netValue_total * ivaPercentage / 100)
        Dim totalValue As Integer = netValue_total + ivaValue

        Dim purchOrder_values As New ValuesModel

        purchOrder_values.NetValue = netValue_total
        purchOrder_values.IVAValue = ivaValue
        purchOrder_values.TotalValue = totalValue

        Return purchOrder_values

    End Function


    Function DeleteAllPurchOrderProdOrServ_by_purchaseOrderId(ByVal purchOrderId As Integer)


        Dim query = (From q In _context.PurchaseOrderProductOrService
                     Where q.PurchaseOrder_id = purchOrderId
                     Select q).ToList()

        For Each item In query
            _context.PurchaseOrderProductOrService.Remove(item)
        Next

        _context.SaveChanges()
    End Function

End Class
