﻿Public Class PurchaseOrderDataProvider : Inherits Context

    Function InsertPurchaseOrder(ByVal newPurchOrder As PurchaseOrder)

        If (newPurchOrder.ContactName Is Nothing) Then
            newPurchOrder.ContactName = ""
        End If
        If (newPurchOrder.Observation Is Nothing) Then
            newPurchOrder.Observation = ""
        End If
        If (newPurchOrder.WorkDestiny Is Nothing) Then
            newPurchOrder.WorkDestiny = ""
        End If

        _context.PurchaseOrder.Add(newPurchOrder)
        _context.SaveChanges()

    End Function

    Function UpdatePurchaseOrder(ByVal newPurchOrder As PurchaseOrder)


        Dim oldPurchOrder As PurchaseOrder = (From q In _context.PurchaseOrder
                                    Where q.PurchaseOrder_id = newPurchOrder.PurchaseOrder_Id
                                    Select q).FirstOrDefault()

        oldPurchOrder.ContactName = newPurchOrder.ContactName
        oldPurchOrder.MethodOfPaymentPO_Id = newPurchOrder.MethodOfPaymentPO_Id
        oldPurchOrder.WorkTypePO_Id = newPurchOrder.WorkTypePO_Id
        oldPurchOrder.WorkDestiny = newPurchOrder.WorkDestiny
        oldPurchOrder.ValidityDate = newPurchOrder.ValidityDate
        oldPurchOrder.Observation = newPurchOrder.Observation

        If (oldPurchOrder.ContactName Is Nothing) Then
            oldPurchOrder.ContactName = ""
        End If
        If (oldPurchOrder.WorkDestiny Is Nothing) Then
            oldPurchOrder.WorkDestiny = ""
        End If
        If (oldPurchOrder.Observation Is Nothing) Then
            oldPurchOrder.Observation = ""
        End If

        _context.SaveChanges()

    End Function


    Function DesactivatePurchaseOrder(ByVal purchOrderId As Integer)

        Dim purchOrder As PurchaseOrder = (From po In _context.PurchaseOrder
                                    Where po.PurchaseOrder_Id = purchOrderId
                                    Select po).FirstOrDefault()

        purchOrder.Active = False

        _context.SaveChanges()

    End Function

    Function GetAllActivePurchaseOrder_formattedForList_by_purchaseOrderNumber_focusDate(ByVal purchaseOrderNumber As String, ByVal mustBeExact As Boolean, ByVal focusDate As DateTime) As List(Of PurchaseOrder_List_Formatted)


        Dim purchaseOrder_list As List(Of PurchaseOrder)

        If (purchaseOrderNumber = -1) Then
            purchaseOrder_list = (From p In _context.PurchaseOrder
                                  Where p.Active = True _
                                  And p.CreationDate > focusDate
                                  Order By p.CreationDate Descending
                                  Select p).ToList()

        ElseIf (mustBeExact) Then
            purchaseOrder_list = (From p In _context.PurchaseOrder
                                  Where p.Active = True _
                                  And p.PurchaseOrderNumber = purchaseOrderNumber _
                                  And p.CreationDate > focusDate
                                  Order By p.CreationDate Descending
                                  Select p).ToList()

        Else
            purchaseOrder_list = (From p In _context.PurchaseOrder
                                  Where p.Active = True _
                                  And p.PurchaseOrderNumber.Contains(purchaseOrderNumber) _
                                  And p.CreationDate > focusDate
                                  Order By p.CreationDate Descending
                                  Select p).ToList()



        End If

        Dim purchaseOrder_formatted_List As List(Of PurchaseOrder_List_Formatted) = Me.GetAllPurchaseOrder_formattedForList_by_purchaseOrderList(purchaseOrder_list)

        Return purchaseOrder_formatted_List

    End Function

    Function GetAllPurchaseOrder_formattedForList_by_purchaseOrderList(ByVal purchaseOrder_list As List(Of PurchaseOrder)) As List(Of PurchaseOrder_List_Formatted)

        Dim tool As New Tools

        Dim allSuppliers_list As List(Of Supplier) = (From s In _context.Supplier
                                                       Select s).ToList

        Dim allWorkTypes_list As List(Of WorkTypePO) = (From wt In _context.WorkTypePO
                                                    Select wt).ToList


        Dim purchOrder_list_formatted As New List(Of PurchaseOrder_List_Formatted)

        For Each purchOrder In purchaseOrder_list

            Dim newPurchOrder As New PurchaseOrder_List_Formatted

            newPurchOrder.PurchaseOrder_Id = purchOrder.PurchaseOrder_Id
            newPurchOrder.PurchaseOrderNumber = purchOrder.PurchaseOrderNumber
            newPurchOrder.CreationDate = tool.GetFormattedDate(purchOrder.CreationDate)
            newPurchOrder.SupplierName = (From s In allSuppliers_list
                                         Where s.Supplier_Id = purchOrder.Supplier_Id
                                         Select s.Name).FirstOrDefault()

            newPurchOrder.WorkType = (From wt In allWorkTypes_list
                                        Where wt.WorkTypePO_Id = purchOrder.WorkTypePO_Id
                                        Select wt.TypeName).FirstOrDefault()
            newPurchOrder.ValidityDate = tool.GetFormattedDate(purchOrder.ValidityDate)
            newPurchOrder.NetValue = "$ " + tool.ThousandSeparator(GetNetValue_by_purchaseOrderId(purchOrder.PurchaseOrder_Id))


            purchOrder_list_formatted.Add(newPurchOrder)

        Next

        Return purchOrder_list_formatted

    End Function


    Function GetActivePurchaseOrder_formattedForAddEdit_by_purchOrderId(ByVal purchOrderId As String) As PurchaseOrder_AddEdit_Formatted

        Dim supplierDP As New SupplierDataProvider
        Dim prodOrServDP As New ProductOrServiceQDataProvider
        Dim purchOrderProdOrServDP As New QuotationProductOrServiceDataProvider
        Dim workTypeDP As New WorkTypePODataProvider
        Dim methodOfPaymentDP As New MethodOfPaymentQDataProvider
        Dim appConfigDP As New AppConfigurationDataProvider
        Dim userDP As New UserDataProvider
        Dim tool As New Tools

        Dim purchOrder As PurchaseOrder = Me.GetActivePurchaseOrder_by_purchOrderId(purchOrderId)
        Dim supplier As Supplier = supplierDP.GetActiveSupplier_by_supplierId(purchOrder.Supplier_Id)


        Dim purchOrderAddEditModel As New PurchaseOrder_AddEdit_Formatted

        purchOrderAddEditModel.PurchaseOrder_Id = purchOrderId
        purchOrderAddEditModel.SupplierRut = supplier.RutNumber
        purchOrderAddEditModel.PurchaseOrderNumber = purchOrder.PurchaseOrderNumber
        purchOrderAddEditModel.ContactName = purchOrder.ContactName
        purchOrderAddEditModel.WorkDestiny = purchOrder.WorkDestiny
        purchOrderAddEditModel.WorkType = workTypeDP.GetActiveWorkTypePO_by_workTypeId(purchOrder.WorkTypePO_Id).TypeName
        purchOrderAddEditModel.MethodOfPayment = methodOfPaymentDP.GetMethodOfPaymentQ_by_id(purchOrder.MethodOfPaymentPO_Id).MethodName
        purchOrderAddEditModel.ValidityDate = tool.GetFormattedDate(purchOrder.ValidityDate.ToString())

        purchOrderAddEditModel.Observations = purchOrder.Observation

        purchOrderAddEditModel.IVAPercentage = purchOrder.IVAPercentage


        Dim userInfo As UserInfo = userDP.GetUserInfo_by_id(purchOrder.User_Id)

        If (userInfo.LastName.Split(" ").Count = 0) Then
            purchOrderAddEditModel.CreatedBy = userInfo.FirstName
        ElseIf (userInfo.LastName.Split(" ").Count = 1) Then
            purchOrderAddEditModel.CreatedBy = userInfo.FirstName + " " + userInfo.LastName.Split(" ")(0).Substring(0, 1) + ". "
        ElseIf (userInfo.LastName.Split(" ").Count = 2) Then
            purchOrderAddEditModel.CreatedBy = userInfo.FirstName + " " + userInfo.LastName.Split(" ")(0).Substring(0, 1) + ". " + userInfo.LastName.Split(" ")(1).Substring(0, 1) + "."
        End If

        purchOrderAddEditModel.CreatedDate = tool.GetFormattedDate(purchOrder.CreationDate)


        Return purchOrderAddEditModel

    End Function


    Function GetNetValue_by_purchaseOrderId(ByVal purchaseOrderId As Integer) As Integer

        Dim purchOrderProdOrServ_list As List(Of PurchaseOrderProductOrService) = (From pops In _context.PurchaseOrderProductOrService
                                                                    Where pops.PurchaseOrder_id = purchaseOrderId
                                                                    Select pops).ToList
        Dim netValue As Integer = 0

        For Each purchOrdProdOrServ As PurchaseOrderProductOrService In purchOrderProdOrServ_list

            netValue += (purchOrdProdOrServ.Quantity * purchOrdProdOrServ.UnitValue)

        Next

        Return netValue

    End Function


    Function GetActivePurchaseOrder_by_purchOrderId(ByVal purchOrderId As Integer) As PurchaseOrder

        Dim purchOrder As PurchaseOrder = (From po In _context.PurchaseOrder
                                  Where po.PurchaseOrder_Id = purchOrderId _
                                  And po.Active = True
                                  Select po).FirstOrDefault

        Return purchOrder

    End Function

    Function GetNewPurchaseOrderNumber() As Integer

        Dim purchOrder_list = (From po In _context.PurchaseOrder
                                        Select po).ToList

        Dim purchOrder2_list As IEnumerable = purchOrder_list.OrderBy(Function(purchOrder) purchOrder.PurchaseOrderNumber.Length).ThenBy(Function(purchOrder) purchOrder.PurchaseOrderNumber).Reverse

        Dim query As Integer = (From q In purchOrder2_list
                                Select q.PurchaseOrderNumber).FirstOrDefault

        Return query + 1

    End Function

    Public Class PurchaseOrder_AddEdit_Formatted

        Public Property PurchaseOrder_Id As String
        Public Property ContactName As String
        Public Property PurchaseOrderNumber As String
        Public Property WorkDestiny As String
        Public Property WorkType As String
        Public Property MethodOfPayment As String
        Public Property ValidityDate As String
        Public Property Observations As String
        Public Property Item_list As String
        Public Property CreatedBy As String
        Public Property CreatedDate As String
        Public Property SupplierRut As String
        Public Property IVAPercentage As String

    End Class

    Public Class PurchaseOrder_List_Formatted

        Public Property PurchaseOrder_Id As String
        Public Property PurchaseOrderNumber As String
        Public Property CreationDate As String
        Public Property SupplierName As String
        Public Property WorkDestiny As String
        Public Property WorkType As String
        Public Property ValidityDate As String
        Public Property NetValue As String

    End Class


End Class
