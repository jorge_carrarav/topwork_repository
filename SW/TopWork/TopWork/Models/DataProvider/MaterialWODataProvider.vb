﻿Public Class MaterialWODataProvider : Inherits Context

    Function InsertMaterialWO(ByVal material As MaterialWO)

        _context.MaterialWO.Add(material)
        _context.SaveChanges()

    End Function

    Function GetMaterialWO_by_id(ByVal materialId As Integer) As MaterialWO

        Dim query = (From m In _context.MaterialWO
                     Where m.MaterialWO_Id = materialId
                     Select m).FirstOrDefault()

        Return query

    End Function

    Function GetAllMaterialWOs() As List(Of MaterialWO)

        Dim material_list As List(Of MaterialWO) = (From m In _context.MaterialWO
                                                 Select m).ToList()

        Return material_list

    End Function
    Function GetMaterialWO_by_description(ByVal description As String) As MaterialWO

        Dim query = (From m In _context.MaterialWO
                     Where m.Description = description
                     Select m).FirstOrDefault()

        Return query

    End Function

    Public Class MaterialWO_Item

        Public Property MaterialWO_Id As String
        Public Property ItemNumber As Integer
        Public Property Description As String
        Public Property UnitOfMeasure As String
        Public Property Quantity As Integer
        Public Property NetValue As Integer
        Public Property IVAPercentage As Double

    End Class

End Class
