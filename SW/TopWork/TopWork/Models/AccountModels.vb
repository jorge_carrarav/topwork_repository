﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Globalization
Imports System.Data.Entity

Public Class UsersContext
    Inherits DbContext

    Public Sub New()
        MyBase.New("TopWorkConnection")
    End Sub

    Public Property UserProfiles As DbSet(Of UserProfile)
End Class


Public Class RegisterExternalLoginModel
    <Required()> _
    <Display(Name:="User name")> _
    Public Property UserName As String

    Public Property ExternalLoginData As String
End Class


<Table("UserProfile")> _
Public Class UserProfile
    <Key()> _
    <DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)> _
    Public Property UserId As Integer

    Public Property UserName As String
End Class

Public Class LoginModel
    <Required()> _
    <Display(Name:="User name")> _
    Public Property UserName As String

    <Required()> _
    <DataType(DataType.Password)> _
    <Display(Name:="Password")> _
    Public Property Password As String

    <Display(Name:="Remember me?")> _
    Public Property RememberMe As Boolean
End Class

Public Class UserRegistrationModel

    Public Property UserId As Integer

    <Display(Name:="Nombre de Usuario")> _
    Public Property UserName As String

    <Display(Name:="Password")> _
    Public Property Password As String
    '<DataType(DataType.Password)> _

    <Display(Name:="Nombre")> _
    Public Property FirstName As String

    <Display(Name:="Apellidos")> _
    Public Property LastName As String

    <Display(Name:="Activo")> _
    Public Property Active As Boolean

End Class
