﻿Public Class WorkStatusTemplate


    'Este template se utiliza al momento de instalar la base de datos.

    Public Property ws_a_id As Integer = 1
    Public Property ws_a_name As String = "Sin O.T."

    Public Property ws_b_id As Integer = 2
    Public Property ws_b_name As String = "En Proceso"

    Public Property ws_c_id As Integer = 3
    Public Property ws_c_name As String = "Terminado"

    Public Property ws_d_id As Integer = 4
    Public Property ws_d_name As String = "Facturado"

    Public Property ws_e_id As Integer = 5
    Public Property ws_e_name As String = "Facturado"

End Class