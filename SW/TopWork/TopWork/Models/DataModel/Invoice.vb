
'------------------------------------------------------------------------------
' <auto-generated>
'    This code was generated from a template.
'
'    Manual changes to this file may cause unexpected behavior in your application.
'    Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic


Partial Public Class Invoice

    Public Property Invoice_Id As Integer

    Public Property WorkOrder_Id As Integer

    Public Property InvoiceNumber As Integer

    Public Property InvoiceDate As Date

    Public Property Comment As String

    Public Property NetValue As Integer

    Public Property IsPaid As Boolean

    Public Property IVAPercentage As Double

    Public Property User_Id As Integer

    Public Property CreationDate As Date

    Public Property Active As Boolean



    Public Overridable Property WorkOrder As WorkOrder

    Public Overridable Property InvoiceWayBill As ICollection(Of InvoiceWayBill) = New HashSet(Of InvoiceWayBill)


End Class
