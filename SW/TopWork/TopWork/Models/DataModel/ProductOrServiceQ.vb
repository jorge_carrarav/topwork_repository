
'------------------------------------------------------------------------------
' <auto-generated>
'    This code was generated from a template.
'
'    Manual changes to this file may cause unexpected behavior in your application.
'    Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic


Partial Public Class ProductOrServiceQ

    Public Property ProductOrServiceQ_Id As Integer

    Public Property Description As String



    Public Overridable Property QuotationProductOrService As ICollection(Of QuotationProductOrService) = New HashSet(Of QuotationProductOrService)

    Public Overridable Property WayBillProductOrService As ICollection(Of WayBillProductOrService) = New HashSet(Of WayBillProductOrService)


End Class
