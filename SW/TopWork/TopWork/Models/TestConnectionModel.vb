﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Globalization
Imports System.Data.Entity

Imports WebMatrix.WebData
Public Class TestConnectionModel


    Dim _context As New TopWorkEntities


    Public Function GetConnectionString() As String


        Dim connectionString As String = _context.Database.Connection.ConnectionString

        Return connectionString

    End Function

End Class
