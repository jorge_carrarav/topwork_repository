﻿Imports WebMatrix.WebData


Public Class AccountController
    Inherits System.Web.Mvc.Controller

    Dim _initController As String = "Quotation"
    Dim _initAction As String = "QuotationList"

    <HttpGet>
    Function Login() As ActionResult

       
        If (User.Identity.IsAuthenticated) Then

            If (IsCurrentUserAnAdmin()) Then
                Return RedirectToAction(_initAction, _initController)
            Else
                If (CheckAppCode()) Then
                    If (CheckExpirationDate()) Then
                        Return RedirectToAction(_initAction, _initController)
                    Else
                        Return RedirectToAction("ExpiredLicence")
                    End If
                Else
                    Return RedirectToAction("DataBaseConflict")
                End If
            End If


        End If

        Return View()


    End Function

    <HttpPost>
    Function Login(ByVal model As LoginModel) As ActionResult


        If (model.UserName Is Nothing) Then
            model.UserName = ""
        End If
        If (model.Password Is Nothing) Then
            model.Password = ""
        End If

       

        Dim userInfoDP As New UserDataProvider
        Dim userId As Integer = WebSecurity.GetUserId(model.UserName)
        If (userId <> -1 And userId <> 0) Then

            If (Not userInfoDP.GetUserInfo_by_id(userId).Active) Then
                ModelState.AddModelError("LogOnError", "Usuario desactivado.")
                Return View()
            Else

                If (CheckAppCode() Or model.UserName.ToLower() = "admin") Then

                    If ModelState.IsValid AndAlso WebSecurity.Login(model.UserName, model.Password, persistCookie:=model.RememberMe) Then

                        If (model.UserName = New AdminModel.AdminCredentials().username) Then
                            ViewBag.UserFirstName = userInfoDP.GetUserInfo_by_id(userId).FirstName
                            Return RedirectToAction(_initAction, _initController)
                        Else
                            If (CheckExpirationDate()) Then
                                ViewBag.UserFirstName = userInfoDP.GetUserInfo_by_id(userId).FirstName
                                Return RedirectToAction(_initAction, _initController)
                            Else
                                Return RedirectToAction("ExpiredLicence")
                            End If
                        End If

                    Else
                        ModelState.AddModelError("LogOnError", "Nombre de Usuario o Contraseña incorrecta.")
                        Return View()
                    End If

                Else
                    Return RedirectToAction("DataBaseConflict")
                End If

            End If

        Else

            ModelState.AddModelError("LogOnError", "Nombre de Usuario o Contraseña incorrecta.")
            Return View()

        End If


    End Function


    Function Logout() As ActionResult

        FormsAuthentication.SignOut()
        Return RedirectToAction("Login", "Account")

    End Function



    Function CheckExpirationDate() As Boolean

        Dim appConfiguration As New AppConfigurationDataProvider
        Dim configuration As AppConfiguration = appConfiguration.GetAppConfiguration

        Dim today As DateTime = DateTime.Now
        Dim expirationdate As DateTime = configuration.AppExpirationDate

        If (today < expirationdate) Then
            Return True
        Else
            Return False
        End If

    End Function

    Function CheckAppCode() As Boolean

        Dim appCodeDP As New AppCodeDataProvider
        Dim appConfigurationDP As New AppConfigurationDataProvider
        Dim dbCode As String = appConfigurationDP.GetAppConfiguration().AppCode

        Return appCodeDP.CheckAppCode_by_dbCode(dbCode)

    End Function

    Function DataBaseConflict() As ActionResult

        Return View()

    End Function

    Function ExpiredLicence() As ActionResult

        Return View()

    End Function

    Function NoPermission() As ActionResult

        Return View()

    End Function


    Function IsCurrentUserAnAdmin() As Boolean

        If (User.Identity.Name = New AdminModel.AdminCredentials().username) Then
            Return True
        Else
            Return False
        End If

    End Function


End Class
