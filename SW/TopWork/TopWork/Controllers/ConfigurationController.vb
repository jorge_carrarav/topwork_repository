﻿Imports WebMatrix.WebData
Namespace TopWork


    Public Class ConfigurationController
        Inherits System.Web.Mvc.Controller




        Public Class RolesNames

            Public adminRole As String = "Administrador"
            Public userRole As String = "Usuario"

        End Class



        Function Install() As ActionResult

            Dim admin As New AdminModel.AdminCredentials
            If (Not WebSecurity.UserExists(admin.username)) Then

                Try
                    CreatingRoles()
                Catch ex As Exception
                    ViewBag.InstallResult = ViewBag.InstallResult + " | " + "Error al crear Roles"
                End Try

                Try
                    CreateAdmin()
                Catch ex As Exception
                    ViewBag.InstallResult = ViewBag.InstallResult + " | " + "Error al crear Roles"
                End Try

                ViewBag.InstallResult = ViewBag.InstallResult + " | " + "Instalación completa."

                Return View()
            Else
                Dim userId As Integer = WebSecurity.GetUserId(admin.username)
                Dim userDP As New UserDataProvider
                Dim user As UserInfo = userDP.GetUserInfo_by_id(userId)

                If (user Is Nothing) Then
                    WebSecurity.Login(admin.username, admin.password)
                    ViewBag.UserFirstName = admin.firstName
                    userDP.InsertUserInformation(userId, admin.firstName, admin.lastName, admin.active)
                    ViewBag.InstallResult = "Usuario " + admin.username + " creado con éxito. (No existía datos en tabla 'UserInfo')"
                    Return View()
                End If

                If (WebSecurity.CurrentUserName = admin.username) Then

                    Try
                        CreateWorkStatus()
                    Catch ex As Exception
                        ViewBag.InstallResult = "Error al crear Status de los Trabajos."
                        Return View()
                    End Try

                    Try
                        CreateServerDetails()
                    Catch ex As Exception
                        ViewBag.InstallResult = "Error al crear Detalles del Servidor."
                        Return View()
                    End Try

                    Try
                        CreateAppConfiguration()
                    Catch ex As Exception
                        ViewBag.InstallResult = "Error al crear Configuración de Aplicación."
                        Return View()
                    End Try


                    ViewBag.InstallResult = "Instalación completa."

                    Return View()
                Else
                    Return RedirectToAction("NoPermission", "Configuration")
                End If
            End If
        End Function

        Function CreateServerDetails()

            Dim serverDetailsDP As New ServerDetailsDataProvider()
            If (serverDetailsDP.GetServerDetails() Is Nothing) Then

                Dim details As New ServerDetails

                details.ServerName = System.Environment.MachineName
                details.OSVersion = System.Environment.OSVersion.VersionString
                details.ProcessorCount = System.Environment.ProcessorCount
                details.DomainName = System.Environment.UserDomainName

                serverDetailsDP.InsertDetails(details)

            End If
        End Function

        Function CreateAppConfiguration()


            Dim appConfigurationDP As New AppConfigurationDataProvider()
            If (appConfigurationDP.GetAppConfiguration() Is Nothing) Then

                Dim licenceDP As New AppCodeDataProvider
                Dim configuration As New AppConfiguration

                Dim today As DateTime = DateTime.Now

                configuration.IVAPercentage = 19
                configuration.AppStartDate = today
                configuration.AppExpirationDate = today.AddMonths(3)
                configuration.AppCode = ""

                appConfigurationDP.InsertAppConfiguration(configuration)

                Dim code_1 As String = licenceDP.GetCode1_forThisServer()
                Dim code_2 As String = licenceDP.GetCode2_by_code1(code_1)
                configuration.AppCode = code_2

                appConfigurationDP.UpdateAppConfiguration(configuration)

            End If
        End Function



        <Authorize>
        Function ServerDetails() As ActionResult

            If (IsCurrentUserAnAdmin()) Then

                ViewBag.ServerName = System.Environment.MachineName
                ViewBag.OSVersion = System.Environment.OSVersion.VersionString
                ViewBag.ProcessorCount = System.Environment.ProcessorCount
                ViewBag.DomainName = System.Environment.UserDomainName

                Return View()
            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If
        End Function

        Function CreateWorkStatus()

            Dim wsTemplate As New WorkStatusTemplate
            Dim workStatudDP As New WorkStatusDataProvider
            Try
                workStatudDP.InsertStatus(wsTemplate.ws_a_id, wsTemplate.ws_a_name)
            Catch ex As Exception
            End Try
            Try
                workStatudDP.InsertStatus(wsTemplate.ws_b_id, wsTemplate.ws_b_name)
            Catch ex As Exception
            End Try
            Try
                workStatudDP.InsertStatus(wsTemplate.ws_c_id, wsTemplate.ws_c_name)
            Catch ex As Exception
            End Try
            Try
                workStatudDP.InsertStatus(wsTemplate.ws_d_id, wsTemplate.ws_d_name)
            Catch ex As Exception
            End Try
            Try
                workStatudDP.InsertStatus(wsTemplate.ws_e_id, wsTemplate.ws_e_name)
            Catch ex As Exception
            End Try

        End Function

        Function CreatingRoles()

            If (Not WebSecurity.UserExists(New AdminModel.AdminCredentials().username)) Then

                Dim role As New RolesNames
                Roles.CreateRole(role.adminRole)
                Roles.CreateRole(role.userRole)

            End If

        End Function


        Function CreateAdmin()

            If (Not WebSecurity.UserExists(New AdminModel.AdminCredentials().username)) Then
                Try

                    Dim admin As New AdminModel.AdminCredentials
                    Dim role As New RolesNames

                    WebSecurity.CreateUserAndAccount(admin.username, admin.password)
                    Roles.AddUserToRole(admin.username, role.adminRole)

                    WebSecurity.Login(admin.username, admin.password)

                    Dim userId As Integer = WebSecurity.GetUserId(admin.username)
                    Dim userDP As New UserDataProvider
                    userDP.InsertUserInformation(userId, admin.firstName, admin.lastName, admin.active)

                    ViewBag.UserFirstName = admin.firstName

                    ViewBag.Result = "Usuario " + admin.username + " creado con éxito."

                Catch ex As Exception
                End Try

            End If

        End Function


        <Authorize>
        Function Admin() As ActionResult

            If (IsCurrentUserAnAdmin()) Then
                Return View()

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

        End Function

#Region "Users"


        <Authorize>
        Function UserList(ByVal result As String) As ActionResult

            If (IsCurrentUserAnAdmin()) Then

                ViewBag.ResultMessage = New Tools().GetResultMessage("User", result)

                Dim userDP As New UserDataProvider
                Dim allUsers = userDP.GetAllUserCompleteInfo

                Return View(allUsers)

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

        End Function

        <Authorize>
        Function UserAdd() As ActionResult

            If (IsCurrentUserAnAdmin()) Then

                Return View()

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

        End Function

        <HttpPost>
        <Authorize>
        Function UserAdd(ByVal model As UserRegistrationModel) As ActionResult

            If (IsCurrentUserAnAdmin()) Then
                Try
                    If (model.UserName Is Nothing Or model.Password Is Nothing Or model.FirstName Is Nothing Or model.LastName Is Nothing) Then
                        ViewBag.ResultMessage = "Debes completar todos los campos requeridos."

                        Return View()
                    End If
                    If (WebSecurity.UserExists(model.UserName)) Then
                        ViewBag.ResultMessage = "Nombre de usuario ya existe."
                        Return View()
                    End If

                    Dim role As New RolesNames

                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password)
                    Roles.AddUserToRole(model.UserName, role.userRole)

                    Dim userId As Integer = WebSecurity.GetUserId(model.UserName)
                    Dim userDP As New UserDataProvider
                    userDP.InsertUserInformation(userId, model.FirstName, model.LastName, model.Active)

                    Dim result As String = "SuccessUserCreation"
                    Return RedirectToAction("UserEdit", "Configuration", New With {.userId = userId, .result = result})

                Catch ex As Exception
                    ViewBag.ResultMessage = "Error al intentar crear usuario."
                    Return View()
                End Try
            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

            Return View()

        End Function

        <Authorize>
        Function UserEdit(ByVal userId As Integer?, ByVal result As String) As ActionResult

            If (IsCurrentUserAnAdmin()) Then

                If (userId Is Nothing) Then
                    ViewBag.ResultMessage = "Debes seleccionar un usuario válido."
                    Return View(New UserRegistrationModel)
                End If


                Dim provider As SimpleMembershipProvider = Membership.Provider
                Dim username As String = provider.GetUserNameFromId(userId)


                If (username IsNot Nothing) Then
                    Dim user As New UserRegistrationModel
                    Dim userDP As New UserDataProvider
                    Dim userInfo As UserInfo = userDP.GetUserInfo_by_id(userId)

                    user.UserId = userId
                    user.UserName = username
                    user.Password = ""
                    user.FirstName = userInfo.FirstName
                    user.LastName = userInfo.LastName
                    user.Active = userInfo.Active

                    ViewBag.ResultMessage = New Tools().GetResultMessage("User", result)

                    Return View(user)

                Else
                    ViewBag.ResultMessage = "Usuario inválido."
                    Return View(New UserRegistrationModel)
                End If

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

        End Function

        <HttpPost>
        <Authorize>
        Function UserEdit(ByVal model As UserRegistrationModel) As ActionResult

            If (IsCurrentUserAnAdmin()) Then
                Try
                    If (model.UserName Is Nothing) Then
                        ViewBag.ResultMessage = "Debes seleccionar un usuario válido."
                        Return View(New UserRegistrationModel)
                    End If

                    If (model.FirstName Is Nothing Or model.LastName Is Nothing) Then
                        ViewBag.ResultMessage = "Debes completar todos los campos requeridos."

                        Return View(New UserRegistrationModel)
                    End If

                    Dim role As New RolesNames

                    If (model.Password IsNot Nothing) Then
                        Dim token As String = WebSecurity.GeneratePasswordResetToken(model.UserName)
                        WebSecurity.ResetPassword(token, model.Password)
                    End If


                    Dim userId As Integer = WebSecurity.GetUserId(model.UserName)
                    Dim userDP As New UserDataProvider
                    userDP.UpdateUserInformation(userId, model.FirstName, model.LastName, model.Active)

                    ViewBag.ResultMessage = "Usuario modificado con éxito."
                    Return View(model)

                Catch ex As Exception

                    ViewBag.ResultMessage = "Error al intentar modificar usuario."
                    Return View(New UserRegistrationModel)

                End Try

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

            Return View()

        End Function

        <Authorize>
        Function UserDelete(ByVal userId As Integer) As ActionResult

            If (IsCurrentUserAnAdmin()) Then
                Try
                    Dim provider As SimpleMembershipProvider = Membership.Provider
                    Dim username As String = provider.GetUserNameFromId(userId)

                    If (username = New AdminModel.AdminCredentials().username) Then
                        Dim result As String = "CannotDeleteAdmin"
                        Return RedirectToAction("UserEdit", "Configuration", New With {.userId = userId, .result = result})
                    Else
                        Dim userDP As New UserDataProvider
                        Dim result As String
                        If (userDP.UserHasQuotations_by_id(userId)) Then
                            result = "UserHasQuotationsImpossibleToDelete"
                            Return RedirectToAction("UserEdit", "Configuration", New With {.userId = userId, .result = result})
                        Else
                            userDP.DeleteUser(userId)
                            result = "SuccessDeletion"
                            Return RedirectToAction("UserList", "Configuration", New With {.result = result})
                        End If
                    End If


                Catch ex As Exception
                    Dim result As String = "ErrorTryingToDelete"
                    Return RedirectToAction("UserEdit", "Configuration", New With {.userId = userId, .result = result})
                End Try

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

        End Function


#End Region

#Region "Work Types"

        <Authorize>
        Function WorkTypeQList(ByVal result As String) As ActionResult

            If (IsCurrentUserAnAdmin()) Then

                Dim workTypeDP As New WorkTypeQDataProvider
                Dim allTypes = workTypeDP.GetAllWorkTypeQ

                ViewBag.ResultMessage = New Tools().GetResultMessage("WorkTypeQ", result)
                Return View(allTypes)

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

        End Function

        <Authorize>
        Function WorkTypeQAdd() As ActionResult

            If (IsCurrentUserAnAdmin()) Then

                Return View()

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

        End Function

        <HttpPost>
        <Authorize>
        Function WorkTypeQAdd(ByVal model As WorkTypeQ) As ActionResult

            If (IsCurrentUserAnAdmin()) Then
                Try
                    If (model.TypeName Is Nothing) Then
                        ViewBag.ResultMessage = "Debes completar todos los campos requeridos."
                        Return View()
                    End If

                    Dim workTypeDP As New WorkTypeQDataProvider
                    If (Not workTypeDP.CheckIfExists_by_name_docType(model.TypeName)) Then
                        Dim newWorkTypeQ As WorkTypeQ = workTypeDP.InsertWorkTypeQ(model)

                        Dim result As String = "SuccessWorkTypeQCreation"
                        Return RedirectToAction("WorkTypeQEdit", "Configuration", New With {.typeId = newWorkTypeQ.WorkTypeQ_Id, .result = result})
                    Else
                        ViewBag.ResultMessage = "Tipo de Trabajo ya existse."
                        Return View()
                    End If

                Catch ex As Exception

                    ViewBag.ResultMessage = "Error al intentar crear Tipo de Trabajo."
                    Return View()

                End Try

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

            Return View()

        End Function


        <Authorize>
        Function WorkTypeQEdit(ByVal typeId As Integer?, ByVal result As String) As ActionResult

            If (IsCurrentUserAnAdmin()) Then

                If (typeId Is Nothing) Then
                    ViewBag.ResultMessage = "Debes seleccionar un tipo de trabajo válido."
                    Return View(New WorkTypeQ)
                End If

                Dim workTypeDP As New WorkTypeQDataProvider
                Dim workTypeModel As WorkTypeQ = workTypeDP.GetWorkTypeQ_by_workTypeId(typeId)

                If (workTypeModel IsNot Nothing) Then

                    ViewBag.ResultMessage = New Tools().GetResultMessage("WorkTypeQ", result)
                    Return View(workTypeModel)

                Else
                    ViewBag.ResultMessage = "Tipo de trabajo inválido."
                    Return View(New WorkTypeQ)
                End If

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

        End Function

        <HttpPost>
        <Authorize>
        Function WorkTypeQEdit(ByVal model As WorkTypeQ) As ActionResult

            If (IsCurrentUserAnAdmin()) Then
                Try
                    If (model.TypeName Is Nothing) Then
                        ViewBag.ResultMessage = "Debes completar todos los campos requeridos."
                        Return View(model)
                    End If

                    Dim workTypeDP As New WorkTypeQDataProvider

                    If (workTypeDP.CheckIfExists_by_name_butNotThisId_docType(model.TypeName, model.WorkTypeQ_Id)) Then
                        ViewBag.ResultMessage = "Tipo de trabajo ya existe."
                        Return View(model)
                    End If

                    workTypeDP.UpdateWorkTypeQ_by_model(model)

                    ViewBag.ResultMessage = "Tipo de trabajo modificado con éxito."
                    Return View(model)

                Catch ex As Exception

                    ViewBag.ResultMessage = "Error al intentar modificar tipo de trabajo."
                    Return View(model)

                End Try

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

            Return View()

        End Function

        <Authorize>
        Function WorkTypeQDelete(ByVal typeId As Integer?) As ActionResult

            If (IsCurrentUserAnAdmin()) Then
                Try
                    Dim result As String
                    Dim workTypeDP As New WorkTypeQDataProvider

                    If (workTypeDP.WorkTypeQHasQuotations_by_id(typeId)) Then
                        result = "WorkTypeQHasQuotationsImpossibleToDelete"
                        Return RedirectToAction("WorkTypeQEdit", "Configuration", New With {.typeId = typeId, .result = result})
                    Else
                        workTypeDP.DeleteWorkTypeQ_by_id(typeId)

                        result = "SuccessDeletion"
                        Return RedirectToAction("WorkTypeQList", "Configuration", New With {.result = result})
                    End If

                Catch ex As Exception
                    Dim result As String = "ErrorTryingToDelete"
                    Return RedirectToAction("WorkTypeQEdit", "Configuration", New With {.typeId = typeId, .result = result})
                End Try

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

        End Function


#End Region

#Region "Method Of Payment"


        <Authorize>
        Function MethodOfPaymentQList(ByVal result As String) As ActionResult

            If (IsCurrentUserAnAdmin()) Then

                Dim methodOfPaymentDP As New MethodOfPaymentQDataProvider
                Dim allTypes = methodOfPaymentDP.GetAllMethodOfPaymentQ()

                ViewBag.ResultMessage = New Tools().GetResultMessage("MethodOfPaymentQ", result)
                Return View(allTypes)

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

        End Function

        <Authorize>
        Function MethodOfPaymentQAdd() As ActionResult

            If (IsCurrentUserAnAdmin()) Then
                Return View()
            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

        End Function

        <HttpPost>
        <Authorize>
        Function MethodOfPaymentQAdd(ByVal model As MethodOfPaymentQ) As ActionResult

            If (IsCurrentUserAnAdmin()) Then
                Try
                    If (model.MethodName Is Nothing) Then
                        ViewBag.ResultMessage = "Debes completar todos los campos requeridos."
                        Return View()
                    End If

                    Dim methodOfPaymentDP As New MethodOfPaymentQDataProvider
                    If (Not methodOfPaymentDP.CheckIfExists_by_name_docType(model.MethodName, "Quotation")) Then
                        model = methodOfPaymentDP.InsertMethodOfPaymentQ(model)

                        Dim result As String = "SuccessMethodOfPaymentQCreation"
                        Return RedirectToAction("MethodOfPaymentQEdit", "Configuration", New With {.methodId = model.MethodOfPaymentQ_Id, .result = result})
                    Else
                        ViewBag.ResultMessage = "Forma de pago ya existe."
                        Return View()
                    End If

                Catch ex As Exception

                    ViewBag.ResultMessage = "Error al intentar crear forma de pago."
                    Return View()

                End Try

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

            Return View()

        End Function

        <Authorize>
        Function MethodOfPaymentQEdit(ByVal methodId As Integer?, ByVal result As String) As ActionResult

            If (IsCurrentUserAnAdmin()) Then

                If (methodId Is Nothing) Then
                    ViewBag.ResultMessage = "Debes seleccionar una forma de pago válida."
                    Return View(New MethodOfPaymentQ)
                End If

                Dim methodOfPaymentDP As New MethodOfPaymentQDataProvider
                Dim methodModel As MethodOfPaymentQ = methodOfPaymentDP.GetMethodOfPaymentQ_by_id(methodId)

                If (methodModel IsNot Nothing) Then

                    ViewBag.ResultMessage = New Tools().GetResultMessage("MethodOfPaymentQ", result)
                    Return View(methodModel)

                Else
                    ViewBag.ResultMessage = "Forma de pago inválida."
                    Return View(New MethodOfPaymentQ)
                End If

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

        End Function

        <HttpPost>
        <Authorize>
        Function MethodOfPaymentQEdit(ByVal model As MethodOfPaymentQ) As ActionResult

            If (IsCurrentUserAnAdmin()) Then
                Try
                    If (model.MethodName Is Nothing) Then
                        ViewBag.ResultMessage = "Debes completar todos los campos requeridos."
                        Return View(model)
                    End If

                    Dim methodOfPaymentDP As New MethodOfPaymentQDataProvider

                    If (methodOfPaymentDP.CheckIfExists_by_name_butNotThisId_docType(model.MethodName, model.MethodOfPaymentQ_Id)) Then
                        ViewBag.ResultMessage = "Forma de pago ya existe."
                        Return View(model)
                    End If

                    methodOfPaymentDP.UpdateMethodOfPaymentQ_by_model(model)

                    ViewBag.ResultMessage = "Forma de pago modificada con éxito."
                    Return View(model)

                Catch ex As Exception

                    ViewBag.ResultMessage = "Error al intentar modificar forma de pago ."
                    Return View(model)

                End Try

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

            Return View()

        End Function

        <Authorize>
        Function MethodOfPaymentQDelete(ByVal methodId As Integer?) As ActionResult

            If (IsCurrentUserAnAdmin()) Then
                Try
                    Dim result As String
                    Dim methodOfPaymentDP As New MethodOfPaymentQDataProvider

                    If (methodOfPaymentDP.MethodOfPaymentQHasQuotations_by_id(methodId)) Then
                        result = "MethodOfPaymentQHasQuotationsImpossibleToDelete"
                        Return RedirectToAction("MethodOfPaymentQs", "Configuration", New With {.methodId = methodId, .result = result})
                    Else
                        methodOfPaymentDP.DeleteMethodOfPaymentQ_by_id(methodId)
                        result = "SuccessDeletion"
                        Return RedirectToAction("MethodOfPaymentQs", "Configuration", New With {.result = result})
                    End If



                Catch ex As Exception
                    Dim result As String = "ErrorTryingToDelete"
                    Return RedirectToAction("MethodOfPaymentQEdit", "Configuration", New With {.methodId = methodId, .result = result})
                End Try

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

        End Function

#End Region

#Region "App Configuration"

        Function AppConfiguration() As ActionResult

            Dim tool As New Tools

            If (IsCurrentUserAnAdmin()) Then

                Dim appConfigurationDP As New AppConfigurationDataProvider
                Dim appCodeDP As New AppCodeDataProvider

                ViewBag.IVAPercentage = appConfigurationDP.GetAppConfiguration.IVAPercentage
                ViewBag.AppStartDate = tool.GetFormattedDate(appConfigurationDP.GetAppConfiguration.AppStartDate)
                ViewBag.AppExpirationDate = tool.GetFormattedDate(appConfigurationDP.GetAppConfiguration.AppExpirationDate)
                ViewBag.AppCode = appConfigurationDP.GetAppConfiguration.AppCode


                ViewBag.ThisServerCode = appCodeDP.GetCode2_forThisServer

                Return View()

            Else
                Return RedirectToAction("NoPermission", "Configuration")
            End If

        End Function





#End Region

        Function IsCurrentUserAnAdmin() As Boolean

            If (User.Identity.Name = New AdminModel.AdminCredentials().username) Then
                Return True
            Else
                Return False
            End If

        End Function



        Function NoPermission() As ActionResult

            Return View()

        End Function


    End Class
End Namespace