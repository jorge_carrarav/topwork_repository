﻿
Imports Microsoft.Reporting.WebForms
Imports TopWork.UserDataProvider
Imports System.Net.Mime.MediaTypeNames


Public Class ReportController
    Inherits System.Web.Mvc.Controller


    '=============================================================================================================================== INFORME DE COTIZACIONES

    <Authorize>
    Function QuotationsReport()

        Dim clientDP As New ClientDataProvider
        Dim allClients As List(Of ClientDataProvider.Client_List_Formatted) = clientDP.GetAllActiveClients_formattedForList()
        Dim workTypeDP As New WorkTypeQDataProvider
        Dim userDP As New UserDataProvider

        ClientAutocompleteSource(allClients)

        Dim reportDp As New ReportDataProvider
        Dim quotationsReportModel As New ReportDataProvider.QuotationsReportModel


        '------------------------------------------Tipo de Monto
        Dim valueType_list As New List(Of String)
        valueType_list.Add("Todos")
        valueType_list.Add("Cot. Monto Neto")
        valueType_list.Add("Cot. Monto IVA")
        valueType_list.Add("Cot. Monto Total")
        ViewBag.ValueType_list = valueType_list
        '------------------------------------------Tipo de Trabajo
        Dim workType_list As New List(Of String)
        workType_list.Add("Todos")
        For Each WorkType As WorkTypeQ In workTypeDP.GetAllActiveWorkTypeQ
            workType_list.Add(WorkType.TypeName)
        Next
        ViewBag.WorkTypeQ_list = workType_list
        '------------------------------------------Estado de Trabajo
        Dim workStatus_list As New List(Of String)
        workStatus_list.Add("Todos")
        workStatus_list.Add("Sin O.T.")
        workStatus_list.Add("En proceso")
        workStatus_list.Add("Entregado")
        workStatus_list.Add("Facturado")
        workStatus_list.Add("Finalizado")
        ViewBag.WorkStatus_list = workStatus_list
        '------------------------------------------Usuarios
        Dim createdBy_list As New List(Of String)
        createdBy_list.Add("Todos")
        For Each user As UserCompleteInfoModel In userDP.GetAllUserCompleteInfo_noAdmin
            createdBy_list.Add(user.FirstName + " " + user.LastName)
        Next
        ViewBag.CreatedBy_list = createdBy_list


        Return View(quotationsReportModel)

    End Function



    <Authorize>
    <HttpPost>
    Function GenerateQuotationsReportPDF(ByVal quotationsReportModel As ReportDataProvider.QuotationsReportModel)



            Dim quotDP As New QuotationDataProvider
            Dim woDP As New WorkOrderDataProvider
            Dim tool As New Tools


            'Definición del report viewer ---------------------------------
            Dim repoViewer As New Microsoft.Reporting.WebForms.ReportViewer()
            repoViewer.ProcessingMode = ProcessingMode.Local
            repoViewer.LocalReport.EnableExternalImages = True
            repoViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/Quotations/QuotationsReport.rdlc ")
            repoViewer.LocalReport.Refresh()
            '--------------------------------------------------------------
            'Variables necesarias para la exportación del report-----------
            Dim streamBytes As Byte() = Nothing
            Dim mimeType As String = ""
            Dim encoding As String = ""
            Dim filenameExtension As String = ""
            Dim streamids As String() = Nothing
            Dim warnings As Warning() = Nothing
            '--------------------------------------------------------------


            'Trae los trabajos según los filtros indicados
            Dim quotFormatted_list As List(Of QuotationDataProvider.Quotation_Report_Formatted) = quotDP.GetAllActiveQuotations_formattedForReport_by_quotationReportModel(quotationsReportModel)

            'Se asocia el datasource al report -------------
            Dim quotationsReport_datasource As New ReportDataSource
            quotationsReport_datasource.Name = "QuotationsDetail"
            quotationsReport_datasource.Value = quotFormatted_list

            repoViewer.LocalReport.DataSources.Add(quotationsReport_datasource)

            If (quotationsReportModel.ValueFrom Is Nothing) Then
                quotationsReportModel.ValueFrom = ""
            End If
            If (quotationsReportModel.ValueTo Is Nothing) Then
                quotationsReportModel.ValueTo = ""
            End If

            Dim currentDate As String = tool.GetFormattedDate(DateTime.Today)

            Dim param_currentDate As New ReportParameter("CurrentDate", currentDate)
            Dim param_clientName As New ReportParameter("ClientName", quotationsReportModel.ClientName)
            Dim param_dateFrom As New ReportParameter("DateFrom", quotationsReportModel.DateFrom)
            Dim param_dateTo As New ReportParameter("DateTo", quotationsReportModel.DateTo)
            Dim param_valueType As New ReportParameter("ValueType", quotationsReportModel.ValueType)
            Dim param_valueFrom As New ReportParameter("ValueFrom", quotationsReportModel.ValueFrom)
            Dim param_valueTo As New ReportParameter("ValueTo", quotationsReportModel.ValueTo)

            Dim param_orderedBy As New ReportParameter("OrderedBy", "Fecha Creación")
            Dim param_workStatus As New ReportParameter("WorkStatus", quotationsReportModel.WorkStatus)



            Dim logoPath As String = New Uri(Server.MapPath("~/Images/report_logo.png")).AbsoluteUri


            Dim quotTotalNetValue As Double = (From w In quotFormatted_list
                                               Select Convert.ToDouble(w.QuotNetValue.Replace(".", "").Replace("$", "").Replace(" ", ""))).Sum
        Dim quotTotalIVAValue As Long = (From w In quotFormatted_list
                                         Select Convert.ToInt64(w.QuotIVAValue.Replace(".", "").Replace("$", "").Replace(" ", ""))).Sum
        Dim quotTotalValue As Long = (From w In quotFormatted_list
                                      Select Convert.ToInt64(w.QuotTotalValue.Replace(".", "").Replace("$", "").Replace(" ", ""))).Sum


        Dim param_quotTotalNetValue As New ReportParameter("QuotTotalNetValue", "$ " + tool.ThousandSeparator(quotTotalNetValue))
            Dim param_quotTotalIVAValue As New ReportParameter("QuotTotalIVAValue", "$ " + tool.ThousandSeparator(quotTotalIVAValue))
            Dim param_quotTotalValue As New ReportParameter("QuotTotalValue", "$ " + tool.ThousandSeparator(quotTotalValue))
            Dim param_logoPath As New ReportParameter("LogoPath", logoPath)
            Dim param_quotationQuantity As New ReportParameter("QuotationQuantity", quotFormatted_list.Count)



            repoViewer.LocalReport.SetParameters(
          New ReportParameter() {
            param_currentDate,
            param_clientName,
            param_dateFrom,
            param_dateTo,
            param_valueType,
            param_valueFrom,
            param_valueTo,
            param_orderedBy,
            param_workStatus,
            param_quotTotalNetValue,
            param_quotTotalIVAValue,
            param_quotTotalValue,
            param_logoPath,
            param_quotationQuantity
          })


            'Exportación del report -------------------------------------
            Dim fileName As String = currentDate + "_InformeDeUtilidades" + ".xls"

            streamBytes = repoViewer.LocalReport.Render("Excel", Nothing, mimeType, encoding, filenameExtension, streamids, warnings)
            Return File(streamBytes, mimeType, fileName)
            '------------------------------------------------------------



    End Function





    '=============================================================================================================================== INFORME DE UTILIDADES
    <Authorize>
    Function UtilitiesReport()

        Dim clientDP As New ClientDataProvider
        Dim allClients As List(Of ClientDataProvider.Client_List_Formatted) = clientDP.GetAllActiveClients_formattedForList()
        Dim workTypeDP As New WorkTypeQDataProvider


        ClientAutocompleteSource(allClients)

        Dim reportDp As New ReportDataProvider
        Dim utilitiesReportModel As New ReportDataProvider.UtilitiesReportModel


        '------------------------------------------Tipo de Monto
        Dim valueType_list As New List(Of String)
        valueType_list.Add("Todos")
        valueType_list.Add("Cot. Monto Neto")
        valueType_list.Add("Cot. Monto IVA")
        valueType_list.Add("Cot. Monto Total")
        valueType_list.Add("Fac. Monto Neto")
        valueType_list.Add("Fac. Monto IVA")
        valueType_list.Add("Fac. Monto Total")
        valueType_list.Add("Fac. Pag. Monto Neto")
        valueType_list.Add("Fac. Pag. Monto IVA")
        valueType_list.Add("Fac. Pag. Monto Total")
        valueType_list.Add("Mat. Monto Neto")
        valueType_list.Add("Mat. Monto IVA")
        valueType_list.Add("Mat. Monto Total")
        valueType_list.Add("Utilidades Esperadas")
        valueType_list.Add("Utilidades Reales")
        ViewBag.ValueType_list = valueType_list
        '------------------------------------------Tipo de Trabajo
        Dim workType_list As New List(Of String)
        workType_list.Add("Todos")
        For Each WorkType As WorkTypeQ In workTypeDP.GetAllActiveWorkTypeQ
            workType_list.Add(WorkType.TypeName)
        Next
        ViewBag.WorkTypeQ_list = workType_list
        '------------------------------------------Estado de Trabajo
        Dim workStatus_list As New List(Of String)
        workStatus_list.Add("Todos")
        workStatus_list.Add("En proceso")
        workStatus_list.Add("Entregado")
        workStatus_list.Add("Facturado")
        workStatus_list.Add("Finalizado")
        ViewBag.WorkStatus_list = workStatus_list
        '------------------------------------------Con O.C.
        Dim withPONumber_list As New List(Of String)
        withPONumber_list.Add("Todos")
        withPONumber_list.Add("Si")
        withPONumber_list.Add("No")
        ViewBag.WithPONumber_list = withPONumber_list


        Return View(utilitiesReportModel)

    End Function



    <Authorize>
    <HttpPost>
    Function GenerateUtilitiesReportPDF(ByVal utilitiesReportModel As ReportDataProvider.UtilitiesReportModel)

        Dim woDP As New WorkOrderDataProvider
        Dim tool As New Tools


        'Definición del report viewer ---------------------------------
        Dim repoViewer As New Microsoft.Reporting.WebForms.ReportViewer()
        repoViewer.ProcessingMode = ProcessingMode.Local
        repoViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/Utilities/UtilitiesReport.rdlc ")
        repoViewer.LocalReport.Refresh()
        '--------------------------------------------------------------
        'Variables necesarias para la exportación del report-----------
        Dim streamBytes As Byte() = Nothing
        Dim mimeType As String = ""
        Dim encoding As String = ""
        Dim filenameExtension As String = ""
        Dim streamids As String() = Nothing
        Dim warnings As Warning() = Nothing
        '--------------------------------------------------------------


        'Trae los trabajos según los filtros indicados
        Dim workFormatted_list As List(Of WorkOrderDataProvider.Work_Report_Formatted) = woDP.GetAllActiveWorks_formattedForReport_by_utilitiesReportModel(utilitiesReportModel)

        'Se asocia el datasource al report -------------
        Dim utilitiesReport_datasource As New ReportDataSource
        utilitiesReport_datasource.Name = "UtilitiesDetail"
        utilitiesReport_datasource.Value = workFormatted_list

        repoViewer.LocalReport.DataSources.Add(utilitiesReport_datasource)

        If (utilitiesReportModel.ValueFrom Is Nothing) Then
            utilitiesReportModel.ValueFrom = ""
        End If
        If (utilitiesReportModel.ValueTo Is Nothing) Then
            utilitiesReportModel.ValueTo = ""
        End If

        Dim currentDate As String = tool.GetFormattedDate(DateTime.Today)

        Dim param_currentDate As New ReportParameter("CurrentDate", currentDate)
        Dim param_clientName As New ReportParameter("ClientName", utilitiesReportModel.ClientName)
        Dim param_dateFrom As New ReportParameter("DateFrom", utilitiesReportModel.DateFrom)
        Dim param_dateTo As New ReportParameter("DateTo", utilitiesReportModel.DateTo)
        Dim param_valueType As New ReportParameter("ValueType", utilitiesReportModel.ValueType)
        Dim param_valueFrom As New ReportParameter("ValueFrom", utilitiesReportModel.ValueFrom)
        Dim param_valueTo As New ReportParameter("ValueTo", utilitiesReportModel.ValueTo)

        Dim param_withPONum As New ReportParameter("WithPONum", utilitiesReportModel.WithPONumber)
        Dim param_wayBillNumber As New ReportParameter("WayBillNumber", utilitiesReportModel.WayBillNumber)
        Dim param_invoiceNumber As New ReportParameter("InvoiceNumber", utilitiesReportModel.InvoiceNumber)
        Dim param_orderedBy As New ReportParameter("OrderedBy", "Fecha Creación")
        Dim param_workStatus As New ReportParameter("WorkStatus", utilitiesReportModel.WorkStatus)



        Dim quotTotalNetValue As Long = (From w In workFormatted_list
                                         Select Convert.ToInt64(w.QuotNetValue.Replace(".", "").Replace("$", "").Replace(" ", ""))).Sum
        Dim quotTotalIVAValue As Long = (From w In workFormatted_list
                                         Select Convert.ToInt64(w.QuotIVAValue.Replace(".", "").Replace("$", "").Replace(" ", ""))).Sum
        Dim quotTotalValue As Long = (From w In workFormatted_list
                                      Select Convert.ToInt64(w.QuotTotalValue.Replace(".", "").Replace("$", "").Replace(" ", ""))).Sum

        Dim invoTotalNetValue As Long = (From w In workFormatted_list
                                         Select Convert.ToInt64(w.InvoNetValue.Replace(".", "").Replace("$", "").Replace(" ", ""))).Sum
        Dim invoTotalIVAValue As Long = (From w In workFormatted_list
                                         Select Convert.ToInt64(w.InvoIVAValue.Replace(".", "").Replace("$", "").Replace(" ", ""))).Sum
        Dim invoTotalValue As Long = (From w In workFormatted_list
                                      Select Convert.ToInt64(w.InvoTotalValue.Replace(".", "").Replace("$", "").Replace(" ", ""))).Sum

        Dim paidInvoTotalNetValue As Long = (From w In workFormatted_list
                                             Select Convert.ToInt64(w.PaidInvoNetValue.Replace(".", "").Replace("$", "").Replace(" ", ""))).Sum
        Dim paidInvoTotalIVAValue As Long = (From w In workFormatted_list
                                             Select Convert.ToInt64(w.PaidInvoIVAValue.Replace(".", "").Replace("$", "").Replace(" ", ""))).Sum
        Dim paidInvoTotalValue As Long = (From w In workFormatted_list
                                          Select Convert.ToInt64(w.PaidInvoTotalValue.Replace(".", "").Replace("$", "").Replace(" ", ""))).Sum

        Dim matTotalNetValue As Long = (From w In workFormatted_list
                                        Select Convert.ToInt64(w.MatNetValue.Replace(".", "").Replace("$", "").Replace(" ", ""))).Sum
        Dim matTotalIVAValue As Long = (From w In workFormatted_list
                                        Select Convert.ToInt64(w.MatIVAValue.Replace(".", "").Replace("$", "").Replace(" ", ""))).Sum
        Dim matTotalValue As Long = (From w In workFormatted_list
                                     Select Convert.ToInt64(w.MatTotalValue.Replace(".", "").Replace("$", "").Replace(" ", ""))).Sum


        Dim expectedUtilitiesTotal As Long = quotTotalNetValue - matTotalNetValue
        Dim realUtilitiesTotal As Long = invoTotalNetValue - matTotalNetValue



        Dim param_quotTotalNetValue As New ReportParameter("QuotTotalNetValue", "$ " + tool.ThousandSeparator(quotTotalNetValue))
        Dim param_quotTotalIVAValue As New ReportParameter("QuotTotalIVAValue", "$ " + tool.ThousandSeparator(quotTotalIVAValue))
        Dim param_quotTotalValue As New ReportParameter("QuotTotalValue", "$ " + tool.ThousandSeparator(quotTotalValue))

        Dim param_invoTotalNetValue As New ReportParameter("InvoTotalNetValue", "$ " + tool.ThousandSeparator(invoTotalNetValue))
        Dim param_invoTotalIVAValue As New ReportParameter("InvoTotalIVAValue", "$ " + tool.ThousandSeparator(invoTotalIVAValue))
        Dim param_invoTotalValue As New ReportParameter("InvoTotalValue", "$ " + tool.ThousandSeparator(invoTotalValue))

        Dim param_paidInvoTotalNetValue As New ReportParameter("PaidInvoTotalNetValue", "$ " + tool.ThousandSeparator(paidInvoTotalNetValue))
        Dim param_paidInvoTotalIVAValue As New ReportParameter("PaidInvoTotalIVAValue", "$ " + tool.ThousandSeparator(paidInvoTotalIVAValue))
        Dim param_paidInvoTotalValue As New ReportParameter("PaidInvoTotalValue", "$ " + tool.ThousandSeparator(paidInvoTotalValue))

        Dim param_matTotalNetValue As New ReportParameter("MatTotalNetValue", "$ " + tool.ThousandSeparator(matTotalNetValue))
        Dim param_matTotalIVAValue As New ReportParameter("MatTotalIVAValue", "$ " + tool.ThousandSeparator(matTotalIVAValue))
        Dim param_matTotalValue As New ReportParameter("MatTotalValue", "$ " + tool.ThousandSeparator(matTotalValue))

        Dim param_expectedUtilitiesTotal As New ReportParameter("ExpectedUtilitiesTotal", "$ " + tool.ThousandSeparator(expectedUtilitiesTotal))
        Dim param_realUtilitiesTotal As New ReportParameter("RealUtilitiesTotal", "$ " + tool.ThousandSeparator(realUtilitiesTotal))


        repoViewer.LocalReport.EnableExternalImages = True
        Dim logoPath As String = New Uri(Server.MapPath("~/Images/report_logo.png")).AbsoluteUri
        Dim param_logPath As New ReportParameter("LogoPath", logoPath)


        repoViewer.LocalReport.SetParameters(
          New ReportParameter() {
            param_currentDate,
            param_clientName,
            param_dateFrom,
            param_dateTo,
            param_valueType,
            param_valueFrom,
            param_valueTo,
            param_withPONum,
            param_wayBillNumber,
            param_invoiceNumber,
            param_orderedBy,
            param_workStatus,
            param_quotTotalNetValue,
            param_quotTotalIVAValue,
            param_quotTotalValue,
            param_invoTotalNetValue,
            param_invoTotalIVAValue,
            param_invoTotalValue,
            param_paidInvoTotalNetValue,
            param_paidInvoTotalIVAValue,
            param_paidInvoTotalValue,
            param_matTotalNetValue,
            param_matTotalIVAValue,
            param_matTotalValue,
            param_expectedUtilitiesTotal,
            param_realUtilitiesTotal,
            param_logPath
          })


        'Exportación del report -------------------------------------
        Dim fileName As String = currentDate + "_InformeDeUtilidades" + ".xls"

        streamBytes = repoViewer.LocalReport.Render("Excel", Nothing, mimeType, encoding, filenameExtension, streamids, warnings)
        Return File(streamBytes, mimeType, fileName)
        '------------------------------------------------------------


    End Function



    Function ClientAutocompleteSource(ByVal allClients As List(Of ClientDataProvider.Client_List_Formatted))

        Dim count As Long = 0
        For Each client In allClients
            ViewBag.RutList += "'" + client.Rut.Replace(".", "") + "'"
            ViewBag.NameList += "'" + client.Name + "'"
            count += 1
            If (count < allClients.Count) Then
                ViewBag.RutList += ","
                ViewBag.NameList += ","
            End If
        Next

    End Function


End Class