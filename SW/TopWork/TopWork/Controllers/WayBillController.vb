﻿Public Class WayBillController
    Inherits System.Web.Mvc.Controller

    '
    ' GET: /WayBill

    Function WayBillList(ByVal wayBillNum As String) As ActionResult

        ViewBag.SearchResult = ""
        ViewBag.ResultMessage = ""

        Dim wayBillDP As New WayBillDataProvider

        Dim wayBill_list_formatted As List(Of WayBillDataProvider.WayBill_List_Formatted)

        If (String.IsNullOrEmpty(wayBillNum)) Then

            wayBill_list_formatted = wayBillDP.GetAllActiveWayBills_formattedForList_by_wayBillNumber(-1)

        Else
            Try
                wayBill_list_formatted = wayBillDP.GetAllActiveWayBills_formattedForList_by_wayBillNumber(wayBillNum)
                ViewBag.SearchResult = "Resulados para la búsqueda de n° de guía de despacho '" + wayBillNum + "'"
            Catch ex As Exception
                ViewBag.ResultMessage = "Número inválido de orden de guía de despacho"
                wayBill_list_formatted = wayBillDP.GetAllActiveWayBills_formattedForList_by_wayBillNumber(-1)
            End Try
        End If

        Return View(wayBill_list_formatted)

    End Function

End Class