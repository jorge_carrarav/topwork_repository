﻿Imports WebMatrix.WebData
Imports TopWork.ProductOrServiceQDataProvider

Imports Microsoft.Reporting.WebForms
Imports System.IO


Public Class QuotationController
    Inherits System.Web.Mvc.Controller


    Function Modal() As ActionResult

        Return View()

    End Function



    Function SetQuotationStatus_viewBags()

        Dim woStatusDP As New WorkStatusDataProvider
        Dim workStatus_list As List(Of WorkStatus) = woStatusDP.GetAllWorkStatus


        ViewBag.ws_a = (From s In workStatus_list
                        Where s.WorkStatus_Id = 1
                        Select s.StatusName).FirstOrDefault()
        ViewBag.ws_b = (From s In workStatus_list
                        Where s.WorkStatus_Id = 2
                        Select s.StatusName).FirstOrDefault()
        ViewBag.ws_c = (From s In workStatus_list
                        Where s.WorkStatus_Id = 3
                        Select s.StatusName).FirstOrDefault()
        ViewBag.ws_d = (From s In workStatus_list
                        Where s.WorkStatus_Id = 4
                        Select s.StatusName).FirstOrDefault()
        ViewBag.ws_e = (From s In workStatus_list
                        Where s.WorkStatus_Id = 5
                        Select s.StatusName).FirstOrDefault()

    End Function

    <Authorize>
    Function QuotationList(ByVal result As String, ByVal quotNum As String, ByVal ws_a As String, ByVal ws_b As String, ByVal ws_c As String, ByVal ws_d As String, ByVal ws_e As String) As ActionResult
        SetQuotationStatus_viewBags()
        ViewBag.SearchResult = ""
        ViewBag.ResultMessage = ""

        Dim quotationDP As New QuotationDataProvider
        Dim allQuotations_list_formatted As List(Of QuotationDataProvider.Quotation_List_Formatted)

        Dim focusDate As DateTime = DateTime.Now
        focusDate = focusDate.AddMonths(-4)

        If (quotNum Is Nothing Or quotNum = "") Then

            allQuotations_list_formatted = quotationDP.GetAllActiveQuotations_formattedForList_by_quotNumVersion_focusDate(-1, True, focusDate)
            allQuotations_list_formatted = FilteringList(allQuotations_list_formatted, ws_a, ws_b, ws_c, ws_d, ws_e)

        Else
            Try
                Dim isNumber As Integer = Convert.ToInt32(quotNum.Split(".")(0))
                If (quotNum.Split(".").Count < 3) Then

                    If (quotNum.Split(".").Count > 1) Then
                        isNumber = Convert.ToInt32(quotNum.Split(".")(1))
                    End If

                    allQuotations_list_formatted = quotationDP.GetAllActiveQuotations_formattedForList_by_quotNumVersion_focusDate(quotNum, True, focusDate)

                    ViewBag.SearchResult = "Resulados para la búsqueda de n° de cotización '" + quotNum + "'"

                Else
                    ViewBag.ResultMessage = "Número inválido de cotización"
                    allQuotations_list_formatted = quotationDP.GetAllActiveQuotations_formattedForList_by_quotNumVersion_focusDate(-1, True, focusDate)
                    allQuotations_list_formatted = FilteringList(allQuotations_list_formatted, ws_a, ws_b, ws_c, ws_d, ws_e)
                End If
            Catch ex As Exception
                ViewBag.ResultMessage = "Número inválido de cotización"
                allQuotations_list_formatted = quotationDP.GetAllActiveQuotations_formattedForList_by_quotNumVersion_focusDate(-1, True, focusDate)
                allQuotations_list_formatted = FilteringList(allQuotations_list_formatted, ws_a, ws_b, ws_c, ws_d, ws_e)
            End Try
        End If

        ViewBag.ResultMessage += New Tools().GetResultMessage("Quotation", result)
        Return View(allQuotations_list_formatted)

    End Function


    <Authorize>
    Function QuotationList_AdvSearch(ByVal quotNum As String, ByVal workOrderNum As String, ByVal purchaseOrderNumber As String, ByVal clientName As String, ByVal clientRut As String, ByVal wayBillNum As String, ByVal invoiceNum As String) As ActionResult
        SetQuotationStatus_viewBags()
        ViewBag.SearchResult = ""
        ViewBag.ResultMessage = ""

        Dim quotationDP As New QuotationDataProvider
        Dim allQuotations_list_formatted As New List(Of QuotationDataProvider.Quotation_List_Formatted)




        If (Not String.IsNullOrEmpty(quotNum)) Then
            Dim focusDate As DateTime = DateTime.Now.AddMonths(-4)
            allQuotations_list_formatted = quotationDP.GetAllActiveQuotations_formattedForList_by_quotNumVersion_focusDate(quotNum, True, focusDate)
            ViewBag.SearchResult = "Resulados de Cotizaciones para búsqueda de cotizacion N°  " + quotNum + ""

        ElseIf (Not String.IsNullOrEmpty(workOrderNum)) Then

            allQuotations_list_formatted = quotationDP.GetAllActiveQuotations_formattedForList_by_workOrderNumber(workOrderNum)
            ViewBag.SearchResult = "Resulados de Cotizaciones para búsqueda de O.T. N°  " + workOrderNum + ""

        ElseIf (Not String.IsNullOrEmpty(purchaseOrderNumber)) Then

            allQuotations_list_formatted = quotationDP.GetAllActiveQuotations_formattedForList_by_purchaseOrderNumber(purchaseOrderNumber)
            ViewBag.SearchResult = "Resulados de Cotizaciones para búsqueda de O.C. N°  " + purchaseOrderNumber + ""

        ElseIf (Not String.IsNullOrEmpty(clientName)) Then

            allQuotations_list_formatted = quotationDP.GetAllActiveQuotations_formattedForList_by_clientName(clientName)
            ViewBag.SearchResult = "Resulados de Cotizaciones para búsqueda de cliente '" + clientName + "'"

        ElseIf (Not String.IsNullOrEmpty(clientRut)) Then

            allQuotations_list_formatted = quotationDP.GetAllActiveQuotations_formattedForList_by_clientRutNumber(clientRut)
            ViewBag.SearchResult = "Resulados de Cotizaciones para búsqueda de Rut de cliente '" + clientRut + "'"

        ElseIf (Not String.IsNullOrEmpty(wayBillNum)) Then

            allQuotations_list_formatted = quotationDP.GetAllActiveQuotations_formattedForList_by_wayBillNumber(wayBillNum)
            ViewBag.SearchResult = "Resulados de Cotizaciones para búsqueda de guía de despacho N° " + wayBillNum + ""

        ElseIf (Not String.IsNullOrEmpty(invoiceNum)) Then

            allQuotations_list_formatted = quotationDP.GetAllActiveQuotations_formattedForList_by_invoiceNumber(invoiceNum)
            ViewBag.SearchResult = "Resulados de Cotizaciones para búsqueda de factura N° " + invoiceNum + ""

        End If

        ViewBag.ResultMessage = "Búsqueda de Cotizaciones"
        Return View(allQuotations_list_formatted)

    End Function


    <Authorize>
    Function FilteringList(ByVal allQuotations As List(Of QuotationDataProvider.Quotation_List_Formatted), ByVal ws_a As String, ByVal ws_b As String, ByVal ws_c As String, ByVal ws_d As String, ByVal ws_e As String) As List(Of QuotationDataProvider.Quotation_List_Formatted)

        Dim allQuotations_filtered As List(Of QuotationDataProvider.Quotation_List_Formatted)

        Dim woStatusDP As New WorkStatusDataProvider
        Dim workStatus_list As List(Of WorkStatus) = woStatusDP.GetAllWorkStatus


        If (ws_a Is Nothing) Then
            ws_a = "1"
        End If
        If (ws_b Is Nothing) Then
            ws_b = "0"
        End If
        If (ws_c Is Nothing) Then
            ws_c = "0"
        End If
        If (ws_d Is Nothing) Then
            ws_d = "0"
        End If
        If (ws_e Is Nothing) Then
            ws_e = "0"
        End If


        If (ws_a = "1") Then
            ws_a = (From w In workStatus_list
                         Where w.WorkStatus_Id = 1
                         Select w.StatusName).FirstOrDefault()
        End If
        If (ws_b = "1") Then
            ws_b = (From w In workStatus_list
                         Where w.WorkStatus_Id = 2
                         Select w.StatusName).FirstOrDefault()
        End If
        If (ws_c = "1") Then
            ws_c = (From w In workStatus_list
                         Where w.WorkStatus_Id = 3
                         Select w.StatusName).FirstOrDefault()
        End If
        If (ws_d = "1") Then
            ws_d = (From w In workStatus_list
                             Where w.WorkStatus_Id = 4
                             Select w.StatusName).FirstOrDefault()
        End If
        If (ws_e = "1") Then
            ws_e = (From w In workStatus_list
                             Where w.WorkStatus_Id = 5
                             Select w.StatusName).FirstOrDefault()
        End If


        allQuotations_filtered = (From q In allQuotations
                                  Where q.Status.Contains(ws_a) _
                                  Or q.Status.Contains(ws_b) _
                                  Or q.Status.Contains(ws_c) _
                                  Or q.Status.Contains(ws_d) _
                                  Or q.Status.Contains(ws_e)
                                  Select q).ToList

        Return allQuotations_filtered

    End Function


    <Authorize>
    Function QuotationAdd(ByVal rut As String, ByVal name As String) As ActionResult

        Dim unitOfMeasDP As New UnitOfMeasureQDataProvider
        Dim appConfigDP As New AppConfigurationDataProvider
        Dim appConfig As AppConfiguration = appConfigDP.GetAppConfiguration

        ViewBag.IVAPercentage = appConfig.IVAPercentage

        ViewBag.QuotationAdd_Rut = rut

        BindAutocompleteClientRuts()
        BindAutocompleteClientNames()

        Bind_DDL_WorkTypeQList()
        Bind_DDL_MethodOfPaymentList()



        If (rut <> "") Then
            Try
                Convert.ToInt32(rut) 'Esto es sólo para determinar si el rut es numérico

                Dim clientDP As New ClientDataProvider
                Dim client As Client = clientDP.GetActiveClient_by_rutNumber(rut)

                BindClientInformation(client)

                ViewBag.ResultMessage = client.Name

            Catch ex As Exception
                ViewBag.ResultMessage = "Rut no encontrado."
            End Try
        ElseIf (name <> "") Then
            Try

                Dim clientDP As New ClientDataProvider
                Dim client As Client = clientDP.GetActiveClient_by_name(name)

                BindClientInformation(client)

                ViewBag.ResultMessage = client.Name

            Catch ex As Exception
                ViewBag.ResultMessage = "Cliente no encontrado."
            End Try
        End If


        Dim jss As New System.Web.Script.Serialization.JavaScriptSerializer
        ViewBag.UnitsOfMeasure = jss.Serialize(unitOfMeasDP.GetAllActiveUnitOfMeasureQ_shortName()).ToString().Replace("""", "").Replace("[", "").Replace("]", "")

        Return View()

    End Function

    <HttpPost>
    <Authorize>
    Function QuotationAdd(ByVal quotAddEditModel As QuotationDataProvider.Quotation_AddEdit_Formatted) As String

        Dim clientDP As New ClientDataProvider
        Dim quotationDP As New QuotationDataProvider
        Dim prodOrServDP As New ProductOrServiceQDataProvider
        Dim quotProdOrServDP As New QuotationProductOrServiceDataProvider
        Dim workTypeQDP_Item As New WorkTypeQDataProvider
        Dim methodOfPaymentDP As New MethodOfPaymentQDataProvider
        Dim appConfigDP As New AppConfigurationDataProvider
        Dim unitOfMeasurementDP As New UnitOfMeasureQDataProvider

        Dim client As Client = clientDP.GetActiveClient_by_rutNumber(quotAddEditModel.ClientRut.Split("-")(0).Replace(".", ""))

        Dim userName = User.Identity.Name
        Dim userId As Integer = WebSecurity.GetUserId(userName)


        '------------------------------------------------------------------------------------------------------------ QUOTATION

        Dim validityDate_str As String = quotAddEditModel.ValidityDate
        Dim validityDate As DateTime
        Try
            validityDate = New DateTime(validityDate_str.Split("-")(2), validityDate_str.Split("-")(1), validityDate_str.Split("-")(0))
        Catch ex As Exception
            validityDate = DateTime.Now
        End Try
        Dim newQuotation As New Quotation

        newQuotation.Client_Id = client.Client_Id
        newQuotation.User_Id = userId
        newQuotation.QuotationNumber = quotationDP.GetNewQuotationNumber
        newQuotation.Version = 1
        newQuotation.CreationDate = DateTime.Now
        newQuotation.ContactName = quotAddEditModel.ContactName
        newQuotation.WorkDestiny = quotAddEditModel.WorkDestiny
        newQuotation.WorkTypeQ_Id = workTypeQDP_Item.GetActiveWorkTypeQ_by_typeName(quotAddEditModel.WorkTypeQ).WorkTypeQ_Id
        newQuotation.MethodOfPaymentQ_Id = methodOfPaymentDP.GetMethodOfPaymentQ_by_methodName(quotAddEditModel.MethodOfPayment).MethodOfPaymentQ_Id
        newQuotation.ValidityDate = validityDate
        newQuotation.Observation = quotAddEditModel.Observations
        newQuotation.IVAPercentage = appConfigDP.GetAppConfiguration.IVAPercentage
        newQuotation.Active = True

        quotationDP.InsertQuotation(newQuotation)

        '------------------------------------------------------------------------------------------------------------ PRODUCT OR SERVICES
        Dim jss As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim item_list As List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item) = jss.Deserialize(quotAddEditModel.Item_list, GetType(List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item)))

        For Each item As ProductOrServiceQDataProvider.ProductOrServiceQ_Item In item_list
            Dim prodOrServ As New ProductOrServiceQ
            prodOrServ.Description = item.Description.ToString().Trim().Replace(System.Environment.NewLine, "|NEWLINE|")
            prodOrServDP.InsertProductOrServiceQ(prodOrServ)
            item.ProductOrServiceQ_Id = prodOrServ.ProductOrServiceQ_Id
        Next

        '------------------------------------------------------------------------------------------------------------ QUOTATION | PRODUCT OR SERVICES

        Dim itemNumber As Integer = 1
        For Each item As ProductOrServiceQDataProvider.ProductOrServiceQ_Item In item_list

            Dim newQuotProdOrServ As New QuotationProductOrService

            newQuotProdOrServ.Quotation_id = newQuotation.Quotation_id
            newQuotProdOrServ.ProductOrServiceQ_Id = item.ProductOrServiceQ_Id
            newQuotProdOrServ.QuotaionVersion = newQuotation.Version
            newQuotProdOrServ.ItemNumber = itemNumber
            newQuotProdOrServ.UnitValue = item.UnitValue
            newQuotProdOrServ.Quantity = item.Quantity
            newQuotProdOrServ.UnitOfMeasureQ_Id = unitOfMeasurementDP.GetUnitOfMeasureQ_by_shortName(item.UnitOfMeasure).UnitOfMeasureQ_Id

            quotProdOrServDP.InsertQuotProdOrServ(newQuotProdOrServ)

            itemNumber += 1
        Next

        Return newQuotation.Quotation_id

    End Function


    <Authorize>
    Function QuotationEdit(ByVal quotId As Integer, ByVal result As String, ByVal fromNode As String) As ActionResult

        Dim woDP As New WorkOrderDataProvider
        If (woDP.GetActiveWorkOrder_by_quotId(quotId) IsNot Nothing) Then
            Return RedirectToAction("QuotationReview", "Quotation", New With {.quotId = quotId, .fromNode = fromNode, .result = result})
        End If


        ViewBag.ResultMessage = New Tools().GetResultMessage("Quotation", result)


        Dim quotationDP As New QuotationDataProvider
        Dim clientDP As New ClientDataProvider
        Dim prodOrServDP As New ProductOrServiceQDataProvider
        Dim quotProdOrServDP As New QuotationProductOrServiceDataProvider
        Dim quotationStatusDP As New WorkStatusDataProvider

        Dim quotation As Quotation = quotationDP.GetActiveQuotation_by_quotId(quotId)


        If (quotation Is Nothing) Then

            ViewBag.ResultMessage = "Cotización no encontrada"

            Dim quotAddEditModel As New QuotationDataProvider.Quotation_AddEdit_Formatted

            Return View(quotAddEditModel)

        Else

            Dim client As Client = clientDP.GetActiveClient_by_clientId(quotation.Client_Id)
            Dim unitOfMeasDP As New UnitOfMeasureQDataProvider


            Dim jss As New System.Web.Script.Serialization.JavaScriptSerializer

            BindClientInformation(client)
            Bind_DDL_WorkTypeQList()
            Bind_DDL_MethodOfPaymentList()

            '------------------------------------------------------------------------------------------------------------ QUOTATION Model

            Dim quotAddEditModel As New QuotationDataProvider.Quotation_AddEdit_Formatted

            quotAddEditModel = quotationDP.GetActiveQuotation_formattedForAddEdit_by_quotId(quotId)

            '------------------------------------------------------------------------------------------------------------ QUOTATION | PRODUCT OR SERVICES 

            Dim prodOrServ_item_list As List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item) = quotProdOrServDP.GetAllProductOrServiceQ_Item_by_quotId_version(quotId, quotation.Version)

            quotAddEditModel.Item_list = jss.Serialize(prodOrServ_item_list)

            '------------------------------------------------------------------------------------------------------------ QUOTATION HISTORY

            Dim quotHistoy_list As List(Of QuotationDataProvider.QuotationOldVersion_Formatted) = quotationDP.GetAllQuotationVersionsButNoLast_by_quotId(quotId)

            quotAddEditModel.OldVersionsInfo = jss.Serialize(quotHistoy_list)

            '--------------------------------------------------------------------------------------------------------------------------------

            ViewBag.CreateWorkOrder = "  (Crear O.T.)"


            ViewBag.UnitsOfMeasure = jss.Serialize(unitOfMeasDP.GetAllActiveUnitOfMeasureQ_shortName()).ToString().Replace("""", "").Replace("[", "").Replace("]", "")

            Return View(quotAddEditModel)

        End If

    End Function

    <HttpPost>
    <Authorize>
    Function QuotationEdit(ByVal quotAddEditModel As QuotationDataProvider.Quotation_AddEdit_Formatted) As String

        Dim clientDP As New ClientDataProvider
        Dim quotationDP As New QuotationDataProvider
        Dim prodOrServDP As New ProductOrServiceQDataProvider
        Dim quotProdOrServDP As New QuotationProductOrServiceDataProvider
        Dim workTypeQDP_Item As New WorkTypeQDataProvider
        Dim methodOfPaymentDP As New MethodOfPaymentQDataProvider
        Dim appConfigDP As New AppConfigurationDataProvider
        Dim unitOfMeasurementDP As New UnitOfMeasureQDataProvider

        Dim client As Client = clientDP.GetActiveClient_by_rutNumber(quotAddEditModel.ClientRut.Split("-")(0))

        Dim validityDate_str As String = quotAddEditModel.ValidityDate
        Dim validityDate As DateTime

        Try
            validityDate = New DateTime(validityDate_str.Split("-")(2), validityDate_str.Split("-")(1), validityDate_str.Split("-")(0))
        Catch ex As Exception
            validityDate = DateTime.Now
        End Try


        Dim newQuotation As New Quotation

        newQuotation.Quotation_id = quotAddEditModel.Quotation_Id
        newQuotation.ContactName = quotAddEditModel.ContactName
        newQuotation.WorkDestiny = quotAddEditModel.WorkDestiny
        newQuotation.ContactName = quotAddEditModel.ContactName
        newQuotation.MethodOfPaymentQ_Id = methodOfPaymentDP.GetMethodOfPaymentQ_by_methodName(quotAddEditModel.MethodOfPayment).MethodOfPaymentQ_Id
        newQuotation.WorkTypeQ_Id = workTypeQDP_Item.GetActiveWorkTypeQ_by_typeName(quotAddEditModel.WorkTypeQ).WorkTypeQ_Id
        newQuotation.ValidityDate = validityDate
        newQuotation.Observation = quotAddEditModel.Observations


        If (CompareItemListJson(quotAddEditModel.Quotation_Id, quotAddEditModel.Version, quotAddEditModel.Item_list)) Then

            '------------------------------------------------------------------------------------------------------------ SAVE CHANGES, QUOTATION
            quotationDP.UpdateQuotation(newQuotation)


        Else
            '------------------------------------------------------------------------------------------------------------ NEW VERSION | SAVE CHANGES QUOTATION | PRODUCT OR SERVICES

            Dim userName = User.Identity.Name
            Dim userId As Integer = WebSecurity.GetUserId(userName)

            'Agregar nueva versión de Quotation
            newQuotation.Version = quotAddEditModel.Version + 1
            newQuotation.CreationDate = DateTime.Now
            newQuotation.User_Id = userId

            quotationDP.InserNewQuotationVersion(newQuotation)

            '------------------------------------------------------------------------------------------------------------ PRODUCT OR SERVICES
            Dim jss As New System.Web.Script.Serialization.JavaScriptSerializer
            Dim newItem_list As List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item) = jss.Deserialize(quotAddEditModel.Item_list, GetType(List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item)))

            For Each item As ProductOrServiceQDataProvider.ProductOrServiceQ_Item In newItem_list

                If (prodOrServDP.GetProductOrServiceQ_by_description(item.Description) Is Nothing) Then

                    Dim prodOrServ As New ProductOrServiceQ
                    prodOrServ.Description = item.Description.ToString().Trim().Replace(vbLf, "")
                    prodOrServDP.InsertProductOrServiceQ(prodOrServ)
                    item.ProductOrServiceQ_Id = prodOrServ.ProductOrServiceQ_Id
                Else
                    item.ProductOrServiceQ_Id = prodOrServDP.GetProductOrServiceQ_by_description(item.Description).ProductOrServiceQ_Id
                End If
            Next

            '------------------------------------------------------------------------------------------------------------ QUOTATION | PRODUCT OR SERVICES
            Dim itemNumber As Integer = 1
            For Each item As ProductOrServiceQDataProvider.ProductOrServiceQ_Item In newItem_list

                Dim newQuotProdOrServ As New QuotationProductOrService

                newQuotProdOrServ.Quotation_id = newQuotation.Quotation_id
                newQuotProdOrServ.ProductOrServiceQ_Id = item.ProductOrServiceQ_Id
                newQuotProdOrServ.QuotaionVersion = newQuotation.Version
                newQuotProdOrServ.ItemNumber = itemNumber
                newQuotProdOrServ.UnitValue = item.UnitValue
                newQuotProdOrServ.Quantity = item.Quantity
                newQuotProdOrServ.UnitOfMeasureQ_Id = unitOfMeasurementDP.GetUnitOfMeasureQ_by_shortName(item.UnitOfMeasure).UnitOfMeasureQ_Id


                quotProdOrServDP.InsertQuotProdOrServ(newQuotProdOrServ)

                itemNumber += 1
            Next


        End If

        Return newQuotation.Quotation_id

    End Function



    <Authorize>
    Function QuotationReview(ByVal quotId As Integer, ByVal fromNode As String, ByVal result As String) As ActionResult

        ViewBag.GoBackPoint = fromNode

        Dim tool As New Tools
        ViewBag.ResultMessage = tool.GetResultMessage("Quotation", result)

        Dim jss As New System.Web.Script.Serialization.JavaScriptSerializer

        Dim quotationDP As New QuotationDataProvider
        Dim clientDP As New ClientDataProvider
        Dim prodOrServDP As New ProductOrServiceQDataProvider
        Dim quotProdOrServDP As New QuotationProductOrServiceDataProvider
        Dim quotationStatusDP As New WorkStatusDataProvider
        Dim workTypeQDP_Item As New WorkTypeQDataProvider
        Dim methodOfPaymentDP As New MethodOfPaymentQDataProvider
        Dim appConfigDP As New AppConfigurationDataProvider
        Dim woDP As New WorkOrderDataProvider
        Dim userDP As New UserDataProvider
        Dim woStatusDP As New WorkStatusDataProvider

        Dim quotation As Quotation = quotationDP.GetActiveQuotation_by_quotId(quotId)

        If (quotation Is Nothing) Then

            ViewBag.ResultMessage = "Cotización no encontrada"

            Dim quotAddEditModel As New QuotationDataProvider.Quotation_AddEdit_Formatted

            Return View(quotAddEditModel)

        Else

            Dim client As Client = clientDP.GetActiveClient_by_clientId(quotation.Client_Id)

            BindClientInformation(client)
            Bind_DDL_WorkTypeQList()
            Bind_DDL_MethodOfPaymentList()

            '------------------------------------------------------------------------------------------------------------ QUOTATION INFO

            Dim quotAddEditModel As New QuotationDataProvider.Quotation_AddEdit_Formatted

            Dim quotationNumber As String = quotation.QuotationNumber
            If (quotation.Version > 1) Then
                quotationNumber += "." + quotation.Version
            End If

            quotAddEditModel.Quotation_Id = quotId
            quotAddEditModel.WorkOrder_Id = woDP.GetActiveWorkOrder_by_quotId(quotId).WorkOrder_Id
            quotAddEditModel.WorkOrderNumber = woDP.GetWorkOrder_by_Id(quotAddEditModel.WorkOrder_Id).WorkOrderNumber
            quotAddEditModel.ClientRut = New Tools().ThousandSeparator(client.RutNumber)
            quotAddEditModel.QuotationNumberVersion = quotationNumber
            quotAddEditModel.WorkDestiny = quotation.WorkDestiny
            quotAddEditModel.WorkTypeQ = workTypeQDP_Item.GetActiveWorkTypeQ_by_workTypeId(quotation.WorkTypeQ_Id).TypeName
            quotAddEditModel.MethodOfPayment = methodOfPaymentDP.GetMethodOfPaymentQ_by_id(quotation.MethodOfPaymentQ_Id).MethodName
            quotAddEditModel.ValidityDate = quotation.ValidityDate.ToString().Replace("-", "/").Split(" ")(0)
            quotAddEditModel.StatusName = woStatusDP.GetWorkStatus_by_quotId_by_version(quotId, quotation.Version).StatusName
            quotAddEditModel.Observations = quotation.Observation
            quotAddEditModel.Version = quotation.Version
            quotAddEditModel.IVAPercentage = quotation.IVAPercentage

            Dim userInfo As UserInfo = userDP.GetUserInfo_by_id(quotation.User_Id)

            If (userInfo.LastName.Split(" ").Count = 0) Then
                quotAddEditModel.CreatedBy = userInfo.FirstName
            ElseIf (userInfo.LastName.Split(" ").Count = 1) Then
                quotAddEditModel.CreatedBy = userInfo.FirstName + " " + userInfo.LastName.Split(" ")(0).Substring(0, 1) + ". "
            ElseIf (userInfo.LastName.Split(" ").Count = 2) Then
                quotAddEditModel.CreatedBy = userInfo.FirstName + " " + userInfo.LastName.Split(" ")(0).Substring(0, 1) + ". " + userInfo.LastName.Split(" ")(1).Substring(0, 1) + "."
            End If

            quotAddEditModel.CreatedDate = tool.GetFormattedDate(quotation.CreationDate)

            '------------------------------------------------------------------------------------------------------------ QUOTATION | PRODUCT OR SERVICES 

            Dim prodOrServ_item_list As List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item) = quotProdOrServDP.GetAllProductOrServiceQ_Item_by_quotId_version(quotId, quotation.Version)

            quotAddEditModel.Item_list = jss.Serialize(prodOrServ_item_list)


            '------------------------------------------------------------------------------------------------------------ QUOTATION HISTORY


            Dim quotHistoy_list As List(Of QuotationDataProvider.QuotationOldVersion_Formatted) = quotationDP.GetAllQuotationVersionsButNoLast_by_quotId(quotId)

            quotAddEditModel.OldVersionsInfo = jss.Serialize(quotHistoy_list)

            '--------------------------------------------------------------------------------------------------------------------------------

            Return View(quotAddEditModel)

        End If
    End Function


    <Authorize>
    Function QuotationOldVersion(ByVal quotNum As String, ByVal version As String, ByVal fromNode As String) As ActionResult

        ViewBag.GoBackPoint = fromNode


        Dim jss As New System.Web.Script.Serialization.JavaScriptSerializer

        Dim quotationDP As New QuotationDataProvider
        Dim clientDP As New ClientDataProvider
        Dim prodOrServDP As New ProductOrServiceQDataProvider
        Dim quotProdOrServDP As New QuotationProductOrServiceDataProvider
        Dim quotationStatusDP As New WorkStatusDataProvider
        Dim workTypeQDP_Item As New WorkTypeQDataProvider
        Dim methodOfPaymentDP As New MethodOfPaymentQDataProvider
        Dim appConfigDP As New AppConfigurationDataProvider
        Dim userDP As New UserDataProvider
        Dim tool As New Tools


        Dim oldQuotation As QuotationHistory = quotationDP.GetOldQuotation_by_quotNum_quotVersion(quotNum, version)
        Dim quotId As Integer = oldQuotation.Quotation_id


        Dim client As Client = clientDP.GetActiveClient_by_clientId(oldQuotation.Client_Id)


        BindClientInformation(client)
        Bind_DDL_WorkTypeQList()
        Bind_DDL_MethodOfPaymentList()


        '------------------------------------------------------------------------------------------------------------ OLD QUOTATION

        Dim quotAddEditModel As New QuotationDataProvider.Quotation_AddEdit_Formatted

        Dim quotationNumber As String = oldQuotation.QuotationNumber
        If (oldQuotation.Version > 1) Then
            quotationNumber += "." + oldQuotation.Version
        End If

        quotAddEditModel.Quotation_Id = quotId
        quotAddEditModel.ClientRut = New Tools().ThousandSeparator(client.RutNumber)
        quotAddEditModel.QuotationNumberVersion = quotationNumber
        quotAddEditModel.WorkDestiny = oldQuotation.WorkDestiny
        quotAddEditModel.WorkTypeQ = workTypeQDP_Item.GetActiveWorkTypeQ_by_workTypeId(oldQuotation.WorkTypeQ_Id).TypeName
        quotAddEditModel.MethodOfPayment = methodOfPaymentDP.GetMethodOfPaymentQ_by_id(oldQuotation.MethodOfPaymentQ_Id).MethodName
        quotAddEditModel.ValidityDate = oldQuotation.ValidityDate.ToString().Replace("-", "/").Split(" ")(0)
        quotAddEditModel.StatusName = "Versión anterior"
        quotAddEditModel.Observations = oldQuotation.Observation
        quotAddEditModel.Version = oldQuotation.Version
        quotAddEditModel.IVAPercentage = oldQuotation.IVAPercentage


        Dim userInfo As UserInfo = userDP.GetUserInfo_by_id(oldQuotation.User_Id)

        If (userInfo.LastName.Split(" ").Count = 0) Then
            quotAddEditModel.CreatedBy = userInfo.FirstName
        ElseIf (userInfo.LastName.Split(" ").Count = 1) Then
            quotAddEditModel.CreatedBy = userInfo.FirstName + " " + userInfo.LastName.Split(" ")(0).Substring(0, 1) + ". "
        ElseIf (userInfo.LastName.Split(" ").Count = 2) Then
            quotAddEditModel.CreatedBy = userInfo.FirstName + " " + userInfo.LastName.Split(" ")(0).Substring(0, 1) + ". " + userInfo.LastName.Split(" ")(1).Substring(0, 1) + "."
        End If

        quotAddEditModel.CreatedDate = tool.GetFormattedDate(oldQuotation.CreationDate)

        '------------------------------------------------------------------------------------------------------------ QUOTATION | PRODUCT OR SERVICES 

        Dim prodOrServ_item_list As List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item) = quotProdOrServDP.GetAllProductOrServiceQ_Item_by_quotId_version(quotId, oldQuotation.Version)

        quotAddEditModel.Item_list = jss.Serialize(prodOrServ_item_list)

        '------------------------------------------------------------------------------------------------------------ QUOTATION HISTORY

        Dim quotHistoy_list As List(Of QuotationDataProvider.QuotationOldVersion_Formatted) = quotationDP.GetAllQuotationVersionsButNoLast_by_quotId(quotId)

        quotAddEditModel.OldVersionsInfo = jss.Serialize(quotHistoy_list)

        '--------------------------------------------------------------------------------------------------------------------------------

        Dim quotation As Quotation = quotationDP.GetActiveQuotation_by_quotId(quotId)

        If (version = 1) Then
            ViewBag.ResultMessage = "Versión anterior (" + quotNum + ")"
        Else
            ViewBag.ResultMessage = "Versión anterior (" + quotNum + "." + version + ")"
        End If

        ViewBag.QuotationNumberVersion = quotNum
        ViewBag.QuotationNumberVersion = quotNum + "." + quotation.Version


        Return View(quotAddEditModel)

    End Function


    <Authorize>
    Function QuotationDelete(ByVal quotId As Integer) As ActionResult

        Dim quotationDP As New QuotationDataProvider

        quotationDP.DesactivateQuotation(quotId)


        Return RedirectToAction("QuotationList", "Quotation", New With {.result = "SuccessDeletion"})

    End Function

    <Authorize>
    Function QuotationAdvancedSearch()

        Dim clientDP As New ClientDataProvider
        Me.ClientAutocompleteSource(clientDP.GetAllActiveClients_formattedForList())

        Return View()

    End Function



    Function CompareItemListJson(ByVal quotId As Integer, ByVal version As String, ByVal itemListStrJson As String)

        Dim quotProdOrServDP As New QuotationProductOrServiceDataProvider
        Dim prodOrServ_item_list As List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item) = quotProdOrServDP.GetAllProductOrServiceQ_Item_by_quotId_version(quotId, version)

        Dim jss As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim currentItemList = jss.Serialize(prodOrServ_item_list)


        currentItemList = currentItemList.Replace("""", "")
        itemListStrJson = itemListStrJson.Replace("""", "")

        Return currentItemList = itemListStrJson

    End Function


    Function BindClientInformation(ByVal client As Client)

        ViewBag.QuotationAdd_ClientName = client.Name
        ViewBag.QuotationAdd_Rut = New Tools().ThousandSeparator(client.RutNumber) + "-" + client.RutNValidator
        ViewBag.QuotationAdd_ServiceType = client.ServiceType
        ViewBag.QuotationAdd_Adderss = client.Address
        ViewBag.QuotationAdd_City = client.City
        ViewBag.QuotationAdd_Email = client.EMail
        ViewBag.QuotationAdd_Phone = client.Phone

    End Function

    Function Bind_DDL_WorkTypeQList()

        Dim workType_list As List(Of String) = New WorkTypeQDataProvider().GetAllActiveWorkTypeQName()
        Dim final_list As New List(Of String)
        final_list.Add("Seleccione Tipo de Trabajo")
        For Each workType As String In workType_list
            final_list.Add(workType)
        Next

        ViewBag.QuotationAdd_WorkTypeQ_list = final_list

    End Function

    Function Bind_DDL_MethodOfPaymentList()

        Dim methodOfPayment_list As List(Of String) = New MethodOfPaymentQDataProvider().GetAllActiveMethodOfPaymentQName()
        Dim final_list As New List(Of String)
        final_list.Add("Seleccione Forma de Pago")

        For Each method As String In methodOfPayment_list
            final_list.Add(method)
        Next

        ViewBag.QuotationAdd_MethodOfPayment_list = final_list

    End Function

    Function BindAutocompleteClientRuts()

        Dim clientDP As New ClientDataProvider
        Dim allClients_list As List(Of ClientDataProvider.Client_List_Formatted) = clientDP.GetAllActiveClients_formattedForList()

        Dim result As String = ""
        Dim count As Integer = 0

        For Each client In allClients_list

            ViewBag.ClientRutList += "'" + client.Rut.Replace(".", "") + "'"
            count += 1
            If (count < allClients_list.Count) Then
                ViewBag.ClientRutList += ","
            End If

        Next

    End Function

    Function BindAutocompleteClientNames()

        Dim clientDP As New ClientDataProvider
        Dim allClients_list As List(Of ClientDataProvider.Client_List_Formatted) = clientDP.GetAllActiveClients_formattedForList()

        Dim result As String = ""
        Dim count As Integer = 0

        For Each client In allClients_list

            ViewBag.ClientNameList += "'" + client.Name + "'"
            count += 1
            If (count < allClients_list.Count) Then
                ViewBag.ClientNameList += ","
            End If

        Next

    End Function


    <Authorize>
    Function GenerateQuotationPDF(ByVal quotId As Integer)
        Dim companyDP As New CompanyDataProvider
        Dim quotationDP As New QuotationDataProvider
        Dim quotProdOrServDP As New QuotationProductOrServiceDataProvider
        Dim clientDP As New ClientDataProvider
        Dim methodOfPaymentDP As New MethodOfPaymentQDataProvider
        Dim userDP As New UserDataProvider

        Dim tool As New Tools


        'Definición del report viewer ---------------------------------
        Dim repoViewer As New Microsoft.Reporting.WebForms.ReportViewer()
        repoViewer.ProcessingMode = ProcessingMode.Local
        repoViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/Quotation/QuotationReport.rdlc")
        repoViewer.LocalReport.Refresh()
        '--------------------------------------------------------------
        'Variables necesarias para la exportación del report-----------
        Dim streamBytes As Byte() = Nothing
        Dim mimeType As String = ""
        Dim encoding As String = ""
        Dim filenameExtension As String = ""
        Dim streamids As String() = Nothing
        Dim warnings As Warning() = Nothing
        '--------------------------------------------------------------


        Dim quotation As Quotation = quotationDP.GetActiveQuotation_by_quotId(quotId)


        '------------------------------------------------------------------------------------------------------------PRODUCT OR SERVICES
        Dim prodOrServ_item_list As List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item) = quotProdOrServDP.GetAllProductOrServiceQ_Item_by_quotId_version(quotId, quotation.Version)


        Dim ds As New DataSet
        Dim itemsTable As DataTable = ds.Tables.Add("Items")
        itemsTable.Columns.Add("ItemNumber", Type.GetType("System.Int32"))
        itemsTable.Columns.Add("Description", Type.GetType("System.String"))
        itemsTable.Columns.Add("Quantity", Type.GetType("System.String"))
        itemsTable.Columns.Add("UnitValue", Type.GetType("System.String"))
        itemsTable.Columns.Add("NetValue", Type.GetType("System.String"))

        Dim netValue_total As Integer = 0

        For Each item As ProductOrServiceQDataProvider.ProductOrServiceQ_Item In prodOrServ_item_list

            Dim dRow As DataRow

            dRow = itemsTable.NewRow()
            dRow("ItemNumber") = item.ItemNumber
            dRow("Description") = item.Description

            Dim quantity As String = tool.ThousandSeparator(item.Quantity)
            Dim unitValue As String = "$ " + tool.ThousandSeparator(item.UnitValue)
            Dim netValue_int As Integer = item.Quantity * item.UnitValue
            Dim netValue As String = "$ " + tool.ThousandSeparator(netValue_int.ToString)

            dRow("Quantity") = quantity
            dRow("UnitValue") = unitValue
            dRow("NetValue") = netValue

            itemsTable.Rows.Add(dRow)

            netValue_total += netValue_int

        Next

        Dim ivaPercentage As Double = quotation.IVAPercentage
        Dim ivaValue As Integer = Math.Round(netValue_total * ivaPercentage / 100)
        Dim totalValue As Integer = netValue_total + ivaValue

        Dim netValue_total_str As String = tool.ThousandSeparator(netValue_total)
        Dim ivaValue_str As String = tool.ThousandSeparator(ivaValue)
        Dim totalValue_str As String = tool.ThousandSeparator(totalValue)


        'Se asocia el datasource al report -------------
        Dim quotItems_datasource As New ReportDataSource
        quotItems_datasource.Name = "DSetQuotItems"
        quotItems_datasource.Value = itemsTable

        repoViewer.LocalReport.DataSources.Add(quotItems_datasource)

        '------------------------------------------------------------------------------------------------------------COMPANY INFO

        Dim company As Company = companyDP.GetCompany_default

        Dim companyName As String = company.Name
        Dim companyRut As String = "R.U.T: " + tool.ThousandSeparator(company.RutNumber) + "-" + company.RutNValidator
        Dim companyServiceType As String = company.ServiceType
        Dim companyAddressCity As String = company.Address + " - " + company.City
        Dim companyPhone As String = company.Phone
        Dim companyEmail As String = company.Email

        Dim param_companyName As New ReportParameter("CompanyName", companyName)
        Dim param_companyRut As New ReportParameter("CompanyRut", companyRut)
        Dim param_companyServiceType As New ReportParameter("CompanyServiceType", companyServiceType)
        Dim param_companyAddressCity As New ReportParameter("CompanyAddressCity", companyAddressCity)
        Dim param_companyPhone As New ReportParameter("CompanyPhone", companyPhone)
        Dim param_companyEmail As New ReportParameter("CompanyEmail", companyEmail)


        '------------------------------------------------------------------------------------------------------------CLIENT INFO
        Dim client As Client = clientDP.GetActiveClient_by_clientId(quotation.Client_Id)

        Dim clientName As String = client.Name
        Dim clientRut As String = tool.ThousandSeparator(client.RutNumber) + "-" + client.RutNValidator
        Dim clientCity As String = client.City
        Dim clientAddress As String = client.Address
        Dim clientPhone As String = client.Phone
        Dim clientEmail As String = client.EMail

        Dim param_clientName As New ReportParameter("ClientName", clientName)
        Dim param_clientRut As New ReportParameter("ClientRut", clientRut)
        Dim param_clientCity As New ReportParameter("ClientCity", clientCity)
        Dim param_clientAddress As New ReportParameter("ClientAddress", clientAddress)
        Dim param_clientPhone As New ReportParameter("ClientPhone", clientPhone)
        Dim param_clientEmail As New ReportParameter("ClientEmail", clientEmail)

        '------------------------------------------------------------------------------------------------------------QUOTATION INFO

        Dim quotationNumber As String = quotation.QuotationNumber
        If (quotation.Version <> 1) Then
            quotationNumber += "." + quotation.Version
        End If
        Dim creationDate As String = tool.GetFormattedDate(quotation.CreationDate)
        Dim validityDate As String = tool.GetFormattedDate(quotation.ValidityDate)
        Dim observation As String = quotation.Observation
        If (observation Is Nothing) Then
            observation = ""
        End If
        Dim workDestiny As String = quotation.WorkDestiny
        Dim contactName As String = quotation.ContactName
        Dim methodOfPayment As String = methodOfPaymentDP.GetMethodOfPaymentQ_by_id(quotation.MethodOfPaymentQ_Id).MethodName

        Dim userInfo As UserInfo = userDP.GetUserInfo_by_id(quotation.User_Id)
        Dim createdBy As String = userInfo.FirstName + " " + userInfo.LastName


        Dim param_quotationNumber_little As New ReportParameter("QuotationNumber", "COTIZACIÓN N° " + quotationNumber)
        Dim param_creationDate As New ReportParameter("CreationDate", creationDate)
        Dim param_validityDate As New ReportParameter("ValidityDate", validityDate)
        Dim param_observation As New ReportParameter("Observation", observation)
        Dim param_workDestiny As New ReportParameter("WorkDestiny", workDestiny)
        Dim param_contactName As New ReportParameter("ContactName", contactName)
        Dim param_methodOfPayment As New ReportParameter("MethodOfPayment", methodOfPayment)

        Dim param_createdBy As New ReportParameter("CreatedBy", createdBy)

        repoViewer.LocalReport.EnableExternalImages = True
        Dim logoPath As String = New Uri(Server.MapPath("~/Images/report_logo.png")).AbsoluteUri


        '------------------------------------------------------------------------------------------------------------TOTALS


        Dim param_netValue As New ReportParameter("NetValue", netValue_total_str)
        Dim param_ivaValue As New ReportParameter("IVAValue", ivaValue_str)
        Dim param_totalValue As New ReportParameter("TotalValue", totalValue_str)

        Dim customFooterText As String = "Ordenes de compras deben ser enviadas exclusivamente a: indumetal.maestranza@gmail.com"
        Dim param_customFooterText As New ReportParameter("CustomFooterText", customFooterText)

        Dim param_logoPath As New ReportParameter("LogoPath", logoPath)

        repoViewer.LocalReport.SetParameters(
            New ReportParameter() {
                param_companyName,
                param_companyRut,
                param_companyServiceType,
                param_companyAddressCity,
                param_companyPhone,
                param_companyEmail,
                param_quotationNumber_little,
                param_clientName,
                param_clientRut,
                param_clientCity,
                param_clientAddress,
                param_clientPhone,
                param_clientEmail,
                param_creationDate,
                param_validityDate,
                param_observation,
                param_workDestiny,
                param_contactName,
                param_methodOfPayment,
                param_netValue,
                param_ivaValue,
                param_totalValue,
                param_createdBy,
                param_customFooterText,
                param_logoPath
            })



        'Exportación del report -------------------------------------

        Dim fileName As String = tool.GetFormattedDate(DateTime.Now.ToShortDateString) + "_Cot-" + quotation.QuotationNumber + ".pdf"

        streamBytes = repoViewer.LocalReport.Render("PDF", Nothing, mimeType, encoding, filenameExtension, streamids, warnings)
        Return File(streamBytes, mimeType, fileName)
        '------------------------------------------------------------



    End Function




    Function ClientAutocompleteSource(ByVal allClients As List(Of ClientDataProvider.Client_List_Formatted))

        Dim count As Integer = 0
        For Each client In allClients
            ViewBag.RutList += "'" + client.Rut.Replace(".", "") + "'"
            ViewBag.NameList += "'" + client.Name + "'"
            count += 1
            If (count < allClients.Count) Then
                ViewBag.RutList += ","
                ViewBag.NameList += ","
            End If
        Next

    End Function



End Class