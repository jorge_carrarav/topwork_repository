﻿Imports WebMatrix.WebData
Imports TopWork.WorkDataProvider

Imports Microsoft.Reporting.WebForms

Public Class WorkOrderController

    Inherits System.Web.Mvc.Controller


    <Authorize>
    Function SetQuotationStatus_viewBags()

        Dim workStatusDP As New WorkStatusDataProvider
        Dim workStatus_list As List(Of WorkStatus) = workStatusDP.GetAllWorkStatus


        ViewBag.ws_a = (From s In workStatus_list
                        Where s.WorkStatus_Id = 1
                        Select s.StatusName).FirstOrDefault()
        ViewBag.ws_b = (From s In workStatus_list
                        Where s.WorkStatus_Id = 2
                        Select s.StatusName).FirstOrDefault()
        ViewBag.ws_c = (From s In workStatus_list
                        Where s.WorkStatus_Id = 3
                        Select s.StatusName).FirstOrDefault()
        ViewBag.ws_d = (From s In workStatus_list
                        Where s.WorkStatus_Id = 4
                        Select s.StatusName).FirstOrDefault()
        ViewBag.ws_e = (From s In workStatus_list
                        Where s.WorkStatus_Id = 5
                        Select s.StatusName).FirstOrDefault()

    End Function


    <Authorize>
    Function WorkOrderList(ByVal result As String, ByVal woNum As String, ByVal ws_b As String, ByVal ws_c As String, ByVal ws_d As String, ByVal ws_e As String) As ActionResult

        SetQuotationStatus_viewBags()
        ViewBag.SearchResult = ""
        ViewBag.ResultMessage = ""

        Dim woDP As New WorkOrderDataProvider
        Dim allWorks_list_formatted As List(Of WorkOrderDataProvider.WorkOrder_List_Formatted)


        If (String.IsNullOrEmpty(woNum)) Then
            allWorks_list_formatted = woDP.GetAllActiveWorkOrders_formattedForList_by_workOrderNumber(-1)
            allWorks_list_formatted = FilteringList(allWorks_list_formatted, ws_b, ws_c, ws_d, ws_e)
        Else

            Try
                Dim isNumber As Integer = Convert.ToInt32(woNum)

                allWorks_list_formatted = woDP.GetAllActiveWorkOrders_formattedForList_by_workOrderNumber(woNum)

                ViewBag.SearchResult = "Resulados para la búsqueda de n° de O.T. '" + woNum + "'"
            Catch ex As Exception
                ViewBag.ResultMessage = "Número inválido de orden de trabajo"
                allWorks_list_formatted = woDP.GetAllActiveWorkOrders_formattedForList_by_workOrderNumber(-1)
                allWorks_list_formatted = FilteringList(allWorks_list_formatted, ws_b, ws_c, ws_d, ws_e)
            End Try
        End If

        ViewBag.ResultMessage += New Tools().GetResultMessage("WorkOrder", result)
        Return View(allWorks_list_formatted)

    End Function

    <Authorize>
    Function WorkOrderList_AdvSearch(ByVal quotNum As String, ByVal workOrderNum As String, ByVal purchaseOrderNumber As String, ByVal clientName As String, ByVal clientRut As String, ByVal wayBillNum As String, ByVal invoiceNum As String) As ActionResult

        ViewBag.SearchResult = ""
        ViewBag.ResultMessage = ""

        Dim woDP As New WorkOrderDataProvider
        Dim allWorkOrders_list_formatted As New List(Of WorkOrderDataProvider.WorkOrder_List_Formatted)

        If (Not String.IsNullOrEmpty(quotNum)) Then

            allWorkOrders_list_formatted = woDP.GetAllActiveWorkOrders_formattedForList_by_quotNumberVersion(quotNum)
            ViewBag.SearchResult = "Resulados de Trabajos para búsqueda de cotización N°  " + quotNum + ""

        ElseIf (Not String.IsNullOrEmpty(workOrderNum)) Then

            allWorkOrders_list_formatted = woDP.GetAllActiveWorkOrders_formattedForList_by_workOrderNumber(workOrderNum)
            ViewBag.SearchResult = "Resulados de Trabajos para búsqueda de O.T. N°  " + workOrderNum + ""

        ElseIf (Not String.IsNullOrEmpty(purchaseOrderNumber)) Then

            allWorkOrders_list_formatted = woDP.GetAllActiveWorkOrders_formattedForList_by_purchaseOrderNumber(purchaseOrderNumber)
            ViewBag.SearchResult = "Resulados de Trabajos para búsqueda de O.C. N°  " + purchaseOrderNumber + ""

        ElseIf (Not String.IsNullOrEmpty(clientName)) Then

            allWorkOrders_list_formatted = woDP.GetAllActiveWorkOrders_formattedForList_by_clientName(clientName)
            ViewBag.SearchResult = "Resulados de Trabajos para búsqueda de cliente '" + clientName + "'"

        ElseIf (Not String.IsNullOrEmpty(clientRut)) Then

            allWorkOrders_list_formatted = woDP.GetAllActiveWorkOrders_formattedForList_by_clientRutNumber(clientRut)
            ViewBag.SearchResult = "Resulados de Trabajos para búsqueda de Rut de cliente '" + clientRut + "'"

        ElseIf (Not String.IsNullOrEmpty(wayBillNum)) Then

            allWorkOrders_list_formatted = woDP.GetAllActiveWorkOrders_formattedForList_by_wayBillNumber(wayBillNum)
            ViewBag.SearchResult = "Resulados de Trabajos para búsqueda de guía de despacho N° " + wayBillNum + ""

        ElseIf (Not String.IsNullOrEmpty(invoiceNum)) Then

            allWorkOrders_list_formatted = woDP.GetAllActiveWorkOrders_formattedForList_by_invoice(invoiceNum)
            ViewBag.SearchResult = "Resulados de Trabajos para búsqueda de factura N° " + invoiceNum + ""

        End If

        ViewBag.ResultMessage = "Búsqueda de Trabajos"
        Return View(allWorkOrders_list_formatted)

    End Function



    <Authorize>
    Function WorkOrderAdd(ByVal quotNum As String, ByVal fromNode As String) As ActionResult

        ViewBag.GoBackPoint = fromNode

        Dim woDP As New WorkOrderDataProvider
        Dim quotationDP As New QuotationDataProvider
        Dim clientDP As New ClientDataProvider

        Dim workOrder_addEdit_formatted As New WorkOrderDataProvider.WorkOrder_AddEdit_Formatted

        If (quotNum Is Nothing Or quotNum = "") Then
            Return View()
        Else

            Try
                Dim isNumber As Integer = Convert.ToInt32(quotNum.Split(".")(0))
                If (quotNum.Split(".").Count < 3) Then

                    If (quotNum.Split(".").Count > 1) Then
                        Try
                            isNumber = Convert.ToInt32(quotNum.Split(".")(1))
                        Catch ex As Exception
                            ViewBag.ResultMessage = "Número inválido de cotización"
                        End Try
                    End If

                    Dim allClient_list As List(Of Client) = clientDP.GetAllActiveClients()


                    Dim quot As Quotation = quotationDP.GetActiveQuotation_by_quotNumberVersion(quotNum)

                    If (quot IsNot Nothing) Then

                        Dim work As WorkOrder = woDP.GetActiveWorkOrder_by_quotId(quot.Quotation_id)

                        If (work Is Nothing) Then

                            workOrder_addEdit_formatted = woDP.GetActiveWorkOrder_formattedForAddEdit_by_quotNumberVersion(quotNum)

                            ViewBag.QuotationId = workOrder_addEdit_formatted.Quotation_Id

                            ViewBag.QuotationNumber = quotNum
                            ViewBag.ResultMessage = "Cotización N° " + quotNum
                            Return View(workOrder_addEdit_formatted)

                        Else
                            ViewBag.ResultMessage = "Cotización ya posee una Orden de Trabajo"
                        End If
                    Else
                        ViewBag.ResultMessage = "Número de cotización no encontrada"
                    End If

                Else
                    ViewBag.ResultMessage = "Número inválido de cotización"
                End If
            Catch ex As Exception
                ViewBag.ResultMessage = "Número inválido de cotización"
            End Try

        End If

        workOrder_addEdit_formatted.QuotationNumber = quotNum
        Return View(workOrder_addEdit_formatted)

    End Function

    <HttpPost>
    <Authorize>
    Function WorkOrderAdd(ByVal WorkOrderAddEditModel As WorkOrderDataProvider.WorkOrder_AddEdit_Formatted) As String

        Dim woDP As New WorkOrderDataProvider

        Dim newWorkOrder As New WorkOrder

        Dim userName = User.Identity.Name
        Dim userId As Integer = WebSecurity.GetUserId(userName)

        Dim finishDate_str As String = WorkOrderAddEditModel.FinishDate
        Dim finishDate As DateTime
        Try
            finishDate = New DateTime(finishDate_str.Split("-")(2), finishDate_str.Split("-")(1), finishDate_str.Split("-")(0))
        Catch ex As Exception
            finishDate = DateTime.Now
        End Try
        newWorkOrder.FinishDate = finishDate

        newWorkOrder.Quotation_id = WorkOrderAddEditModel.Quotation_Id
        newWorkOrder.User_Id = userId
        newWorkOrder.WorkOrderNumber = woDP.GetNewWorkOrderNumber()
        newWorkOrder.FinishDate = finishDate
        newWorkOrder.PurchaseOrderNumber = WorkOrderAddEditModel.PurchaseOrderNumber
        newWorkOrder.Observation = WorkOrderAddEditModel.Observations
        newWorkOrder.CreationDate = DateTime.Now
        newWorkOrder.Active = True

        woDP.InsertWorkOrder(newWorkOrder)

        Return newWorkOrder.WorkOrder_Id

    End Function


    <Authorize>
    Function WorkOrderEdit(ByVal workId As String, ByVal woNum As String, ByVal result As String, ByVal fromNode As String, ByVal activeTab As String) As ActionResult

        Dim unitOfMeasureQDP As New UnitOfMeasureQDataProvider
        Dim quotationDP As New QuotationDataProvider

        ViewBag.GoBackPoint = fromNode

        ViewBag.ResultMessage = New Tools().GetResultMessage("Work", result)
        ViewBag.ActiveTab = activeTab

        Dim woDP As New WorkOrderDataProvider

        Dim workOrder_addEdit_formatted As WorkOrderDataProvider.WorkOrder_AddEdit_Formatted

        If (Not String.IsNullOrEmpty(workId)) Then
            workOrder_addEdit_formatted = woDP.GetWorkOrder_formattedForAddEdit_by_workId(workId)
        Else
            workOrder_addEdit_formatted = woDP.GetWorkOrder_formattedForAddEdit_by_workOrderNumber(woNum)
        End If


        ViewBag.QuotationId = workOrder_addEdit_formatted.Quotation_Id
        Dim jss As New System.Web.Script.Serialization.JavaScriptSerializer
        ViewBag.UnitsOfMeasure = jss.Serialize(unitOfMeasureQDP.GetAllActiveUnitOfMeasureQ_shortName()).ToString().Replace("""", "").Replace("[", "").Replace("]", "")

        Dim quotId As Integer = workOrder_addEdit_formatted.Quotation_Id
        Me.SetStatus(quotId, quotationDP.GetActiveQuotation_by_quotId(quotId).Version)

        Return View(workOrder_addEdit_formatted)

    End Function

    <HttpPost>
    <Authorize>
    Function WorkOrderEdit(workOrder_addEdit_formatted As WorkOrderDataProvider.WorkOrder_AddEdit_Formatted) As String

        Dim woDP As New WorkOrderDataProvider
        Dim materialDP As New MaterialWODataProvider
        Dim workMaterialWODP As New WorkOrderMaterialDataProvider
        Dim wayBillDP As New WayBillDataProvider()
        Dim wayBillProdOrServDP As New WayBillProductOrServiceDataProvider()
        Dim prodOrServDP As New ProductOrServiceQDataProvider()
        Dim invoiceDP As New InvoiceDataProvider
        Dim invoiceWayBillDP As New InvoiceWayBIllDataProvider
        Dim unitOfMeasureQDP As New UnitOfMeasureQDataProvider


        Dim newWO As WorkOrder = woDP.GetWorkOrder_by_Id(workOrder_addEdit_formatted.WorkOrder_Id)

        Dim finishDate_str As String = workOrder_addEdit_formatted.FinishDate
        Dim finishDate As DateTime
        Try
            finishDate = New DateTime(finishDate_str.Split("-")(2), finishDate_str.Split("-")(1), finishDate_str.Split("-")(0))
        Catch ex As Exception
            finishDate = DateTime.Now
        End Try
        newWO.FinishDate = finishDate
        newWO.PurchaseOrderNumber = workOrder_addEdit_formatted.PurchaseOrderNumber
        newWO.Observation = workOrder_addEdit_formatted.Observations

        woDP.UpdateWorkOrder(newWO)

        '------------------------------------------------------------------------------------------------------------ MATERIALS
        Dim jss As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim materialList As List(Of MaterialWODataProvider.MaterialWO_Item) = jss.Deserialize(workOrder_addEdit_formatted.Material_list, GetType(List(Of MaterialWODataProvider.MaterialWO_Item)))

        For Each mat As MaterialWODataProvider.MaterialWO_Item In materialList

            Dim material As MaterialWO = materialDP.GetMaterialWO_by_description(mat.Description)

            If (material Is Nothing) Then
                Dim newMat As New MaterialWO
                newMat.Description = mat.Description
                materialDP.InsertMaterialWO(newMat)
                mat.MaterialWO_Id = newMat.MaterialWO_Id
            Else
                mat.MaterialWO_Id = material.MaterialWO_Id
            End If
        Next

        '------------------------------------------------------------------------------------------------------------ WORK ORDER | MATERIAL

        workMaterialWODP.DeleteAllWorkMaterial_by_workId(newWO.WorkOrder_Id)

        Dim itemNumber As Integer = 1
        For Each mat As MaterialWODataProvider.MaterialWO_Item In materialList

            Dim newWorkMaterialWO As New WorkOrderMaterial

            newWorkMaterialWO.WorkOrder_Id = newWO.WorkOrder_Id
            newWorkMaterialWO.MaterialWO_Id = mat.MaterialWO_Id
            newWorkMaterialWO.ItemNumber = itemNumber
            newWorkMaterialWO.Quantity = mat.Quantity
            newWorkMaterialWO.UnitOfMeasure_Id = unitOfMeasureQDP.GetUnitOfMeasureQ_by_shortName(mat.UnitOfMeasure).UnitOfMeasureQ_Id
            newWorkMaterialWO.NetValue = mat.NetValue
            newWorkMaterialWO.IVAPercentage = mat.IVAPercentage

            workMaterialWODP.InsertWorkMaterial(newWorkMaterialWO)

            itemNumber += 1
        Next

        '------------------------------------------------------------------------------------------------------------ WORK ORDER | WAY BILL | Product or Services

        Dim productOrService_list As List(Of ProductOrServiceQ) = prodOrServDP.GetAllProductOrServiceQ()

        wayBillDP.DeleteAllWayBills_by_workId(newWO.WorkOrder_Id)
        Dim wayBillList As List(Of WayBillDataProvider.WayBill_AddEdit_Formatted) = jss.Deserialize(workOrder_addEdit_formatted.WayBill_list, GetType(List(Of WayBillDataProvider.WayBill_AddEdit_Formatted)))

        For Each WayBill As WayBillDataProvider.WayBill_AddEdit_Formatted In wayBillList

            Dim newWayBill As New WayBill

            newWayBill.WorkOrder_Id = newWO.WorkOrder_Id

            newWayBill.WayBillNumber = WayBill.WayBillNumber
            Try
                newWayBill.WayBillDate = WayBill.WayBillDate
            Catch
                newWayBill.WayBillDate = Date.Today
            End Try
            newWayBill.Comment = WayBill.Comment

            Dim wayBill_id As Integer = wayBillDP.InsertWayBill(newWayBill)

            For Each prodOrServItem As WayBillProductOrServiceDataProvider.WayBillProductOrService_Item In WayBill.ItemsAndQuantity_list

                Dim newWayBillProdOrServ As New WayBillProductOrService

                newWayBillProdOrServ.WayBill_Id = wayBill_id


                newWayBillProdOrServ.ProductOrServiceQ_Id = (From p In productOrService_list
                                                             Where p.Description = prodOrServItem.Description
                                                             Select p.ProductOrServiceQ_Id).FirstOrDefault()

                newWayBillProdOrServ.Quantity = prodOrServItem.Quantity

                wayBillProdOrServDP.InsertWayBillProductOrService(newWayBillProdOrServ)

            Next

        Next

        '------------------------------------------------------------------------------------------------------------ WORK ORDER | INVOICE

        invoiceDP.DeleteAllInvoices_by_workId(newWO.WorkOrder_Id)
        Dim invoiceList As List(Of InvoiceDataProvider.Invoice_AddEdit_Formatted) = jss.Deserialize(workOrder_addEdit_formatted.Invoice_list, GetType(List(Of InvoiceDataProvider.Invoice_AddEdit_Formatted)))
        Dim wayBill_list As List(Of WayBill) = wayBillDP.GetAllWayBills_by_workId(newWO.WorkOrder_Id)

        Dim ivaPercentage As Double = New Tools().GetCurrentIVAPercentage()

        For Each invoice As InvoiceDataProvider.Invoice_AddEdit_Formatted In invoiceList

            Dim newInvoice As New Invoice

            newInvoice.WorkOrder_Id = newWO.WorkOrder_Id

            newInvoice.InvoiceNumber = invoice.InvoiceNumber
            Try
                newInvoice.InvoiceDate = invoice.InvoiceDate
            Catch
                newInvoice.InvoiceDate = Date.Today
            End Try
            newInvoice.IsPaid = invoice.IsPaid
            newInvoice.NetValue = Convert.ToInt32(invoice.NetValue)
            newInvoice.IVAPercentage = ivaPercentage
            newInvoice.Comment = invoice.Comment

            Dim invoiceId As Integer = invoiceDP.InsertInvoice(newInvoice)

            For Each wayBillNumber As Integer In invoice.WayBillNumber_list

                Dim newInvWayBill As New InvoiceWayBill
                newInvWayBill.Invoice_Id = invoiceId
                newInvWayBill.WayBill_Id = (From w In wayBill_list
                                            Where w.WayBillNumber = wayBillNumber
                                            Select w.WayBill_Id).FirstOrDefault()

                invoiceWayBillDP.InsertInvoiceWayBills(newInvWayBill)

            Next

        Next

        '-----------------------------------------------------------------------------------------------------------------------------------------
        Return newWO.WorkOrder_Id

    End Function

    Function SetStatus(ByVal quotId As Integer, ByVal version As Integer) As String

        Dim quotationDP As New QuotationDataProvider
        Dim woStatusDP As New WorkStatusDataProvider

        Dim status As WorkStatus = woStatusDP.GetWorkStatus_by_quotId_by_version(quotId, version)

        If (status.WorkStatus_Id = 1) Then
            ViewBag.StatusName = ""
            ViewBag.StatusIconColor = "gray"
        ElseIf (status.WorkStatus_Id = 2) Then
            ViewBag.StatusName = status.StatusName
            ViewBag.StatusIconColor = "yellow"
        ElseIf (status.WorkStatus_Id = 3) Then
            ViewBag.StatusName = status.StatusName
            ViewBag.StatusIconColor = "blue"
        ElseIf (status.WorkStatus_Id = 4) Then
            ViewBag.StatusName = status.StatusName
            ViewBag.StatusIconColor = "red"
        ElseIf (status.WorkStatus_Id = 5) Then
            ViewBag.StatusName = status.StatusName
            ViewBag.StatusIconColor = "green"
        End If

    End Function

    <Authorize>
    Function WorkOrderDelete(ByVal workId As Integer) As ActionResult

        Dim woDP As New WorkOrderDataProvider

        woDP.DesactivateWorkOrder(workId)


        Return RedirectToAction("WorkOrderList", "Work", New With {.ws_b = "1", .result = "SuccessDeletion"})

    End Function


    <Authorize>
    Function WorkOrderAdvancedSearch()

        Dim clientDP As New ClientDataProvider
        Me.ClientAutocompleteSource(clientDP.GetAllActiveClients_formattedForList())

        Return View()

    End Function

    <Authorize>
    Function GenerateWorkOrderPDF(ByVal workId As Integer) As ActionResult
        Dim woDP As New WorkOrderDataProvider
        Dim quotProdOrServDP As New QuotationProductOrServiceDataProvider
        Dim quotationDP As New QuotationDataProvider
        Dim woMaterialDP As New WorkOrderMaterialDataProvider
        Dim workTypeDP As New WorkTypeQDataProvider
        Dim clientDP As New ClientDataProvider
        Dim tool As New Tools



        'Definición del report viewer ---------------------------------
        Dim repoViewer As New Microsoft.Reporting.WebForms.ReportViewer()
        repoViewer.ProcessingMode = ProcessingMode.Local
        repoViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/WorkOrder/WorkOrderReport.rdlc")
        repoViewer.LocalReport.Refresh()
        '--------------------------------------------------------------
        'Variables necesarias para la exportación del report-----------
        Dim streamBytes As Byte() = Nothing
        Dim mimeType As String = ""
        Dim encoding As String = ""
        Dim filenameExtension As String = ""
        Dim streamids As String() = Nothing
        Dim warnings As Warning() = Nothing
        '--------------------------------------------------------------



        Dim quotation As Quotation = woDP.GetQuotation_by_workId(workId)
        Dim work As WorkOrder = woDP.GetActiveWorkOrder_by_quotId(quotation.Quotation_id)


        '---------------------------------------------------------------------------------------------------------------------------- PRODUCT OR SERVICES 
        Dim prodOrServ_item_list As List(Of ProductOrServiceQDataProvider.ProductOrServiceQ_Item) = quotProdOrServDP.GetAllProductOrServiceQ_Item_by_quotId_version(quotation.Quotation_id, quotation.Version)

        Dim prodOrServTable As New DataTable("WorkProductOrService")
        prodOrServTable.Columns.Add("PoS_ItemNumber", Type.GetType("System.Int32"))
        prodOrServTable.Columns.Add("PoS_Description", Type.GetType("System.String"))
        prodOrServTable.Columns.Add("PoS_Quantity", Type.GetType("System.String"))

        For Each item As ProductOrServiceQDataProvider.ProductOrServiceQ_Item In prodOrServ_item_list

            Dim dRow As DataRow

            dRow = prodOrServTable.NewRow()
            dRow("PoS_ItemNumber") = item.ItemNumber
            dRow("PoS_Description") = item.Description
            dRow("PoS_Quantity") = tool.ThousandSeparator(item.Quantity)

            prodOrServTable.Rows.Add(dRow)

        Next

        '---------------------------------------------------------------------------------------------------------------------------- MATERIALS 
        Dim material_item_list As List(Of MaterialWODataProvider.MaterialWO_Item) = woMaterialDP.GetAllMaterialWO_Item_by_workId(workId)

        Dim materialTable As New DataTable("WorkMaterialWO")
        materialTable.Columns.Add("Mat_ItemNumber", Type.GetType("System.Int32"))
        materialTable.Columns.Add("Mat_Description", Type.GetType("System.String"))
        materialTable.Columns.Add("Mat_Quantity", Type.GetType("System.String"))



        For Each material As MaterialWODataProvider.MaterialWO_Item In material_item_list

            Dim dRow As DataRow

            dRow = materialTable.NewRow()
            dRow("Mat_ItemNumber") = material.ItemNumber
            dRow("Mat_Description") = material.Description


            If (material.UnitOfMeasure = "") Then
                dRow("Mat_Quantity") = tool.ThousandSeparator(material.Quantity)
            Else
                dRow("Mat_Quantity") = tool.ThousandSeparator(material.Quantity) + " " + material.UnitOfMeasure
            End If


            materialTable.Rows.Add(dRow)

        Next



        'Se asocia el datasource al report -------------
        Dim woProdOrServ_datasource As New ReportDataSource
        woProdOrServ_datasource.Name = "WorkOrderProdOrServ"
        woProdOrServ_datasource.Value = prodOrServTable
        repoViewer.LocalReport.DataSources.Add(woProdOrServ_datasource)

        Dim material_datasource As New ReportDataSource
        material_datasource.Name = "WorkOrderMats"
        material_datasource.Value = materialTable
        repoViewer.LocalReport.DataSources.Add(material_datasource)


        'woRepo.Subreports("SB_WorkOrderProductOrServices").SetDataSource(prodOrServTable)
        'woRepo.Subreports("SB_WorkOrderMaterialWOs").SetDataSource(materialTable)


        '----------------------------------------------------------------------------------------------------------------------------- WORK ORDER GENERAL INFORMATION


        Dim param_woNumber_little As New ReportParameter("WONumber_little", "O.T. N° " + work.WorkOrderNumber)
        Dim param_woNumber_big As New ReportParameter("WONumber_big", "ORDEN DE TRABAJO N° " + work.WorkOrderNumber)
        Dim param_clientName As New ReportParameter("ClientName", clientDP.GetClient_by_quotationId(quotation.Quotation_id).Name)
        Dim param_creationDate As New ReportParameter("CreationDate", tool.GetFormattedDate(DateTime.Now))
        Dim param_finishDate As New ReportParameter("FinishDate", tool.GetFormattedDate(work.FinishDate))
        Dim param_workType As New ReportParameter("WorkType", workTypeDP.GetActiveWorkTypeQ_by_workId(workId).TypeName)
        Dim param_observations As New ReportParameter("Observation", work.Observation)


        repoViewer.LocalReport.EnableExternalImages = True
        Dim logoPath As String = New Uri(Server.MapPath("~/Images/report_logo.png")).AbsoluteUri
        Dim param_logPath As New ReportParameter("LogoPath", logoPath)



        '----------------------------------------------------------------------------------------------------------------------------------------------------------------


        repoViewer.LocalReport.SetParameters(
         New ReportParameter() {
            param_woNumber_little,
            param_woNumber_big,
            param_clientName,
            param_creationDate,
            param_finishDate,
            param_workType,
            param_observations,
            param_logPath
        })



        'Exportación del report -------------------------------------

        Dim fileName As String = tool.GetFormattedDate(DateTime.Now.ToShortDateString) + "_OT-" + work.WorkOrderNumber + ".pdf"

        streamBytes = repoViewer.LocalReport.Render("PDF", Nothing, mimeType, encoding, filenameExtension, streamids, warnings)
        Return File(streamBytes, mimeType, fileName)
        '------------------------------------------------------------


    End Function





    Function ClientAutocompleteSource(ByVal allClients As List(Of ClientDataProvider.Client_List_Formatted))

        Dim count As Integer = 0
        For Each client In allClients
            ViewBag.RutList += "'" + client.Rut.Replace(".", "") + "'"
            ViewBag.NameList += "'" + client.Name + "'"
            count += 1
            If (count < allClients.Count) Then
                ViewBag.RutList += ","
                ViewBag.NameList += ","
            End If
        Next

    End Function


    <Authorize>
    Function FilteringList(ByVal allWorkOrders As List(Of WorkOrderDataProvider.WorkOrder_List_Formatted), ByVal ws_b As String, ByVal ws_c As String, ByVal ws_d As String, ByVal ws_e As String) As List(Of WorkOrderDataProvider.WorkOrder_List_Formatted)

        Dim allWorkOrders_filtered As List(Of WorkOrderDataProvider.WorkOrder_List_Formatted)
        Dim woStatusDP As New WorkStatusDataProvider

        Dim workStatus_list As List(Of WorkStatus) = woStatusDP.GetAllWorkStatus


        If (ws_b Is Nothing) Then
            ws_b = "1"
        End If
        If (ws_c Is Nothing) Then
            ws_c = "0"
        End If
        If (ws_d Is Nothing) Then
            ws_d = "0"
        End If
        If (ws_e Is Nothing) Then
            ws_e = "0"
        End If

        If (ws_b = "1") Then
            ws_b = (From w In workStatus_list
                         Where w.WorkStatus_Id = 2
                         Select w.StatusName).FirstOrDefault()
        End If
        If (ws_c = "1") Then
            ws_c = (From w In workStatus_list
                         Where w.WorkStatus_Id = 3
                         Select w.StatusName).FirstOrDefault()
        End If
        If (ws_d = "1") Then
            ws_d = (From w In workStatus_list
                             Where w.WorkStatus_Id = 4
                             Select w.StatusName).FirstOrDefault()
        End If
        If (ws_e = "1") Then
            ws_e = (From w In workStatus_list
                             Where w.WorkStatus_Id = 5
                             Select w.StatusName).FirstOrDefault()
        End If


        allWorkOrders_filtered = (From q In allWorkOrders
                                  Where q.Status.Contains(ws_b) _
                                  Or q.Status.Contains(ws_c) _
                                  Or q.Status.Contains(ws_d) _
                                  Or q.Status.Contains(ws_e)
                                  Select q).ToList

        Return allWorkOrders_filtered

    End Function



End Class