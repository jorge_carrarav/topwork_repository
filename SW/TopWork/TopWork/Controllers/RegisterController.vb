﻿Imports WebMatrix.WebData
Namespace TopWork
    Public Class RegisterController
        Inherits System.Web.Mvc.Controller



#Region "Proveedores"
        <Authorize>
        Function SupplierList(ByVal result As String, ByVal rut As String, ByVal name As String) As ActionResult
            ViewBag.SearchResult = ""

            Dim supplierDP As New SupplierDataProvider
            Dim allSuppliers As List(Of SupplierDataProvider.Supplier_List_Formatted) = supplierDP.GetAllSuppliers

            SupplierAutocompleteSource(allSuppliers)


            If (rut IsNot Nothing) Then
                allSuppliers = supplierDP.GetAllSuppliers_by_rutNumber_active(rut.Split("-")(0))
                ViewBag.SearchResult = "Resulados para la búsqueda de Rut '" + rut + "'"
            ElseIf (name IsNot Nothing) Then
                allSuppliers = supplierDP.GetAllActiveSuppliers_by_name(name)
                ViewBag.SearchResult = "Resulados para la búsqueda de nombre '" + name + "'"
            End If


            ViewBag.ResultMessage = New Tools().GetResultMessage("Supplier", result)
            Return View(allSuppliers)

        End Function

        Function SupplierAutocompleteSource(ByVal allSuppliers As List(Of SupplierDataProvider.Supplier_List_Formatted))

            Dim count As Integer = 0
            For Each prov In allSuppliers
                ViewBag.RutList += "'" + prov.Rut.Replace(".", "") + "'"
                ViewBag.NameList += "'" + prov.Name + "'"
                count += 1
                If (count < allSuppliers.Count) Then
                    ViewBag.RutList += ","
                    ViewBag.NameList += ","
                End If
            Next

        End Function

        <Authorize>
        Function SupplierAdd() As ActionResult

            Return View()

        End Function

        <HttpPost>
       <Authorize>
        Function SupplierAdd(ByVal model As Supplier) As ActionResult

            Try
                If (model.RutNumber = Double.NaN Or
                    model.RutNValidator Is Nothing Or
                    model.Name Is Nothing Or
                    model.ServiceType Is Nothing Or
                    model.Address Is Nothing Or
                    model.City Is Nothing) Then
                    ViewBag.ResultMessage = "Debe completar todos los campos requeridos"
                    Return View()
                End If

                If (Not New Tools().IsValidRut(model.RutNumber, model.RutNValidator)) Then
                    ViewBag.ResultMessage = "Debe ingresar un Rut válido"
                    Return View()
                End If

                Dim supplierDP As New SupplierDataProvider

                If (supplierDP.GetSupplier_by_rutNumber(model.RutNumber) IsNot Nothing) Then
                    ViewBag.ResultMessage = "El Rut ingresado ya existe"
                    Return View()
                End If

                If (supplierDP.GetSupplier_by_name(model.Name) IsNot Nothing) Then
                    ViewBag.ResultMessage = "El nombre ingresado ya existe"
                    Return View()
                End If

                model.Active = True
                Dim supplierId As Integer = supplierDP.InsertSupplier(model)

                Dim result As String = "SuccessSupplierCreation"
                Return RedirectToAction("SupplierEdit", "Register", New With {.supplierId = supplierId, .result = result})

            Catch ex As Exception
                ViewBag.ResultMessage = "Error al intentar crear proveedor"
                Return View()
            End Try

            Return View()

        End Function

        <Authorize>
        Function SupplierEdit(ByVal supplierId As Integer?, ByVal result As String) As ActionResult

            If (supplierId Is Nothing) Then
                ViewBag.ResultMessage = "Debes seleccionar un proveedor válido"
                Return View(New Supplier)
            End If

            ViewBag.ResultMessage = New Tools().GetResultMessage("Supplier", result)

            Dim supplierDP As New SupplierDataProvider
            Dim model As Supplier = supplierDP.GetSupplier_by_id(supplierId)

            Return View(model)

        End Function

        <HttpPost>
        <Authorize>
        Function SupplierEdit(ByVal model As Supplier) As ActionResult

            Try
                If (model.RutNumber = Double.NaN Or
                    model.RutNValidator Is Nothing Or
                    model.Name Is Nothing Or
                    model.ServiceType Is Nothing Or
                    model.Address Is Nothing Or
                    model.City Is Nothing) Then
                    ViewBag.ResultMessage = "Debes completar todos los campos requeridos"
                    Return View()
                End If

                If (Not New Tools().IsValidRut(model.RutNumber, model.RutNValidator)) Then
                    ViewBag.ResultMessage = "Debe ingresar un Rut válido"
                    Return View()
                End If

                Dim supplierDP As New SupplierDataProvider

                If (supplierDP.GetSupplier_by_rutNumber_butNotThisId(model.RutNumber, model.Supplier_Id) IsNot Nothing) Then
                    ViewBag.ResultMessage = "El Rut ingresado ya existe"
                    Return View()
                End If

                If (supplierDP.GetSupplier_by_name_butNotThisId(model.Name, model.Supplier_Id) IsNot Nothing) Then
                    ViewBag.ResultMessage = "El nombre ingresado ya existe"
                    Return View()
                End If

                supplierDP.UpdateSupplier(model)

                ViewBag.ResultMessage = "Proveedor modificado con éxito"
                Return View(model)

            Catch ex As Exception

                ViewBag.ResultMessage = "Error al intentar modificar proveedor"
                Return View(model)

            End Try

            Return View()

        End Function


        <Authorize>
        Function SupplierDelete(ByVal supplierId As Integer) As ActionResult
            Try

                Dim supplierDP As New SupplierDataProvider
                supplierDP.DeleteSupplier_by_id(supplierId)

                Dim result As String = "SuccessDeletion"
                Return RedirectToAction("SupplierList", "Register", New With {.result = result})


            Catch ex As Exception
                Dim result As String = "ErrorTryingToDelete"
                Return RedirectToAction("SupplierEdit", "Register", New With {.supplierId = supplierId, .result = result})
            End Try
        End Function

#End Region



#Region "Clientes"
        <Authorize>
        Function ClientList(ByVal result As String, ByVal rut As String, ByVal name As String) As ActionResult
            ViewBag.SearchResult = ""

            Dim clientDP As New ClientDataProvider
            Dim allClients As List(Of ClientDataProvider.Client_List_Formatted) = clientDP.GetAllActiveClients_formattedForList()

            ClientAutocompleteSource(allClients)


            If (rut IsNot Nothing) Then
                allClients = clientDP.GetAllClients_by_rutNumber(rut.Split("-")(0))
                ViewBag.SearchResult = "Resulados para la búsqueda de rut '" + rut + "'"
            ElseIf (name IsNot Nothing) Then
                allClients = clientDP.GetAllClients_by_name(name)
                ViewBag.SearchResult = "Resulados para la búsqueda de nombre '" + name + "'"
            End If


            ViewBag.ResultMessage = New Tools().GetResultMessage("Client", result)
            Return View(allClients)

        End Function

        Function ClientAutocompleteSource(ByVal allClients As List(Of ClientDataProvider.Client_List_Formatted))

            Dim count As Integer = 0
            For Each client In allClients
                ViewBag.RutList += "'" + client.Rut.Replace(".", "") + "'"
                ViewBag.NameList += "'" + client.Name + "'"
                count += 1
                If (count < allClients.Count) Then
                    ViewBag.RutList += ","
                    ViewBag.NameList += ","
                End If
            Next

        End Function

        <Authorize>
        Function ClientAdd() As ActionResult

            Return View()

        End Function

        <HttpPost>
       <Authorize>
        Function ClientAdd(ByVal model As Client) As ActionResult

            Try
                If (model.RutNumber = Double.NaN Or
                    model.RutNValidator Is Nothing Or
                    model.Name Is Nothing Or
                    model.ServiceType Is Nothing Or
                    model.Address Is Nothing Or
                    model.City Is Nothing) Then
                    ViewBag.ResultMessage = "Debes completar todos los campos requeridos"
                    Return View()
                End If

                If (Not New Tools().IsValidRut(model.RutNumber, model.RutNValidator)) Then
                    ViewBag.ResultMessage = "Debe ingresar un Rut válido"
                    Return View()
                End If

                Dim clientDP As New ClientDataProvider

                If (clientDP.GetActiveClient_by_rutNumber(model.RutNumber) IsNot Nothing) Then
                    ViewBag.ResultMessage = "El Rut ingresado ya existe"
                    Return View()
                End If

                If (clientDP.GetActiveClient_by_name(model.Name) IsNot Nothing) Then
                    ViewBag.ResultMessage = "El nombre ingresado ya existe"
                    Return View()
                End If

                model.Active = True
                Dim clientId As Integer = clientDP.InsertClient(model)

                Dim result As String = "SuccessClientCreation"
                Return RedirectToAction("ClientEdit", "Register", New With {.clientId = clientId, .result = result})

            Catch ex As Exception
                ViewBag.ResultMessage = "Error al intentar crear cliente"
                Return View()
            End Try

            Return View()

        End Function

        <Authorize>
        Function ClientEdit(ByVal clientId As Integer?, ByVal result As String) As ActionResult

            If (clientId Is Nothing) Then
                ViewBag.ResultMessage = "Debes seleccionar un cliente válido"
                Return View(New Client)
            End If

            ViewBag.ResultMessage = New Tools().GetResultMessage("Client", result)

            Dim clientDP As New ClientDataProvider
            Dim model As Client = clientDP.GetActiveClient_by_clientId(clientId)

            Return View(model)

        End Function

        <HttpPost>
        <Authorize>
        Function ClientEdit(ByVal model As Client) As ActionResult

            Try
                If (model.RutNumber = Double.NaN Or
                    model.RutNValidator Is Nothing Or
                    model.Name Is Nothing Or
                    model.ServiceType Is Nothing Or
                    model.Address Is Nothing Or
                    model.City Is Nothing) Then
                    ViewBag.ResultMessage = "Debes completar todos los campos requeridos"
                    Return View()
                End If

                If (Not New Tools().IsValidRut(model.RutNumber, model.RutNValidator)) Then
                    ViewBag.ResultMessage = "Debe ingresar un Rut válido"
                    Return View()
                End If

                Dim clientDP As New ClientDataProvider

                If (clientDP.GetActiveClient_by_rutNumber_butNotThisId(model.RutNumber, model.Client_Id) IsNot Nothing) Then
                    ViewBag.ResultMessage = "El Rut ingresado ya existe"
                    Return View()
                End If

                If (clientDP.GetClient_by_name_butNotThisId(model.Name, model.Client_Id) IsNot Nothing) Then
                    ViewBag.ResultMessage = "El nombre ingresado ya existe"
                    Return View()
                End If

                clientDP.UpdateClient(model)

                ViewBag.ResultMessage = "Cliente modificado con éxito"
                Return View(model)

            Catch ex As Exception

                ViewBag.ResultMessage = "Error al intentar modificar cliente"
                Return View(model)

            End Try

            Return View()

        End Function


        <Authorize>
        Function ClientDelete(ByVal clientId As Integer) As ActionResult
            Try

                Dim clientDP As New ClientDataProvider


                If (clientDP.ClientHasQuotations_by_id(clientId)) Then
                    Dim result As String = "ClientHasQuotationsImpossibleToDelete"
                    Return RedirectToAction("ClientEdit", "Register", New With {.clientId = clientId, .result = result})
                Else
                    clientDP.DeleteClient_by_id(clientId)

                    Dim result As String = "SuccessDeletion"
                    Return RedirectToAction("ClientList", "Register", New With {.result = result})
                End If
            Catch ex As Exception
                Dim result As String = "ErrorTryingToDelete"
                Return RedirectToAction("ClientEdit", "Register", New With {.clientId = clientId, .result = result})
            End Try
        End Function

#End Region


    End Class

End Namespace