﻿Imports WebMatrix.WebData
Imports TopWork.ProductOrServicePODataProvider

Imports Microsoft.Reporting.WebForms
Imports System.IO


Public Class PurchaseOrderController
    Inherits System.Web.Mvc.Controller


    Function Modal() As ActionResult

        Return View()

    End Function




    <Authorize>
    Function PurchaseOrderList(ByVal result As String, ByVal purchOrderNumber As String) As ActionResult

        ViewBag.SearchResult = ""
        ViewBag.ResultMessage = ""

        Dim purchaseOrderDP As New PurchaseOrderDataProvider
        Dim allPurchaseOrders_list_formatted As List(Of PurchaseOrderDataProvider.PurchaseOrder_List_Formatted)

        Dim focusDate As DateTime = DateTime.Now.AddMonths(-6)

        If (String.IsNullOrEmpty(purchOrderNumber)) Then

            allPurchaseOrders_list_formatted = purchaseOrderDP.GetAllActivePurchaseOrder_formattedForList_by_purchaseOrderNumber_focusDate(-1, True, focusDate)

        Else
            Try
                allPurchaseOrders_list_formatted = purchaseOrderDP.GetAllActivePurchaseOrder_formattedForList_by_purchaseOrderNumber_focusDate(purchOrderNumber, True, focusDate)
                ViewBag.SearchResult = "Resulados para la búsqueda de n° de orden de compra '" + purchOrderNumber + "'"

            Catch ex As Exception
                ViewBag.ResultMessage = "Número inválido de orden de compra"
                allPurchaseOrders_list_formatted = purchaseOrderDP.GetAllActivePurchaseOrder_formattedForList_by_purchaseOrderNumber_focusDate(-1, True, focusDate)
            End Try
        End If

        ViewBag.ResultMessage += New Tools().GetResultMessage("PurchaseOrder", result)
        Return View(allPurchaseOrders_list_formatted)

    End Function



    <Authorize>
    Function PurchaseOrderAdd(ByVal rut As String, ByVal name As String) As ActionResult

        Dim unitOfMeasDP As New UnitOfMeasurePODataProvider
        Dim appConfigDP As New AppConfigurationDataProvider
        Dim appConfig As AppConfiguration = appConfigDP.GetAppConfiguration
        Dim supplierDP As New SupplierDataProvider

        ViewBag.IVAPercentage = appConfig.IVAPercentage

        ViewBag.PurchaseOrderAdd_Rut = rut

        BindAutocompleteSupplierList()

        Bind_DDL_WorkTypeList()
        Bind_DDL_MethodOfPaymentList()


        If (rut <> "") Then
            Try
                Convert.ToInt32(rut) 'Esto es sólo para determinar si el rut es numérico

                Dim supplier As Supplier = supplierDP.GetActiveSupplier_by_rutNumber(rut)

                BindSupplierInformation(supplier)

                ViewBag.ResultMessage = supplier.Name

            Catch ex As Exception
                ViewBag.ResultMessage = "Rut no encontrado."
            End Try
        ElseIf (name <> "") Then
            Try

                Dim supplier As Supplier = supplierDP.GetActiveSupplier_by_name(name)

                BindSupplierInformation(Supplier)

                ViewBag.ResultMessage = Supplier.Name

            Catch ex As Exception
                ViewBag.ResultMessage = "Proveedor no encontrado."
            End Try
        End If


        Dim jss As New System.Web.Script.Serialization.JavaScriptSerializer
        ViewBag.UnitsOfMeasure = jss.Serialize(unitOfMeasDP.GetAllActiveUnitOfMeasurePO_shortName()).ToString().Replace("""", "").Replace("[", "").Replace("]", "")

        Return View()

    End Function

    <HttpPost>
    <Authorize>
    Function PurchaseOrderAdd(ByVal purchOrderAddEditModel As PurchaseOrderDataProvider.PurchaseOrder_AddEdit_Formatted) As String

        Dim supplierDP As New SupplierDataProvider
        Dim purchaseOrderDP As New PurchaseOrderDataProvider
        Dim prodOrServDP As New ProductOrServicePODataProvider
        Dim purchaseOrderProdOrServDP As New PurchaseOrderProductOrServiceDataProvider
        Dim workTypeDP As New WorkTypePODataProvider
        Dim methodOfPaymentDP As New MethodOfPaymentPODataProvider
        Dim appConfigDP As New AppConfigurationDataProvider
        Dim unitOfMeasurementDP As New UnitOfMeasurePODataProvider

        Dim supplier As Supplier = supplierDP.GetActiveSupplier_by_rutNumber(purchOrderAddEditModel.SupplierRut.Split("-")(0).Replace(".", ""))

        Dim userName = User.Identity.Name
        Dim userId As Integer = WebSecurity.GetUserId(userName)


        '------------------------------------------------------------------------------------------------------------ PURCHASE ORDER

        Dim validityDate_str As String = purchOrderAddEditModel.ValidityDate
        Dim validityDate As DateTime
        Try
            validityDate = New DateTime(validityDate_str.Split("-")(2), validityDate_str.Split("-")(1), validityDate_str.Split("-")(0))
        Catch ex As Exception
            validityDate = DateTime.Now
        End Try
        Dim newPurchaseOrder As New PurchaseOrder

        newPurchaseOrder.Supplier_Id = supplier.Supplier_Id
        newPurchaseOrder.User_Id = userId
        newPurchaseOrder.PurchaseOrderNumber = purchaseOrderDP.GetNewPurchaseOrderNumber
        newPurchaseOrder.CreationDate = DateTime.Now
        newPurchaseOrder.ContactName = purchOrderAddEditModel.ContactName
        newPurchaseOrder.WorkDestiny = purchOrderAddEditModel.WorkDestiny
        newPurchaseOrder.WorkTypePO_Id = workTypeDP.GetActiveWorkTypePO_by_typeName(purchOrderAddEditModel.WorkType).WorkTypePO_Id
        newPurchaseOrder.MethodOfPaymentPO_Id = methodOfPaymentDP.GetMethodOfPaymentPO_by_methodName_docType(purchOrderAddEditModel.MethodOfPayment).MethodOfPaymentPO_Id
        newPurchaseOrder.ValidityDate = validityDate
        newPurchaseOrder.Observation = purchOrderAddEditModel.Observations
        newPurchaseOrder.IVAPercentage = appConfigDP.GetAppConfiguration.IVAPercentage
        newPurchaseOrder.Active = True

        purchaseOrderDP.InsertPurchaseOrder(newPurchaseOrder)

        '------------------------------------------------------------------------------------------------------------ PRODUCT OR SERVICES
        Dim jss As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim item_list As List(Of ProductOrServicePODataProvider.ProductOrServicePO_Item) = jss.Deserialize(purchOrderAddEditModel.Item_list, GetType(List(Of ProductOrServicePODataProvider.ProductOrServicePO_Item)))

        For Each item As ProductOrServicePODataProvider.ProductOrServicePO_Item In item_list
            Dim prodOrServ As New ProductOrServicePO
            prodOrServ.Description = item.Description.ToString().Trim().Replace(System.Environment.NewLine, "|NEWLINE|")
            prodOrServDP.InsertProductOrServicePO(prodOrServ)
            item.ProductOrServicePO_Id = prodOrServ.ProductOrServicePO_Id
        Next

        '------------------------------------------------------------------------------------------------------------ PURCHASE ORDER | PRODUCT OR SERVICES

        Dim itemNumber As Integer = 1
        For Each item As ProductOrServicePODataProvider.ProductOrServicePO_Item In item_list

            Dim newPurchOrderProdOrServ As New PurchaseOrderProductOrService

            newPurchOrderProdOrServ.PurchaseOrder_id = newPurchaseOrder.PurchaseOrder_Id
            newPurchOrderProdOrServ.ProductOrServicePO_Id = item.ProductOrServicePO_Id
            newPurchOrderProdOrServ.ItemNumber = itemNumber
            newPurchOrderProdOrServ.UnitValue = item.UnitValue
            newPurchOrderProdOrServ.Quantity = item.Quantity
            newPurchOrderProdOrServ.UnitOfMeasurePO_Id = unitOfMeasurementDP.GetUnitOfMeasurePO_by_shortName(item.UnitOfMeasure).UnitOfMeasurePO_Id

            purchaseOrderProdOrServDP.InsertPurchaseOrderProdOrServ(newPurchOrderProdOrServ)

            itemNumber += 1
        Next

        Return newPurchaseOrder.PurchaseOrder_Id

    End Function


    <Authorize>
    Function PurchaseOrderEdit(ByVal purchOrderId As Integer, ByVal result As String, ByVal fromNode As String) As ActionResult


        ViewBag.ResultMessage = New Tools().GetResultMessage("PurchaseOrder", result)


        Dim purchaseOrderDP As New PurchaseOrderDataProvider
        Dim supplierDP As New SupplierDataProvider
        Dim prodOrServDP As New ProductOrServicePODataProvider
        Dim purchOrderProdOrServDP As New PurchaseOrderProductOrServiceDataProvider

        Dim purchOrder As PurchaseOrder = purchaseOrderDP.GetActivePurchaseOrder_by_purchOrderId(purchOrderId)


        If (purchOrder Is Nothing) Then

            ViewBag.ResultMessage = "Orden de Compra no encontrada"

            Dim purchOrderAddEditModel As New PurchaseOrderDataProvider.PurchaseOrder_AddEdit_Formatted

            Return View(purchOrderAddEditModel)

        Else

            Dim supplier As Supplier = supplierDP.GetActiveSupplier_by_supplierId(purchOrder.Supplier_Id)
            Dim unitOfMeasDP As New UnitOfMeasurePODataProvider


            Dim jss As New System.Web.Script.Serialization.JavaScriptSerializer

            BindSupplierInformation(supplier)
            Bind_DDL_WorkTypeList()
            Bind_DDL_MethodOfPaymentList()

            '------------------------------------------------------------------------------------------------------------ PURCHASE ORDER Model

            Dim purchOrderAddEditModel As New PurchaseOrderDataProvider.PurchaseOrder_AddEdit_Formatted

            purchOrderAddEditModel = purchaseOrderDP.GetActivePurchaseOrder_formattedForAddEdit_by_purchOrderId(purchOrderId)

            '------------------------------------------------------------------------------------------------------------ PURCHASE ORDER | PRODUCT OR SERVICES 

            Dim prodOrServ_item_list As List(Of ProductOrServicePODataProvider.ProductOrServicePO_Item) = purchOrderProdOrServDP.GetAllProductOrService_Item_by_purchOrderId(purchOrderId)
            purchOrderAddEditModel.Item_list = jss.Serialize(prodOrServ_item_list)


            '--------------------------------------------------------------------------------------------------------------------------------

            ViewBag.UnitsOfMeasure = jss.Serialize(unitOfMeasDP.GetAllActiveUnitOfMeasurePO_shortName()).ToString().Replace("""", "").Replace("[", "").Replace("]", "")


            Return View(purchOrderAddEditModel)

        End If

    End Function

    <HttpPost>
    <Authorize>
    Function PurchaseOrderEdit(ByVal purchOrderAddEditModel As PurchaseOrderDataProvider.PurchaseOrder_AddEdit_Formatted) As String

        Dim supplierDP As New SupplierDataProvider
        Dim purchaseOrderDP As New PurchaseOrderDataProvider
        Dim prodOrServDP As New ProductOrServicePODataProvider
        Dim purchOrderProdOrServDP As New PurchaseOrderProductOrServiceDataProvider
        Dim workTypeDP As New WorkTypePODataProvider
        Dim methodOfPaymentDP As New MethodOfPaymentPODataProvider
        Dim appConfigDP As New AppConfigurationDataProvider
        Dim unitOfMeasurementDP As New UnitOfMeasurePODataProvider

        Dim supplier As Supplier = supplierDP.GetActiveSupplier_by_rutNumber(purchOrderAddEditModel.SupplierRut.Split("-")(0))

        Dim validityDate_str As String = purchOrderAddEditModel.ValidityDate
        Dim validityDate As DateTime

        Try
            validityDate = New DateTime(validityDate_str.Split("-")(2), validityDate_str.Split("-")(1), validityDate_str.Split("-")(0))
        Catch ex As Exception
            validityDate = DateTime.Now
        End Try


        Dim newPurchaseOrder As New PurchaseOrder

        newPurchaseOrder.PurchaseOrder_id = purchOrderAddEditModel.PurchaseOrder_Id
        newPurchaseOrder.ContactName = purchOrderAddEditModel.ContactName
        newPurchaseOrder.WorkDestiny = purchOrderAddEditModel.WorkDestiny
        newPurchaseOrder.ContactName = purchOrderAddEditModel.ContactName
        newPurchaseOrder.MethodOfPaymentPO_Id = methodOfPaymentDP.GetMethodOfPaymentPO_by_methodName_docType(purchOrderAddEditModel.MethodOfPayment).MethodOfPaymentPO_Id
        newPurchaseOrder.WorkTypePO_Id = workTypeDP.GetActiveWorkTypePO_by_typeName(purchOrderAddEditModel.WorkType).WorkTypePO_Id
        newPurchaseOrder.ValidityDate = validityDate
        newPurchaseOrder.Observation = purchOrderAddEditModel.Observations

        purchaseOrderDP.UpdatePurchaseOrder(newPurchaseOrder)

        If (Not CompareItemListJson(purchOrderAddEditModel.PurchaseOrder_Id, purchOrderAddEditModel.Item_list)) Then

            Dim userName = User.Identity.Name
            Dim userId As Integer = WebSecurity.GetUserId(userName)

            'Agregar nueva versión de PurchaseOrder
            newPurchaseOrder.CreationDate = DateTime.Now
            newPurchaseOrder.User_Id = userId


            '------------------------------------------------------------------------------------------------------------ PRODUCT OR SERVICES
            Dim jss As New System.Web.Script.Serialization.JavaScriptSerializer
            Dim newItem_list As List(Of ProductOrServicePODataProvider.ProductOrServicePO_Item) = jss.Deserialize(purchOrderAddEditModel.Item_list, GetType(List(Of ProductOrServicePODataProvider.ProductOrServicePO_Item)))

            For Each item As ProductOrServicePODataProvider.ProductOrServicePO_Item In newItem_list

                If (prodOrServDP.GetProductOrServicePO_by_description(item.Description) Is Nothing) Then

                    Dim prodOrServ As New ProductOrServicePO
                    prodOrServ.Description = item.Description.ToString().Trim().Replace(vbLf, "")
                    prodOrServDP.InsertProductOrServicePO(prodOrServ)

                    item.ProductOrServicePO_Id = prodOrServ.ProductOrServicePO_Id
                Else
                    item.ProductOrServicePO_Id = prodOrServDP.GetProductOrServicePO_by_description(item.Description).ProductOrServicePO_Id
                End If
            Next

            '------------------------------------------------------------------------------------------------------------ Remove all items from PurchOrderProductOrService
            purchOrderProdOrServDP.DeleteAllPurchOrderProdOrServ_by_purchaseOrderId(newPurchaseOrder.PurchaseOrder_Id)
            '------------------------------------------------------------------------------------------------------------ PURCHASE ORDER | PRODUCT OR SERVICES
            Dim itemNumber As Integer = 1
            For Each item As ProductOrServicePODataProvider.ProductOrServicePO_Item In newItem_list

                Dim newPurchOrderProdOrServ As New PurchaseOrderProductOrService

                newPurchOrderProdOrServ.PurchaseOrder_id = newPurchaseOrder.PurchaseOrder_Id
                newPurchOrderProdOrServ.ProductOrServicePO_Id = item.ProductOrServicePO_Id
                newPurchOrderProdOrServ.ItemNumber = itemNumber
                newPurchOrderProdOrServ.UnitValue = item.UnitValue
                newPurchOrderProdOrServ.Quantity = item.Quantity
                newPurchOrderProdOrServ.UnitOfMeasurePO_Id = unitOfMeasurementDP.GetUnitOfMeasurePO_by_shortName(item.UnitOfMeasure).UnitOfMeasurePO_Id

                purchOrderProdOrServDP.InsertPurchaseOrderProdOrServ(newPurchOrderProdOrServ)

                itemNumber += 1
            Next

        End If

        Return newPurchaseOrder.PurchaseOrder_id

    End Function


    <Authorize>
    Function PurchaseOrderDelete(ByVal purchOrderId As Integer) As ActionResult

        Dim purchaseOrderDP As New PurchaseOrderDataProvider

        purchaseOrderDP.DesactivatePurchaseOrder(purchOrderId)

        Return RedirectToAction("PurchaseOrderList", "PurchaseOrder", New With {.result = "SuccessDeletion"})

    End Function



    Function CompareItemListJson(ByVal purchOrderId As Integer, ByVal itemListStrJson As String)

        Dim purchOrderProdOrServDP As New PurchaseOrderProductOrServiceDataProvider
        Dim prodOrServ_item_list As List(Of ProductOrServicePODataProvider.ProductOrServicePO_Item) = purchOrderProdOrServDP.GetAllProductOrService_Item_by_purchOrderId(purchOrderId)

        Dim jss As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim currentItemList = jss.Serialize(prodOrServ_item_list)


        currentItemList = currentItemList.Replace("""", "")
        itemListStrJson = itemListStrJson.Replace("""", "")

        Return currentItemList = itemListStrJson

    End Function


    Function BindSupplierInformation(ByVal supplier As Supplier)

        ViewBag.PurchaseOrderAdd_SupplierName = supplier.Name
        ViewBag.PurchaseOrderAdd_Rut = New Tools().ThousandSeparator(supplier.RutNumber) + "-" + supplier.RutNValidator
        ViewBag.PurchaseOrderAdd_ServiceType = supplier.ServiceType
        ViewBag.PurchaseOrderAdd_Adderss = supplier.Address
        ViewBag.PurchaseOrderAdd_City = supplier.City
        ViewBag.PurchaseOrderAdd_Email = supplier.Email
        ViewBag.PurchaseOrderAdd_Phone = supplier.Phone

    End Function

    Function Bind_DDL_WorkTypeList()

        Dim workType_list As List(Of String) = New WorkTypePODataProvider().GetAllActiveWorkTypePO()
        Dim final_list As New List(Of String)
        final_list.Add("Seleccione Tipo de Trabajo")
        For Each workType As String In workType_list
            final_list.Add(workType)
        Next

        ViewBag.PurchaseOrderAdd_WorkTypePO_list = final_list

    End Function

    Function Bind_DDL_MethodOfPaymentList()

        Dim methodOfPayment_list As List(Of String) = New MethodOfPaymentPODataProvider().GetAllActiveMethodOfPaymentPOName()
        Dim final_list As New List(Of String)
        final_list.Add("Seleccione Forma de Pago")

        For Each method As String In methodOfPayment_list
            final_list.Add(method)
        Next

        ViewBag.PurchaseOrderAdd_MethodOfPayment_list = final_list

    End Function

    Function BindAutocompleteSupplierList()

        Dim supplierDP As New SupplierDataProvider
        Dim allSuppliers_list As List(Of SupplierDataProvider.Supplier_List_Formatted) = supplierDP.GetAllActiveSuppliers_formattedForList()

        Dim result As String = ""
        Dim count As Integer = 0

        For Each supplier In allSuppliers_list

            count += 1
            ViewBag.SupplierRutList += "'" + supplier.Rut.Replace(".", "") + "'"

            If (count < allSuppliers_list.Count) Then
                ViewBag.SupplierRutList += ","
            End If

            ViewBag.SupplierNameList += "'" + supplier.Name + "'"
            If (count < allSuppliers_list.Count) Then
                ViewBag.SupplierNameList += ","
            End If

        Next

    End Function


    <Authorize>
    Function GeneratePurchaseOrderPDF(ByVal purchOrderId As Integer)
        Dim companyDP As New CompanyDataProvider
        Dim purchaseOrderDP As New PurchaseOrderDataProvider
        Dim purchOrderProdOrServDP As New PurchaseOrderProductOrServiceDataProvider
        Dim supplierDP As New SupplierDataProvider
        Dim methodOfPaymentDP As New MethodOfPaymentPODataProvider
        Dim userDP As New UserDataProvider

        Dim tool As New Tools


        'Definición del report viewer ---------------------------------
        Dim repoViewer As New Microsoft.Reporting.WebForms.ReportViewer()
        repoViewer.ProcessingMode = ProcessingMode.Local
        repoViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/PurchaseOrder/PurchaseOrderReport.rdlc")
        repoViewer.LocalReport.Refresh()
        '--------------------------------------------------------------
        'Variables necesarias para la exportación del report-----------
        Dim streamBytes As Byte() = Nothing
        Dim mimeType As String = ""
        Dim encoding As String = ""
        Dim filenameExtension As String = ""
        Dim streamids As String() = Nothing
        Dim warnings As Warning() = Nothing
        '--------------------------------------------------------------


        Dim purchaseOrder As PurchaseOrder = purchaseOrderDP.GetActivePurchaseOrder_by_purchOrderId(purchOrderId)


        '------------------------------------------------------------------------------------------------------------PRODUCT OR SERVICES
        Dim prodOrServ_item_list As List(Of ProductOrServicePODataProvider.ProductOrServicePO_Item) = purchOrderProdOrServDP.GetAllProductOrService_Item_by_purchOrderId(purchOrderId)


        Dim ds As New DataSet
        Dim itemsTable As DataTable = ds.Tables.Add("Items")
        itemsTable.Columns.Add("ItemNumber", Type.GetType("System.Int32"))
        itemsTable.Columns.Add("Description", Type.GetType("System.String"))
        itemsTable.Columns.Add("Quantity", Type.GetType("System.String"))
        itemsTable.Columns.Add("UnitValue", Type.GetType("System.String"))
        itemsTable.Columns.Add("NetValue", Type.GetType("System.String"))

        Dim netValue_total As Integer = 0

        For Each item As ProductOrServicePODataProvider.ProductOrServicePO_Item In prodOrServ_item_list

            Dim dRow As DataRow

            dRow = itemsTable.NewRow()
            dRow("ItemNumber") = item.ItemNumber
            dRow("Description") = item.Description

            Dim quantity As String = tool.ThousandSeparator(item.Quantity)
            Dim unitValue As String = "$ " + tool.ThousandSeparator(item.UnitValue)
            Dim netValue_int As Integer = item.Quantity * item.UnitValue
            Dim netValue As String = "$ " + tool.ThousandSeparator(netValue_int.ToString)

            dRow("Quantity") = quantity
            dRow("UnitValue") = unitValue
            dRow("NetValue") = netValue

            itemsTable.Rows.Add(dRow)

            netValue_total += netValue_int

        Next

        Dim ivaPercentage As Double = purchaseOrder.IVAPercentage
        Dim ivaValue As Integer = Math.Round(netValue_total * ivaPercentage / 100)
        Dim totalValue As Integer = netValue_total + ivaValue

        Dim netValue_total_str As String = tool.ThousandSeparator(netValue_total)
        Dim ivaValue_str As String = tool.ThousandSeparator(ivaValue)
        Dim totalValue_str As String = tool.ThousandSeparator(totalValue)


        'Se asocia el datasource al report -------------
        Dim purchaseOrderItems_datasource As New ReportDataSource
        purchaseOrderItems_datasource.Name = "PurchaseOrderItem"
        purchaseOrderItems_datasource.Value = itemsTable

        repoViewer.LocalReport.DataSources.Add(purchaseOrderItems_datasource)

        '------------------------------------------------------------------------------------------------------------COMPANY INFO

        Dim company As Company = companyDP.GetCompany_default

        Dim companyName As String = company.Name
        Dim companyRut As String = "R.U.T: " + tool.ThousandSeparator(company.RutNumber) + "-" + company.RutNValidator
        Dim companyServiceType As String = company.ServiceType
        Dim companyAddressCity As String = company.Address + " - " + company.City
        Dim companyPhone As String = company.Phone
        Dim companyEmail As String = company.Email

        Dim param_companyName As New ReportParameter("CompanyName", companyName)
        Dim param_companyRut As New ReportParameter("CompanyRut", companyRut)
        Dim param_companyServiceType As New ReportParameter("CompanyServiceType", companyServiceType)
        Dim param_companyAddressCity As New ReportParameter("CompanyAddressCity", companyAddressCity)
        Dim param_companyPhone As New ReportParameter("CompanyPhone", companyPhone)
        Dim param_companyEmail As New ReportParameter("CompanyEmail", companyEmail)


        '------------------------------------------------------------------------------------------------------------CLIENT INFO
        Dim supplier As Supplier = supplierDP.GetActiveSupplier_by_supplierId(purchaseOrder.Supplier_Id)

        Dim supplierName As String = supplier.Name
        Dim supplierRut As String = tool.ThousandSeparator(supplier.RutNumber) + "-" + supplier.RutNValidator
        Dim supplierCity As String = supplier.City
        Dim supplierAddress As String = supplier.Address
        Dim supplierPhone As String = supplier.Phone
        Dim supplierEmail As String = supplier.Email

        Dim param_supplierName As New ReportParameter("SupplierName", supplierName)
        Dim param_supplierRut As New ReportParameter("SupplierRut", supplierRut)
        Dim param_supplierCity As New ReportParameter("SupplierCity", supplierCity)
        Dim param_supplierAddress As New ReportParameter("SupplierAddress", supplierAddress)
        Dim param_supplierPhone As New ReportParameter("SupplierPhone", supplierPhone)
        Dim param_supplierEmail As New ReportParameter("SupplierEmail", supplierEmail)

        '------------------------------------------------------------------------------------------------------------PURCHASE ORDER INFO

        Dim purchaseOrderNumber As String = purchaseOrder.PurchaseOrderNumber

        Dim creationDate As String = tool.GetFormattedDate(purchaseOrder.CreationDate)
        Dim validityDate As String = tool.GetFormattedDate(purchaseOrder.ValidityDate)
        Dim observation As String = purchaseOrder.Observation
        If (observation Is Nothing) Then
            observation = ""
        End If
        Dim workDestiny As String = purchaseOrder.WorkDestiny
        Dim contactName As String = purchaseOrder.ContactName
        Dim methodOfPayment As String = methodOfPaymentDP.GetMethodOfPaymentPO_by_id(purchaseOrder.MethodOfPaymentPO_Id).MethodName

        Dim userInfo As UserInfo = userDP.GetUserInfo_by_id(purchaseOrder.User_Id)
        Dim createdBy As String = userInfo.FirstName + " " + userInfo.LastName


        Dim param_purchaseOrderNumber_little As New ReportParameter("PurchaseOrderNumber", "ORDEN DE COMPRA N° " + purchaseOrderNumber)
        Dim param_creationDate As New ReportParameter("CreationDate", creationDate)
        Dim param_validityDate As New ReportParameter("ValidityDate", validityDate)
        Dim param_observation As New ReportParameter("Observation", observation)
        Dim param_workDestiny As New ReportParameter("WorkDestiny", workDestiny)
        Dim param_contactName As New ReportParameter("ContactName", contactName)
        Dim param_methodOfPayment As New ReportParameter("MethodOfPayment", methodOfPayment)

        Dim param_createdBy As New ReportParameter("CreatedBy", createdBy)


        '------------------------------------------------------------------------------------------------------------TOTALS


        Dim param_netValue As New ReportParameter("NetValue", netValue_total_str)
        Dim param_ivaValue As New ReportParameter("IVAValue", ivaValue_str)
        Dim param_totalValue As New ReportParameter("TotalValue", totalValue_str)


        Dim customFooterText As String = ""
        Dim param_customFooterText As New ReportParameter("CustomFooterText", customFooterText)


        repoViewer.LocalReport.EnableExternalImages = True
        Dim logoPath As String = New Uri(Server.MapPath("~/Images/report_logo.png")).AbsoluteUri
        Dim param_logPath As New ReportParameter("LogoPath", logoPath)


        repoViewer.LocalReport.SetParameters(
            New ReportParameter() {
                param_companyName,
                param_companyRut,
                param_companyServiceType,
                param_companyAddressCity,
                param_companyPhone,
                param_companyEmail,
                param_purchaseOrderNumber_little,
                param_supplierName,
                param_supplierRut,
                param_supplierCity,
                param_supplierAddress,
                param_supplierPhone,
                param_supplierEmail,
                param_creationDate,
                param_validityDate,
                param_observation,
                param_workDestiny,
                param_contactName,
                param_methodOfPayment,
                param_netValue,
                param_ivaValue,
                param_totalValue,
                param_createdBy,
                param_customFooterText,
                param_logPath
            })



        'Exportación del report -------------------------------------

        Dim fileName As String = tool.GetFormattedDate(DateTime.Now.ToShortDateString) + "_OC-" + purchaseOrder.PurchaseOrderNumber + ".pdf"

        streamBytes = repoViewer.LocalReport.Render("PDF", Nothing, mimeType, encoding, filenameExtension, streamids, warnings)
        Return File(streamBytes, mimeType, fileName)
        '------------------------------------------------------------



    End Function




    Function SupplierAutocompleteSource(ByVal allSuppliers As List(Of SupplierDataProvider.Supplier_List_Formatted))

        Dim count As Integer = 0
        For Each supplier In allSuppliers
            ViewBag.RutList += "'" + supplier.Rut.Replace(".", "") + "'"
            ViewBag.NameList += "'" + supplier.Name + "'"
            count += 1
            If (count < allSuppliers.Count) Then
                ViewBag.RutList += ","
                ViewBag.NameList += ","
            End If
        Next

    End Function



End Class